import React, { Component } from "react";
import Button from "./Button";

interface Props {
  children: React.ReactNode;
  error: React.ReactNode;
}

interface State {
  hasError: boolean;
  count: number;
}

class ErrorBoundary extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = { hasError: false, count: 1 };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return this.props.error ? (
        this.props.error
      ) : (
        <div style={{ textAlign: "center" }}>
          <p>Something went wrong (x{this.state.count})</p>
          <Button size="small" onClick={() => this.retry()}>
            Retry
          </Button>
        </div>
      );
    }

    return this.props.children;
  }

  retry() {
    this.setState({ hasError: false, count: this.state.count + 1 });
  }
}

export default ErrorBoundary;
