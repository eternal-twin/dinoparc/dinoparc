import React from "react";
import ReactTooltip, { TooltipProps as ReactTooltipProps } from "react-tooltip";

import "./Tooltip.scss";

export const rebuild = ReactTooltip.rebuild;

export type TooltipProps = Omit<
  ReactTooltipProps,
  "backgroundColor" | "html"
> & {
  size?: "large" | "medium" | "small" | "smallFont";
  custom?: boolean;
};

export default function Tooltip({
  children,
  size = "large",
  custom,
  className = "",
  ...attrs
}: TooltipProps) {
  const componentClassName = custom ? "tooltip-custom" : `tooltip is-${size}`;
  return (
    <ReactTooltip
      className={`${componentClassName} ${className}`.trim()}
      html={children ? false : true}
      backgroundColor="transparent"
      {...attrs}
    >
      {children}
    </ReactTooltip>
  );
}
