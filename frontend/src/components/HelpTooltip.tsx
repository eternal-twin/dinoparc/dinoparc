import React, { useId } from "react";
import Tooltip, { TooltipProps } from "./Tooltip";

type Props = React.HTMLAttributes<HTMLSpanElement> & {
  tooltipAttrs?: TooltipProps;
} & (
    | {
        tip: string;
        children?: undefined;
      }
    | {
        tip?: undefined;
        children: TooltipProps["children"];
      }
  );

export default function HelpTooltip({
  className = "",
  tip,
  children,
  tooltipAttrs,
  ...attrs
}: Props) {
  const id = useId();
  const tooltipId = `help-tooltip_${id}`;

  return (
    <>
      <span
        className={`imageSpan ${className}`.trim()}
        data-for={tooltipId}
        data-tip={tip}
        data-place="right"
        tabIndex={0}
        {...attrs}
      ></span>
      <Tooltip id={tooltipId} wrapper="span" {...tooltipAttrs}>
        {children}
      </Tooltip>
    </>
  );
}
