import React from "react";

import "./BonsAmount.css";

type CashAmountProps = {
  amount: number;
};

export default function CashAmount({ amount }: CashAmountProps) {
  const displayedAmount = amount.toLocaleString();

  return <span className="bonsAmount">{displayedAmount}</span>;
}
