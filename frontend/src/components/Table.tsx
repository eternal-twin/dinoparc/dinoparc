import React from "react";
import "./Table.scss";

type Props = {
  children: React.ReactNode;
};

export default function Table({ children }: Props) {
  return <table className="table">{children}</table>;
}
