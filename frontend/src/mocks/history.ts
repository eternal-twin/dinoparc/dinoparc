import { History } from "../types/history";

const defaultData: History = {
  buyAmount: 26000,
  expGained: 10,
  fromDinozName: "fromDinozName",
  fromUserName: "fromUserName",
  hasWon: false,
  icon: "hist_default.gif",
  lifeLost: 3,
  numericalDate: "2022-12-13 14:49:18",
  object: "Potion Irma",
  playerId: "debf15d6-c8a5-4393-a2c9-92aaad949caf",
  seen: true,
  toDinozName: "toDinozName",
  toUserName: "toUserName",
  type: "newDay",
};

let tempFakeSecond = 0;
const getHistoryData = (data: Partial<History>) => ({
  ...defaultData,
  numericalDate:
    "2022-12-13 14:49:" + (++tempFakeSecond % 59).toString().padStart(2, "0"),
  ...data,
});

export const historyListMock: History[] = [
  // defense (by icon) lost (& unseen)
  getHistoryData({
    type: "fight",
    icon: "hist_defense.gif",
    hasWon: false,
    expGained: 10,
    lifeLost: 3,
    seen: false,
  }),
  // defense (by icon) win
  getHistoryData({
    type: "fight",
    icon: "hist_defense.gif",
    hasWon: true,
    expGained: 10,
    lifeLost: 3,
  }),
  // fight win
  getHistoryData({
    type: "fight",
    icon: "hist_fight.gif",
    hasWon: true,
    expGained: 10,
    lifeLost: 3,
  }),
  // fight lost
  getHistoryData({
    type: "fight",
    icon: "hist_fight.gif",
    hasWon: false,
    expGained: 10,
    lifeLost: 3,
  }),
  // tournament lost
  getHistoryData({
    type: "fight",
    icon: "hist_tournament.gif",
    hasWon: false,
    expGained: 10,
    lifeLost: 3,
  }),
  // fight translated dinoz (start with @)
  getHistoryData({
    type: "fight",
    icon: "hist_fight.gif",
    hasWon: false,
    expGained: 10,
    lifeLost: 3,
    toDinozName: "@JEUNE-KABUKI",
  }),
  // fight translated dinoz (split by -)
  getHistoryData({
    type: "fight",
    icon: "hist_fight.gif",
    hasWon: false,
    expGained: 10,
    lifeLost: 3,
    toDinozName: "toDinozName_FR-toDinozName_ES-toDinozName_EN",
  }),

  getHistoryData({ type: "buyDinoz", icon: "hist_xp.gif" }),
  getHistoryData({ type: "buyObject", icon: "hist_buy.gif" }),
  getHistoryData({ type: "death", icon: "hist_death.gif" }),
  getHistoryData({ type: "newDay", icon: "hist_buy.gif" }),
  getHistoryData({ type: "buy_irma", icon: "hist_buy.gif" }),
  getHistoryData({ type: "buy_pruniac", icon: "hist_buy.gif" }),
  getHistoryData({ type: "buyObjectCrater", icon: "hist_buy.gif" }),
  getHistoryData({ type: "buyObjectAnomaly", icon: "hist_buy.gif" }),
  getHistoryData({ type: "buyObjectPlains", icon: "hist_buy.gif" }),
  getHistoryData({ type: "buyObjectSecretLair", icon: "hist_buy.gif" }),
  getHistoryData({ type: "level_up", icon: "hist_level.gif" }),
  getHistoryData({ type: "fled", icon: "hist_defense.gif" }),
  getHistoryData({ type: "fusion", icon: "hist_reset.gif" }),
  getHistoryData({ type: "sellToJean", icon: "hist_buy.gif" }),
  getHistoryData({ type: "sacrifice", icon: "hist_death.gif" }),
  getHistoryData({ type: "steal_home", icon: "hist_sneak.gif" }),
  getHistoryData({ type: "steal_victim", icon: "hist_sneak.gif" }),
  getHistoryData({ type: "buy_bons", icon: "hist_buy.gif" }),
  getHistoryData({ type: "sell_bons", icon: "hist_buy.gif" }),
  getHistoryData({ type: "bazarListing", icon: "hist_default.gif" }),
];

export const historyListMockMax = new Array(200)
  .fill({})
  .map(() => getHistoryData({}));
