import axios from "axios";
import { apiUrl } from "..";

export type ShopType = "crater" | "anomaly" | "plains" | "pirates";

type ShopProps = {
  accountId: string;
  dinozId?: string;
  type?: ShopType;
};

export const shopItemBasePriceMap = {
  "Potion Irma": 1000,
  "Potion Ange": 3000,
  "Nuage Burger": 700,
  "Pain Chaud": 4500,
  "Tarte Viande": 1000,
  "Médaille Chocolat": 2000,
  bonus_fire: 300,
  bonus_wood: 300,
  bonus_water: 300,
  bonus_thunder: 300,
  bonus_air: 300,
  bonus_claw: 700,
  "Bave Loupi": 1000,
  "Bol Ramens": 2000,
  bonus_tigereye: 3000,
  bonus_pill: 400,

  // plains
  bonus_prismatik: 2200,

  // anomaly
  bonus_boostaggro: 2000,
  bonus_boostnature: 2000,
  bonus_boostwater: 2000,

  // pirates
  "Bière de Dinojak": 10,
  "bonus_mahamuti": 1,
  "Lait de Cargou": 1,
  "Coulis Cerise": 100,
  "EGG_11": 150,
  "EGG_18": 125,
  "EGG_X": 200,
  "Eternity Pill": 30,
  "Potion Sombre": 25,
  "Ticket Cadeau": 1,
  "Ticket Champifuz Special": 1,
  "Ticket Monochrome": 1,
  "Ticket Magik": 200,
  "Tisane des bois": 1,
};

export type ShopItemKey = keyof typeof shopItemBasePriceMap;

export const getShopInformation = ({ accountId, dinozId, type }: ShopProps) =>
  axios
    .get<{
      accountCashAmount: number;
      pricesMap: { [key in ShopItemKey]: number };
      quantityMap: { [key in ShopItemKey]: number };
    }>(_getBaseUrl({ accountId, dinozId, type }))
    .then((res) => {
      const { accountCashAmount, pricesMap, quantityMap } = res.data;

      const sortedKeys = Object.keys(shopItemBasePriceMap).filter(
        (key) => pricesMap[key] != null
      );

      const sortedPricesMap = Object.fromEntries(
        sortedKeys.map((key) => [key, pricesMap[key]])
      );
      const filteredQuantityMap = Object.fromEntries(
        sortedKeys.map((key) => [key, quantityMap[key]])
      ) as { [key in ShopItemKey]: number };

      return {
        ...res,
        data: {
          accountCashAmount,
          pricesMap: sortedPricesMap,
          quantityMap: filteredQuantityMap,
        },
      };
    });

export const postShopBuy = ({
  accountId,
  dinozId,
  type,
  totalPrice,
  ...quantityMap
}: ShopProps & {
  totalPrice: number;
} & {
  [key in ShopItemKey]?: number;
} & {
  [key: string]: any;
}) => {
  const data = {
    totalPrice,
    ..._getQuantityPostData(quantityMap),
  };

  return axios.post<boolean>(
    `${_getBaseUrl({ accountId, dinozId, type })}/buy`,
    data
  );
};

function _getBaseUrl({ accountId, dinozId, type }: ShopProps) {
  const dinozIdParam = dinozId ? `/${dinozId}` : "";
  const typeParam = type ? `/${type}` : "";

  return `${apiUrl}/account/${accountId}${dinozIdParam}/shop${typeParam}`;
}

function _getQuantityPostData(quantityMap: Record<string, number>) {
  const dataEntries = Object.entries(quantityMap).map(([key, value]) => [
    _getQuantityPostDataKey(key),
    value,
  ]);

  return Object.fromEntries(dataEntries);
}

function _getQuantityPostDataKey(key: string) {
  switch (key) {
    case "Potion Irma":
      return "irmaBuy";
    case "Potion Ange":
      return "angeBuy";
    case "Nuage Burger":
      return "burgerBuy";
    case "Pain Chaud":
      return "painBuy";
    case "Tarte Viande":
      return "tarteBuy";
    case "Médaille Chocolat":
      return "medailleBuy";
    case "bonus_fire":
      return "feuBuy";
    case "bonus_wood":
      return "terreBuy";
    case "bonus_water":
      return "eauBuy";
    case "bonus_thunder":
      return "foudreBuy";
    case "bonus_air":
      return "airBuy";
    case "bonus_claw":
      return "griffesBuy";
    case "Bave Loupi":
      return "loupiBuy";
    case "Bol Ramens":
      return "ramensBuy";
    case "bonus_tigereye":
      return "tigerBuy";
    case "bonus_pill":
      return "antidoteBuy";

    // plains
    case "bonus_prismatik":
      return "prismatikBuy";

    // anomaly
    case "bonus_boostaggro":
      return "focusAggroBuy";
    case "bonus_boostnature":
      return "focusNatureBuy";
    case "bonus_boostwater":
      return "focusWaterBuy";

    default:      // pirates
      return "pirateItemBuy";
  }
}
