export interface GranitMissionData {
  id: string;
  playerId: string;
  complete: boolean;
  completeAndValidated: boolean;
  moueffe: number;
  picori: number;
  castivore: number;
  sirain: number;
  winks: number;
  gorilloz: number;
  cargou: number;
  hippoclamp: number;
  rokky: number;
  pigmou: number;
  wanwan: number;
  goupignon: number;
  kump: number;
  pteroz: number;
  santaz: number;
  korgon: number;
  kabuki: number;
  serpantin: number;
  soufflet: number;
  feross: number;

  mannyLife: number;
  mannyLocation: number;
}
