type DialogContent = {
  [key: number]: {
    options?: (keyof DialogContent)[];
    img?: string;
  };
};

export type DialogData = {
  id: string;
  img: string;
} & DialogContent;
