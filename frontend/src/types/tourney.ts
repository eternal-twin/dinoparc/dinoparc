import { TourneyRanking } from "./tourney-ranking";

export interface Tourney {
  actualPoints: number;
  nbInscrits: number;
  position: number;
  currentWins: number;
  currentLosses: number;
  totalWins: number;
  totalLosses: number;
  record: number;
  rankings: TourneyRanking[];
}
