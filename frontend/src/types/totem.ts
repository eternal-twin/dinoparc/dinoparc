export interface Totem {
  totemRewards: unknown;
  totemNumber: number;
  currentBuze: number;
  currentPlumes: number;
  currentDents: number;
  currentCrins: number;
  currentBois: number;
  totalBuzeNeeded: number;
  totalPlumesNeeded: number;
  totalDentsNeeded: number;
  totalCrinsNeeded: number;
  totalBoisNeeded: number;
  full: unknown;
}
