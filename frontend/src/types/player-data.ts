export interface PlayerData {
  cash: unknown;
  dailyShinyFought: unknown;
  nbPoints: unknown;
  role: unknown;
  sacrificePoints: unknown;
  inventoryItemsMap: unknown;
  lastLogin: string | number;
}
