import axios from "axios";
import { useEffect, useState } from "react";
import { apiUrl } from "..";

type Props = {
  userId: string;
};

export const useUserCollection = ({ userId }: Props) => {
  const [isLoading, setIsLoading] = useState(true);
  const [collection, setCollection] = useState<number[]>([]);
  const [collectionEpic, setCollectionEpic] = useState<number[]>([]);

  useEffect(() => {
    setIsLoading(true);
    const getTrueData = (data: (string | number)[] = []) => [
      ...new Set(data.map((i) => Number(i))),
    ];
    Promise.all([
      axios
        .get<(string | number)[]>(apiUrl + "/account/" + userId + "/collection")
        .then(({ data }) => {
          setCollection(getTrueData(data));
        }),

      axios
        .get(apiUrl + "/account/" + userId + "/collectionEpic")
        .then(({ data }) => {
          setCollectionEpic(getTrueData(data.collectionEpic));
        }),
    ]).finally(() => {
      setIsLoading(false);
    });
  }, [userId]);

  return { collection, collectionEpic, isLoading };
};
