import { DialogData } from "../../types/dialog";

const data: DialogData = {
  id: "touristGuideDialog",
  img: require("../../media/pnj/touristik_fr.jpg"),

  0: { options: [1, 2, 3] },
  1: { options: [5, 6, 7], img: require("../../media/pnj/touristik_eng.jpg") },
  2: { options: [5, 6, 7] },
  3: {
    options: [4, 5, 6, 7],
    img: require("../../media/pnj/touristik_sp.jpg"),
  },
  4: {
    options: [7, 5, 6, 50],
    img: require("../../media/pnj/touristik_sp.jpg"),
  },
  5: {
    options: [8, 6, 7, 50],
    img: require("../../media/pnj/touristik_sp.jpg"),
  },
  6: { options: [5, 7, 50], img: require("../../media/pnj/touristik_eng.jpg") },
  7: { options: [9, 10, 5, 6, 50] },
  8: { options: [6, 7, 50] },
  9: {
    options: [11, 10, 5, 6, 50],
    img: require("../../media/pnj/touristik_sp.jpg"),
  },
  10: {
    options: [9, 5, 6, 50],
    img: require("../../media/pnj/touristik_eng.jpg"),
  },
  11: { options: [10, 5, 6, 50] },
};

export default data;
