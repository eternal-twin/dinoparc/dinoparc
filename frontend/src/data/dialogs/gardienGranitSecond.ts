import { DialogData } from "../../types/dialog";

const data: DialogData = {
  id: "gardienGranitDialogSecond",
  img: require("../../media/pnj/garde_granit.png"),
  0: { options: [1, 2] },
  1: { options: [3]},
  2: { options: [1]},
  3: { options: [4]},
  4: { options: []}
};
export default data;