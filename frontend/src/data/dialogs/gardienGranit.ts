import { DialogData } from "../../types/dialog";

const data: DialogData = {
  id: "gardienGranitDialogFirst",
  img: require("../../media/pnj/garde_granit.png"),
  0: { options: [1] },
  1: { options: [10]},
  10: { options: [13, 14, 15], img: require("../../media/lieux/extra/taverne.png") },
  13: { options: [14, 15]},
  14: { options: [17] },
  15: { options: [13, 14] },
  17: { options: [18] },
  18: { options: [19] },
  19: { options: [] },
};
export default data;