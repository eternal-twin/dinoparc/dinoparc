import { DialogData } from "../../types/dialog";

const data: DialogData = {
  id: "signDialog",
  img: require("../../media/pnj/guide.jpg"),
  0: { options: [1, 2, 20] },
  2: { options: [3, 4] },
  3: { options: [1] },
  4: { options: [3] },
  1: { options: [10, 11, 12] },
  10: { options: [13], img: require("../../media/lieux/islandView.jpg") },
  11: { options: [14, 15] },
  12: { options: [15] },
  13: { options: [16, 11] },
  14: { options: [17] },
  15: { options: [17, 14] },
  16: { options: [17, 14, 11] },
  17: { options: [18] },
  18: { options: [19] },
  20: { options: [21, 2] },
  21: { options: [22, 2] },
  22: { options: [1, 2] },
};

export default data;
