import admin from "../../media/actions/act_admin.gif";
import cancel from "../../media/actions/act_cancel.gif";
import defaut from "../../media/actions/act_default.gif";
import fight from "../../media/actions/act_fight.gif";
import fouiller from "../../media/actions/act_fouiller.gif";
import here from "../../media/actions/act_here.gif";
import lvlup from "../../media/actions/act_levelup.gif";
import potionirma from "../../media/actions/act_potion.gif";
import potionangel from "../../media/actions/act_potionRes.gif";
import speak from "../../media/actions/act_speak.gif";
import tournament from "../../media/actions/act_tournament.gif";
import tournament_consult from "../../media/actions/act_consult.gif";
import liquor from "../../media/actions/act_liquor.gif";
import pieces from "../../media/actions/act_buy.gif";
import mystery from "../../media/actions/act_mystery.gif";
import death from "../../media/actions/act_unsubscribe.jpg";
import bons from "../../media/actions/act_bill.gif";
import cadeau from "../../media/actions/act_gift.png";
import lock from "../../media/actions/act_lock.gif";
import flammes from "../../media/actions/act_fire.gif";
import fishing from "../../media/actions/fishing.png";
import picking from "../../media/actions/picking.png";
import chaudron from "../../media/actions/chaudron.png";
import egg_hatcher from "../../media/actions/egg_hatcher.png";

import move from "../../media/actions/act_move.gif";
import move_e from "../../media/actions/act_move_e.gif";
import move_n from "../../media/actions/act_move_n.gif";
import move_ne from "../../media/actions/act_move_ne.gif";
import move_nw from "../../media/actions/act_move_nw.gif";
import move_s from "../../media/actions/act_move_s.gif";
import move_se from "../../media/actions/act_move_se.gif";
import move_sw from "../../media/actions/act_move_sw.gif";
import move_w from "../../media/actions/act_move_w.gif";
import move_spec from "../../media/actions/act_moveSpec.gif";
import pyramid from "../../media/actions/act_temple.gif";

import sneakybeaky from "../../media/actions/act_sneak.gif";

export default function getActionImageFromActionString(action) {
  switch (action) {
    case "Faction_1_Ozonite_Portail":
      return pyramid;
    case "Faction_2_Gorak_Portail":
      return pyramid;
    case "Faction_3_Buldozor_Portail":
      return pyramid;
    case "Enter Demon":
      return mystery;
    case "Telescope":
      return here;
    case "GardeGranit":
      return speak;
    case "GardeGranitMissionStatus":
      return here;
    case "Pyramids":
      return pyramid;
    case "Bain de Flammes":
      return flammes;
    case "raidExchange":
      return lock;
    case "PlainsShop":
      return pieces;
    case "Gift":
      return cadeau;
    case "Bons":
      return bons;
    case "CreditBank":
      return pieces;
    case "JazzBazar":
      return pieces;
    case "Merchant":
      return pieces;
    case "AutelSacrifices":
      return death;
    case "NPCDinozBuyer":
    case "Talk":
      return speak;
    case "CentreFusions":
      return mystery;
    case "AnomalyShop":
      return pieces;
    case "CraterShop":
      return pieces;
    case "PruneShop":
      return liquor;
    case "Potion Ange":
      return potionangel;
    case "Combat":
      return fight;
    case "Deplacer":
      return move;
    case "Tournament":
      return tournament;
    case "TournoideDinoville":
      return tournament;
    case "TournoideDinoplage":
      return tournament;
    case "TournoiduMontDino":
      return tournament;
    case "TournoiduTempleBouddhiste":
      return tournament;
    case "TournoidesRuinesMayincas":
      return tournament;
    case "PosteMissions":
      return speak;
    case "Fouiller":
      return fouiller;
    case "Rock":
      return admin;
    case "LevelUp":
      return lvlup;
    case "Potion Irma":
      return potionirma;
    case "HeroTalk":
      return sneakybeaky;
    case "HordesHalloweenEvent":
      return here;
    case "CerbereTalk":
      return tournament_consult;
    case "Catapulte":
      return move_spec;
    case "Barrage":
      return move_spec;
    case "PassageGranit":
      return move_spec;
    case "PassageBaleine":
      return move_spec;
    case "PassageLabyrinthe":
      return move_spec;
    case "LosBandidos":
      return defaut;
    case "Chaudron":
      return chaudron;
    case "Pêche":
      return fishing;
    case "Cueillette":
      return picking;
    case "Here":
      return here;
    case "Default":
      return defaut;
    case "Cancel":
      return cancel;
    case "🡹":
      return move_n;
    case "🡻":
      return move_s;
    case "🡸":
      return move_w;
    case "🡺":
      return move_e;
    case "🡽":
      return move_ne;
    case "🡾":
      return move_se;
    case "🡿":
      return move_sw;
    case "🡼":
      return move_nw;
    case "Pamaguerre":
      return speak;
    case "MARCHÉPIRATE":
      return bons;
    case "INUIT":
      return speak;
    case "EggHatcher":
      return egg_hatcher;
    default:
      return defaut;
  }
}
