import faction1 from "../../media/passives/faction_1.gif";
import faction2 from "../../media/passives/faction_2.gif";
import faction3 from "../../media/passives/faction_3.gif";
import noFaction from "../../media/minis/help.gif";

export default function getFactionImageFromActionString(faction) {
  if (faction === "Faction_1_Ozonite") {
    return faction1;
  } else if (faction === "Faction_2_Gorak") {
    return faction2;
  } else if (faction === "Faction_3_Buldozor") {
    return faction3;
  }
  return noFaction;
}
