import charmeFeu from "../../media/passives/bonus_fire.gif";
import charmeTerre from "../../media/passives/bonus_wood.gif";
import charmeEau from "../../media/passives/bonus_water.gif";
import charmeFoudre from "../../media/passives/bonus_thunder.gif";
import charmeAir from "../../media/passives/bonus_air.gif";
import charmePrismatik from "../../media/passives/bonus_prismatik.gif";
import buffAggro from "../../media/passives/bonus_boostaggro.gif";
import buffNature from "../../media/passives/bonus_boostnature.gif";
import buffWater from "../../media/passives/bonus_boostwater.gif";
import buffMahamuti from "../../media/passives/bonus_boostmahamuti.gif";
import griffes from "../../media/passives/bonus_claw.gif";
import immunity from "../../media/passives/bonus_immunity.gif";
import antidote from "../../media/passives/bonus_pill.gif";
import oeil from "../../media/passives/bonus_tigereye.gif";
import god1 from "../../media/passives/faction_1.gif";
import god2 from "../../media/passives/faction_2.gif";
import god3 from "../../media/passives/faction_3.gif";
import god4 from "../../media/passives/faction_unused.gif";
import peace_flag from "../../media/passives/peace_flag.png";
import ice from "../../media/passives/effect_ice.gif";
import kabuki from "../../media/passives/effect_kabuki.gif";
import poison from "../../media/passives/effect_poison.gif";
import virus from "../../media/passives/effect_virus.gif";
import cherry from "../../media/passives/effect_cherry.gif";
import beer from "../../media/passives/effect_beer.png";
import milk from "../../media/passives/effect_milk.gif";
import fishing from "../../media/passives/fishing.gif";

export default function getPassiveIconByKey(key) {
  switch (key) {
    case "Coulis Cerise":
      return cherry;
    case "Lait de Cargou":
      return milk;
    case "bonus_prismatik":
      return charmePrismatik;
    case "bonus_fire":
      return charmeFeu;
    case "bonus_wood":
      return charmeTerre;
    case "bonus_water":
      return charmeEau;
    case "bonus_thunder":
      return charmeFoudre;
    case "bonus_air":
      return charmeAir;
    case "bonus_boostaggro":
      return buffAggro;
    case "bonus_boostnature":
      return buffNature;
    case "bonus_boostwater":
      return buffWater;
    case "bonus_mahamuti":
      return buffMahamuti;
    case "bonus_immunity":
      return immunity;
    case "bonus_pill":
      return antidote;
    case "bonus_tigereye":
      return oeil;
    case "bonus_claw":
      return griffes;
    case "effect_god1":
      return god1;
    case "effect_god2":
      return god2;
    case "effect_god3":
      return god3;
    case "effect_poison":
      return poison;
    case "effect_virus":
      return virus;
    case "effect_kabuki":
      return kabuki;
    case "effect_ice":
      return ice;
    case "Bière de Dinojak":
      return beer;
    case "CanneAPeche":
      return fishing;

    case "Faction_1_Ozonite.warrior":
      return god1;
    case "Faction_2_Gorak.warrior":
      return god2;
    case "Faction_3_Buldozor.warrior":
      return god3;
    case "Pve_Faction.warrior":
      return god4;
    case "QG.warrior":
      return peace_flag;

    default:
      return ice;
  }
}
