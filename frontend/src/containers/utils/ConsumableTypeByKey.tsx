export default function getConsumableTypeByKey(key: string) {
    const multiple: string[] = [];

    const single = [
        "bonus_fire",
        "bonus_wood",
        "bonus_water",
        "bonus_thunder",
        "bonus_air",
        "bonus_prismatik",

        "Nuage Burger",
        "Tarte Viande",
        "Médaille Chocolat",
        "Pain Chaud",
        "bonus_pill",
        "Bave Loupi",
        "Bol Ramens",
        "Tisane des bois",

        "Bière de Dinojak",
        "bonus_claw",
        "bonus_tigereye",
        "bonus_boostaggro",
        "bonus_boostnature",
        "bonus_boostwater",
        "bonus_mahamuti",
        "Potion Sombre",
        "Coulis Cerise",
        "Lait de Cargou",
        "CanneAPeche"
    ];

    if (multiple.includes(key)) {
        return "multiple";
    } else if (single.includes(key)) {
        return "single";
    }

    return false;
}
