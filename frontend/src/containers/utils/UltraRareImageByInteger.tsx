import c1 from "../../media/consumables/Bave Loupi.gif";
import c2 from "../../media/consumables/ticket_monochrome.gif";
import c3 from "../../media/consumables/liquor.gif";

import c101 from "../../media/tools/rod.png";

export default function getUltraRareByInteger(key) {
  switch (key) {
    case 1:
      return c1;
    case 2:
      return c2;
    case 3:
      return c3;

      //Special cases :
    case 101:
      return c101;

    default:
      return c1;
  }
}
