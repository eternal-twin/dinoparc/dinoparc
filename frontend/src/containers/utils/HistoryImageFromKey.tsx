import histBuy from "../../media/actions/hist_buy.gif";
import histFight from "../../media/actions/hist_fight.gif";
import histCharm from "../../media/actions/hist_charm.gif";
import histDeath from "../../media/actions/hist_death.gif";
import histDefault from "../../media/actions/hist_default.gif";
import histDefense from "../../media/actions/hist_defense.gif";
import histError from "../../media/actions/hist_error.gif";
import histLevel from "../../media/actions/hist_level.gif";
import histReport from "../../media/actions/hist_report.gif";
import histReset from "../../media/actions/hist_reset.gif";
import histSneak from "../../media/actions/hist_sneak.gif";
import histTournament from "../../media/actions/hist_tournament.gif";
import histXp from "../../media/actions/hist_xp.gif";
import histPoison from "../../media/actions/hist_poison.gif";

export default function getHistoryImageFromKey(key) {
  switch (key) {
    case "hist_buy.gif":
      return histBuy;
    case "hist_fight.gif":
      return histFight;
    case "hist_charm.gif":
      return histCharm;
    case "hist_death.gif":
      return histDeath;
    case "hist_default.gif":
      return histDefault;
    case "hist_defense.gif":
      return histDefense;
    case "hist_error.gif":
      return histError;
    case "hist_level.gif":
      return histLevel;
    case "hist_report.gif":
      return histReport;
    case "hist_reset.gif":
      return histReset;
    case "hist_sneak.gif":
      return histSneak;
    case "hist_tournament.gif":
      return histTournament;
    case "hist_xp.gif":
      return histXp;
    case "hist_poison.gif":
      return histPoison;
    default:
      return histDefault;
  }
}
