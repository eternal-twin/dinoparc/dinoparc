import buze from "../../media/totem/totem_buze.png";
import feather from "../../media/totem/totem_feather.png";
import tooth from "../../media/totem/totem_tooth.png";
import crins from "../../media/totem/totem_unicorn.png";
import bois from "../../media/totem/totem_wood.png";

export default function getTotemImageByInteger(object) {
  if (object === 1) {
    return buze;
  } else if (object === 2) {
    return feather;
  } else if (object === 3) {
    return tooth;
  } else if (object === 4) {
    return crins;
  } else if (object === 5) {
    return bois;
  }
}
