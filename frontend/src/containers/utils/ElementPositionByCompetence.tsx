export default function getElementPositionByCompetence(competence) {
  if (competence === "Force") {
    return "A";
  } else if (competence === "Endurance") {
    return "B";
  } else if (competence === "Perception") {
    return "C";
  } else if (competence === "Intelligence") {
    return "D";
  } else if (competence === "Agilité") {
    return "E";
  } else if (competence === "Fouiller") {
    return "B";
  } else if (competence === "Rock") {
    return "D";
  } else if (competence === "Arts Martiaux") {
    return "A";
  } else if (competence === "Camouflage") {
    return "E";
  } else if (competence === "Médecine") {
    return "D";
  } else if (competence === "Natation") {
    return "C";
  } else if (competence === "Escalade") {
    return "B";
  } else if (competence === "Survie") {
    return "A";
  } else if (competence === "Stratégie") {
    return "E";
  } else if (competence === "Commerce") {
    return "D";
  } else if (competence === "Navigation") {
    return "C";
  } else if (competence === "Course") {
    return "B";
  } else if (competence === "Contre Attaque") {
    return "A";
  } else if (competence === "Chance") {
    return "E";
  } else if (competence === "Musique") {
    return "D";
  } else if (competence === "Cuisine") {
    return "C";
  } else if (competence === "Saut") {
    return "B";
  } else if (competence === "Voler") {
    return "D";
  } else if (competence === "Taunt") {
    return "C";
  } else if (competence === "Apprenti Feu") {
    return "F";
  } else if (competence === "Apprenti Terre") {
    return "F";
  } else if (competence === "Apprenti Eau") {
    return "F";
  } else if (competence === "Apprenti Foudre") {
    return "F";
  } else if (competence === "Pouvoir Sombre") {
    return "F";
  } else if (competence === "Protection du Feu") {
    return "A";
  } else if (competence === "Solubilité") {
    return "C";
  } else if (competence === "Résilience") {
    return "A";
  } else if (competence === "Bain de Flammes") {
    return "A";
  } else if (competence === "Cueillette") {
    return "B";
  } else if (competence === "Pêche") {
    return "C";
  } else if (competence === "Ingénieur") {
    return "D";
  } else if (competence === "Coup Critique") {
    return "E";
  } else if (competence === "Chasseur") {
    return "E";
  } else if (competence === "Piqûre") {
    return "E";
  } else if (competence === "Forteresse") {
    return "B";
  }
}
