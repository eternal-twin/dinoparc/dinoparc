import { useTranslation } from "react-i18next";
import React from "react";
import portroyal from "../../../../media/lieux/zone24.png";
import GameSection from "./shared/GameSection";
import ContentWithImage, {
    ImageWrap,
} from "../../../../components/ContentWithImage";
import ShopForm from "../../shared/ShopForm";

type Props = {
    dinozId: string;
    refresh: () => void;
    returnToDinoz: () => void;
};

export default function GuildesPirates({dinozId, refresh, returnToDinoz,}: Props) {
    const { t } = useTranslation();

    return (
        <GameSection title={t("MARCHÉPIRATE")} returnToDinoz={returnToDinoz}>
            <>
                <ContentWithImage>
                    <ImageWrap>
                        <img alt="" src={portroyal}></img>
                    </ImageWrap>
                    <p>{t("description.MARCHÉPIRATE")}</p>
                </ContentWithImage>

                <ShopForm
                    className="mt-6"
                    dinozId={dinozId}
                    type="pirates"
                    onSubmitted={() => {refresh();}}
                    returnToDinoz={() => {returnToDinoz();}}
                />
            </>
        </GameSection>
    );
}
