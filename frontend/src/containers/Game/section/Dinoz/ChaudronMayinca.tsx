import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import ruines from "../../../../media/lieux/zone13.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import { CraftResult } from "../../../../types/craft-result";
import { useUserData } from "../../../../context/userData";
import Section from "../../../../components/Section";
import Row, { Col } from "../../../../components/Row";
import Content from "../../../../components/Content";
import { Loader } from "../../../../components/Loader";
import urlJoin from "url-join";

const enum PageSection {
  cauldron,
  recipes,
}

type Objects = {
  "Feuille de Pacifique": number;
  "Oréade blanc": number;
  "Tige de Roncivore": number;
  "Anémone solitaire": number;
  "Perche perlée": number;
  "Grémille grelottante": number;
  "Cube de glu": number;
};

type Props = {
  dinozId: string;
};

export default function ChaudronMayinca({ dinozId }: Props) {
  const { t, i18n } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(false);
  const [craftSuccess, setCraftSuccess] = useState<boolean | null>(null);
  const [craftResult, setCraftResult] = useState<CraftResult | null>(null);
  const [sectionCurrentlyActive, setSectionCurrentlyActive] = useState<PageSection>(PageSection.recipes);

  useEffect(() => {}, [dinozId]);

  function postUpForCraft(recipeNumber) {
    setCraftSuccess(null);
    setCraftResult(null);
    axios.post(urlJoin(apiUrl, "account", accountId, dinozId, "chaudron", "postUpForCraft", String(recipeNumber)))
        .then(({ data }) => {
          setCraftSuccess(data.success);
          setCraftResult(data);
          setIsLoading(false);
        });
  }

  return (
    <div className="TEMP_mainContainer">
      <Section title={t("Chaudron")}>
        <Row>
          <Col>
            <img alt="" className="imgIrma" src={ruines} />
          </Col>
          <Col grow={true}>
            <Content>
              <p>{t("Chaudron.details") + " " + t("Chaudron.details2")}</p>
            </Content>
          </Col>
        </Row>

        {isLoading === true ? (<Loader css="margin: 2rem auto;" />) : (
          <div>

            {sectionCurrentlyActive === PageSection.recipes && (
              <div>
                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Perche perlée")} />
                    x50
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Grémille grelottante")}
                    />
                    x10
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x50
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x10
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(1)}>
                    <img alt="" src={getConsumableByKey("Nuage Burger")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Perche perlée")} />
                    x50
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                        alt=""
                        src={getConsumableByKey("Grémille grelottante")}
                    />
                    x10
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                        alt=""
                        src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x50
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Cube de glu")} />
                    x1
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(7)}>
                    <img alt="" src={getConsumableByKey("bonus_pill")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Perche perlée")} />
                    x75
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Grémille grelottante")}
                    />
                    x15
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x60
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x1
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(2)}>
                    <img alt="" src={getConsumableByKey("Tarte Viande")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x90
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x60
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x25
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(3)}>
                    <img alt="" src={getConsumableByKey("Médaille Chocolat")} />
                    x10
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x5
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x3
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Anémone solitaire")} />
                    x1
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(4)}>
                    <img alt="" src={getConsumableByKey("Tisane des bois")} />
                    x1
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img
                      alt=""
                      src={getConsumableByKey("Grémille grelottante")}
                    />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Cube de glu")} />
                    x1
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x20
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Anémone solitaire")} />
                    x2
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(5)}>
                    <img alt="" src={getConsumableByKey("Bière de Dinojak")} />
                    x1
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Cube de glu")} />
                    x5
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Anémone solitaire")} />
                    x5
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(8)}>
                    <img alt="" src={getConsumableByKey("Lait de Cargou")} />
                    x1
                  </a>
                </div>

                <div className="recettes">
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Perche perlée")} />
                    x300
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                        alt=""
                        src={getConsumableByKey("Grémille grelottante")}
                    />
                    x100
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img
                        alt=""
                        src={getConsumableByKey("Feuille de Pacifique")}
                    />
                    x300
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Oréade blanc")} />
                    x100
                  </a>
                  <span className="plusEquals">+</span>
                  <a className="recetteElement">
                    <img alt="" src={getConsumableByKey("Tige de Roncivore")} />
                    x20
                  </a>
                  <span className="plusEquals">=</span>
                  <a className="recetteElementResult" onClick={(e) => postUpForCraft(6)}>
                    <img alt="" src={getConsumableByKey("Potion Irma")} />
                    x100
                  </a>
                </div>
              </div>
            )}

            {craftSuccess && craftResult !== null && (
              <div>
                <div className="merchantMessage">{t("accept")}</div>
                <a className="recetteElement">
                  <div className="resultCraftChaudron">
                    <img
                      alt=""
                      src={getConsumableByKey(craftResult.generatedObject)}
                    />
                    <>{"   "}</>
                    <span className="espaceNom">
                      {"x" + craftResult.generatedObjectQty}
                    </span>
                    <>{"   "}</>
                    <>{"   "}</>
                    <span className="espaceNom">
                      {t("boutique." + craftResult.generatedObject)}
                    </span>
                    <span
                      className="imageSpan"
                      lang={i18n.language}
                      data-place="right"
                      data-tip={t(
                        "boutique.tooltip." + craftResult.generatedObject
                      )}
                    />
                    <ReactTooltip
                      className="largetooltip"
                      html={true}
                      backgroundColor="transparent"
                    />
                  </div>
                </a>
              </div>
            )}
            {craftSuccess != null && craftSuccess !== true && (
              <div className="errorResultFuzPrix">{t("refuser")}</div>
            )}
          </div>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </Section>
    </div>
  );
}
