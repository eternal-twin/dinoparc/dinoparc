import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import source from "../../../../media/lieux/zone19.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import urlJoin from "url-join";

export default function DinoFountain(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {}, [props.dinozId]);

  function confirm(dinozId) {
    setIsLoading(true);
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "drink-fountain"))
      .then(() => {
        setIsLoading(false);
        props.refresh();
      });
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("DinoFountain")}</header>
        <div className="displayFusions">
          <img alt="" className="imgIrma" src={source} />
          <span className="textIrma">{t("location.source.details")}</span>
        </div>
        {isLoading === true && (
          <MoonLoader color="#c37253" css="margin-left : 240px;" />
        )}
        {isLoading === false && (
          <button
            id="btnGive"
            className="buttonGiveaway"
            onClick={(e) => {
              if (window.confirm(t("confirm"))) {
                confirm(props.dinozId);
              }
            }}
          >
            {t("drinkWater")}
            <br />
          </button>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </div>
    </div>
  );
}
