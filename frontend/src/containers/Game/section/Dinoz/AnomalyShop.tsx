import { useTranslation } from "react-i18next";
import React from "react";
import anomalie from "../../../../media/lieux/zone20.gif";
import GameSection from "./shared/GameSection";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import ShopForm from "../../shared/ShopForm";

type Props = {
  dinozId: string;
  refresh: () => void;
  returnToDinoz: () => void;
};

export default function AnomalyShop({
  dinozId,
  refresh,
  returnToDinoz,
}: Props) {
  const { t } = useTranslation();

  return (
    <GameSection title={t("AnomalyShop")} returnToDinoz={returnToDinoz}>
      <>
        <ContentWithImage>
          <ImageWrap>
            <img alt="" src={anomalie}></img>
          </ImageWrap>
          <p>{t("boutiqueAnomaly.header")}</p>
        </ContentWithImage>
        <p className="textSpecial1">{"-*" + t("boutiqueAnomaly.text") + "*"}</p>

        <ShopForm
          className="mt-6"
          dinozId={dinozId}
          type="anomaly"
          onSubmitted={() => {
            refresh();
          }}
          returnToDinoz={() => {returnToDinoz();}}
        />
      </>
    </GameSection>
  );
}
