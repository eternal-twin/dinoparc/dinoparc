import { useTranslation } from "react-i18next";
import React, {useEffect, useState} from "react";
import GameSection from "./shared/GameSection";
import Button from "../../../../components/Button";
import { DialogData } from "../../../../types/dialog";
import { PlaceNumber } from "../../../utils/LocationImageByPlaceNumber";

//Declare new dialogs here :
import signDialog from "../../../../data/dialogs/signDialog";
import oldManDialog from "../../../../data/dialogs/oldManDialog";
import touristGuideDialog from "../../../../data/dialogs/touristGuideDialog";
import gardienGranitDialogFirst from "../../../../data/dialogs/gardienGranit";
import gardienGranitDialogSecond from "../../../../data/dialogs/gardienGranitSecond";
import axios from "axios";
import urlJoin from "url-join";
import {apiUrl} from "../../../../index";
import Store from "../../../../utils/Store";

//Default Dialogs that are not wrapped into other components. (Only for infos)
export const placeDialogs = {
  [PlaceNumber.Clairiere]: signDialog,
  [PlaceNumber.hutte]: oldManDialog,
  [PlaceNumber.bordeciel]: touristGuideDialog,
};

export const placeWithDialogs = Object.keys(placeDialogs).map(
  (key) => Number(key) as keyof typeof placeDialogs
);

type Props = {
  placeNumber: PlaceNumber;
  returnToDinoz: () => void;
};

function getWrappedDialogDataFromPlaceNumber(placeNumber: number) {
  if (placeNumber == 7) {
    return gardienGranitDialogFirst;
  }
  if (placeNumber == 7.1) {
    return gardienGranitDialogSecond;
  }
  return null;
}

export default function Dialog(props: Props) {
  const { t } = useTranslation();
  const [npcText, setNpcText] = useState(0);
  const placeNumber = props.placeNumber;
  const dialogData: DialogData | null = placeNumber in placeDialogs ? placeDialogs[placeNumber] : getWrappedDialogDataFromPlaceNumber(props.placeNumber);
  const dialogId = dialogData?.id;
  const imageNpc = dialogData?.[npcText]?.img || dialogData?.img;
  const options = dialogData?.[npcText]?.options || [];
  let store = Store.getInstance();

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const selection = (e.target as HTMLFormElement).selection?.value;
    const selectedAnswer = selection ? Number(selection) : null;
    if (selectedAnswer == null) {
      return;
    }

    setNpcText(selectedAnswer);
    if (props.placeNumber == 7 && dialogId == "gardienGranitDialogFirst" && npcText == 18) {
      axios.put(urlJoin(apiUrl, "account", store.getAccountId(), "start-granit-mission"));
    }
  }

  return (
    <GameSection title={t(dialogId)} returnToDinoz={props.returnToDinoz}>
      {dialogData == null ? (
        <></>
      ) : (
        <>
          <div className="flexDivsIrma">
            <img alt="" src={imageNpc} className="imgIrma imgFixedSize" />
            <div>
              <div className="dialogNPC">
                <p className="textNpc italic">{t("N" + dialogId + npcText)}</p>
              </div>
            </div>
          </div>

          {options.length !== 0 && (
            <form onSubmit={handleSubmit}>
              {options.map((dialogKey) => (
                <div key={npcText + "_" + dialogKey} className="alignedOptions">
                  <label>
                    <input type="radio" name="selection" required value={dialogKey}/>
                    {t("P" + dialogId + dialogKey)}
                  </label>
                </div>
              ))}
              <div className="has-text-centered mt-4">
                <Button type="submit">{t("sayToNpc")}</Button>
              </div>
            </form>
          )}
        </>
      )}
    </GameSection>
  );
}