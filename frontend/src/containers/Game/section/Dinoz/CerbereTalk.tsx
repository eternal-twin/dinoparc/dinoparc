import { useTranslation } from "react-i18next";
import React, { useEffect } from "react";
import cerbere from "../../../../media/pnj/cerbere_pluie.png";

export default function CerbereTalk() {
  const { t } = useTranslation();
  useEffect(() => {}, []);

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("Cerbere")}</header>
        <div className="displayFusions">
          <img alt="" className="imgFactions" src={cerbere} />
          <span className="textCerbere">{t("Cerbere.discours")}</span>
        </div>
        <br></br>
        <p className="cerbereTourneyPrizes">{t("Cerbere.tournoi")}</p>
        <p className="cerbereTourneyPrizes">{t("Cerbere.goodluck")}</p>
      </div>
    </div>
  );
}
