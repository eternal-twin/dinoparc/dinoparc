import { useTranslation } from "react-i18next";
import React, { useEffect, useRef, useState } from "react";
import demons from "../../../../media/pnj/demons.png";
import sacrifices from "../../../../media/game/sacrifices.gif";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import axios from "axios";
import { apiUrl } from "../../../../index";
import GameSection from "./shared/GameSection";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import AsyncSection from "../../../../components/AsyncSection";
import ButtonWithIcon from "../../../../components/ButtonWithIcon";
import Message from "../../../../components/Message";
import Button from "../../../../components/Button";

import "./Sacrifices.scss";
import { useUserData } from "../../../../context/userData";
import { Dinoz } from "../../../../types/dinoz";
import urlJoin from "url-join";

type Props = {
  dinoz: Dinoz;
  refresh: () => void;
  returnToDinoz: () => void;
};

export default function Sacrifices(props: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [isBeforeSac, setIsBeforeSac] = useState(true);
  const [percentage, setPercentage] = useState(0);
  const [showBack, setShowBack] = useState(true);
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    axios.get<number>(urlJoin(apiUrl, "account", accountId, "sacrifice-progress"))
      .then(({ data }) => {
        setIsLoading(false);
        setPercentage(data);
      });
  }, [props.dinoz.id, accountId]);

  useEffect(() => {
    var canvas = canvasRef.current;
    if (!canvas) {
      return;
    }
    const { width, height } = canvas;
    const centerX = width / 2 - 1.5; // TODO fix img center
    const centerY = height / 2;
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, width, height);
    ctx.beginPath();
    ctx.arc(
      centerX,
      centerY,
      63,
      0 - Math.PI / 2,
      0 - Math.PI / 2 + percentage * (2 * Math.PI),
      false
    );
    ctx.lineWidth = 7;
    ctx.strokeStyle = "#7952c8";
    ctx.stroke();
  }, [percentage]);

  function confirmSacrifice(dinozId) {
    setIsLoading(true);
    axios.put<null | { potionsWon: number; newPercentage: number }>(urlJoin(apiUrl, "account", accountId, dinozId, "giveaway", "sacrifice"))
      .then(({ data }) => {
        if (data != null) {
          setIsLoading(false);
          if (data.potionsWon === 0) {
            setShowBack(false);
            setIsBeforeSac(false);
            setPercentage(data.newPercentage);
          } else {
            props.refresh();
          }
        }
      });
  }

  return (
    <GameSection
      title={t("AutelSacrifices")}
      returnToDinoz={showBack ? props.returnToDinoz : undefined}
    >
      <ContentWithImage>
        <ImageWrap>
          <img alt="" src={demons} />
        </ImageWrap>
        <div dangerouslySetInnerHTML={{ __html: t("sacrifices.text") }} />
        <p>{t("sacrifices.talk")}</p>
        <p>{t("sacrifices.conclusion")}</p>
        <p className="textWarningPitie">{t("sacrifices.warning")}</p>
      </ContentWithImage>

      <AsyncSection loading={isLoading}>
        <div>
          <div className="sacrificeImgWrap mt-4">
            <img src={sacrifices} alt="" />
            <canvas ref={canvasRef} />
          </div>
          <p className="textPercentage">
            {(percentage * 100).toFixed(0) + "%"}
          </p>
          <div className="centerBtn">
            {isBeforeSac ? (
              <ButtonWithIcon
                iconSrc={getActionImageFromActionString("AutelSacrifices")}
                onClick={() => {
                  if (window.confirm(t("confirm"))) {
                    confirmSacrifice(props.dinoz.id);
                  }
                }}
              >
                {t("confirmSacrifice") + props.dinoz.name + "!"}
              </ButtonWithIcon>
            ) : (
              <div>
                <Message className="mb-4">{t("demonsNotHappy")}</Message>
                <div className="has-text-centered">
                  <Button size="larger" onClick={() => {props.refresh();}}>{t("continue")}</Button>
                </div>
              </div>
            )}
          </div>
        </div>
      </AsyncSection>
    </GameSection>
  );
}
