import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import plains from "../../../../media/lieux/zone37.gif";
import getElementImageByString from "../../../utils/ElementImageByString";
import GameSection from "./shared/GameSection";
import ShopForm from "../../shared/ShopForm";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import Content from "../../../../components/Content";
import AsyncSection from "../../../../components/AsyncSection";
import { useUserData } from "../../../../context/userData";

type Props = {
  dinozId: string;
  refresh: () => void;
  returnToDinoz: () => void;
};

export default function PlainsShop({ dinozId, refresh, returnToDinoz }: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [cluesRaid, setCluesRaid] = useState("");

  useEffect(() => {
    setIsLoading(true);
    axios
      .get<string>(`${apiUrl}/account/${accountId}/${dinozId}/clues-raid`)
      .then(({ data }) => {
        setCluesRaid(data);
        setIsLoading(false);
        window.scrollTo(0, 0);
      });
  }, [accountId, dinozId]);

  return (
    <GameSection title={t("PlainsShop")} returnToDinoz={returnToDinoz}>
      <>
        <ContentWithImage>
          <ImageWrap>
            <img alt="" src={plains}></img>
          </ImageWrap>
          <p>{t("boutiqueAnomaly.header")}</p>
        </ContentWithImage>

        <AsyncSection loading={isLoading}>
          <ClueRaid cluesRaid={cluesRaid} />
        </AsyncSection>

        <ShopForm
          className="mt-6"
          dinozId={dinozId}
          type="plains"
          onSubmitted={() => {
            refresh();
          }}
          returnToDinoz={() => {returnToDinoz();}}
        />
      </>
    </GameSection>
  );
}

type ClueRaidProps = {
  cluesRaid: string;
};

const ClueRaid = ({ cluesRaid }: ClueRaidProps) => {
  const { t } = useTranslation();
  const image = cluesRaid.toString().includes(" - ")
    ? getElementImageByString(cluesRaid.substring(4, cluesRaid.length))
    : undefined;
  const text = image != null ? cluesRaid.substring(0, 4) : cluesRaid;

  return (
    !!text && (
      <Content className="mt-2">
        <>
          {t("raidclues") + " " + text}
          {image != null && <img alt="" src={image} />}
        </>
      </Content>
    )
  );
};
