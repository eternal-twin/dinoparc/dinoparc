import { useTranslation } from "react-i18next";
import React, {useState} from "react";
import pecheur from "../../../../media/pnj/inuit.png";
import GameSection from "./shared/GameSection";
import Row, {Col} from "../../../../components/Row";
import Content from "../../../../components/Content";
import {useUserData} from "../../../../context/userData";
import {CraftResult} from "../../../../types/craft-result";
import axios from "axios";
import urlJoin from "url-join";
import {apiUrl} from "../../../../index";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import ReactTooltip from "react-tooltip";
import i18n from "i18next";

type Props = {
    dinozId: string;
    refresh: () => void;
    returnToDinoz: () => void;
};

export default function Inuit({dinozId, refresh, returnToDinoz}: Props) {
    const { t } = useTranslation();
    const { accountId } = useUserData();
    const [craftSuccess, setCraftSuccess] = useState<boolean | null>(null);
    const [craftResult, setCraftResult] = useState<CraftResult | null>(null);

    function tradeRamenWithFisherman() {
        setCraftSuccess(null);
        setCraftResult(null);
        axios.post(urlJoin(apiUrl, "account", accountId, dinozId, "trade-ramen"))
            .then(({ data }) => {
                if (data.success === false) {
                    returnToDinoz();

                } else {
                    setCraftSuccess(data.success);
                    setCraftResult(data);
                }
            });
    }

    return (
        <GameSection title={t("INUIT")} returnToDinoz={returnToDinoz}>
            <>
                <Row>
                    <img alt="" className="imgPnjNotResizable" src={pecheur}/>
                    <Col>
                        <Content>
                            <p>{t("texte.INUIT")}</p>
                        </Content>
                    </Col>
                </Row>
                <br></br>
                <br></br>
                <Content>
                    <p>{t("trade.INUIT")}</p>

                    <a className="fisherRamenElementResult" onClick={(e) => tradeRamenWithFisherman()}>
                        {t("pecheur.donner")}
                        <img alt="" src={getConsumableByKey("Bol Ramens")} />
                        {t("pecheur.donner.details")}
                    </a>

                    {craftSuccess && craftResult !== null && (
                        <div>
                            <br></br>
                            <div className="merchantMessage">{t("pecheur.result")}</div>
                            <a className="recetteElement">
                                <div className="resultCraftChaudron">
                                    <img alt="" src={getConsumableByKey(craftResult.generatedObject)}/>
                                    <>{"   "}</>
                                    <span className="espaceNom">{"x" + craftResult.generatedObjectQty}</span>
                                    <>{"   "}</>
                                    <>{"   "}</>
                                    <span className="espaceNom">{t("boutique." + craftResult.generatedObject)}</span>
                                    <span className="imageSpan" lang={i18n.language} data-place="right" data-tip={t("boutique.tooltip." + craftResult.generatedObject)}/>
                                    <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                                </div>
                            </a>
                        </div>
                    )}
                </Content>
            </>
        </GameSection>
    );
}
