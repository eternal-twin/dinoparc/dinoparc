import React, { useEffect, useState } from "react";
import credit from "../../../../media/lieux/zone14.gif";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { MoonLoader } from "react-spinners";
import Store from "../../../../utils/Store";
import gerard from "../../../../media/pnj/banquier.png";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import tinybill from "../../../../media/minis/tiny_bill.gif";
import urlJoin from "url-join";

export default function Credit(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [bonsBuy, setBonsBuy] = useState(0);
  const [bonsSellTotalPrice, setBonsSellTotalPrice] = useState(0);
  const [bonsSellTotalQty, setBonsSellTotalQty] = useState(0);
  const [displayBonsBuyError, setDisplayBonsBuyError] = useState(false);
  const [displayBonsSellError, setDisplayBonsSellError] = useState(false);
  let [actualGold, setActualGold] = useState(0);
  let [actualBons, setActualBons] = useState(0);
  let [bonsBuyValue, setBonsBuyValue] = useState(0);
  let [bonsBuyTotalPrice, setBonsBuyTotalPrice] = useState(0);
  let [bonsSellValue, setBonsSellValue] = useState(0);

  useEffect(() => {
    axios
      .get(urlJoin(apiUrl, "utils", "creditInfos", store.getAccountId()))
      .then(({ data }) => {
        actualGold = data[2];
        setActualGold(actualGold);
        actualBons = data[3];
        setActualBons(actualBons);
        bonsBuyValue = data[0];
        setBonsBuyValue(bonsBuyValue);
        bonsSellValue = data[1];
        setBonsSellValue(bonsSellValue);
        setIsLoading(false);
      });
  }, []);

  function bonsBuyChange(e) {
    setBonsBuy(e.target.value);
    setBonsBuyTotalPrice(bonsBuyValue * e.target.value);
  }

  function buyBons() {
    setDisplayBonsBuyError(false);
    axios.post(urlJoin(apiUrl, "utils", store.getAccountId(), "buyBons", String(bonsBuyValue)),
        {
          totalPrice: bonsBuyTotalPrice,
          bonsBuy: bonsBuy,
        }
      )
      .then(({ data }) => {
        if (data === true) {
          props.refresh();
        } else {
          setDisplayBonsBuyError(true);
        }
      });
  }

  function bonsSellChange(e) {
    setBonsSellTotalQty(e.target.value);
    setBonsSellTotalPrice(bonsSellValue * e.target.value);
  }

  function sellBons() {
    setDisplayBonsSellError(false);
    axios.post(urlJoin(apiUrl, "utils", store.getAccountId(), "sellBons", String(bonsSellValue)),
        {
          totalPrice: bonsSellTotalPrice,
          bonsSell: bonsSellTotalQty,
        }
      )
      .then(({ data }) => {
        if (data === true) {
          props.refresh();
        } else {
          setDisplayBonsSellError(true);
        }
      });
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("location.credit")}</header>
        {isLoading === true && (
          <MoonLoader color="#c37253" css="margin-left : 240px;" />
        )}
        {isLoading === false && (
          <div>
            <div className="displayFusions">
              <img alt="" className="imgIrma" src={credit} />
              <span className="textIrma">{t("location.credit.details")}</span>
            </div>
            <h2 className="miniHeaders2">{t("comptoirService")}</h2>
            <br />
            <div className="displayFusions">
              <img alt="" className="imgRaymond" src={gerard} />
              <span className="textNpcTalk">{t("bankTextOne")}</span>
            </div>

            <h2 className="miniHeaders2">{t("sell")}</h2>
            <p className="textIrma2">{t("bankTextThree")}</p>
            {displayBonsSellError && (
              <div className="errorResultFuzPrix">{t("bonsSellError")}</div>
            )}
            <div className="blockBuySingleItem">
              <td className="iconIrma">
                <img alt="" src={getConsumableByKey("COINS")} />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  {bonsSellValue}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="countIrmaShop">
                <input
                  className="numberField"
                  type="number"
                  min="0"
                  id="bonsSell"
                  onInput={(e) => {
                    bonsSellChange(e);
                  }}
                />
                <img alt="" className="coin" src={tinybill} />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  Total = {bonsSellTotalPrice}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="priceIrma">
                <button
                  onClick={(e) => {
                    sellBons();
                  }}
                  className="buttonSingleBuy"
                >
                  {t("sell")}
                </button>
              </td>
            </div>

            <h2 className="miniHeaders2">{t("buy")}</h2>
            <p className="textIrma2">{t("bankTextTwo")}</p>
            {displayBonsBuyError && (
              <div className="errorResultFuzPrix">{t("bonsBuyError")}</div>
            )}
            <div className="blockBuySingleItem">
              <td className="iconIrma">
                <img alt="" src={getConsumableByKey("Bons du Trésor")} />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  {bonsBuyValue}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="countIrmaShop">
                <input
                  className="numberField"
                  type="number"
                  min="0"
                  id="bonsBuy"
                  onInput={(e) => {
                    bonsBuyChange(e);
                  }}
                />
                <img alt="" className="coin" src={tinybill} />
              </td>
              <td className="priceIrma">
                <span className="colorBlackPO">
                  Total = {bonsBuyTotalPrice}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </td>
              <td className="priceIrma">
                <button
                  onClick={(e) => {
                    buyBons();
                  }}
                  className="buttonSingleBuy"
                >
                  {t("buy")}
                </button>
              </td>
            </div>
          </div>
        )}
      </div>
      <br />
    </div>
  );
}
