import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import axios from "axios";
import { apiUrl } from "../../../../index";
import tinyDrop from "../../../../media/minis/tiny_drop.gif";
import actDrop from "../../../../media/actions/act_drop.gif";
import tanniere from "../../../../media/game/tanniere.png";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import urlJoin from "url-join";

export default function RaidExchange(props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  let [buySuccess, setBuySuccess] = useState(true);
  const [accountDropAmount, setAccountDropAmount] = useState(0);

  const [pricesMap, setPricesMap] = useState({});
  const [quantityMap, setQuantityMap] = useState({});
  let [affichage, setAffichage] = useState(0);

  let [eternityPillBuy, setEternityPillBuy] = useState(0);
  let [newSpellTotal, setNewSpellTotal] = useState(0);

  let [egg11Buy, setEgg11Buy] = useState(0);
  let [newEgg11Total, setNewEgg11Total] = useState(0);

  let [egg18Buy, setEgg18Buy] = useState(0);
  let [newEgg18Total, setNewEgg18Total] = useState(0);

  useEffect(() => {
    axios.get(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "shop-secret", "plains"))
      .then(({ data }) => {
        setAccountDropAmount(data.accountCashAmount);
        setPricesMap(data.pricesMap);
        setQuantityMap(data.quantityMap);
        setIsLoading(false);
      });
  }, [props.dinozId]);

  function eternityPillChange(e, price) {
    eternityPillBuy = e.target.value;
    if (eternityPillBuy >= 0 && eternityPillBuy <= 99) {
      setEternityPillBuy(eternityPillBuy);
      newSpellTotal = price * eternityPillBuy;
      setNewSpellTotal(newSpellTotal);
    }
    refreshTotal();
  }

  function egg11Change(e, price) {
    egg11Buy = e.target.value;
    if (egg11Buy >= 0 && egg11Buy <= 99) {
      setEgg11Buy(egg11Buy);
      newEgg11Total = price * egg11Buy;
      setNewEgg11Total(newEgg11Total);
    }
    refreshTotal();
  }

  function egg18Change(e, price) {
    egg18Buy = e.target.value;
    if (egg18Buy >= 0 && egg18Buy <= 99) {
      setEgg18Buy(egg18Buy);
      newEgg18Total = price * egg18Buy;
      setNewEgg18Total(newEgg18Total);
    }
    refreshTotal();
  }

  function refreshTotal() {
    affichage = newSpellTotal + newEgg11Total + newEgg18Total;
    setAffichage(affichage);
  }

  function buyAll() {
    if (affichage <= 0) {
      alert(t("boutique.nothingtobuy"));
    } else if (affichage <= accountDropAmount && affichage > 0) {
      axios.post(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "shop-secret", "plains", "buy"),
          {
            totalPrice: affichage,
            eternityPillBuy: eternityPillBuy,
            egg11Buy: egg11Buy,
            egg18Buy: egg18Buy,
          }
        )
        .then(({ data }) => {
          buySuccess = data;
          setBuySuccess(buySuccess);
          props.refresh();
        });
    } else {
      alert(t("boutique.notenoughcoins"));
    }
  }

  return (
    <div>
      {isLoading === true && (
        <MoonLoader color="#c37253" css="margin-left : 240px;" />
      )}
      {isLoading === false && (
        <div>
          <header className="pageCategoryHeader">{t("tanniere")}</header>
          <div className="flexDivsIrma">
            <img alt="" className="imgIrma" src={tanniere} />
            <span className="textIrmaNarrow">{t("tanniere.text")}</span>
          </div>

          <div className="componentBoutique">
            <p className="text">
              {t("youHave")}
              {accountDropAmount}
              <img alt="" className="coin" src={tinyDrop} />
            </p>
            <div
              className="buySommaire"
              onClick={(e) => {
                buyAll();
              }}
              data-tip={t("boutique.tooltip.acheter")}
            >
              <span className="textBoutiqueSommaire">
                {t("boutique.sommaire")}
              </span>
              <span className="textBoutiqueTotal">
                {t("boutique.sommaire.total")}
                {affichage}
                <img alt="" className="coin" src={tinyDrop} />
              </span>
              <br />
              {eternityPillBuy === 0 && egg11Buy === 0 && egg18Buy === 0 && (
                <span className="textBoutique">
                  {t("boutique.sommaire.empty")}
                </span>
              )}
              {eternityPillBuy > 0 && (
                <p className="textBoutiqueElement">
                  {"- " +
                    t("boutique.Eternity Pill") +
                    " (x" +
                    eternityPillBuy +
                    ")"}
                </p>
              )}
              {egg11Buy > 0 && (
                <p className="textBoutiqueElement">
                  {"- " + t("boutique.EGG_11") + " (x" + egg11Buy + ")"}
                </p>
              )}
              {egg18Buy > 0 && (
                <p className="textBoutiqueElement">
                  {"- " + t("boutique.EGG_18") + " (x" + egg18Buy + ")"}
                </p>
              )}
              <img alt="" className="buyPic" src={actDrop} />
            </div>

            <table className="shopItemsBoutique">
              <tbody>
                <tr>
                  <th className="empty" />
                  <th className="nameTitle">{t("boutique.nom")}</th>
                  <th className="stock">{t("boutique.prixR")}</th>
                  <th className="priceHeader">{t("boutique.prixV")}</th>
                  <th className="quantityHeader">{t("boutique.quantity")}</th>
                </tr>

                <tr>
                  <td className="icon">
                    <img
                      alt={t("boutique.Eternity Pill")}
                      src={getConsumableByKey("Eternity Pill")}
                    />
                  </td>

                  <td className="name">
                    <p className="titreObjet">
                      <span className="espaceNom">
                        {t("boutique.Eternity Pill")}
                      </span>
                      <span
                        className="imageSpan"
                        lang={i18n.language}
                        data-place="right"
                        data-tip={t("boutique.tooltip.Eternity Pill")}
                      />
                    </p>
                    <div className="possession">
                      {t("boutique.possession")}
                      {quantityMap["Eternity Pill"]}
                    </div>
                  </td>

                  <td className="price">
                    <span className="colorBlackPO">
                      {1100}
                      <img alt="" className="coin" src={tinyDrop} />
                    </span>
                  </td>

                  <td className="price">
                    <span className="colorBlackPO">
                      {pricesMap["Eternity Pill"]}
                      <img alt="" className="coin" src={tinyDrop} />
                    </span>
                  </td>

                  <td className="count">
                    <input
                      className="numberField"
                      type="number"
                      min="0"
                      max="99"
                      id="feu"
                      onInput={(e) => {
                        eternityPillChange(e, pricesMap["Eternity Pill"]);
                      }}
                    />
                  </td>
                </tr>

                <tr>
                  <td className="icon">
                    <img
                      alt={t("boutique.EGG_18")}
                      src={getConsumableByKey("EGG_18")}
                    />
                  </td>

                  <td className="name">
                    <p className="titreObjet">
                      <span className="espaceNom">{t("boutique.EGG_18")}</span>
                      <span
                        className="imageSpan"
                        lang={i18n.language}
                        data-place="right"
                        data-tip={t("boutique.tooltip.EGG_18")}
                      />
                    </p>
                    <div className="possession">
                      {t("boutique.possession")}
                      {quantityMap["EGG_18"]}
                    </div>
                  </td>

                  <td className="price">
                    <span className="colorBlackPO">
                      {2200}
                      <img alt="" className="coin" src={tinyDrop} />
                    </span>
                  </td>

                  <td className="price">
                    <span className="colorBlackPO">
                      {pricesMap["EGG_18"]}
                      <img alt="" className="coin" src={tinyDrop} />
                    </span>
                  </td>

                  <td className="count">
                    <input
                      className="numberField"
                      type="number"
                      min="0"
                      max="99"
                      id="feu"
                      onInput={(e) => {
                        egg18Change(e, pricesMap["EGG_18"]);
                      }}
                    />
                  </td>
                </tr>

                <tr>
                  <td className="icon">
                    <img
                      alt={t("boutique.EGG_11")}
                      src={getConsumableByKey("EGG_11")}
                    />
                  </td>

                  <td className="name">
                    <p className="titreObjet">
                      <span className="espaceNom">{t("boutique.EGG_11")}</span>
                      <span
                        className="imageSpan"
                        lang={i18n.language}
                        data-place="right"
                        data-tip={t("boutique.tooltip.EGG_11")}
                      />
                    </p>
                    <div className="possession">
                      {t("boutique.possession")}
                      {quantityMap["EGG_11"]}
                    </div>
                  </td>

                  <td className="price">
                    <span className="colorBlackPO">
                      {3300}
                      <img alt="" className="coin" src={tinyDrop} />
                    </span>
                  </td>

                  <td className="price">
                    <span className="colorBlackPO">
                      {pricesMap["EGG_11"]}
                      <img alt="" className="coin" src={tinyDrop} />
                    </span>
                  </td>

                  <td className="count">
                    <input
                      className="numberField"
                      type="number"
                      min="0"
                      max="99"
                      id="feu"
                      onInput={(e) => {
                        egg11Change(e, pricesMap["EGG_11"]);
                      }}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <ReactTooltip
            className="largetooltip"
            html={true}
            backgroundColor="transparent"
          />
        </div>
      )}
    </div>
  );
}
