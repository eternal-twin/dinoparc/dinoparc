import { useTranslation } from "react-i18next";
import React, { useEffect, useId, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../../index";
import getConsumableByKey from "../../../../utils/ConsumableByKey";
import getConsumableTypeByKey from "../../../../utils/ConsumableTypeByKey";
import actApply from "../../../../../media/minis/buy.gif";
import { Dinoz } from "../../../../../types/dinoz";
import HelpTooltip from "../../../../../components/HelpTooltip";
import AsyncSection from "../../../../../components/AsyncSection";
import Tooltip from "../../../../../components/Tooltip";
import Message from "../../../../../components/Message";
import { useUserData } from "../../../../../context/userData";

import "./DinozInventory.scss";
import urlJoin from "url-join";

interface Response {
  successMessageFr: string;
  successMessageEs: string;
  successMessageEn: string;
}

type Props = {
  dinoz: Dinoz;
  reloader: { reload: boolean; setReload: (state: boolean) => void };
};

type Item = {
  id: string;
  quantity: number;
};

export default function DinozInventoryCharmes(props: Props) {
  const id = useId();
  const tooltipId = `lorem_${id}`;
  const dinozId = props.dinoz.id;
  const itemsMap: Map<string, string> = new Map([
    ["Nuage Burger", "/nuage/" + dinozId + "/1"],
    ["Tarte Viande", "/tarte/" + dinozId],
    ["Pain Chaud", "/pain/" + dinozId],
    ["bonus_pill", "/pill/" + dinozId],
    ["Bave Loupi", "/bave/" + dinozId],
    ["Bol Ramens", "/ramens/" + dinozId + "/1"],
    ["Tisane des bois", "/tea/" + dinozId + "/1"],
  ]);

  const { t, i18n } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [availableItems, setAvailableItems] = useState<Item[]>([]);
  const [inutileError, setInutileError] = useState(false);
  const [limitReachedError, setLimitReachedError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [response, setResponse] = useState<Response | false>(false);

  useEffect(() => {
    axios
      .get(urlJoin(apiUrl, "account", accountId, "inventory"))
      .then(({ data }) => {
        const inventoryItemsMap: { [itemId: string]: number } = data.inventoryItemsMap;
        const items = Object.entries(inventoryItemsMap)
          .map(([id, quantity]) => ({id, quantity,}))
          .filter((item) => isConsumableInList(item.id) && getConsumableTypeByKey(item.id));
        setAvailableItems(items);
        setIsLoading(false);
      });
  }, [success, accountId]);

  function giveObject(key) {
    setLimitReachedError(false);
    setInutileError(false);
    setSuccess(false);

    axios
      .put(urlJoin(apiUrl, "account", accountId, itemsMap.get(key)))
      .then(({ data }) => {
        setInutileError(data.inutileError);
        setLimitReachedError(data.limitReached);
        if (data.success) {
          setResponse(data);
          setSuccess(data.success);

          setAvailableItems((availableItems) =>
            availableItems
              .map((item) =>
                item.id === key
                  ? { ...item, quantity: item.quantity - 1 }
                  : item
              )
              .filter((item) => item.quantity > 0)
          );
          props.reloader.setReload(!props.reloader.reload);
        }
      });
  }

  function getOrder(item: Item) {
    if (item.id === "Nuage Burger") {
      return 1;
    } else if (item.id === "Tarte Viande") {
      return 2;
    } else if (item.id === "Pain Chaud") {
      return 3;
    } else if (item.id === "bonus_pill") {
      return 4;
    } else if (item.id === "Bave Loupi") {
      return 5;
    } else if (item.id === "Bol Ramens") {
      return 6;
    } else if (item.id === "Tisane des bois") {
      return 7;
    }
    return 8;
  }

  const errorMsg = inutileError ? t("inventaire.nouse") : limitReachedError ? t("limitReachedError") : null;
  const successMsg = success && response !== false ? getSuccessMsg(response, i18n.language) : null;
  const visibleItems = availableItems
    .filter((item) => item.quantity > 0).sort((a, b) => getOrder(a) - getOrder(b));

  return (
    <AsyncSection className="borderMap" loading={isLoading}>
      <>
        {errorMsg != null && (
          <Message className="m-1" type="danger">
            {errorMsg}
          </Message>
        )}
        {successMsg != null && (
          <Message className="m-1" type="success">
            {successMsg}
          </Message>
        )}

        {visibleItems.length !== 0 && (
          <>
            <table className="dinoInventoryTable">
              <tbody>
                {visibleItems.map((item) => (
                  <tr key={item.id}>
                    <td>
                      <img alt="" src={getConsumableByKey(item.id)} />
                    </td>

                    <td>
                      {t("boutique." + item.id)}
                      <HelpTooltip
                        className="ml-1"
                        tip={t("boutique.tooltip." + item.id)}
                      />
                    </td>

                    <td className="possession">({item.quantity})</td>

                    <td
                      className="submitDinozInventory"
                      data-place="right"
                      data-for={tooltipId}
                      data-tip={t("tooltip.giveItemDinoz")}
                      tabIndex={0}
                      onClick={() => giveObject(item.id)}
                    >
                      <img alt="" src={actApply} />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <Tooltip id={tooltipId} />
          </>
        )}
      </>
    </AsyncSection>
  );
}

function isConsumableInList(consumable: string) {
  const list = [
    "Nuage Burger",
    "Tarte Viande",
    "Pain Chaud",
    "bonus_pill",
    "Bave Loupi",
    "Bol Ramens",
    "Tisane des bois",
  ];
  return list.includes(consumable);
}

function getSuccessMsg(response: Response, lang: string): string {
  switch (lang) {
    case "fr":
      return response.successMessageFr;
    case "es":
      return response.successMessageEs;
    case "en":
    default:
      return response.successMessageEn;
  }
}
