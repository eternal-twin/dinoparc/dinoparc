import { useTranslation } from "react-i18next";
import React, { useEffect, useId, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../../index";
import getConsumableByKey from "../../../../utils/ConsumableByKey";
import getConsumableTypeByKey from "../../../../utils/ConsumableTypeByKey";
import actApply from "../../../../../media/minis/buy.gif";
import { Dinoz } from "../../../../../types/dinoz";
import HelpTooltip from "../../../../../components/HelpTooltip";
import AsyncSection from "../../../../../components/AsyncSection";
import Tooltip from "../../../../../components/Tooltip";
import Message from "../../../../../components/Message";
import { useUserData } from "../../../../../context/userData";

import "./DinozInventory.scss";
import urlJoin from "url-join";

interface Response {
  successMessageFr: string;
  successMessageEs: string;
  successMessageEn: string;
}

type Props = {
  dinoz: Dinoz;
  reloader: { reload: boolean; setReload: (state: boolean) => void };
};

type Item = {
  id: string;
  quantity: number;
};

export default function DinozInventoryCharmes(props: Props) {
  const id = useId();
  const tooltipId = `lorem_${id}`;
  const dinozId = props.dinoz.id;
  const itemsMap: Map<string, string> = new Map([
    ["bonus_fire", "/charme-feu/" + dinozId + "/1"],
    ["bonus_wood", "/charme-terre/" + dinozId + "/1"],
    ["bonus_water", "/charme-eau/" + dinozId + "/1"],
    ["bonus_thunder", "/charme-foudre/" + dinozId + "/1"],
    ["bonus_air", "/charme-air/" + dinozId + "/1"],
    ["bonus_prismatik", "/charme-prismatik/" + dinozId + "/1"]
  ]);

  const { t, i18n } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [availableItems, setAvailableItems] = useState<Item[]>([]);
  const [inutileError, setInutileError] = useState(false);
  const [limitReachedError, setLimitReachedError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [response, setResponse] = useState<Response | false>(false);

  useEffect(() => {
    axios
      .get(urlJoin(apiUrl, "account", accountId, "inventory"))
      .then(({ data }) => {
        const inventoryItemsMap: { [itemId: string]: number } =
          data.inventoryItemsMap;
        const items = Object.entries(inventoryItemsMap)
          .map(([id, quantity]) => ({
            id,
            quantity,
          }))
          .filter(
            (item) =>
              isConsumableInList(item.id) && getConsumableTypeByKey(item.id)
          );
        setAvailableItems(items);
        setIsLoading(false);
      });
  }, [success, accountId]);

  function giveObject(key) {
    setLimitReachedError(false);
    setInutileError(false);
    setSuccess(false);

    axios
      .put(urlJoin(apiUrl, "account", accountId, itemsMap.get(key)))
      .then(({ data }) => {
        setInutileError(data.inutileError);
        setLimitReachedError(data.limitReached);
        if (data.success) {
          setResponse(data);
          setSuccess(data.success);
          setAvailableItems((availableItems) =>
            availableItems
              .map((item) =>
                item.id === key
                  ? { ...item, quantity: item.quantity - 1 }
                  : item
              )
              .filter((item) => item.quantity > 0)
          );
          props.reloader.setReload(!props.reloader.reload);
        }
      });
  }

  function getOrder(item: Item) {
    if (item.id === "bonus_fire") {
      return 1;
    } else if (item.id === "bonus_wood") {
      return 2;
    } else if (item.id === "bonus_water") {
      return 3;
    } else if (item.id === "bonus_thunder") {
      return 4;
    } else if (item.id === "bonus_air") {
      return 5;
    } else if (item.id === "bonus_prismatik") {
      return 6;
    }
    return 7;
  }

  const errorMsg = inutileError ? t("inventaire.nouse") : limitReachedError ? t("limitReachedError") : null;
  const successMsg = success && response !== false ? getSuccessMsg(response, i18n.language) : null;

  const visibleItems = availableItems
    .filter((item) => item.quantity > 0)
      .filter((item) => item.quantity > 0).sort((a, b) => getOrder(a) - getOrder(b));

  return (
    <AsyncSection className="borderMap" loading={isLoading}>
      <>
        {errorMsg != null && (
          <Message className="m-1" type="danger">
            {errorMsg}
          </Message>
        )}
        {successMsg != null && (
          <Message className="m-1" type="success">
            {successMsg}
          </Message>
        )}

        {visibleItems.length !== 0 && (
          <>
            <table className="dinoInventoryTable">
              <tbody>
                {visibleItems.map((item) => (
                  <tr key={item.id}>
                    <td>
                      <img alt="" src={getConsumableByKey(item.id)} />
                    </td>

                    <td>
                      {t("boutique." + item.id)}
                      <HelpTooltip
                        className="ml-1"
                        tip={t("boutique.tooltip." + item.id)}
                      />
                    </td>

                    <td className="possession">({item.quantity})</td>

                    <td
                      className="submitDinozInventory"
                      data-place="right"
                      data-for={tooltipId}
                      data-tip={t("tooltip.giveItemDinoz")}
                      tabIndex={0}
                      onClick={() => giveObject(item.id)}
                    >
                      <img alt="" src={actApply} />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <Tooltip id={tooltipId} />
          </>
        )}
      </>
    </AsyncSection>
  );
}

function isConsumableInList(consumable: string) {
  const list = [
    "bonus_fire",
    "bonus_wood",
    "bonus_water",
    "bonus_thunder",
    "bonus_air",
    "bonus_prismatik"
  ];
  return list.includes(consumable);
}

function getSuccessMsg(response: Response, lang: string): string {
  switch (lang) {
    case "fr":
      return response.successMessageFr;
    case "es":
      return response.successMessageEs;
    case "en":
    default:
      return response.successMessageEn;
  }
}
