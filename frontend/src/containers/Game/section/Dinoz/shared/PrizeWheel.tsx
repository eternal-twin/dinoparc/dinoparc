import React from "react";
import "../../../../../assets/prizeWheel.css";
import getConsumableByKey from "../../../../utils/ConsumableByKey";
import tinycoins from "../../../../../media/minis/tiny_coin.gif";

interface PrizeWheelProps {
  chosen: string;
  items: string[];
  onSelectItem?: (selectedItem: string) => unknown;
}

interface PrizeWheelState {
  selectedItem: string;
}

export default class PrizeWheel extends React.Component<
  PrizeWheelProps,
  PrizeWheelState
> {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
    };
    this.selectItem = this.selectItem.bind(this);
  }

  selectItem() {
    if (this.state.selectedItem === null) {
      const selectedItem = this.props.chosen;
      if (this.props.onSelectItem) {
        this.props.onSelectItem(selectedItem);
      }
      this.setState({ selectedItem });
    } else {
      this.setState({ selectedItem: null });
      setTimeout(this.selectItem, 500);
    }
  }

  render() {
    const { selectedItem } = this.state;
    const { items } = this.props;
    const wheelVars = {
      "--nb-item": items.length,
      "--selected-item": selectedItem,
    };

    const channelArray: Array<string> = [
        'zeroBronze', 'twoBronze', 'threeBronze', 'tenBronze',
      'zeroSilver', 'twoSilver', 'threeSilver', 'tenSilver',
      'zeroGold', 'twoGold', 'threeGold', 'tenGold'
    ];

    function getMoneyAmountFromItemString(item) {
      if (item.includes("zero")) {
        return "0";
      } else if (item === "twoBronze") {
        return "10,000"
      } else if (item === "threeBronze") {
        return "15,000"
      } else if (item === "tenBronze") {
        return "50,000"
      } else if (item === "twoSilver") {
        return "50,000"
      } else if (item === "threeSilver") {
        return "75,000"
      } else if (item === "tenSilver") {
        return "250,000"
      } else if (item === "twoGold") {
        return "1,000,000"
      } else if (item === "threeGold") {
        return "1,500,000"
      } else if (item === "tenGold") {
        return "5,000,000"
      }
      return "0";
    }

    function isFromCasino(item) {
      if (channelArray.includes(item)) {
        return true;
      }
      return false;
    }

    const spinning = selectedItem !== null ? "spinning" : "";
    return (
      <div className="wheel-container">
        <div
          className={`wheel ${spinning}`}
          style={wheelVars as any}
          onClick={this.selectItem}
        >
          {items.map((item, index) => (
            <div className="wheel-item" key={index} style={{ "--item-nb": index } as any}>
              {!isFromCasino(item) && <img src={getConsumableByKey(item)} alt="" />}
              {isFromCasino(item) &&
                  <>
                    {getMoneyAmountFromItemString(item)}
                    <img alt="" className="coin" src={tinycoins} />
                  </>
              }
            </div>
          ))}
        </div>
      </div>
    );
  }
}
