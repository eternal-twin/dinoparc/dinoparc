import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useRef, useState } from "react";
import ReactSWF from "react-swf";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import axios from "axios";
import { apiUrl } from "../../../../index";
import DinozRenderTile from "../../shared/DinozRenderTile";
import raymond from "../../../../media/pnj/raymond.gif";
import qmark from "../../../../media/game/qmark.png";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import getElementImageByString from "../../../utils/ElementImageByString";
import { Dinoz } from "../../../../types/dinoz";
import { ElementsValues } from "../../../../types/elements-values";
import dinoSwf from "../../../../media/swf/dinoz.swf";
import loaderSwf from "../../../../media/swf/loader.swf";
import fusionSwf from "../../../../media/swf/fusion.swf";
import urlJoin from "url-join";

export default function FusionCenter(props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingFusion, setIsLoadingFusion] = useState(false);
  const [isLoadingSimulation, setIsLoadingSimulation] = useState(false);
  const [displayFusionAnimation, setDisplayFusionAnimation] = useState(false);
  const [displaySimulationResult, setDisplaySimulationResult] = useState(false);
  const [showPriceError, setShowPriceError] = useState(false);
  const [cantPayFormSimulation, setCantPayFormSimulation] = useState(false);
  const [newLevel, setNewLevel] = useState(0);
  const [champifuzzAmount, setChampifuzzAmount] = useState(0);
  const [specialChampifuzzAmount, setSpecialChampifuzzAmount] = useState(0);
  const [eternityPillAmount, setEternityPillAmount] = useState(0);
  const [elementsDeltas, setElementsDeltas] = useState<Partial<ElementsValues>>(
    {}
  );
  const selectorRef = useRef<HTMLSelectElement>();
  const [resultsSimulation, setResultsSimulation] = useState([]);

  let [possibleDinozFusionList, setPossibleDinozFusionList] = useState([]);
  let [selectedDinozForFusion, setSelectedDinozForFusion] = useState<
    string & Partial<Dinoz>
  >("");
  let [levelUp, setLevelUp] = useState(0);
  let [levelGros, setLevelGros] = useState(0);
  let [accountMoney, setAccountMoney] = useState(0);
  let [resultingAppCode, setResultingAppCode] = useState("");
  let [appCode1, setAppCode1] = useState("");
  let [appCode2, setAppCode2] = useState("");

  useEffect(() => {
    axios.get(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "fusionable", "init"))
      .then(({ data }) => {
        possibleDinozFusionList = data;
        setPossibleDinozFusionList(possibleDinozFusionList);
        setIsLoading(false);
        window.scrollTo(0, 0);
      });

    axios
      .get(urlJoin(apiUrl, "account", store.getAccountId(), "money"))
      .then(({ data }) => {
        accountMoney = data;
        setAccountMoney(accountMoney);
      });
  }, [props.dinozId]);

  function selectDinozForFusion() {
    setShowPriceError(false);
    selectedDinozForFusion = selectorRef.current.value;
    if (selectedDinozForFusion != "") {
      setSelectedDinozForFusion(JSON.parse(selectedDinozForFusion));
    } else {
      setSelectedDinozForFusion("");
    }
  }

  function getDinozText(indice, homeDinozLvl, fusionCandidateLvl) {
    if (homeDinozLvl > fusionCandidateLvl) {
      return t("biggestDinozFusion");
    } else if (fusionCandidateLvl > homeDinozLvl) {
      return t("smallestDinozFusion");
    } else if (fusionCandidateLvl === homeDinozLvl && indice == 1) {
      return t("biggestDinozFusion");
    } else if (fusionCandidateLvl === homeDinozLvl && indice == 2) {
      return t("smallestDinozFusion");
    }
  }

  function getResultText() {
    return t("resultDinozFusion");
  }

  function computeResultLevel(homeDinozLvl, fusionCandidateLvl) {
    let resultLevel;
    if (homeDinozLvl >= fusionCandidateLvl) {
      resultLevel = Math.ceil(homeDinozLvl + fusionCandidateLvl / 2);
      levelUp = Math.ceil(fusionCandidateLvl / 2);
      levelGros = homeDinozLvl;
    } else {
      resultLevel = Math.ceil(fusionCandidateLvl + homeDinozLvl / 2);
      levelUp = Math.ceil(homeDinozLvl / 2);
      levelGros = fusionCandidateLvl;
    }

    return resultLevel;
  }

  function getFusionPrice() {
    return (1000 + 75 * levelGros) * levelUp;
  }

  function doSimulation(dinozIdOne, dinozIdTwo) {
    selectorRef.current.disabled = true;
    setIsLoadingSimulation(true);

    axios.get(urlJoin(apiUrl, "account", store.getAccountId(), "fusion-simulation", dinozIdOne, dinozIdTwo))
      .then(({ data }) => {
        setSelectedDinozForFusion("");
        setIsLoadingSimulation(false);
        setDisplaySimulationResult(true);

        if (data !== "") {
          setResultsSimulation(data);
          props.refreshCash();
        } else {
          setCantPayFormSimulation(true);
        }
      });
  }

  function doFusion(dinozIdOne, dinozIdTwo, appCode1, appCode2) {
    selectorRef.current.disabled = true;
    setIsLoadingFusion(true);
    let fusionPrice = getFusionPrice();
    if (fusionPrice > accountMoney) {
      setShowPriceError(true);
    }
    setAppCode1(appCode1);
    setAppCode2(appCode2);
    let finalLevel = computeResultLevel(
      props.dinoz.level,
      selectedDinozForFusion.level
    );
    axios
      .get(urlJoin(apiUrl, "account", store.getAccountId(), "fusion", dinozIdOne, dinozIdTwo, String(fusionPrice), String(finalLevel)))
      .then(({ data }) => {
        if (data.resultingAppCode != null) {
          ReactTooltip.rebuild();
          document.getElementById("returnFromFuzMenu").remove();
          setSelectedDinozForFusion("");
          resultingAppCode = data.resultingAppCode;
          setResultingAppCode(data.resultingAppCode);
          setNewLevel(data.newLevel);
          setChampifuzzAmount(data.champifuzzAmount);
          setSpecialChampifuzzAmount(data.specialChampifuzzAmount);
          setEternityPillAmount(data.eternityPillsAmount);
          setElementsDeltas(data.elementsDeltas);
          setIsLoadingFusion(false);
          setDisplayFusionAnimation(true);
        } else {
          selectorRef.current.disabled = false;
        }
      });
  }

  function continueAfterFusion() {
    props.refresh();
  }

  return (
    <div>
      {isLoading === true && (
        <MoonLoader color="#c37253" css="margin-left : 240px;" />
      )}
      {isLoading === false && (
        <div>
          <header className="pageCategoryHeader">{t("CentreFusions")}</header>
          <div className="displayFusions">
            <img alt="" className="imgRaymond" src={raymond} />
            <span className="textFusions">{t("fusions.header")}</span>
          </div>
          <p className="textSpecialFusions">
            {t("fusions.text")}
            <strong>{props.dinoz.name}</strong>
            {" :"}
          </p>

          {possibleDinozFusionList.length > 0 && (
            <div className="componentFusions">
              <select
                className="choiceInventory"
                id="selector"
                ref={selectorRef}
                onChange={(e) => {
                  selectDinozForFusion();
                }}
              >
                <option value="">{t("inventaire.choisir")}</option>
                {possibleDinozFusionList.map((dinoz, index) => {
                  return (
                    <option value={JSON.stringify(dinoz)}>
                      {dinoz.name} {" :"} {t("niveau")} {dinoz.level}
                    </option>
                  );
                })}
              </select>

              {selectedDinozForFusion != "" && isLoadingFusion === false && (
                <div>
                  <h2 className="headerFusion">{t("prepareFusion")}</h2>
                  {showPriceError && (
                    <div className="errorResultFuzPrix">
                      {t("notenoughcoins")}
                    </div>
                  )}
                  <table className="fusionTable">
                    <tbody>
                      <tr>
                        <td align="center">
                          <DinozRenderTile
                            className="dinoSheetWrapFusion"
                            appCode={props.dinoz.appearanceCode}
                          />
                        </td>
                        <td className="plusEqualsSigns">{" + "}</td>
                        <td align="center">
                          <DinozRenderTile
                            className="dinoSheetWrapFusion"
                            appCode={selectedDinozForFusion.appearanceCode}
                          />
                        </td>
                        <td className="plusEqualsSigns">{" = "}</td>
                        <td>
                          <img
                            alt=""
                            className="dinoSheetWrapFusion"
                            src={qmark}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          {t("niveau") + " "}
                          {props.dinoz.level}
                        </td>
                        <td className="plusEqualsSigns">{" + "}</td>
                        <td>
                          {t("niveau") + " "}
                          {selectedDinozForFusion.level as number}
                        </td>
                        <td className="plusEqualsSigns">{" = "}</td>
                        <td>
                          {t("niveau") + " "}
                          {computeResultLevel(
                            props.dinoz.level,
                            selectedDinozForFusion.level
                          )}
                        </td>
                      </tr>
                      <tr>
                        <td>
                          {getDinozText(
                            "1",
                            props.dinoz.level,
                            selectedDinozForFusion.level
                          )}
                        </td>
                        <td className="plusEqualsSigns"> </td>
                        <td>
                          {getDinozText(
                            "2",
                            selectedDinozForFusion.level,
                            props.dinoz.level
                          )}
                        </td>
                        <td className="plusEqualsSigns"> </td>
                        <td>{getResultText()}</td>
                      </tr>
                    </tbody>
                  </table>

                  <p className="textFuzSummary">{t("fusionElements")}</p>

                  <button
                      id="btnFuz"
                      className="buttonFusion"
                      onClick={(e) => {
                        if (window.confirm(t("confirm"))) {
                          doSimulation(
                              props.dinoz.id,
                              selectedDinozForFusion.id
                          );
                        }
                      }}
                  >
                    {t("doSimulation")}
                    <br />
                    {4900}
                    <img alt="" className="coin" src={tinycoins} />
                  </button>

                  <button
                    id="btnFuz"
                    className="buttonFusion"
                    onClick={(e) => {
                      if (window.confirm(t("confirm"))) {
                        doFusion(
                          props.dinoz.id,
                          selectedDinozForFusion.id,
                          props.dinoz.appearanceCode,
                          selectedDinozForFusion.appearanceCode
                        );
                      }
                    }}
                  >
                    {t("confirmerFusion")}
                    <br />
                    {getFusionPrice()}
                    <img alt="" className="coin" src={tinycoins} />
                  </button>
                </div>
              )}

              {selectedDinozForFusion != "" &&
                isLoadingFusion === true &&
                displayFusionAnimation === false && (
                  <MoonLoader
                    color="#c37253"
                    css="margin-left : 235px; margin-top : 30px;"
                    size={100}
                  />
                )}

              {selectedDinozForFusion != "" &&
                isLoadingSimulation === true &&
                displaySimulationResult === false && (
                  <MoonLoader
                    color="#c37253"
                    css="margin-left : 235px; margin-top : 30px;"
                    size={100}
                  />
                )}

              {selectedDinozForFusion == "" &&
                displayFusionAnimation === true && (
                  <div>
                    {/*{!navigator.userAgent.includes("etwin") && (*/}
                    {/*  <div className="fusionResult">*/}
                    {/*    <div>*/}
                    {/*      <DinozRenderTile*/}
                    {/*        className="mx-auto"*/}
                    {/*        key={resultingAppCode}*/}
                    {/*        appCode={resultingAppCode}*/}
                    {/*      />*/}
                    {/*      <p className="fuzSummaryLevel">*/}
                    {/*        {t("niveau")} {newLevel}*/}
                    {/*      </p>*/}
                    {/*    </div>*/}
                    {/*  </div>*/}
                    {/*)}*/}
                    <div className="fusionWrapper">
                      <ReactSWF
                          src={loaderSwf}
                          flashVars={"swf_url=" + fusionSwf + "?$version=2&infos=" +
                              appCode1 + ":" + appCode2 + ":" + resultingAppCode
                              + "&dinozUrl=" + dinoSwf
                          }
                          width="700"
                          height="300"
                      />
                    </div>
                    <br />
                    <a className="fuzSummaryElement">
                      <img alt="" src={getElementImageByString("Feu")} /> {"+"}
                      {elementsDeltas.Feu as number}
                    </a>
                    <a className="fuzSummaryElement">
                      <img alt="" src={getElementImageByString("Terre")} />{" "}
                      {"+"}
                      {elementsDeltas.Terre as number}
                    </a>
                    <a className="fuzSummaryElement">
                      <img alt="" src={getElementImageByString("Eau")} /> {"+"}
                      {elementsDeltas.Eau as number}
                    </a>
                    <a className="fuzSummaryElement">
                      <img alt="" src={getElementImageByString("Foudre")} />{" "}
                      {"+"}
                      {elementsDeltas.Foudre as number}
                    </a>
                    <a className="fuzSummaryElement">
                      <img alt="" src={getElementImageByString("Air")} /> {"+"}
                      {elementsDeltas.Air as number}
                    </a>
                    <br />
                    <br />
                    <a className="fuzSummaryElement">{t("youHaveGot")}</a>

                    <table className="tableRewardFuz">
                      <tr>
                        <td className="icon">
                          <img alt="" src={getConsumableByKey("champifuzz")} />
                        </td>
                        <td className="name">
                          <p className="titreObjet">
                            <a className="marginRewardFuz">
                              {"(x"}
                              {champifuzzAmount}
                              {")"}
                            </a>
                            <span className="espaceNom">
                              {t("boutique.champifuzz")}
                            </span>
                            <span
                              className="imageSpan"
                              lang={i18n.language}
                              data-place="right"
                              data-tip={t("boutique.tooltip.champifuzz")}
                            />
                          </p>
                        </td>
                      </tr>

                      {eternityPillAmount !== null &&
                        eternityPillAmount > 0 && (
                          <tr>
                            <td className="icon">
                              <img
                                alt=""
                                src={getConsumableByKey("Eternity Pill")}
                              />
                            </td>
                            <td className="name">
                              <p className="titreObjet">
                                <a className="marginRewardFuz">
                                  {"(x"}
                                  {eternityPillAmount}
                                  {")"}
                                </a>
                                <span className="espaceNom">
                                  {t("boutique.Eternity Pill")}
                                </span>
                                <span
                                  className="imageSpan"
                                  lang={i18n.language}
                                  data-place="right"
                                  data-tip={t("boutique.tooltip.Eternity Pill")}
                                />
                              </p>
                            </td>
                          </tr>
                        )}

                      {specialChampifuzzAmount !== null &&
                        specialChampifuzzAmount > 0 && (
                          <tr>
                            <td className="icon">
                              <img
                                alt=""
                                src={getConsumableByKey(
                                  "Ticket Champifuz Special"
                                )}
                              />
                            </td>
                            <td className="name">
                              <p className="titreObjet">
                                <a className="marginRewardFuz">
                                  {"(x"}
                                  {specialChampifuzzAmount}
                                  {")"}
                                </a>
                                <span className="espaceNom">
                                  {t("boutique.Ticket Champifuz Special")}
                                </span>
                                <span
                                  className="imageSpan"
                                  lang={i18n.language}
                                  data-place="right"
                                  data-tip={t(
                                    "boutique.tooltip.Ticket Champifuz Special"
                                  )}
                                />
                              </p>
                            </td>
                          </tr>
                        )}
                      <ReactTooltip
                        className="largetooltip"
                        html={true}
                        backgroundColor="transparent"
                      />
                    </table>
                    <button
                      className="buttonFusionContinue"
                      onClick={(e) => {
                        continueAfterFusion();
                      }}
                    >
                      {t("continue")}
                    </button>
                  </div>
                )}

              {selectedDinozForFusion == "" &&
                displaySimulationResult === true &&
                cantPayFormSimulation === false && (
                  <div className="gridGenome">
                    <p className="textSpecialFusions">{t("simulationText")}</p>
                    {Object.keys(resultsSimulation).map(function (key, idx) {
                      return (
                        <a className="resultGridDinoz">
                          <div className="dinoSheetWrapFusion">
                            <DinozRenderTile appCode={resultsSimulation[idx]} size={100}/>
                          </div>
                        </a>
                      );
                    })}
                  </div>
                )}

              {selectedDinozForFusion == "" &&
                displaySimulationResult === true &&
                cantPayFormSimulation === false &&
                store.getImgMode() === "NONE" && (
                  <div className="gridGenome"></div>
                )}

              {selectedDinozForFusion == "" &&
                displaySimulationResult === true &&
                cantPayFormSimulation === false &&
                  (store.getImgMode() === "BASE_64" || store.getImgMode() === "HYBRID") && (
                  <div className="gridGenome"></div>
                )}

              {selectedDinozForFusion == "" &&
                displaySimulationResult === true &&
                cantPayFormSimulation === true && (
                  <div className="errorResultFuzPrix">
                    {t("notenoughcoins")}
                  </div>
                )}
            </div>
          )}
          {possibleDinozFusionList.length == 0 && (
            <div className="componentFusions">
              <div className="fusionMessage">
                {t("nofusion") + props.dinoz.name + "."}
              </div>
            </div>
          )}
          <ReactTooltip
            className="largetooltip"
            html={true}
            backgroundColor="transparent"
          />
        </div>
      )}
    </div>
  );
}
