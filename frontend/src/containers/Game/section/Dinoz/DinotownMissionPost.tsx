import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import PeauDeVignes from "../../../../media/game/peaudevignes.jpg";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getLocationImageByNumber, {PlaceNumber,} from "../../../utils/LocationImageByPlaceNumber";
import Table from "../../../../components/Table";
import {Dinoz} from "../../../../types/dinoz";
import GameSection from "./shared/GameSection";
import Content from "../../../../components/Content";
import Row, {Col} from "../../../../components/Row";
import {useUserData} from "../../../../context/userData";
import Countdown from "react-countdown";
import Button from "../../../../components/Button";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import i18n from "i18next";
import {MissionRewardDto} from "../../../../types/mission-reward-dto";
import getTotemImageByName from "../../../utils/TotemImageByName";
import rewardIcon from "../../../../media/minis/inventory.gif";
import getPassiveIconByKey from "../../../utils/PassiveIconByKey";
import urlJoin from "url-join";

type Props = {
  dinoz: Dinoz;
  returnToDinoz: () => void;
  refreshCash: () => void;
};

export default function DinotownMissionPost(props : Props) {
  const { t } = useTranslation();
  const {accountId} = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [availableMissions, setAvailableMissions] = useState([]);
  const [rewardSectionDisplayed, setRewardSectionDisplayed] = useState(false);
  let [missionRewards, setMissionRewards] = useState<Partial<MissionRewardDto>>({});

  useEffect(() => {
    axios.get(urlJoin(apiUrl, "rec-missions", accountId, props.dinoz.id, "availableMissions"))
      .then(({ data }) => {
        setAvailableMissions(data);
        setIsLoading(false);
      });
  }, [props.dinoz.id]);

  function getSpecificToDinozValue(dinozId, dinozName) {
    if (dinozId === "" || dinozId === undefined || dinozId === null) {
      return t("mission.dinotown.tousDinozConfondus");
    }
    return dinozName;
  }

  function getMissionRarity(rarity) {
    if (rarity === 1) {
      return "⭐";
    } else if (rarity === 2) {
      return "⭐⭐";
    } else if (rarity === 3) {
      return "⭐⭐⭐";
    } else if (rarity === 4) {
      return "⭐⭐⭐⭐";
    } else if (rarity === 5) {
      return "⭐⭐⭐⭐⭐";
    }
    return rarity;
  }

  function getEndDate(expirationDate) {
    const newEndDate = new Date(0);
    newEndDate.setUTCSeconds(expirationDate / 1000);
    return (<Countdown date={newEndDate}/>);
  }

  function validateMission(mission) {
    axios.post(urlJoin(apiUrl, "rewards", accountId, props.dinoz.id, mission.id, "submit-mission"))
        .then(({data}) => {
          props.refreshCash();
          mission.rewardClaimed = true;
          setMissionRewards(data.globalRewardsMap);
          setRewardSectionDisplayed(true);
          window.scrollBy(0, 300);
        });
  }

  function getRewardsAsImages(reward: MissionRewardDto) {
    return (
        <table>
          {Object.keys(reward.globalRewardsMap).map(function (key) {
            return (
                <td className="rewardsFromMissions">
                  {!key.includes("totem") && <img alt="" src={getConsumableByKey(key)}/>}
                  {key.includes("totem") && <img alt="" src={getTotemImageByName(key)}/>}
                  <br></br>{" x "}{reward.globalRewardsMap[key]}
                </td>
            );
          })}
        </table>);
  }

  return (
      <GameSection
          isLoading={isLoading}
          title={t("missionPost")}
          returnToDinoz={props.returnToDinoz}
      >
        <div>
          <Row>
            <img
                alt=""
                className="imgIrma"
                src={getLocationImageByNumber(PlaceNumber.Dinoville)}
            />
            <Col grow={true}>
              <Content>
                <p>{t("missionPostIntro")}</p>
              </Content>
            </Col>
          </Row>

          <Row className="my-4">
            <img alt="" className="displayPeauDeVignes" src={PeauDeVignes}/>
            <Col grow={true}>
              <Content>
                <p>{t("text-peaudevignes")}</p>
              </Content>
            </Col>
          </Row>

          <table className="tableMissionPost">
            <thead>
            <tr className="trRankings">
              <th className="has-text-centered">{t('mission.dinotown.title')}</th>
              <th className="position">{t('mission.dinotown.counter')}</th>
              <th className="position">{t('mission.dinotown.goal')}</th>
              <th className="position">{t('mission.dinotown.specific')}</th>
              <th className="position">{t('mission.dinotown.rarity')}</th>
              <th className="position">{t('mission.dinotown.timeLeft')}</th>
              <th className="position">{t('mission.dinotown.reward')}</th>
              <th className="position"></th>
            </tr>
            </thead>
            <tbody>
            {Object.entries(availableMissions).map(function ([key, value]) {
              return (
                  <tr key={key} className="trRankings">
                    <td>{t('mission.dinotown.' + value.counterName)}</td>
                    <td>{value.counterCurrentValue - value.counterInitialValue}</td>
                    <td>{value.counterTargetValue - value.counterInitialValue}</td>
                    <td>{getSpecificToDinozValue(value.dinozId, value.dinozName)}</td>
                    <td>{getMissionRarity(value.rarity)}</td>
                    {(!value.rewardClaimed) && <td>{getEndDate((value.expirationDate))}</td>}
                    {(value.rewardClaimed) && <td>  ✔️</td>}

                    <td>{getRewardsAsImages(value.reward)}</td>
                    <td>
                      {(value.counterCurrentValue === value.counterTargetValue && !value.rewardClaimed && value.expirationDate > Date.now()) &&
                          <Button size={"small"} onClick={() => {validateMission(value)}}>{t('mission.dinotown.validate')}</Button>
                      }
                    </td>
                    <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                  </tr>
              );
            })}
            </tbody>
          </table>
          {rewardSectionDisplayed && (
                    <div>
                      <br></br>
                      <p className="rewardsArena">{t("youHaveGot")}</p>
                      <table className="tableRewardHermit">
                        {Object.keys(missionRewards).map(function (key) {
                          return (
                              <tr>
                                <td className="icon">
                                  {!key.includes("totem") && <img alt="" src={getConsumableByKey(key)}/>}
                                  {key.includes("totem") && <img alt="" src={getTotemImageByName(key)}/>}
                                </td>
                                <td className="name">
                                  {!key.includes("totem") && <p className="titreObjetHermit">
                                    <a className="marginRewardHermit">{"x"}{missionRewards[key]}</a>
                                    <a className="espaceNom">{t("boutique." + key)}</a>
                                    <span
                                        className="imageSpan"
                                        lang={i18n.language}
                                        data-place="right"
                                        data-tip={t("boutique.tooltip." + key)}
                                    />
                                  </p>}
                                  {key.includes("totem") && <p className="titreObjetHermit">
                                    <a className="marginRewardHermit">{"x"}{missionRewards[key]}</a>
                                    <a className="espaceNom">{t(key)}</a>
                                    <span
                                        className="imageSpan"
                                        lang={i18n.language}
                                        data-place="right"
                                        data-tip={t("tooltip." + key)}
                                    />
                                  </p>}
                                </td>
                                <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                              </tr>
                          );
                        })}
                      </table>
                    </div>
                )}
        </div>
      </GameSection>
  );
}