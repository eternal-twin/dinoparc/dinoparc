import { useTranslation } from "react-i18next";
import React, { useEffect } from "react";
import hero from "../../../../media/game/sibilin.png";

export default function HeroRareTalk() {
  const { t } = useTranslation();
  useEffect(() => {}, []);

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("HeroTalk")}</header>
        <div className="displayFusions">
          <img alt="" className="imgFactions" src={hero} />
          <span className="textIrmaNarrow">{t("SibilinDiscours")}</span>
        </div>
      </div>
    </div>
  );
}
