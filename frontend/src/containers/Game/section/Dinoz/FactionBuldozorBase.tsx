import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getLocationImageByNumber, {PlaceNumber,} from "../../../utils/LocationImageByPlaceNumber";
import {Dinoz} from "../../../../types/dinoz";
import GameSection from "./shared/GameSection";
import Content from "../../../../components/Content";
import Row, {Col} from "../../../../components/Row";
import {useUserData} from "../../../../context/userData";
import buldozor from "../../../../media/clans/buldozor.gif";
import {Clan} from "../../../../types/clan";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import i18n from "i18next";
import ReactTooltip from "react-tooltip";
import {OccupationSummary} from "../../../../types/occupation-summary";
import getPassiveIconByKey from "../../../utils/PassiveIconByKey";
import getLocationByNumber from "../../../utils/LocationByNumber";
import Table from "../../../../components/Table";
import Countdown from "react-countdown";
import urlJoin from "url-join";

type Props = {
    dinoz: Dinoz;
    returnToDinoz: () => void;
    refreshCash: () => void;
};

export default function FactionBuldozorBase(props : Props) {
    const { t } = useTranslation();
    const {accountId} = useUserData();
    const [isLoading, setIsLoading] = useState(true);
    let [actualClanDto, setActualClanDto] = useState<Partial<Clan>>({});
    let [reload, setReload] = useState(true);
    let [statusFromRedeem, setStatusFromRedeem] = useState(0);
    let [occupationSummary, setOccupationSummary] = useState<Partial<OccupationSummary>>({});

    useEffect(() => {
        axios.get(urlJoin(apiUrl, "clans", accountId))
            .then(({ data }) => {
                setActualClanDto(data);
                axios.get(urlJoin(apiUrl, "clans", "occupation-summary"))
                    .then(({ data }) => {
                        setOccupationSummary(data);
                        setIsLoading(false);
                    });
            });
    }, [props.dinoz.id, reload]);

    function redeemIrmasFromChest() {
        setStatusFromRedeem(0);
        let potionIrmaToRedeem = document.getElementById("Potion Irma Selector") as HTMLInputElement | null;

        if (Number(potionIrmaToRedeem.value) > 0) {
            axios.post(urlJoin(apiUrl, "clans", accountId, "redeem-chest", "irmas", potionIrmaToRedeem.value))
                .then(({data}) => {
                    setReload(!reload);
                    if (data) {
                        setStatusFromRedeem(1);
                    } else {
                        setStatusFromRedeem(2);
                    }
                });
        }
    }

    function redeemPrismatiksFromChest() {
        setStatusFromRedeem(0);
        let prismatiksToRedeem = document.getElementById("Prismatik Selector") as HTMLInputElement | null;

        if (Number(prismatiksToRedeem.value) > 0) {
            axios.post(urlJoin(apiUrl, "clans", accountId, "redeem-chest", "prismatiks", prismatiksToRedeem.value))
                .then(({data}) => {
                    setReload(!reload);
                    if (data) {
                        setStatusFromRedeem(1);
                    } else {
                        setStatusFromRedeem(2);
                    }
                });
        }
    }

    function giveWarriorPotionToActualDinoz() {
        axios.put(urlJoin(apiUrl, "clans", accountId, props.dinoz.id, "warrior-potion"))
            .then(({data}) => {
                props.returnToDinoz();
            });
    }

    function removeWarriorPotionFromActualDinoz() {
        if (window.confirm(t('confirm'))) {
            axios.put(urlJoin(apiUrl, "clans", accountId, props.dinoz.id, "warrior-potion-remove"))
                .then(({data}) => {
                    props.returnToDinoz();
                });
        }
    }

    function getCountdown(expirationDate) {
        const newEndDate = new Date(0);
        newEndDate.setUTCSeconds(expirationDate);
        return (<Countdown daysInHours={true} date={newEndDate}/>)
    }

    function isWarrior(malusList: string[]) {
        return (malusList.includes("Faction_1_Ozonite.warrior") || malusList.includes("Faction_2_Gorak.warrior") || malusList.includes("Faction_3_Buldozor.warrior"))
    }

    return (
        <GameSection
            isLoading={isLoading}
            title={t("Faction_3_Buldozor_Portail")}
            returnToDinoz={props.returnToDinoz}
        >
            <div>
                <Row>
                    <img
                        alt=""
                        className="imgFactions"
                        src={getLocationImageByNumber(PlaceNumber.faction3)}
                    />
                    <Col grow={true}>
                        <Content>
                            <p>{t('BuldozorIntro')}</p>
                        </Content>
                    </Col>
                </Row>
                <br></br>
                <Row>
                    <img
                        alt=""
                        className="imgFactions"
                        src={buldozor}
                    />
                    <Col grow={true}>
                        <Content>
                            <p>{t('BuldozorTalk')}</p>
                        </Content>
                    </Col>
                </Row>
                <br></br>

                <p>{t('clan.chest.text')}</p>
                {statusFromRedeem === 1 && (<div className="clanChestSuccess">{t("clanChestSuccess")}</div>)}
                {statusFromRedeem === 2 && (<div className="clanChestError">{t("clanChestError")}</div>)}

                <table className="clanChestItems">
                    <tbody>
                    <tr className="tableColor">
                        <td className="icon">
                            <img alt="" src={getConsumableByKey("Potion Irma")}/>
                        </td>
                        <td className="nameClanChest">
                            <p className="titreObjet">
                                <span className="espaceNom">{t("boutique.Potion Irma")}</span>
                                <span
                                    className="imageSpan"
                                    lang={i18n.language}
                                    data-place="right"
                                    data-tip={t("tooltip." + "Potion Irma")}
                                />
                            </p>
                            <div className="possession">
                                {t("clanChest.possession")}{!isLoading && <>{actualClanDto.chest["Potion Irma"]}</>}
                            </div>
                        </td>
                        <td className="count">
                            <button className="giveClan" onClick={(e) => {redeemIrmasFromChest()}}>{t("clan.takeFromChest")}</button>
                            {!isLoading && <input
                                className="numberFieldInv"
                                type="number"
                                min="0"
                                max={actualClanDto.chest["Potion Irma"]}
                                id={"Potion Irma Selector"}
                            />}
                        </td>
                    </tr>

                    <tr className="tableColor">
                        <td className="icon">
                            <img alt="" src={getConsumableByKey("bonus_prismatik")}/>
                        </td>
                        <td className="nameClanChest">
                            <p className="titreObjet">
                                <span className="espaceNom">{t("boutique.bonus_prismatik")}</span>
                                <span
                                    className="imageSpan"
                                    lang={i18n.language}
                                    data-place="right"
                                    data-tip={t("tooltip." + "bonus_prismatik")}
                                />
                            </p>
                            <div className="possession">
                                {t("clanChest.possession")}{!isLoading && <>{actualClanDto.chest["bonus_prismatik"]}</>}
                            </div>
                        </td>
                        <td className="count">
                            <button className="giveClan" onClick={(e) => {redeemPrismatiksFromChest()}}>{t("clan.takeFromChest")}</button>
                            {!isLoading && <input
                                className="numberFieldInv"
                                type="number"
                                min="0"
                                max={actualClanDto.chest["bonus_prismatik"]}
                                id={"Prismatik Selector"}
                            />}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <p>{t('clan.chest.warpotion1') + props.dinoz.name + t('clan.chest.warpotion2')}</p>
                <table className="clanChestItems">
                    <tbody>
                    <tr className="tableColor">
                        <td className="icon">
                            <img alt="" src={getConsumableByKey("Potion Guerre")}/>
                        </td>
                        <td className="nameClanChest">
                            <p className="titreObjet">
                                <span className="espaceNom">{t("boutique.Potion Guerre")}</span>
                                <span
                                    className="imageSpan"
                                    lang={i18n.language}
                                    data-place="right"
                                    data-tip={t("boutique.tooltip." + "Potion Guerre")}
                                />
                            </p>
                            <div className="possession">
                                {t("clanChest.possession")}{!isLoading && <>{actualClanDto.chest["Potion Guerre"]}</>}
                            </div>
                        </td>
                        <td className="count">
                            {isWarrior(props.dinoz.malusList) &&
                                <button className="giveClan" onClick={(e) => {removeWarriorPotionFromActualDinoz()}}>{t("removeWarrior")}</button>
                            }
                            {!isWarrior(props.dinoz.malusList) &&
                                <button className="giveClan" onClick={(e) => {giveWarriorPotionToActualDinoz()}}>{t("donnerA") + props.dinoz.name}</button>
                            }
                        </td>
                    </tr>
                    </tbody>
                    <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                </table>

                {occupationSummary.occupationData!== null && occupationSummary.occupationData !== undefined &&
                    <div>
                        <p>{t('clan.occupation-summary')}</p>
                        <p>{t('clan.occupation-summary-note')}</p>
                        <p>{t('clan.piet')} {getCountdown((occupationSummary.nextRefresh.valueOf()))}</p>
                        <p>{t('clan.nbBotsVivants')} {occupationSummary.alivePVEWarEnnemies.length}</p>
                        <Table>
                            <thead>
                            <tr className="tableColor">
                                <th>{t('lieu')}</th>
                                <th>{t('factionClan')}</th>
                                <th>%</th>
                            </tr>
                            </thead>
                            <tbody>
                            {occupationSummary.occupationData.map((data) => {
                                return (
                                    <tr className="tableColor">
                                        <td>{getLocationByNumber(parseFloat(data.location))}</td>
                                        <td>
                                            {data.faction !== null &&
                                                <img src={getPassiveIconByKey(data.faction + ".warrior")}
                                                     data-place="right"
                                                     data-tip={t("tooltip." + data.faction)}></img>}
                                        </td>
                                        <td>{data.percentage !== null && <>{Math.trunc(Number(data.percentage))}{" %"}</>}</td>
                                    </tr>
                                );
                            })}
                            </tbody>
                        </Table>
                        <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                </div>}
            </div>
        </GameSection>
    );
}