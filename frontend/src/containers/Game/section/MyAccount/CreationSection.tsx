import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import Button from "../../../../components/Button";
import Content from "../../../../components/Content";
import Row from "../../../../components/Row";
import Section from "../../../../components/Section";
import DinozRenderTile from "../../shared/DinozRenderTile";

const races = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L"
];

const allAlphabet = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
];

const ORIGINAL_CHAR = "0";
const SOMBRE_CHAR = "#";
const CERISE_CHAR = "$";
const SHINY_CHAR = "&";

const defaultAppCode = new Array(15).fill(allAlphabet[0]).join("");

export default function CreationSection() {
  const { t } = useTranslation();
  const [creationAppCode, setCreationAppCode] = useState(defaultAppCode);

  function changeChar(newChar: string, position: number) {
    const newAppCode =
      creationAppCode.substring(0, position) +
      newChar +
      creationAppCode.substring(position + 1);
    if (position > 0 || (position === 0 && races.includes(newChar))) {
      setCreationAppCode(newAppCode);
    }
  }

  function goLeft(position: number) {
    const currentChar = creationAppCode[position];
    const currentIndex = allAlphabet.indexOf(currentChar);
    const newChar =
      allAlphabet[
        currentIndex <= 0 ? allAlphabet.length - 1 : currentIndex - 1
      ];
    changeChar(newChar, position);
  }

  function goRight(position: number) {
    const currentChar = creationAppCode[position];
    const currentIndex = allAlphabet.indexOf(currentChar);
    const newChar =
      allAlphabet[
        currentIndex >= allAlphabet.length - 1 ? 0 : currentIndex + 1
      ];
    changeChar(newChar, position);
  }

  function setOriginal() {
    changeChar(ORIGINAL_CHAR, 14);
  }

  function setSombre() {
    changeChar(SOMBRE_CHAR, 14);
  }

  function setCerise() {
    changeChar(CERISE_CHAR, 14);
  }

  function setShiny() {
    changeChar(SHINY_CHAR, 14);
  }

  const handlePaste: React.ClipboardEventHandler<HTMLInputElement> = (
    event
  ) => {
    setCreationAppCodeFromStringIfCan(event.clipboardData.getData("text"));
  };

  function setCreationAppCodeFromStringIfCan(text: string) {
    const allowedChar = [
      ...allAlphabet,
      ORIGINAL_CHAR,
      SOMBRE_CHAR,
      CERISE_CHAR,
      SHINY_CHAR,
    ];
    const isValidCode =
      text.length >= 14 &&
      text.length < 25 &&
      text.split("").every((char) => allowedChar.includes(char) && races.includes(text.charAt(0)));
    if (isValidCode) {
      setCreationAppCode(text.substring(0, 15).padEnd(15, allAlphabet[0]));
    }
  }

  return (
    <Section title={t("espaceCreatif")}>
      <div className="creationContainer">
        <Content>
          <DinozRenderTile appCode={creationAppCode} size={300} />

          <p className="mt-4">
            {t("myAppCodeIs")} <code>{creationAppCode}</code>
          </p>
        </Content>
        <div>
          {creationAppCode.split("").map((char, i) => {
            return (
              <Row
                key={i}
                gap="small"
                justify="center"
                vcenter={true}
                className="mt-3"
              >
                <Button tabIndex={-1} size="small" onClick={() => goLeft(i)}>
                  {"◄"}
                </Button>
                <input
                  className=""
                  placeholder={allAlphabet[0]}
                  value={allAlphabet[0] === char ? "" : char}
                  maxLength={1}
                  style={{ width: "1.5rem", textAlign: "center" }}
                  onInput={(e) => {
                    const value = e.currentTarget.value;
                    const finalChar = allAlphabet.includes(value)
                      ? value
                      : allAlphabet[0];
                    changeChar(finalChar, i);
                  }}
                  onKeyUp={(e) => {
                    if (e.code === "ArrowLeft") {
                      goLeft(i);
                    } else if (e.code === "ArrowRight") {
                      goRight(i);
                    }
                  }}
                  onClick={(e) =>
                    (e.target as HTMLInputElement).setSelectionRange(0, 1)
                  }
                  onPaste={handlePaste}
                />
                <Button tabIndex={-1} size="small" onClick={() => goRight(i)}>
                  {"►"}
                </Button>
              </Row>
            );
          })}

          <Row gap="small" className="mt-3">
            <Button size="small" onClick={() => setOriginal()}>
              {t("original")}
            </Button>

            <Button size="small" onClick={() => setSombre()}>
              {t("sombre")}
            </Button>

            <Button size="small" onClick={() => setCerise()}>
              {t("cerise")}
            </Button>

            <Button size="small" onClick={() => setShiny()}>
              {"Shiny"}
            </Button>
          </Row>
        </div>
      </div>
    </Section>
  );
}
