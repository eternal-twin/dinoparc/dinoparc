import { useTranslation } from "react-i18next";
import React from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { useUserData } from "../../../../context/userData";
import Section from "../../../../components/Section";
import Button from "../../../../components/Button";
import Content from "../../../../components/Content";
import urlJoin from "url-join";

type Props = {};

export default function AccountExportSection(props: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();

  function generateDatas() {
    axios.get(urlJoin(apiUrl, "account", accountId, "download"))
        .then((csv) => {
      const url = window.URL.createObjectURL(new Blob([csv.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "DinozDataExport.csv");
      document.body.appendChild(link);
      link.click();
    });
  }

  return (
    <Section title={t("datas")}>
      <Content>
        <p>{t("datas.dl")}</p>
        <div className="has-text-centered">
          <Button onClick={() => generateDatas()}>{t("datas.generate")}</Button>
        </div>
      </Content>
    </Section>
  );
}
