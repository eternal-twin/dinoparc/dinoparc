import { useTranslation } from "react-i18next";
import React from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { useUserData } from "../../../../context/userData";
import Section from "../../../../components/Section";
import Content from "../../../../components/Content";
import urlJoin from "url-join";

type Props = {};

export default function AccountClanSection(props: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();

  function removeApplicationsFromClan() {
    if (window.confirm(t("confirm"))) {
      axios.put(urlJoin(apiUrl, "clans", "remove-apply", accountId));
    }
  }

  return (
    <Section title={t("clan")}>
      <Content>
        <button
          className="links mt-4"
          onClick={() => {
            removeApplicationsFromClan();
          }}
        >
          {t("clan.cancel.applications")}
        </button>
      </Content>
    </Section>
  );
}
