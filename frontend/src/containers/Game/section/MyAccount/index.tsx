import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import CreationSection from "./CreationSection";
import AccountParametersSection from "./AccountParametersSection";
import AccountExportSection from "./AccountExportSection";
import AccountClanSection from "./AccountClanSection";
import Section from "../../../../components/Section";
import { useUserData } from "../../../../context/userData";
import HelpFlagToggleSection from "./HelpFlagToggleSection";

type Props = {
  reload?: boolean;
};

export default function MyAccount(props: Props) {
  const { t } = useTranslation();
  const { refreshUser } = useUserData();

  useEffect(() => {
    refreshUser();
  }, []);

  return (
      <div className="TEMP_mainContainer">
          <Section title={t("myAccountLabel")}>
              <p className="compteHeader">{t("moncompte.header")}</p>
          </Section>

          <HelpFlagToggleSection/>
          <AccountExportSection/>
          <AccountParametersSection/>
          <AccountClanSection/>
          <CreationSection/>
      </div>
  );
}
