import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import axios from "axios";
import { apiUrl } from "../../../index";
import Store from "../../../utils/Store";
import { Clan } from "../../../types/clan";
import { ClanPage } from "../../../types/clan-page";
import totem from "../../../media/totem/totem.png";
import chest from "../../../media/totem/chest.png";
import getActionImageFromActionString from "../../utils/ActionImageFromString";
import getHistoryImageFromKey from "../../utils/HistoryImageFromKey";
import { ClanMessage } from "../../../types/clan-message";
import getFactionImageFromActionString from "../../utils/FactionImageFromString";
import getTotemImageByName from "../../utils/TotemImageByName";
import ReactTooltip from "react-tooltip";
import getConsumableByKey from "../../utils/ConsumableByKey";
import urlJoin from "url-join";

type Props = {
  openPlayerProfile: (playerId: string) => void;
};

export default function MyClan({ openPlayerProfile }: Props) {
  let { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [isInAClan, setIsInAClan] = useState(false);
  const [alliances, setAlliances] = useState([]);
  const [messageToSend, setMessageToSend] = useState("");
  const [editPageTitle, setEditPageTitle] = useState("");
  const [editPageContent, setEditPageContent] = useState("");
  const [refreshComponent, setRefreshComponent] = useState(false);
  const [memberTitleEdit, setMemberTitleEdit] = useState("");
  let [hasAdminRightsOnClan, setHasAdminRightsOnClan] = useState(false);
  let [sectionCurrentlyActive, setSectionCurrentlyActive] = useState(0);
  let [clan, setClan] = useState<Partial<Clan>>({});
  let [clanMessages, setClanMessages] = useState<Partial<ClanMessage>>({});
  let [clanPages, setClanPages] = useState({});
  let [actualSelectedPage, setActualSelectedPage] = useState<Partial<ClanPage>>(
    {}
  );
  let [isPublic, setIsPublic] = useState(true);
  let [createPageTitle, setCreatePageTitle] = useState("");
  let [createPageContent, setCreatePageContent] = useState("");
  let [clanMembers, setClanMembers] = useState([]);
  let [userToEditHasAdminRightsOnClan, setUserToEditHasAdminRightsOnClan] =
    useState(false);
  let [userToEdit, setUserToEdit] = useState("");

  useEffect(() => {
    setEditPageTitle("");
    setEditPageContent("");
    setCreatePageTitle("");
    setCreatePageContent("");
    setIsPublic(true);
    setIsLoading(true);
    setUserToEditHasAdminRightsOnClan(false);
    setUserToEdit("");
    setMemberTitleEdit("");
    setSectionCurrentlyActive(0);
    window.scrollTo(0, 0);
    refreshClan(true);
  }, [refreshComponent]);

  function refreshClan(isInit) {
    setIsLoading(true);
    axios.get(urlJoin(apiUrl, "clans", store.getAccountId())).then(({ data }) => {
      if (!data) {
        setIsInAClan(false);
        setIsLoading(false);
      } else {
        setIsInAClan(true);
        clan = data;
        setClan(clan);
        setAlliances(clan.allies);

        if (isInit) {
          clanPages = Object.values(data.pages);
          setClanPages(clanPages);
          actualSelectedPage = clanPages[0];
          setActualSelectedPage(actualSelectedPage);

          axios
            .get(urlJoin(apiUrl, "clans", "messages" + store.getAccountId()))
            .then(({ data }) => {
              if (data) {
                clanMessages = data;
                setClanMessages(clanMessages);
              }
            });

          axios
            .get(urlJoin(apiUrl, "clans", "rights", store.getAccountId()))
            .then(({ data }) => {
              if (data) {
                hasAdminRightsOnClan = data;
                setHasAdminRightsOnClan(hasAdminRightsOnClan);
              }
              setIsLoading(false);
            });

          axios
            .get(urlJoin(apiUrl, "clans", "members", clan.id.toString(), store.getAccountId()))
            .then(({ data }) => {
              clanMembers = data;
              setClanMembers(clanMembers);
              setIsLoading(false);
            });
        } else {
          setIsLoading(false);
        }
      }
    });
  }

  function changePage(newPage) {
    setUserToEditHasAdminRightsOnClan(false);
    setEditPageTitle("");
    setEditPageContent("");
    setCreatePageTitle("");
    setCreatePageContent("");
    setIsPublic(true);
    actualSelectedPage = newPage;
    setActualSelectedPage(actualSelectedPage);
  }

  function getTotalNumberOfPoints() {
    return clan.nbPointsOccupation + clan.nbPointsFights + clan.nbPointsTotem;
  }

  function loadNewUISection(newSectionToLoad) {
    setIsPublic(true);
    window.scrollTo(0, 0);
    setSectionCurrentlyActive(newSectionToLoad);
  }

  function renderClanMessages() {
    if (clanMessages.messages) {
      return (
        <div>
          {clanMessages.messages.map(
            (clanMsg: { author: string; message: string; date: string }) => {
              return (
                <div className="msg">
                  <div className="headerClan">
                    <div className="clanMsgAuthor">
                      <a>{clanMsg.author}</a>
                    </div>
                    <div className="clanMsgDate">
                      <a>{clanMsg.date}</a>
                    </div>
                  </div>
                  <div className="clanMessageRender">{clanMsg.message}</div>
                </div>
              );
            }
          )}
        </div>
      );
    }
  }

  function sendMessage() {
    if (messageToSend.trim().length > 0) {
      setIsLoading(true);
      axios
        .post(urlJoin(apiUrl, "clans", "postMessage"), {
          authorId: store.getAccountId(),
          message: messageToSend,
        })
        .then(({ data }) => {
          if (data) {
            setClanMessages(data);
            loadNewUISection(0);
          }
          setIsLoading(false);
          setMessageToSend("");
        });
    }
  }

  function modifyClanPage() {
    if (editPageTitle.trim().length > 0 || editPageContent.trim().length > 0) {
      setIsLoading(true);
      axios
        .put(urlJoin(apiUrl, "clans", "modifyClanPage"), {
          accountId: store.getAccountId(),
          clanName: editPageTitle,
          clanDescription: editPageContent,
          pageId: actualSelectedPage.pageId,
        })
        .then(({ data }) => {
          setIsLoading(false);
          if (data) {
            setRefreshComponent(!refreshComponent);
          }
        });
    } else if (createPageTitle.length > 0) {
      setIsLoading(true);
      axios
        .put(urlJoin(apiUrl, "clans", "modifyClanPage"), {
          accountId: store.getAccountId(),
          clanName: createPageTitle,
          clanDescription: createPageContent,
          private: !isPublic.valueOf(),
        })
        .then(({ data }) => {
          setIsLoading(false);
          if (data) {
            setRefreshComponent(!refreshComponent);
          }
        });
    }
  }

  function deleteClanPage() {
    if (window.confirm(t("confirm"))) {
      setIsLoading(true);
      axios
        .put(urlJoin(apiUrl, "clans", "deleteClanPage"), {
          accountId: store.getAccountId(),
          pageId: actualSelectedPage.pageId,
        })
        .then(({ data }) => {
          setIsLoading(false);
          if (data) {
            setRefreshComponent(!refreshComponent);
          }
        });
    }
  }

  function togglePublicCheckBox() {
    isPublic = !isPublic;
    setIsPublic(isPublic);
  }

  function getHelpEditSection() {
    return (
      <>
        <br />
        <br />
        <h2 className="miniHeaders2">{t("helpLabel")}</h2>
        <p>{t("clan.img.help")}</p>
        <code>
          &lt;img src="http://www.dinoparc.com/img/news/kabuki.gif"&gt;
        </code>
        <br />
        <br />
        <img alt="" src="http://www.dinoparc.com/img/news/kabuki.gif" />
        <br />
        <br />
        <button
          className="hoverReturn"
          onClick={(e) => {
            loadNewUISection(0);
          }}
        >
          <img alt="" src={getActionImageFromActionString("🡸")} />
        </button>
      </>
    );
  }

  function processCandidature(accepted: boolean, playerId: string) {
    if (window.confirm(t("confirm"))) {
      setIsLoading(true);
      axios.put(urlJoin(apiUrl, "clans", "process-application", clan.id.toString(), playerId, String(accepted), store.getAccountId()))
        .then(({ data }) => {
          setIsLoading(false);
          if (data) {
            setRefreshComponent(!refreshComponent);
          }
        });
    }
  }

  function quitClan() {
    if (window.confirm(t("clan.leadertransfer"))) {
      setIsLoading(true);
      axios
        .put(urlJoin(apiUrl, "clans", "quit", store.getAccountId()))
        .then(({ data }) => {
          setIsLoading(false);
          if (data) {
            setRefreshComponent(!refreshComponent);
          }
        });
    }
  }

  function editMemberRights(playerId: any) {
    userToEdit = playerId;
    setUserToEdit(userToEdit);
    axios.get(urlJoin(apiUrl, "clans", "rights", playerId))
        .then(({ data }) => {
      if (data) {
        userToEditHasAdminRightsOnClan = data;
        setUserToEditHasAdminRightsOnClan(userToEditHasAdminRightsOnClan);
        setIsLoading(false);
        loadNewUISection(4);
      } else {
        setIsLoading(false);
        loadNewUISection(4);
      }
    });
  }

  function toggleEditForAdminCheckBox() {
    userToEditHasAdminRightsOnClan = !userToEditHasAdminRightsOnClan;
    setUserToEditHasAdminRightsOnClan(userToEditHasAdminRightsOnClan);

    if (window.confirm(t("confirm"))) {
      setIsLoading(true);
      axios.put(urlJoin(apiUrl, "clans", "change-admin-flag", clan.id.toString(), store.getAccountId(), userToEdit))
        .then(() => {
          setIsLoading(false);
          setRefreshComponent(!refreshComponent);
        });
    }
  }

  function editMemberTitle() {
    if (window.confirm(t("confirm"))) {
      setIsLoading(true);
      axios.put(urlJoin(apiUrl, "clans", "change-title", clan.id.toString(), store.getAccountId(), userToEdit, memberTitleEdit))
        .then(() => {
          setIsLoading(false);
          setRefreshComponent(!refreshComponent);
        });
    }
  }

  function completeTotem() {
    if (window.confirm(t("clan.completetotem") + "?")) {
      setIsLoading(true);
      axios
        .post(urlJoin(apiUrl, "clans", store.getAccountId(), "/totem"))
        .then(({ data }) => {
          setIsLoading(false);
          setRefreshComponent(!refreshComponent);
          refreshClan(false);
        });
    }
  }

  function redeemTotemRewards() {
    setIsLoading(true);
    axios
      .put(urlJoin(apiUrl, "clans", "redeemTotemRewards", store.getAccountId()))
      .then(({ data }) => {
        setIsLoading(false);
        setRefreshComponent(!refreshComponent);
      });
  }

  return (
    <div>
      {isLoading === true && (
        <MoonLoader color="#c37253" css="margin-left : 240px;" />
      )}
      {isLoading === false && (
        <>
          {sectionCurrentlyActive === 0 && (
            <div>
              {!isInAClan && (
                <>
                  <header className="pageCategoryHeader">
                    {t("myClanLabel")}
                  </header>
                  <div className="newsPopup">{t("noClan")}</div>
                </>
              )}
              {isInAClan && (
                <div className="clanPane">
                  <header className="pageCategoryHeader">{clan.name}</header>
                  <h2 className="miniHeaders2">{t("clan.menu")}</h2>
                  <br />
                  {Object.keys(clanPages).map(function (key) {
                    return (
                      <ul className="elemPagesClan">
                        {!clanPages[key].private && (
                          <li
                            className="clickablePageFromClan"
                            onClick={(e) => {
                              changePage(clanPages[key]);
                            }}
                          >
                            {clanPages[key].pageTitle}
                          </li>
                        )}
                      </ul>
                    );
                  })}
                  <br />
                  <a className="privatePageLabel">{t("privatePages")}</a>
                  <br />
                  <br />
                  {Object.keys(clanPages).map(function (key) {
                    return (
                      <ul className="elemPagesClan">
                        {clanPages[key].private && (
                          <li
                            className="clickablePageFromClan"
                            onClick={(e) => {
                              changePage(clanPages[key]);
                            }}
                          >
                            {clanPages[key].pageTitle}
                          </li>
                        )}
                      </ul>
                    );
                  })}

                  <h2 className="miniHeaders2">
                    {actualSelectedPage.pageTitle}
                  </h2>
                  <div className="clanSection">
                    {actualSelectedPage.rawContent && (
                      <div
                        className="privatePageLabel"
                        dangerouslySetInnerHTML={{
                          __html: actualSelectedPage.rawContent,
                        }}
                      />
                    )}
                    {hasAdminRightsOnClan && (
                      <div>
                        <p
                          className="editAndManage"
                          onClick={(e) => {
                            loadNewUISection(2);
                          }}
                        >
                          [ {t("clan.editpage")} ]
                        </p>
                        <p
                          className="editAndManage"
                          onClick={(e) => {
                            loadNewUISection(3);
                          }}
                        >
                          [ {t("clan.addpage")} ]
                        </p>
                      </div>
                    )}
                  </div>

                  <h2 className="miniHeaders2">{t("clan.totem")}</h2>
                  <div className="clanSection">
                    <div className="noFlexTop">
                      <img alt="" className="totem" src={totem} />
                      <div>
                        <span className="textTotem">
                          {t("totem.demande.1") +
                            "#" +
                            clan.totem.totemNumber +
                            t("totem.demande.2")}
                        </span>
                        <div className="totemSpace">
                          <td className="iconTotem">
                            <img
                              alt={t("totemBuze")}
                              src={getTotemImageByName("totemBuze")}
                            ></img>
                          </td>
                          <td className="nameTotem">
                            <p className="titreObjetTotem">
                              <span className="espaceNomTotem">
                                {t("totemBuze")}
                              </span>
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip.totemBuze")}
                              ></span>
                            </p>
                            <div className="possessionTotem">
                              {clan.totem["currentBuze"]}
                              {" / "}
                              {clan.totem["totalBuzeNeeded"]}
                            </div>
                          </td>
                        </div>

                        <div className="totemSpace">
                          <td className="iconTotem">
                            <img
                              alt={t("totemPlume")}
                              src={getTotemImageByName("totemPlume")}
                            ></img>
                          </td>
                          <td className="nameTotem">
                            <p className="titreObjetTotem">
                              <span className="espaceNomTotem">
                                {t("totemPlume")}
                              </span>
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip.totemPlume")}
                              ></span>
                            </p>
                            <div className="possessionTotem">
                              {clan.totem["currentPlumes"]}
                              {" / "}
                              {clan.totem["totalPlumesNeeded"]}
                            </div>
                          </td>
                        </div>

                        <div className="totemSpace">
                          <td className="iconTotem">
                            <img
                              alt={t("totemDent")}
                              src={getTotemImageByName("totemDent")}
                            ></img>
                          </td>
                          <td className="nameTotem">
                            <p className="titreObjetTotem">
                              <span className="espaceNomTotem">
                                {t("totemDent")}
                              </span>
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip.totemDent")}
                              ></span>
                            </p>
                            <div className="possessionTotem">
                              {clan.totem["currentDents"]}
                              {" / "}
                              {clan.totem["totalDentsNeeded"]}
                            </div>
                          </td>
                        </div>

                        <div className="totemSpace">
                          <td className="iconTotem">
                            <img
                              alt={t("totemCrin")}
                              src={getTotemImageByName("totemCrin")}
                            ></img>
                          </td>
                          <td className="nameTotem">
                            <p className="titreObjetTotem">
                              <span className="espaceNomTotem">
                                {t("totemCrin")}
                              </span>
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip.totemCrin")}
                              ></span>
                            </p>
                            <div className="possessionTotem">
                              {clan.totem["currentCrins"]}
                              {" / "}
                              {clan.totem["totalCrinsNeeded"]}
                            </div>
                          </td>
                        </div>

                        <div className="totemSpace">
                          <td className="iconTotem">
                            <img
                              alt={t("totemBois")}
                              src={getTotemImageByName("totemBois")}
                            ></img>
                          </td>
                          <td className="nameTotem">
                            <p className="titreObjetTotem">
                              <span className="espaceNomTotem">
                                {t("totemBois")}
                              </span>
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("tooltip.totemBois")}
                              ></span>
                            </p>
                            <div className="possessionTotem">
                              {clan.totem["currentBois"]}
                              {" / "}
                              {clan.totem["totalBoisNeeded"]}
                            </div>
                          </td>
                        </div>
                      </div>

                      <div>
                        <span className="textTotem">
                          {t("clan.chest.rewards")}
                        </span>
                        {Object.keys(clan.totem.totemRewards).map(function (
                          key
                        ) {
                          return (
                            <div className="totemSpace">
                              <td className="iconTotem">
                                <img
                                  alt={t(key)}
                                  src={getConsumableByKey(key)}
                                ></img>
                              </td>
                              <td>
                                <p className="titreObjetTotem">
                                  <span className="espaceNomTotemRewards">
                                    {" x "}
                                    {clan.totem.totemRewards[key]}
                                  </span>
                                  <span
                                    className="imageSpan"
                                    lang={i18n.language}
                                    data-place="right"
                                    data-tip={t("boutique.tooltip." + key)}
                                  ></span>
                                </p>
                              </td>
                            </div>
                          );
                        })}
                      </div>
                    </div>

                    <br />
                    <div className="clanTotemLinks">
                      <span
                        className="clickablePageFromClan"
                        onClick={(e) => {
                          refreshClan(false);
                        }}
                      >
                        {t("clan.refreshtotem")}
                      </span>
                    </div>
                    <br />

                    {!clan.totem.full && (
                      <div className="clanTotemLinks">
                        <span
                          className="clickablePageFromClan"
                          onClick={(e) => {
                            completeTotem();
                          }}
                        >
                          {t("clan.completetotem")}
                        </span>
                      </div>
                    )}

                    {clan.totem.full && hasAdminRightsOnClan && (
                      <button
                        id="btnGive"
                        className="buttonGiveaway"
                        onClick={(e) => {
                          if (window.confirm(t("confirm"))) {
                            redeemTotemRewards();
                          }
                        }}
                      >
                        {t("clan.totem.validate")}
                      </button>
                    )}
                    <br />
                    <ReactTooltip
                      className="largetooltip"
                      html={true}
                      backgroundColor="transparent"
                    />
                  </div>

                  <h2 className="miniHeaders2">{t("clan.chest")}</h2>
                  <div className="clanSection">
                    <div className="noFlexTop">
                      <img alt="" className="totem" src={chest} />
                      <div>
                        <span className="textTotem">
                          {t("clan.chest.text")}
                        </span>
                      </div>
                      <div className="offsetClanChest">
                        {Object.keys(clan.chest).map(function (key) {
                          return (
                            <div className="totemSpace">
                              <td className="iconTotem">
                                <img
                                  alt={t(key)}
                                  src={getConsumableByKey(key)}
                                ></img>
                              </td>
                              <td>
                                <p className="titreObjetTotem">
                                  <span className="espaceNomTotemRewards">
                                    {" x "}
                                    {clan.chest[key]}
                                  </span>
                                  <span
                                    className="imageSpan"
                                    lang={i18n.language}
                                    data-place="right"
                                    data-tip={t("boutique.tooltip." + key)}
                                  ></span>
                                </p>
                              </td>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>

                  <h2 className="miniHeaders2">{t("clan.guerre")}</h2>
                  <div className="clanSection">
                    <div>
                      <span className="statsClan">
                        {t("factionClan")}
                        {" : "}
                        <img alt="" src={getFactionImageFromActionString(clan.faction)}
                             data-place="right"
                             data-tip={t("tooltip." + clan.faction) + "</strong>"}
                        />
                        {","}
                      </span>
                      <span className="statsClan">
                        {t("classement.position")}
                        {" : "}
                        <strong>{clan.position}</strong>
                        {","}
                      </span>
                      <span className="statsClan">
                        Dinoz{" : "}
                        <strong>{clan.nbDinozTotal}</strong>
                        {","}
                      </span>
                      <span className="statsClan">
                        Dinoz {t("nbDinozWarriorClan")}
                        {" : "}
                        <strong>{clan.nbDinozWarriors}</strong>
                        {","}
                      </span>
                      <span className="statsClan">
                        {t("creationDateClan")}
                        {" : "}
                        <strong>{clan.creationDate}</strong>
                      </span>
                    </div>

                    <table className="pointsSummary">
                      <tr>
                        <td>
                          <img
                            alt=""
                            className="clanPtsIcons"
                            src={getActionImageFromActionString("Rock")}
                          />
                        </td>
                        <td>{t("alliedClans")}</td>
                      </tr>
                      {alliances.map((alliance, i) => {
                        return (
                          <tr>
                            <td>
                              <img alt="" className="clanPtsIcons" src={getHistoryImageFromKey("hist_xp.gif")}/>
                            </td>
                            <td>{alliance}</td>
                          </tr>
                        );
                      })}
                    </table>

                    <table className="pointsSummary">
                      <tr>
                        <td>
                          <img
                            alt=""
                            className="clanPtsIcons"
                            src={getActionImageFromActionString("Pyramids")}
                          />
                        </td>
                        <td>{t("nbPointsOccupationClan")}</td>
                        <td>{clan.nbPointsOccupation}</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            alt=""
                            className="clanPtsIcons"
                            src={getActionImageFromActionString("Combat")}
                          />
                        </td>
                        <td>{t("nbPointsCombatsClan")}</td>
                        <td>{clan.nbPointsFights}</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            alt=""
                            className="clanPtsIcons"
                            src={getActionImageFromActionString("Fouiller")}
                          />
                        </td>
                        <td>{t("nbPointsTotemClan")}</td>
                        <td>{clan.nbPointsTotem}</td>
                      </tr>
                      <tr>
                        <td> </td>
                        <td>{t("boutique.sommaire.total")}</td>
                        <td>{getTotalNumberOfPoints()}</td>
                      </tr>
                    </table>
                  </div>

                  <h2 className="miniHeaders2">{t("clan.derniermsg")}</h2>
                  <div className="clanSection">
                    <div
                      className="clickablePageFromClan"
                      onClick={(e) => {
                        loadNewUISection(1);
                      }}
                    >
                      {t("clan.writemsg")}
                    </div>
                    {renderClanMessages()}
                  </div>

                  <h2 className="miniHeaders2">{t("clan.listemembres")}</h2>
                  <div className="clanSection">
                    <table className="publicClanMembersTable">
                      <tbody>
                        <tr>
                          <th>{t("classement.pseudo")}</th>
                          <th>
                            <img
                              alt=""
                              className="clanPtsIcons"
                              src={getActionImageFromActionString("Combat")}
                            />
                          </th>
                          <th>
                            <img
                              alt=""
                              className="clanPtsIcons"
                              src={getActionImageFromActionString("Fouiller")}
                            />
                          </th>
                          {hasAdminRightsOnClan && (
                            <th>{t("boutique.action")}</th>
                          )}
                        </tr>
                        {clanMembers.map((clanMember, i) => {
                          return (
                            <tr>
                              <td>
                                <span
                                  onClick={(e) => {
                                    openPlayerProfile(clanMember.playerId);
                                  }}
                                  className="clickablePageFromClan"
                                >
                                  {clanMember.playerName}
                                </span>
                                {clanMember.title && (
                                  <span className="blackTitle">
                                    ({clanMember.title})
                                  </span>
                                )}
                              </td>
                              <td>{clanMember.nbPointsFights}</td>
                              <td>{clanMember.nbPointsTotem}</td>
                              {hasAdminRightsOnClan && (
                                <td>
                                  <a
                                    onClick={(e) =>
                                      editMemberRights(clanMember.playerId)
                                    }
                                    className="clickablePageFromClan"
                                  >
                                    {t("clan.editpage")}
                                  </a>
                                  {clanMember.playerId !==
                                    store.getAccountId() && (
                                    <a
                                      onClick={(e) =>
                                        processCandidature(
                                          false,
                                          clanMember.playerId
                                        )
                                      }
                                      className="clickablePageFromClan"
                                    >
                                      {t("clan.renvoyer")}
                                    </a>
                                  )}
                                </td>
                              )}
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>

                  {hasAdminRightsOnClan && (
                    <div>
                      <h2 className="miniHeaders2">{t("clan.candidates")}</h2>
                      <div className="clanSection">
                        {clan.candidates.length > 0 && (
                          <div>
                            <table className="publicClanMembersTable">
                              <tbody>
                                <tr>
                                  <th>{t("classement.pseudo")}</th>
                                  <th>{t("boutique.action")}</th>
                                </tr>
                                {clan.candidates.map((candidat: string, i) => {
                                  return (
                                    <tr>
                                      <td>
                                        <span className="clickablePageFromClan">
                                          {candidat.substring(
                                            0,
                                            candidat.indexOf("=")
                                          )}
                                        </span>
                                      </td>
                                      <td>
                                        <a
                                          onClick={(e) =>
                                            processCandidature(
                                              true,
                                              candidat.substring(
                                                candidat.indexOf("=") + 1,
                                                candidat.length
                                              )
                                            )
                                          }
                                          className="clickablePageFromClan"
                                        >
                                          {t("acceptClan")}
                                        </a>
                                        <a
                                          onClick={(e) =>
                                            processCandidature(
                                              false,
                                              candidat.substring(
                                                candidat.indexOf("=") + 1,
                                                candidat.length
                                              )
                                            )
                                          }
                                          className="clickablePageFromClan"
                                        >
                                          {t("refuserClan")}
                                        </a>
                                      </td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                            </table>
                          </div>
                        )}
                        {clan.candidates.length == 0 && (
                          <div>{t("clan.encours.rien")}</div>
                        )}
                        <div className="errorResultFuzPrix">
                          {t("clan.reminder.multi")}
                        </div>
                      </div>
                    </div>
                  )}
                  <div
                    className="clickablePageFromClan"
                    onClick={(e) => {
                      quitClan();
                    }}
                  >
                    {t("clan.quit")}
                  </div>
                </div>
              )}
            </div>
          )}

          {sectionCurrentlyActive === 1 && isInAClan && (
            <div>
              <header className="pageCategoryHeader">
                {t("clan.writemsg")}
              </header>
              <p className="compteHeader">{t("clan.writemsg.text")}</p>
              <p className="compteHeader">
                <strong>{t("clan.writemsg.votre")}</strong>
              </p>
              <textarea
                className="messageBoxClan"
                value={messageToSend}
                onInput={(e) => setMessageToSend(e.currentTarget.value)}
                id="clanDescription"
              />
              <br />
              <br />
              <input
                type="submit"
                value={t("clan.writemsg.send") as string}
                className="sendMsgClanButton"
                onClick={(e) => sendMessage()}
              />
              <br />
              <br />
              <button
                className="hoverReturn"
                onClick={(e) => {
                  loadNewUISection(0);
                }}
              >
                <img alt="" src={getActionImageFromActionString("🡸")} />
              </button>
            </div>
          )}

          {sectionCurrentlyActive === 2 && isInAClan && (
            <div className="compteHeader">
              <header className="pageCategoryHeader">
                {t("clan.editpage")}
              </header>
              <p>
                <strong>{t("classement.clanName") + ": " + clan.name}</strong>
              </p>
              <p>
                <strong>{t("clan.namepage") + ": "}</strong>
              </p>
              <textarea
                className="simpleTextArea"
                onChange={(e) => setEditPageTitle(e.target.value)}
              >
                {actualSelectedPage.pageTitle}
              </textarea>
              <br />
              <br />
              <p>
                <strong>{t("clan.content") + ": "}</strong>
              </p>
              <textarea
                className="bigTextArea"
                onChange={(e) => setEditPageContent(e.target.value)}
              >
                {actualSelectedPage.rawContent}
              </textarea>
              <br />
              <input
                type="submit"
                value={t("clan.modifiy") as string}
                className="sendMsgClanButton"
                onClick={(e) => modifyClanPage()}
              />
              <br />
              <br />
              {Object.keys(clanPages).length > 1 && (
                <a
                  className="clickablePageFromClan"
                  onClick={(e) => deleteClanPage()}
                >
                  {t("clan.deletepage")}
                </a>
              )}
              {getHelpEditSection()}
            </div>
          )}

          {sectionCurrentlyActive === 3 && isInAClan && (
            <div className="compteHeader">
              <header className="pageCategoryHeader">
                {t("clan.addpage")}
              </header>
              <p>
                <strong>{t("classement.clanName") + ": " + clan.name}</strong>
              </p>
              <p>
                <strong>{t("clan.namepage") + ": "}</strong>
              </p>
              <textarea
                className="simpleTextArea"
                onChange={(e) => setCreatePageTitle(e.target.value)}
              >
                {createPageTitle}
              </textarea>
              <br />
              <br />
              <a>{t("clan.page.public")}</a>
              <input
                type="checkbox"
                onChange={(e) => togglePublicCheckBox()}
                checked={isPublic}
              />
              <br />
              <br />
              <p>
                <strong>{t("clan.content") + ": "}</strong>
              </p>
              <textarea
                className="bigTextArea"
                onChange={(e) => setCreatePageContent(e.target.value)}
              >
                {createPageContent}
              </textarea>
              <br />
              <input
                type="submit"
                value={t("create") as string}
                className="sendMsgClanButton"
                onClick={(e) => modifyClanPage()}
              />
              {getHelpEditSection()}
            </div>
          )}

          {sectionCurrentlyActive === 4 &&
            isInAClan &&
            hasAdminRightsOnClan && (
              <div>
                <header className="pageCategoryHeader">
                  {t("clan.editpage")}
                </header>
                <p className="compteHeader">{t("clan.rights.text") + ":"}</p>
                <p>
                  <input
                    type="checkbox"
                    onChange={(e) => toggleEditForAdminCheckBox()}
                    checked={userToEditHasAdminRightsOnClan}
                  />
                  <a className="compteHeader">{t("clan.rights.text2")}</a>
                </p>
                <br />
                <a className="compteHeader">{t("clan.member.edittitle")}</a>
                <div>
                  <input
                    className="inputClanName"
                    value={memberTitleEdit}
                    onInput={(e) => setMemberTitleEdit(e.currentTarget.value)}
                    type="text"
                    id="clanName"
                    maxLength={40}
                  />
                  <input
                    type="submit"
                    value={t("clan.editpage") as string}
                    className="createClanButton"
                    onClick={(e) => editMemberTitle()}
                  />
                </div>
                <button
                  className="hoverReturn"
                  onClick={(e) => {
                    setRefreshComponent(!refreshComponent);
                  }}
                >
                  <img alt="" src={getActionImageFromActionString("🡸")} />
                </button>
              </div>
            )}
        </>
      )}
    </div>
  );
}
