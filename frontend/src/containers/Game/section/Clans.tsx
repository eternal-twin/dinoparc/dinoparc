import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import Store from "../../../utils/Store";
import axios from "axios";
import { apiUrl } from "../../../index";
import getActionImageFromActionString from "../../utils/ActionImageFromString";
import { Clan } from "../../../types/clan";
import PublicClan from "../shared/PublicClan";
import MyClan from "./MyClan";
import urlJoin from "url-join";

type Props = {
  openPlayerProfile: (playerId: string) => void;
  /** @deprecated */
  refreshCash: () => void;
  /** @deprecated */
  refreshPage: boolean;
};

export default function Clans(props: Props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [isInAClan, setIsInAClan] = useState(false);
  const [justCreatedAClan, setJustCreatedAClan] = useState(false);
  const [clanName, setClanName] = useState("null");
  const [createClanMode, setCreateClanMode] = useState(false);
  const [clanNameForForm, setClanNameForForm] = useState("");
  const [clanDescriptionForForm, setClanDescriptionForForm] = useState("");
  const [errorCreatingClan, setErrorCreatingClan] = useState(false);
  const [latestClansCreated, setLatestClansCreated] = useState([]);
  const [renderPublicClanMode, setRenderPublicClanMode] = useState(false);
  const [loadedClanIsOwnClan, setLoadedClanIsOwnClan] = useState(false);
  let [publicClanVisited, setPublicClanVisited] = useState<Partial<Clan>>({});

  useEffect(() => {
    setRenderPublicClanMode(false);
    setLoadedClanIsOwnClan(false);
    window.scrollTo(0, 0);
    axios
      .get(urlJoin(apiUrl, "clans", "isInAClan", store.getAccountId()))
      .then(({ data }) => {
        if (data.inAClan) {
          setIsInAClan(true);
          setClanName(data.clanName);
        }
        setIsLoading(false);
      });

    axios
      .get(urlJoin(apiUrl, "clans", "displayLatests", store.getAccountId()))
      .then(({ data }) => {
        setLatestClansCreated(data);
        setIsLoading(false);
      });
  }, [props.refreshPage]);

  function toggleMode() {
    setCreateClanMode(true);
  }

  function createClan() {
    setIsLoading(true);

    const store = Store.getInstance();
    axios
      .post(urlJoin(apiUrl, "clans", "createClan"), {
        accountId: store.getAccountId(),
        clanName: clanNameForForm,
        clanDescription: clanDescriptionForForm,
      })
      .then(({ data }) => {
        setIsLoading(false);
        if (data) {
          setJustCreatedAClan(true);
          props.refreshCash();
        } else {
          setErrorCreatingClan(true);
        }
      });
  }

  function returnToPrincipalClanMenu() {
    setCreateClanMode(false);
    setErrorCreatingClan(false);
    setJustCreatedAClan(false);
  }

  function visitPublicClanPage(clanId) {
    setRenderPublicClanMode(true);
    setIsLoading(true);

    axios
      .get(urlJoin(apiUrl, "clans", "public", clanId, store.getAccountId()))
      .then(({ data }) => {
        publicClanVisited = data;
        setPublicClanVisited(publicClanVisited);
        if (publicClanVisited.members.includes(store.getAccountId())) {
          setLoadedClanIsOwnClan(true);
        }
        setIsLoading(false);
      });
  }

  return (
    <div>
      {!renderPublicClanMode && (
        <div>
          <header className="pageCategoryHeader">{t("clansLabel")}</header>
          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && (
            <>
              {!isInAClan && (
                <>
                  <div className="newsPopup">{t("noClan")}</div>
                  {createClanMode === false && (
                    <button
                      id="btnGive"
                      className="buttonCreateClan"
                      onClick={(e) => toggleMode()}
                    >
                      <img
                        alt=""
                        src={getActionImageFromActionString("PosteMissions")}
                      />
                      <span className="verticalAlignCenter">
                        {t("createClan")}
                      </span>
                      <img
                        alt=""
                        src={getActionImageFromActionString("PosteMissions")}
                      />
                    </button>
                  )}
                  {createClanMode && !justCreatedAClan && (
                    <div className="helpSection">
                      <header className="pageCategoryHeader">
                        {t("createClan")}
                      </header>
                      <p className="helpText">{t("createClanText")}</p>
                      <label>Nom du clan : </label>
                      <input
                        className="inputClanName"
                        value={clanNameForForm}
                        onInput={(e) =>
                          setClanNameForForm(e.currentTarget.value)
                        }
                        type="text"
                        id="clanName"
                        maxLength={40}
                      />
                      <br />
                      <br />
                      <label>Description : </label>
                      <textarea
                        className="inputClanDesc"
                        value={clanDescriptionForForm}
                        onInput={(e) =>
                          setClanDescriptionForForm(e.currentTarget.value)
                        }
                        id="clanDescription"
                      />
                      <br />
                      <br />
                      <input
                        type="submit"
                        value={t("create") as string}
                        className="createClanButton"
                        onClick={(e) => createClan()}
                      />
                      {errorCreatingClan && (
                        <div className="errorResultFuzPrix">
                          {t("errorCreateClan")}
                        </div>
                      )}
                      <br />
                      <br />
                      <button
                        className="hoverReturn"
                        onClick={(e) => {
                          returnToPrincipalClanMenu();
                        }}
                      >
                        <img alt="" src={getActionImageFromActionString("🡸")} />
                      </button>
                    </div>
                  )}
                  {createClanMode && justCreatedAClan && (
                    <div className="helpSection">
                      <header className="pageCategoryHeader">
                        {t("createClan")}
                      </header>
                      <p className="helpText">{t("createClanText")}</p>
                      <div className="newsPopup">{t("createClanSuccess")}</div>
                    </div>
                  )}
                </>
              )}
              {isInAClan && (
                <div>
                  <div className="newsPopup">
                    {t("haveClan")}
                    {clanName}
                    {"."}
                  </div>
                </div>
              )}

              <header className="pageCategoryHeader">
                {t("clan.recently")}
              </header>
              <div className="clanSection">
                <table className="recentClansTable">
                  <tbody>
                    <tr>
                      <th className="clanNameCl">{t("evo.name")}</th>
                      <th>{t("clan.creator")}</th>
                      <th>{t("clan.nbPlayers")}</th>
                      <th>{t("creationDateClan")}</th>
                    </tr>
                    {latestClansCreated.map((clanDto, i) => {
                      return (
                        <tr>
                          <td
                            onClick={(e) => visitPublicClanPage(clanDto.clanId)}
                            className="clickablePageFromClan"
                          >
                            {clanDto.clanName}
                          </td>
                          <td>{clanDto.creatorName}</td>
                          <td>{clanDto.nbPlayers}</td>
                          <td>{clanDto.creationDate}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </>
          )}
        </div>
      )}

      {renderPublicClanMode && (
        <div>
          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && !loadedClanIsOwnClan && (
            <PublicClan
              openPlayerProfile={props.openPlayerProfile}
              publicClanData={publicClanVisited}
              isInAClan={isInAClan}
              refreshMoneyAfterApplication={props.refreshCash}
            />
          )}
          {isLoading === false && loadedClanIsOwnClan && (
            <MyClan openPlayerProfile={props.openPlayerProfile} />
          )}
        </div>
      )}
    </div>
  );
}
