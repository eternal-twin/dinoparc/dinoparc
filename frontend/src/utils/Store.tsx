import { ActionMode } from "../types/account";
import { ImageMode } from "../types/account";

/** @deprecated Use useUserData.ts instead */
export default class Store {
  static myInstance: null | Store = null;

  _username = "";
  _accountId = "";
  _imgMode: ImageMode = "RUFFLE";
  _actionMode: ActionMode = "STANDARD";
  _selectedDinoz = "";
  _isAuthenticated = false;
  _totalPrice = 0;
  _selectedUserId = "";
  _token = "";
  _ongoingFightId = 0;

  /**
   * @returns {Store}
   * This acts as a Singleton to store some values.
   */
  static getInstance() {
    if (Store.myInstance == null) {
      Store.myInstance = new Store();
    }

    return this.myInstance;
  }

  getUsername() {
    return this._username;
  }

  setUsername(name) {
    this._username = name;
  }

  getAccountId() {
    return this._accountId;
  }

  setAccountId(id) {
    this._accountId = id;
  }

  getImgMode() {
    return this._imgMode;
  }

  setImgMode(imgMode: ImageMode) {
    this._imgMode = imgMode;
  }

  getActionMode() {
    return this._actionMode;
  }

  setActionMode(actionMode: ActionMode) {
    this._actionMode = actionMode;
  }

  getSelectedDinoz() {
    return this._selectedDinoz;
  }

  setSelectedDinoz(id) {
    this._selectedDinoz = id;
  }

  isUserAuthenticated() {
    return this._isAuthenticated;
  }

  setUserAuthenticated(flag) {
    this._isAuthenticated = flag;
  }

  getTotalPrice() {
    return this._totalPrice;
  }

  setTotalPrice(price) {
    this._totalPrice = price;
  }

  getToken() {
    return this._token;
  }

  setToken(token) {
    this._token = token;
  }

  getOngoingFightId() {
    return this._ongoingFightId;
  }

  setOngoingFightId(fightId) {
    this._ongoingFightId = fightId;
  }
}
