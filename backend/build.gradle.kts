import org.jooq.meta.jaxb.Logging
import org.springframework.boot.gradle.plugin.SpringBootPlugin
import org.yaml.snakeyaml.Yaml
import java.io.InputStream
import java.io.IOException

buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath("org.yaml:snakeyaml:2.2")
    classpath("org.flywaydb:flyway-database-postgresql:10.7.2")
  }
}

plugins {
  kotlin("jvm") version "1.9.24"
  id("nu.studer.jooq") version "9.0"
  id("org.flywaydb.flyway") version "10.7.2"
  id("org.springframework.boot") version "3.1.11"
  java
  application
}

group = "com.jonathan.dinoparc"
version = "0.1.0"

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

kotlin {
  target {
    compilations.all {
      kotlinOptions {
        jvmTarget = "17"
      }
    }
  }
}

tasks.withType<JavaCompile> {
  options.encoding = "UTF-8"
  options.compilerArgs.addAll(listOf("-Xlint:deprecation", "-parameters"))
}

repositories {
  mavenCentral()
  // https://gitlab.com/eternaltwin/eternaltwin/-/packages
  maven(url = "https://gitlab.com/api/v4/projects/17810311/packages/maven")
  // https://gitlab.com/eternaltwin/oauth/-/packages
  maven(url = "https://gitlab.com/api/v4/projects/23125194/packages/maven")
}

dependencies {
  implementation(platform(SpringBootPlugin.BOM_COORDINATES))
  implementation(platform("io.opentelemetry.instrumentation:opentelemetry-instrumentation-bom:2.6.0"))
  implementation("io.opentelemetry.instrumentation:opentelemetry-spring-boot-starter")
  implementation("io.opentelemetry:opentelemetry-api")
  implementation("io.opentelemetry:opentelemetry-sdk")
  implementation("org.apache.commons:commons-lang3:3.12.0")
  implementation("org.apache.commons:commons-collections4:4.4")
  implementation("org.apache.commons:commons-csv:1.8")
  implementation("org.bouncycastle:bcprov-jdk15on:1.69")
  implementation("org.postgresql:postgresql:42.7.3")
  implementation("org.springframework:spring-jdbc:6.1.6")
  implementation("org.springframework.boot:spring-boot-starter-jdbc:3.1.11")
  implementation("org.springframework.boot:spring-boot-starter-web:3.1.11")
  implementation("org.springframework.boot:spring-boot-starter-security:3.1.11")
  implementation("org.springframework.security:spring-security-core:6.2.4")
  implementation("org.springframework.security:spring-security-web:6.2.4")
  implementation("org.springframework.security:spring-security-config:6.2.4")
  implementation("org.flywaydb:flyway-core:10.7.2")
  implementation("org.flywaydb:flyway-database-postgresql:10.7.2")
  implementation("com.discord4j:discord4j-core:3.2.5")
  implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.5.0")
  implementation("org.jsoup:jsoup:1.17.2")
  implementation("net.eternaltwin:oauth-client:0.1.7")
  implementation("net.eternaltwin:etwin:0.7.0")
  implementation("com.squareup.okhttp3:okhttp:4.9.1")
  implementation("com.auth0:java-jwt:3.18.3")
  testImplementation("org.springframework.boot:spring-boot-starter-test:3.1.5")
  testImplementation("io.zonky.test:embedded-database-spring-test:2.5.1")
  jooqGenerator("org.postgresql:postgresql:42.7.3")
}

tasks.test {
  // Use the built-in JUnit support of Gradle.
  useJUnitPlatform()
}

// todo: reuse spring data config loader instead of badly reimplementing it
fun readConfig(): Map<String, Any?> {
  val script: File = project.buildscript.sourceFile ?: throw Error("FailedToResolveBuildScriptFile");
  val backendDir: File = script.parentFile;
  val resourcesDir = File(File(File(backendDir, "src"), "main"), "resources");
  val defaultConfigFile = File(resourcesDir, "application.yml");
  val localConfigFile = File(backendDir, "application.yml");

  val defaultConfig = parseConfig(defaultConfigFile.inputStream())
  val localConfig = try {
    parseConfig(localConfigFile.inputStream())
  } catch (e: IOException) {
    mapOf()
  }

  return defaultConfig + localConfig;
}

fun parseConfig(input: InputStream): Map<String, Any?> {
  val yaml = Yaml();
  val doc: Any = yaml.load(input)

  val flat = mutableMapOf<String, Any?>();
  flattenConfig(flat, "", doc);
  return flat
}

fun flattenConfig(flat: MutableMap<String, Any?>, path: String, value: Any?) {
  // https://bitbucket.org/snakeyaml/snakeyaml/wiki/Documentation#markdown-header-yaml-tags-and-java-types
  when (value) {
    is Map<*, *> -> {
      for (entry in value) {
        val k = entry.key as String
        val v = entry.value
        val newPath: String = if (path.isEmpty()) { k } else { "$path.$k" };
        flattenConfig(flat, newPath, v)
      }
    }
    else -> {
      assert(path.isNotEmpty())
      flat[path] = value
    }
  }
}

val config = readConfig();
val isDev = (config["server.backend"] as String).contains("localhost")

jooq {
  // jooq version to use
  // see <https://mvnrepository.com/artifact/org.jooq/jooq>
  version.set("3.19.8")
  edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)

  configurations {
    create("main") {  // name of the jOOQ configuration
      generateSchemaSourceOnCompilation.set(true)  // default (can be omitted)

      jooqConfiguration.apply {
        logging = Logging.WARN
        jdbc.apply {
          driver = "org.postgresql.Driver"
          url = config["spring.datasource.url"] as String
          user = config["spring.datasource.username"] as String
          password = config["spring.datasource.password"] as String
        }
        generator.apply {
          name = "org.jooq.codegen.DefaultGenerator"
          database.apply {
            name = "org.jooq.meta.postgres.PostgresDatabase"
            inputSchema = "public"
          }
          generate.apply {
            isDeprecated = false
            isRecords = true
            isImmutablePojos = true
            isFluentSetters = true
          }
          target.apply {
            packageName = "com.dinoparc"
            directory = "build/generated-src/jooq/main"  // default (can be omitted)
          }
          strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
        }
      }
    }
  }
}

flyway {
  url = config["spring.datasource.url"] as String
  user = config["spring.datasource.username"] as String
  password = config["spring.datasource.password"] as String
  locations = arrayOf("filesystem:src/main/resources/db/migration")
  // Disable `clean` outside of the dev environment (prevent errors when deployed)
  cleanDisabled = !isDev
}

tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq") {
  allInputsDeclared.set(true)
}

springBoot {
  buildInfo {
  }
}

application {
  mainClass.set("com.dinoparc.api.DinoparcBackendServer")
}
