package com.dinoparc.api.schedulers;

import com.dinoparc.api.repository.PgClientInformationBisRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CleanClientInformationsBisScheduler {

    private PgClientInformationBisRepository clientInformationBisRepository;

    public CleanClientInformationsBisScheduler(PgClientInformationBisRepository clientInformationBisRepository) {
        this.clientInformationBisRepository = clientInformationBisRepository;
    }

    @Scheduled(cron = "0 30 * * * ?", zone = "Europe/Paris")
    public void cleanClientInformationsBis() {
        clientInformationBisRepository.cleanUp();
    }
}
