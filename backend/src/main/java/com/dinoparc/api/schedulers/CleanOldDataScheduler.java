package com.dinoparc.api.schedulers;

import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.repository.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CleanOldDataScheduler {
    private PgClientInformationRepository clientInformationRepository;

    private PgHistoryRepository historyRepository;

    private PgPlayerMissionRepository playerMissionRepository;

    private PgPlayerRepository playerRepository;

    private PgPlayerStatRepository playerStatRepository;

    public CleanOldDataScheduler(PgClientInformationRepository clientInformationRepository,
                                 PgHistoryRepository historyRepository,
                                 PgPlayerMissionRepository playerMissionRepository,
                                 PgPlayerRepository playerRepository,
                                 PgPlayerStatRepository playerStatRepository) {
        this.clientInformationRepository = clientInformationRepository;
        this.historyRepository = historyRepository;
        this.playerMissionRepository = playerMissionRepository;
        this.playerRepository = playerRepository;
        this.playerStatRepository = playerStatRepository;
    }

    @Scheduled(cron = "0 30 4 * * ?", zone = "Europe/Paris")
    public void cleanOldData() {
        // Clean old client information entries
        clientInformationRepository.cleanData();

        // Clean inactive players data : history, missions, dinoz stats
        for (Player p : playerRepository.findAll()) {
            if (!p.activeDuringLast7Days()) {
                // Player did not play during last 7 days : remove its history
                historyRepository.cleanPlayerHistory(p.getId());
            }
            if (!p.activeDuringLast30Days()) {
                // Player did not play during last 30 days, remove its missions
                // And remove its dinozs stats (keep only player global stats)
                playerStatRepository.deleteDinozStatsByPlayerId(p.getId());
                playerMissionRepository.deletePlayerMission(UUID.fromString(p.getId()));
            }
        }
    }
}
