package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Player implements Comparable<Player> {
    private String id;
    private String name;
    private Integer cash;
    private List<Integer> unlockedDinoz;
    private Integer nbPoints;
    private Integer averageLevelPoints;
    private boolean messages;
    private HashMap<String, String> blackList;
    private Long lastLogin;
    private Integer sacrificePoints;
    private Integer dailyShiniesFought;
    @JsonIgnore
    private String role;
    private Integer hermitStage;
    private Integer hermitStageCurrentWins;
    private String hermitStageFightingDinoz;
    private Integer accountDinozLimit;
    private Integer wistitiCaptured;
    private Integer eventCounter;
    private PlayerImgModeEnum imgMode;
    private Boolean helpToggle;
    private Boolean hasClaimedTradeFromFishermanToday;
    private PlayerActionModeEnum actionMode;

    private List<PlayerDinozStore> dinozIsList;

    @JsonIgnore
    private boolean banned;

    @JsonIgnore
    private boolean blocked;

    @JsonIgnore
    private boolean bannedPvp;
    @JsonIgnore
    private boolean bannedRaid;
    @JsonIgnore
    private boolean bannedShiny;
    @JsonIgnore
    private boolean bannedWistiti;
    @JsonIgnore
    private boolean bannedBazar;
    @JsonIgnore
    private boolean historySkip;

    @JsonIgnore
    private Integer hunterGroup;

    @JsonIgnore
    OffsetDateTime lastWistitiFight;
    @JsonIgnore
    OffsetDateTime lastGeneratedStore;
    @JsonIgnore
    String lastPirateBuy;

    public Player() {
        dinozIsList = new ArrayList<>();
        blackList = new HashMap<String, String>();
        this.accountDinozLimit = 300;

        this.unlockedDinoz = new ArrayList<Integer>();
        this.unlockedDinoz.addAll(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 16));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCash() {
        return cash;
    }

    public void setCash(Integer cash) {
        this.cash = cash;
    }

    public List<Integer> getUnlockedDinoz() {
        return unlockedDinoz;
    }

    public void setUnlockedDinoz(List<Integer> unlockedDinoz) {
        this.unlockedDinoz = unlockedDinoz;
    }

    public Integer getNbPoints() {
        return nbPoints;
    }

    public void setNbPoints(Integer nbPoints) {
        this.nbPoints = nbPoints;
    }

    public Integer getAverageLevelPoints() {
        return averageLevelPoints;
    }

    public void setAverageLevelPoints(Integer averageLevelPoints) {
        this.averageLevelPoints = averageLevelPoints;
    }

    public Long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getSacrificePoints() {
        return sacrificePoints;
    }

    public void setSacrificePoints(Integer sacrificePoints) {
        this.sacrificePoints = sacrificePoints;
    }

    public Integer getDailyShiniesFought() {
        return dailyShiniesFought;
    }

    public void setDailyShiniesFought(Integer dailyShiniesFought) {
        this.dailyShiniesFought = dailyShiniesFought;
    }

    public boolean isMessages() {
        return messages;
    }

    public void setMessages(boolean messages) {
        this.messages = messages;
    }

    public HashMap<String, String> getBlackList() {
        return blackList;
    }

    public void setBlackList(HashMap<String, String> blackList) {
        this.blackList = blackList;
    }

    public Integer getHunterGroup() {
        return hunterGroup;
    }

    public void setHunterGroup(Integer hunterGroup) {
        this.hunterGroup = hunterGroup;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getHermitStage() {
        return hermitStage;
    }

    public void setHermitStage(Integer hermitStage) {
        this.hermitStage = hermitStage;
    }

    public Integer getHermitStageCurrentWins() {
        return hermitStageCurrentWins;
    }

    public void setHermitStageCurrentWins(Integer hermitStageCurrentWins) {
        this.hermitStageCurrentWins = hermitStageCurrentWins;
    }

    public String getHermitStageFightingDinoz() {
        return hermitStageFightingDinoz;
    }

    public void setHermitStageFightingDinoz(String hermitStageFightingDinoz) {
        this.hermitStageFightingDinoz = hermitStageFightingDinoz;
    }

    public Integer getAccountDinozLimit() {
        return accountDinozLimit;
    }

    public void setAccountDinozLimit(Integer accountDinozLimit) {
        this.accountDinozLimit = accountDinozLimit;
    }

    public Integer getWistitiCaptured() {
        return wistitiCaptured;
    }

    public void setWistitiCaptured(Integer wistitiCaptured) {
        this.wistitiCaptured = wistitiCaptured;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isBannedPvp() {
        return bannedPvp;
    }

    public void setBannedPvp(boolean bannedPvp) {
        this.bannedPvp = bannedPvp;
    }

    public boolean isBannedRaid() {
        return bannedRaid;
    }

    public void setBannedRaid(boolean bannedRaid) {
        this.bannedRaid = bannedRaid;
    }

    public boolean isBannedShiny() {
        return bannedShiny;
    }

    public void setBannedShiny(boolean bannedShiny) {
        this.bannedShiny = bannedShiny;
    }

    public boolean isBannedWistiti() {
        return bannedWistiti;
    }

    public void setBannedWistiti(boolean bannedWistiti) {
        this.bannedWistiti = bannedWistiti;
    }

    public boolean isBannedBazar() {
        return bannedBazar;
    }

    public void setBannedBazar(boolean bannedBazar) {
        this.bannedBazar = bannedBazar;
    }

    public OffsetDateTime getLastWistitiFight() {
        return lastWistitiFight;
    }

    public void setLastWistitiFight(OffsetDateTime lastWistitiFight) {
        this.lastWistitiFight = lastWistitiFight;
    }

    public OffsetDateTime getLastGeneratedStore() {
        return lastGeneratedStore;
    }

    public void setLastGeneratedStore(OffsetDateTime lastGeneratedStore) {
        this.lastGeneratedStore = lastGeneratedStore;
    }

    public String getLastPirateBuy() {
        return lastPirateBuy;
    }

    public void setLastPirateBuy(String lastPirateBuy) {
        this.lastPirateBuy = lastPirateBuy;
    }

    public List<PlayerDinozStore> getDinozIsList() {
        return dinozIsList;
    }

    public void setDinozIsList(List<PlayerDinozStore> dinozIsList) {
        this.dinozIsList = dinozIsList;
    }

    public Integer getEventCounter() {
        return eventCounter;
    }

    public void setEventCounter(Integer eventCounter) {
        this.eventCounter = eventCounter;
    }

    public PlayerImgModeEnum getImgMode() {
        return imgMode;
    }

    public void setImgMode(PlayerImgModeEnum imgMode) {
        this.imgMode = imgMode;
    }

    public PlayerActionModeEnum getActionMode() {
        return actionMode;
    }

    public void setActionMode(PlayerActionModeEnum actionMode) {
        this.actionMode = actionMode;
    }

    public boolean isHistorySkip() {
        return historySkip;
    }

    public void setHistorySkip(boolean historySkip) {
        this.historySkip = historySkip;
    }

    public Boolean getHelpToggle() {
        return helpToggle;
    }

    public void setHelpToggle(Boolean helpToggle) {
        this.helpToggle = helpToggle;
    }

    public Boolean getHasClaimedTradeFromFishermanToday() {
        return hasClaimedTradeFromFishermanToday;
    }

    public void setHasClaimedTradeFromFishermanToday(Boolean hasClaimedTradeFromFishermanToday) {
        this.hasClaimedTradeFromFishermanToday = hasClaimedTradeFromFishermanToday;
    }

    @Override
    public int compareTo(Player other) {
        if (this.getDailyShiniesFought() < other.getDailyShiniesFought()) {
            return 1;
        } else if (this.getDailyShiniesFought() == other.getDailyShiniesFought()) {
            return 0;
        } else {
            return -1;
        }
    }

    public boolean activeDuringLast7Days() {
        return (ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond() - lastLogin) < 604800;
    }

    public boolean activeDuringLast30Days() {
        return (ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond() - lastLogin) < 2592000;
    }
}
