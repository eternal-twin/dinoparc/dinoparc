package com.dinoparc.api.domain.clan;

public class PlayerClan {

    private String playerId;
    private String playerName;
    private String clanId;
    private String clanName;
    private String role;
    private String title;
    private Boolean isActiveWarrior;
    private Integer nbPointsOccupation;
    private Integer nbPointsFights;
    private Integer nbPointsTotem;

    public PlayerClan() {

    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public String getClanName() {
        return clanName;
    }

    public void setClanName(String clanName) {
        this.clanName = clanName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getActiveWarrior() {
        return isActiveWarrior;
    }

    public void setActiveWarrior(Boolean activeWarrior) {
        isActiveWarrior = activeWarrior;
    }

    public Integer getNbPointsOccupation() {
        return nbPointsOccupation;
    }

    public void setNbPointsOccupation(Integer nbPointsOccupation) {
        this.nbPointsOccupation = nbPointsOccupation;
    }

    public Integer getNbPointsFights() {
        return nbPointsFights;
    }

    public void setNbPointsFights(Integer nbPointsFights) {
        this.nbPointsFights = nbPointsFights;
    }

    public Integer getNbPointsTotem() {
        return nbPointsTotem;
    }

    public void setNbPointsTotem(Integer nbPointsTotem) {
        this.nbPointsTotem = nbPointsTotem;
    }
}
