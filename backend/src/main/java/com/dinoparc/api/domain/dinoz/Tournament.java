package com.dinoparc.api.domain.dinoz;

import java.util.ArrayList;
import java.util.List;
import com.dinoparc.api.domain.account.DinoparcConstants;

public class Tournament {

  private boolean hasAccess;
  private int actualPoints;
  private int position;
  private int nbInscrits;
  private int currentWins;
  private int currentLosses;
  private int totalWins;
  private int totalLosses;
  private int record;
  private List<TournamentRanking> rankings;

  public Tournament() {
    this.hasAccess = true;
    this.actualPoints = 0;
    this.position = 0;
    this.nbInscrits = 0;
    this.currentWins = 0;
    this.currentLosses = 0;
    this.totalLosses = 0;
    this.totalWins = 0;
    this.record = 0;
    this.rankings = new ArrayList<TournamentRanking>();
  }

  public Tournament(int actualPoints, int position, int nbInscrits, int currentWins,
      int currentLosses, int totalWins, int totalLosses, int record) {
    this.actualPoints = actualPoints;
    this.position = position;
    this.nbInscrits = nbInscrits;
    this.currentWins = currentWins;
    this.currentLosses = currentLosses;
    this.totalWins = totalWins;
    this.totalLosses = totalLosses;
    this.record = record;
  }

  public boolean isHasAccess() {
    return hasAccess;
  }

  public void setHasAccess(boolean hasAccess) {
    this.hasAccess = hasAccess;
  }

  public int getActualPoints() {
    return actualPoints;
  }

  public void setActualPoints(int actualPoints) {
    this.actualPoints = actualPoints;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public int getNbInscrits() {
    return nbInscrits;
  }

  public void setNbInscrits(int nbInscrits) {
    this.nbInscrits = nbInscrits;
  }

  public int getCurrentWins() {
    return currentWins;
  }

  public void setCurrentWins(int currentWins) {
    this.currentWins = currentWins;
  }

  public int getCurrentLosses() {
    return currentLosses;
  }

  public void setCurrentLosses(int currentLosses) {
    this.currentLosses = currentLosses;
  }

  public int getTotalWins() {
    return totalWins;
  }

  public void setTotalWins(int totalWins) {
    this.totalWins = totalWins;
  }

  public int getTotalLosses() {
    return totalLosses;
  }

  public void setTotalLosses(int totalLosses) {
    this.totalLosses = totalLosses;
  }

  public int getRecord() {
    return record;
  }

  public void setRecord(int record) {
    this.record = record;
  }

  public List<TournamentRanking> getRankings() {
    return rankings;
  }

  public void setRankings(List<TournamentRanking> rankings) {
    this.rankings = rankings;
  }

  public static String getTournamentByPlaceNumber(Integer placeNumber) {

    if (placeNumber == 0) {
      return DinoparcConstants.TournoiDinoville;

    } else if (placeNumber == 3) {
      return DinoparcConstants.TournoiDinoplage;

    } else if (placeNumber == 6) {
      return DinoparcConstants.TournoiMontDino;

    } else if (placeNumber == 10) {
      return DinoparcConstants.TournoiTemple;

    } else if (placeNumber == 13) {
      return DinoparcConstants.TournoiRuines;
    }

    return null;
  }

}
