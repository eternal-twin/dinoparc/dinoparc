package com.dinoparc.api.domain.clan;

import java.util.HashMap;
import java.util.Map;

public class Totem {

    private Integer totemNumber;
    private Map<String, Integer> totemRewards;
    private Boolean full = false;

    private Integer currentBuze;
    private Integer currentPlumes;
    private Integer currentDents;
    private Integer currentCrins;
    private Integer currentBois;
    private Integer totalBuzeNeeded;
    private Integer totalPlumesNeeded;
    private Integer totalDentsNeeded;
    private Integer totalCrinsNeeded;
    private Integer totalBoisNeeded;

    public Totem() {
        this.full = false;
        this.totemRewards = new HashMap<>();
        this.currentBuze = 0;
        this.currentPlumes = 0;
        this.currentDents = 0;
        this.currentCrins = 0;
        this.currentBois = 0;
    }

    public Totem(Integer totemNumber, Map<String, Integer> totemRewards, Integer totalBuzeNeeded, Integer totalPlumesNeeded, Integer totalDentsNeeded, Integer totalCrinsNeeded, Integer totalBoisNeeded) {
        this();
        this.totemNumber = totemNumber;
        this.totalBuzeNeeded = totalBuzeNeeded;
        this.totalPlumesNeeded = totalPlumesNeeded;
        this.totalDentsNeeded = totalDentsNeeded;
        this.totalCrinsNeeded = totalCrinsNeeded;
        this.totalBoisNeeded = totalBoisNeeded;
        this.totemRewards = totemRewards;
    }

    public Integer getTotemNumber() {
        return totemNumber;
    }

    public void setTotemNumber(Integer totemNumber) {
        this.totemNumber = totemNumber;
    }

    public Integer getCurrentBuze() {
        return currentBuze;
    }

    public void setCurrentBuze(Integer currentBuze) {
        this.currentBuze = currentBuze;
    }

    public Integer getCurrentPlumes() {
        return currentPlumes;
    }

    public void setCurrentPlumes(Integer currentPlumes) {
        this.currentPlumes = currentPlumes;
    }

    public Integer getCurrentDents() {
        return currentDents;
    }

    public void setCurrentDents(Integer currentDents) {
        this.currentDents = currentDents;
    }

    public Integer getCurrentCrins() {
        return currentCrins;
    }

    public void setCurrentCrins(Integer currentCrins) {
        this.currentCrins = currentCrins;
    }

    public Integer getCurrentBois() {
        return currentBois;
    }

    public void setCurrentBois(Integer currentBois) {
        this.currentBois = currentBois;
    }

    public Integer getTotalBuzeNeeded() {
        return totalBuzeNeeded;
    }

    public void setTotalBuzeNeeded(Integer totalBuzeNeeded) {
        this.totalBuzeNeeded = totalBuzeNeeded;
    }

    public Integer getTotalPlumesNeeded() {
        return totalPlumesNeeded;
    }

    public void setTotalPlumesNeeded(Integer totalPlumesNeeded) {
        this.totalPlumesNeeded = totalPlumesNeeded;
    }

    public Integer getTotalDentsNeeded() {
        return totalDentsNeeded;
    }

    public void setTotalDentsNeeded(Integer totalDentsNeeded) {
        this.totalDentsNeeded = totalDentsNeeded;
    }

    public Integer getTotalCrinsNeeded() {
        return totalCrinsNeeded;
    }

    public void setTotalCrinsNeeded(Integer totalCrinsNeeded) {
        this.totalCrinsNeeded = totalCrinsNeeded;
    }

    public Integer getTotalBoisNeeded() {
        return totalBoisNeeded;
    }

    public void setTotalBoisNeeded(Integer totalBoisNeeded) {
        this.totalBoisNeeded = totalBoisNeeded;
    }

    public Map<String, Integer> getTotemRewards() {
        return totemRewards;
    }

    public void setTotemRewards(Map<String, Integer> totemRewards) {
        this.totemRewards = totemRewards;
    }

    public Boolean getFull() {
        return full;
    }

    public void setFull(Boolean full) {
        this.full = full;
    }
}
