package com.dinoparc.api.domain.account;

public class GameMasterAction {
    private String sourceAccountId;
    private String targetAccountId;
    private String actionType;
    private String actionDetail;
    private String result;
    private Boolean success;
    private Boolean forbidden;
    private String numericalDate;

    public GameMasterAction() {
    }

    public GameMasterAction(String sourceAccountId, String targetAccountId, String actionType, String actionDetail, String result, Boolean success, Boolean forbidden, String numericalDate) {
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.actionType = actionType;
        this.actionDetail = actionDetail;
        this.result = result;
        this.success = success;
        this.forbidden = forbidden;
        this.numericalDate = numericalDate;
    }

    public String getSourceAccountId() {
        return sourceAccountId;
    }

    public void setSourceAccountId(String sourceAccountId) {
        this.sourceAccountId = sourceAccountId;
    }

    public String getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(String targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionDetail() {
        return actionDetail;
    }

    public void setActionDetail(String actionDetail) {
        this.actionDetail = actionDetail;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean getForbidden() {
        return forbidden;
    }

    public void setForbidden(Boolean forbidden) {
        this.forbidden = forbidden;
    }

    public String getNumericalDate() {
        return numericalDate;
    }

    public void setNumericalDate(String numericalDate) {
        this.numericalDate = numericalDate;
    }
}
