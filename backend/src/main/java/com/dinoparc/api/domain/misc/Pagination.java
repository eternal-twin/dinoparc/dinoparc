package com.dinoparc.api.domain.misc;

import java.util.List;

public class Pagination<T> {
    public final static String OFFSET_HEADER = "X-Offset";
    public final static String TOTAL_COUNT_HEADER = "X-Total-Count";

    private List<T> elements;
    private Integer offset;
    private Integer totalCount;

    public Pagination() {
        super();
    }

    public Pagination(List<T> elements, Integer offset, Integer totalCount) {
        super();
        this.elements = elements;
        this.offset = offset;
        this.totalCount = totalCount;
    }

    public List<T> getElements() {
        return elements;
    }

    public void setElements(List<T> elements) {
        this.elements = elements;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
