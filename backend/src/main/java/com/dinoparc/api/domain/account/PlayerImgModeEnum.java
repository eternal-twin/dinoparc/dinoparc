package com.dinoparc.api.domain.account;

public enum PlayerImgModeEnum {
    RUFFLE,
    BASE_64,
    NONE,
    HYBRID
}
