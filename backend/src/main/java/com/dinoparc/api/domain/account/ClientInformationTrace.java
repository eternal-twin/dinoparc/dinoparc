package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;

public class ClientInformationTrace {
    private String playerId;
    private String dinozId;
    private Integer status;
    private String details;
    private OffsetDateTime retrievedDate;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public OffsetDateTime getRetrievedDate() {
        return retrievedDate;
    }

    public void setRetrievedDate(OffsetDateTime retrievedDate) {
        this.retrievedDate = retrievedDate;
    }
}
