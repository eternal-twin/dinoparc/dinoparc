package com.dinoparc.api.domain.account;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;

public class PlayerDinozStore {

    private UUID id;
    private UUID playerId;
    private String race;
    private String appearanceCode;
    private Map<String, Integer> initialCompList;
    private Map<String, Integer> initialElementsValues;
    private Integer price;
    private Integer rank;

    public PlayerDinozStore(UUID id, UUID playerId, int rank) {
        this.id = id;
        this.playerId = playerId;
        this.rank = rank;

        initialCompList = new HashMap<String, Integer>();
        initialCompList.put(DinoparcConstants.FORCE, 0);
        initialCompList.put(DinoparcConstants.ENDURANCE, 0);
        initialCompList.put(DinoparcConstants.PERCEPTION, 0);
        initialCompList.put(DinoparcConstants.INTELLIGENCE, 0);
        initialCompList.put(DinoparcConstants.AGILITÉ, 0);

        initialElementsValues = new HashMap<String, Integer>();
        initialElementsValues.put(DinoparcConstants.FEU, 0);
        initialElementsValues.put(DinoparcConstants.TERRE, 0);
        initialElementsValues.put(DinoparcConstants.EAU, 0);
        initialElementsValues.put(DinoparcConstants.FOUDRE, 0);
        initialElementsValues.put(DinoparcConstants.AIR, 0);
    }

    public PlayerDinozStore(String race, String appearanceCode, Map<String, Integer> initialCompList,
                        Map<String, Integer> initialElementsValues, Integer price) {
        this.id = UUID.randomUUID();
        this.race = race;
        this.appearanceCode = appearanceCode;
        this.initialCompList = initialCompList;
        this.initialElementsValues = initialElementsValues;
        this.price = price;
    }

    public PlayerDinozStore() {}

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getAppearanceCode() {
        return appearanceCode;
    }

    public void setAppearanceCode(String appearanceCode) {
        this.appearanceCode = appearanceCode;
    }

    public Map<String, Integer> getInitialCompList() {
        return initialCompList;
    }

    public void setInitialCompList(Map<String, Integer> initialCompList) {
        this.initialCompList = initialCompList;
    }

    public Map<String, Integer> getInitialElementsValues() {
        return initialElementsValues;
    }

    public void setInitialElementsValues(Map<String, Integer> initialElementsValues) {
        this.initialElementsValues = initialElementsValues;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public static PlayerDinozStore generateOneDinozStore(UUID accountId, List<Integer> unlockedDinoz, int rank) {
        PlayerDinozStore newBornDinoz = new PlayerDinozStore(UUID.randomUUID(), accountId, rank);

        // Distribution des % pour la boutique de Dinoz (% d'apparition, # de la race.)
        RandomCollection<Object> randomRace = new RandomCollection<Object>()
                .add(9 * (250), 0) // Moueffe
                .add(9 * (250), 1) // Picori
                .add(9 * (250), 2) // Castivore
                .add(9 * (250), 3) // Sirain
                .add(9 * (250), 4) // Winks
                .add(9 * (250), 5) // Gorilloz
                .add(9 * (250), 6) // Cargou
                .add(6 * (250), 7) // Hippoclamp
                .add(6 * (250), 8) // Rokky
                .add(9 * (250), 9) // Pigmou
                .add(7 * (250), 10) // Wanwan
                .add(9 * (250), 12) // Kump
                .add(6 * (250), 13) // Pteroz
                .add(1 * (250), 16) // Korgon
                .add(1 * (250), 17) // Kabuki
                .add(1, 19); // Soufflet

        int randomRaceNumber = (int) randomRace.next();
        if (unlockedDinoz.contains(randomRaceNumber)) {
            int[] randomCompetences = new Random().ints(1, 6).distinct().limit(3).toArray();
            Map<String, Integer> initialCompetences = new HashMap<String, Integer>();
            initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[0]), 3);
            initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[1]), 2);
            initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[2]), 1);
            newBornDinoz.setInitialCompList(initialCompetences);
            initializeDinoz(newBornDinoz, randomRaceNumber);
            return newBornDinoz;
        }
        return null;
    }

    public static PlayerDinozStore generateOneDinozSingleRaceStore(String race, UUID accountId, int rank) {
        PlayerDinozStore newBornDinoz = new PlayerDinozStore(UUID.randomUUID(), accountId, rank);

        HashMap<String, Integer> racesNumbers = new HashMap<>();
        racesNumbers.put("Moueffe", 0);
        racesNumbers.put("Picori", 1);
        racesNumbers.put("Castivore", 2);
        racesNumbers.put("Sirain", 3);
        racesNumbers.put("Winks", 4);
        racesNumbers.put("Gorilloz", 5);
        racesNumbers.put("Cargou", 6);
        racesNumbers.put("Hippoclamp", 7);
        racesNumbers.put("Rokky", 8);
        racesNumbers.put("Pigmou", 9);
        racesNumbers.put("Wanwan", 10);
        racesNumbers.put("Kump", 12);
        racesNumbers.put("Pteroz", 13);
        racesNumbers.put("Korgon", 16);

        int[] randomCompetences = new Random().ints(1, 6).distinct().limit(3).toArray();
        Map<String, Integer> initialCompetences = new HashMap<String, Integer>();
        initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[0]), 3);
        initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[1]), 2);
        initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[2]), 1);
        newBornDinoz.setInitialCompList(initialCompetences);
        initializeDinoz(newBornDinoz, racesNumbers.get(race));

        return newBornDinoz;
    }

    public static PlayerDinozStore generateOneDinozSingleColorStore(String color, UUID accountId, int rank, List<Integer> unlockedDinoz) {
        PlayerDinozStore dinoz = generateOneDinozStore(accountId, unlockedDinoz, rank);
        if (Objects.nonNull(dinoz)) {
            StringBuffer colorBuffer = new StringBuffer(dinoz.getAppearanceCode());
            colorBuffer.replace(9, 14, color + color + color + color + color);
            dinoz.setAppearanceCode(colorBuffer.toString());
            return dinoz;
        }
        return null;
    }

    public static PlayerDinozStore generateOneDinozStoreForMagikTicket(String color, UUID accountId, int rank) {
        PlayerDinozStore newBornDinoz = new PlayerDinozStore(UUID.randomUUID(), accountId, rank);

        // Distribution des % pour la boutique de Dinoz Magik (% d'apparition, # de la race.)
        RandomCollection<Object> randomRace = new RandomCollection<Object>()
                .add(25, 11) // Goupignon
                .add(60, 14) // Santaz
                .add(45, 18) // Serpantin
                .add(15, 19) // Soufflet
                .add(5, 20); // Feross

        int randomRaceNumber = (int) randomRace.next();
        int[] randomCompetences = new Random().ints(1, 6).distinct().limit(3).toArray();
        Map<String, Integer> initialCompetences = new HashMap<String, Integer>();
        if (randomRaceNumber == 19) {
            initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[0]), 3);
            initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[1]), 2);
            initialCompetences.put(getCompetenceNameFromNumber(randomCompetences[2]), 1);
        }
        newBornDinoz.setInitialCompList(initialCompetences);

        initializeDinoz(newBornDinoz, randomRaceNumber);

        if (Objects.nonNull(color)) {
            StringBuffer colorBuffer = new StringBuffer(newBornDinoz.getAppearanceCode());
            colorBuffer.replace(9, 14, color + color + color + color + color);
            newBornDinoz.setAppearanceCode(colorBuffer.toString());
        }

        return newBornDinoz;
    }

    private static String getCompetenceNameFromNumber(int competenceNumber) {
        switch (competenceNumber) {
            case 1:
                return DinoparcConstants.FORCE;
            case 2:
                return DinoparcConstants.ENDURANCE;
            case 3:
                return DinoparcConstants.PERCEPTION;
            case 4:
                return DinoparcConstants.INTELLIGENCE;
            case 5:
                return DinoparcConstants.AGILITÉ;
            default:
                return null;
        }
    }

    private static void initializeDinoz(PlayerDinozStore newBornDinoz, int randomRace) {
        var randomHexString = DinozUtils.getRandomHexString();

        switch (randomRace) {
            case 0:
                newBornDinoz.setRace(DinoparcConstants.MOUEFFE);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FEU, 2);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("0" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 1:
                newBornDinoz.setRace(DinoparcConstants.PICORI);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR, 2);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("1" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 2:
                newBornDinoz.setRace(DinoparcConstants.CASTIVORE);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR, 1);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 1);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("2" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 3:
                newBornDinoz.setRace(DinoparcConstants.SIRAIN);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 2);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("3" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 4:
                newBornDinoz.setRace(DinoparcConstants.WINKS);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 1);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FOUDRE, 1);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("4" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 5:
                newBornDinoz.setRace(DinoparcConstants.GORILLOZ);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 2);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("5" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 6:
                newBornDinoz.setRace(DinoparcConstants.CARGOU);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 1);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 1);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("6" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 7:
                newBornDinoz.setRace(DinoparcConstants.HIPPOCLAMP);
                for (int i = 1; i <= 5; i++) {
                    String randomDrawnElement = Dinoz.getRandomElement();
                    newBornDinoz.getInitialElementsValues().putIfAbsent(randomDrawnElement, 0);
                    newBornDinoz.getInitialElementsValues().replace(randomDrawnElement, newBornDinoz.getInitialElementsValues().get(randomDrawnElement) + 1);
                }
                newBornDinoz.setPrice(12000);
                newBornDinoz.setAppearanceCode("7" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 8:
                newBornDinoz.setRace(DinoparcConstants.ROKKY);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FOUDRE, 1);
                newBornDinoz.setPrice(10000);
                newBornDinoz.setAppearanceCode("8" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                newBornDinoz.getInitialCompList().put(DinoparcConstants.ROCK, 1);
                break;

            case 9:
                newBornDinoz.setRace(DinoparcConstants.PIGMOU);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FEU, 2);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("9" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 10:
                newBornDinoz.setRace(DinoparcConstants.WANWAN);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FOUDRE, 2);
                newBornDinoz.setPrice(9000);
                newBornDinoz.setAppearanceCode("A" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 11:
                newBornDinoz.setRace(DinoparcConstants.GOUPIGNON);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FEU, 1);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 2);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 3);
                newBornDinoz.setPrice(300000);
                newBornDinoz.setAppearanceCode("B" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                newBornDinoz.getInitialCompList().put(DinoparcConstants.SOLUBILITE, 1);
                break;

            case 12:
                newBornDinoz.setRace(DinoparcConstants.KUMP);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 2);
                newBornDinoz.setPrice(8000);
                newBornDinoz.setAppearanceCode("C" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 13:
                newBornDinoz.setRace(DinoparcConstants.PTEROZ);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 1);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR, 4);
                newBornDinoz.setPrice(10000);
                newBornDinoz.setAppearanceCode("D" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                newBornDinoz.getInitialCompList().put(DinoparcConstants.FOUILLER, 1);
                break;

            case 14:
                newBornDinoz.setRace(DinoparcConstants.SANTAZ);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FEU, 1);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 2);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR, 1);
                newBornDinoz.setPrice(100000);
                newBornDinoz.setAppearanceCode("E" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 16:
                newBornDinoz.setRace(DinoparcConstants.KORGON);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 2);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FOUDRE, 1);
                newBornDinoz.setPrice(15000);
                newBornDinoz.setAppearanceCode("G" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 17:
                newBornDinoz.setRace(DinoparcConstants.KABUKI);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 2);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR, 5);
                newBornDinoz.setPrice(20000);
                newBornDinoz.setAppearanceCode("H" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 18:
                newBornDinoz.setRace(DinoparcConstants.SERPANTIN);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FEU, 5);
                newBornDinoz.setPrice(250000);
                newBornDinoz.setAppearanceCode("I" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                newBornDinoz.getInitialCompList().put(DinoparcConstants.PROTECTIONDUFEU, 1);
                break;

            case 19:
                newBornDinoz.setRace(DinoparcConstants.SOUFFLET);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FOUDRE, 2);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR, 2);
                newBornDinoz.setPrice(250000);
                newBornDinoz.setAppearanceCode("J" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                break;

            case 20:
                newBornDinoz.setRace(DinoparcConstants.FEROSS);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE, 4);
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU, 2);
                newBornDinoz.setPrice(1000000);
                newBornDinoz.setAppearanceCode("K" + randomHexString);
                concatElementWithCompetence(newBornDinoz);
                newBornDinoz.getInitialCompList().put(DinoparcConstants.FORTERESSE, 1);
                break;

            default:
                break;
        }
    }

    public static void concatElementWithCompetence(PlayerDinozStore newBornDinoz) {
        for (String competence : newBornDinoz.getInitialCompList().keySet()) {
            if (newBornDinoz.getInitialCompList().get(competence) > 0) {
                adjustElementsFromCompetence(competence, newBornDinoz.getInitialCompList().get(competence), newBornDinoz);
            }
        }
    }

    public static void adjustElementsFromCompetence(String competence, Integer value, PlayerDinozStore newBornDinoz) {
        switch (competence) {
            case DinoparcConstants.FORCE:
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FEU,
                        newBornDinoz.getInitialElementsValues().get(DinoparcConstants.FEU) + value);
                break;
            case DinoparcConstants.ENDURANCE:
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.TERRE,
                        newBornDinoz.getInitialElementsValues().get(DinoparcConstants.TERRE) + value);
                break;
            case DinoparcConstants.PERCEPTION:
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.EAU,
                        newBornDinoz.getInitialElementsValues().get(DinoparcConstants.EAU) + value);
                break;
            case DinoparcConstants.INTELLIGENCE:
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.FOUDRE,
                        newBornDinoz.getInitialElementsValues().get(DinoparcConstants.FOUDRE) + value);
                break;
            case DinoparcConstants.AGILITÉ:
                newBornDinoz.getInitialElementsValues().replace(DinoparcConstants.AIR,
                        newBornDinoz.getInitialElementsValues().get(DinoparcConstants.AIR) + value);
                break;
            default:
                break;
        }
    }
}
