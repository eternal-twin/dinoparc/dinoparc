package com.dinoparc.api.domain.dinoz;

import java.util.UUID;

public class ArmyDinozInputDTO {
    private String dinozId;
    private UUID masterId;
    private UUID bossId;

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public UUID getMasterId() {
        return masterId;
    }

    public void setMasterId(UUID masterId) {
        this.masterId = masterId;
    }

    public UUID getBossId() {
        return bossId;
    }

    public void setBossId(UUID bossId) {
        this.bossId = bossId;
    }
}
