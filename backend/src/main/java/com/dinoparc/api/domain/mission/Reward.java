package com.dinoparc.api.domain.mission;

import com.dinoparc.api.domain.account.DinoparcConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Reward {
    UUID id;
    String description;
    List<String> collectionContent;
    Integer goldAmount;
    Map<String, Integer> inventoryContent;
    Map<String, Integer> globalRewardsMap;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getCollectionContent() {
        return collectionContent;
    }

    public void setCollectionContent(List<String> collectionContent) {
        this.collectionContent = collectionContent;
    }

    public Integer getGoldAmount() {
        return goldAmount;
    }

    public void setGoldAmount(Integer goldAmount) {
        this.goldAmount = goldAmount;
    }

    public Map<String, Integer> getInventoryContent() {
        return inventoryContent;
    }

    public void setInventoryContent(Map<String, Integer> inventoryContent) {
        this.inventoryContent = inventoryContent;
    }

    public Map<String, Integer> getGlobalRewardsMap() {
        return globalRewardsMap;
    }

    public void generateDisplayableRewardsMap() {
        this.globalRewardsMap = new HashMap<>();
        if (this.collectionContent != null && !this.collectionContent.isEmpty()) {
            for (String collectionItem : this.collectionContent) {
                this.globalRewardsMap.put("collection-" + collectionItem, 1);
            }
        }

        if (this.inventoryContent != null && !this.inventoryContent.isEmpty()) {
            this.globalRewardsMap.putAll(this.inventoryContent);
        }

        if (this.goldAmount != null && this.goldAmount > 0) {
            this.globalRewardsMap.put(DinoparcConstants.COINS, this.goldAmount);
        }
    }
}
