package com.dinoparc.api.domain.dinoz;

import java.util.UUID;

public class DinozBossInfosDto {
    private String bossId;
    private String bossCategory;
    private ArmyDinoz armyDinoz;

    public String getBossId() {
        return bossId;
    }

    public void setBossId(String bossId) {
        this.bossId = bossId;
    }

    public String getBossCategory() {
        return bossCategory;
    }

    public void setBossCategory(String bossCategory) {
        this.bossCategory = bossCategory;
    }

    public ArmyDinoz getArmyDinoz() {
        return armyDinoz;
    }

    public void setArmyDinoz(ArmyDinoz armyDinoz) {
        this.armyDinoz = armyDinoz;
    }
}
