package com.dinoparc.api.domain.clan;

import com.dinoparc.api.domain.dinoz.Dinoz;

import java.util.ArrayList;
import java.util.List;

public class OccupationSummary {

    public long nextRefresh;
    public List<OccupationData> occupationData;
    public List<Dinoz> alivePVEWarEnnemies;

    public OccupationSummary() {
        this.occupationData = new ArrayList<>();
        this.alivePVEWarEnnemies = new ArrayList<>();
    }

    public OccupationSummary(long nextRefresh, List<OccupationData> occupationData) {
        this.nextRefresh = nextRefresh;
        this.occupationData = occupationData;
        this.alivePVEWarEnnemies = new ArrayList<>();
    }

    public long getNextRefresh() {
        return nextRefresh;
    }

    public void setNextRefresh(long nextRefresh) {
        this.nextRefresh = nextRefresh;
    }

    public List<OccupationData> getOccupationData() {
        return occupationData;
    }

    public void setOccupationData(List<OccupationData> occupationData) {
        this.occupationData = occupationData;
    }

    public List<Dinoz> getAlivePVEWarEnnemies() {
        return alivePVEWarEnnemies;
    }

    public void setAlivePVEWarEnnemies(List<Dinoz> alivePVEWarEnnemies) {
        this.alivePVEWarEnnemies = alivePVEWarEnnemies;
    }
}
