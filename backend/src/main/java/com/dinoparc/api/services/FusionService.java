package com.dinoparc.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.repository.*;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;
import kotlin.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.dinoparc.api.controllers.dto.FusionSummaryDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.dinoz.Dinoz;

@Component
public class FusionService {
  private final PgPlayerRepository playerRepository;
  private final PgPlayerStatRepository playerStatRepository;
  private final PgPlayerMissionRepository playerMissionRepository;
  private final PgDinozRepository dinozRepository;
  private final PgHistoryRepository historyRepository;
  private final PgInventoryRepository inventoryRepository;
  private final DiscordService discordService;
 
  @Autowired
  public FusionService(PgPlayerRepository playerRepository, PgPlayerStatRepository playerStatRepository, PgPlayerMissionRepository playerMissionRepository, PgDinozRepository dinozRepository,
                       PgHistoryRepository historyRepository, PgInventoryRepository inventoryRepository, DiscordService discordService) {
    this.playerRepository = playerRepository;
    this.playerStatRepository = playerStatRepository;
    this.playerMissionRepository = playerMissionRepository;
    this.dinozRepository = dinozRepository;
    this.historyRepository = historyRepository;
    this.inventoryRepository = inventoryRepository;
    this.discordService = discordService;
  }

  public FusionSummaryDto fusionTwoDinozTogether(
      Dinoz dinoz1, 
      Dinoz dinoz2, 
      Integer fusionPrice,
      Integer resultLevel) {
    
    FusionSummaryDto fusionSummary = new FusionSummaryDto();
    Optional<Player> account = playerRepository.findById(dinoz1.getMasterId());
    if (validateFusionParameters(dinoz1, dinoz2, fusionPrice, resultLevel) && account.isPresent()) {
      try {
        //1. Définir le Dinoz à modifier.
        //2. Définir le Dinoz à supprimer.
        Dinoz dinozToSave = null;
        Dinoz dinozToDelete = null;
        
        if (isFirstDinozBiggestOfBoth(dinoz1, dinoz2)) {
          dinozToSave = dinoz1;
          dinozToDelete = dinoz2;
        } else {
          dinozToSave = dinoz2;
          dinozToDelete = dinoz1;
        }
        
        Integer oldLevelsTotal = dinozToSave.getLevel() + dinozToDelete.getLevel();
        Integer newLevel = computeResultLevel((double)dinozToSave.getLevel(), (double)dinozToDelete.getLevel());
        Integer delta = (newLevel - dinozToSave.getLevel());
        if (newLevel.equals(resultLevel)) {
          dinozToSave.setLevel(newLevel);
          fusionSummary.setNewLevel(newLevel);
          
          //3. Tirer les éléments au hasard:
          Integer counter = 0;
          while (counter < delta) {
            String randomElement = Dinoz.getRandomElementWithWeights(dinozToDelete.getElementsValues());
            if (dinozToDelete.getElementsValues().get(randomElement) != null && dinozToDelete.getElementsValues().get(randomElement) > 0) {
              dinozToDelete.getElementsValues().put(randomElement, dinozToDelete.getElementsValues().get(randomElement) - 1);
              dinozToSave.getElementsValues().put(randomElement, dinozToSave.getElementsValues().get(randomElement) + 1);
              fusionSummary.getElementsDeltas().put(randomElement, fusionSummary.getElementsDeltas().get(randomElement) + 1);
              counter++;
            }
          }

          Boolean mainDinozIsDark = (dinozToSave.getAppearanceCode().endsWith("%23") 
              || dinozToSave.getAppearanceCode().endsWith("#") 
              || dinozToSave.getSkillsMap().containsKey(DinoparcConstants.APPRENTI_FEU) 
              || dinozToSave.isDark());
          
          //4. Tirer l'apparence au hasard:
          String resultingAppCode = "";
          String paddedAppCode1 = String.format("%-21s", dinozToSave.getAppearanceCode().replace("#", "").replace("%23", "")).replace(' ', '0');
          String paddedAppCode2 = String.format("%-21s", dinozToDelete.getAppearanceCode().replace("#", "").replace("%23", "")).replace(' ', '0');
          
          for (int i = 0; i < 21; i++) {
            resultingAppCode+= selectRandomCharFromEitherDinozAtRandomIndice(i, paddedAppCode1, paddedAppCode2);
          }
          
          if (mainDinozIsDark) {
            resultingAppCode += "#";
          } else if (dinozToSave.getAppearanceCode().endsWith("$")) {
            resultingAppCode += "$";
          } else if (dinozToSave.getAppearanceCode().endsWith("&")) {
            resultingAppCode += "&";
          }
          
          fusionSummary.setResultingAppCode(resultingAppCode);
          dinozToSave.setAppearanceCode(resultingAppCode);
          
          //5. Tirer le nb de tickets Champifuzz & pillules si applicable:
          if (delta <= 20) {
            fusionSummary.setChampifuzzAmount(ThreadLocalRandom.current().nextInt(5, 10 + 1));
          } else {
            fusionSummary.setChampifuzzAmount(ThreadLocalRandom.current().nextInt(10, 20 + 1));
            if (ThreadLocalRandom.current().nextInt(1, 10) == 1) {
              fusionSummary.setSpecialChampifuzzAmount(1);
            } else {
              fusionSummary.setSpecialChampifuzzAmount(0);
            }
          }

          switch (dinozToSave.getRace()) {
            case DinoparcConstants.FEROSS -> fusionSummary.setEternityPillsAmount(1);
            case DinoparcConstants.OUISTITI -> fusionSummary.setEternityPillsAmount(1);
            default -> fusionSummary.setEternityPillsAmount(0);
          }

          //7. Ajouter les stats du dinoz conservé
          playerStatRepository.addStats(UUID.fromString(dinozToSave.getMasterId()), dinozToSave.getId(), List.of(
                  new Pair(PgPlayerStatRepository.NB_FUSION, 1),
                  new Pair(PgPlayerStatRepository.SUM_FUSION, fusionPrice))
          );

          //8. Retirer les stats et missions du dinoz supprimé
          playerStatRepository.deleteDinozStats(dinozToDelete.getId());
          playerMissionRepository.deletePlayerDinozMission(dinozToDelete.getId());

          //9. Updater la liste des Dinoz sur le compte (enlever le Dinoz supprimé)
          Player accountToModify = account.get();
          playerRepository.deleteDinoz(accountToModify.getId(), dinozToDelete.getId());

          //10. Enlever le coût en P.O. du compte:
          playerRepository.updateCash(accountToModify.getId(), -fusionPrice);

          //11. Updater l'inventaire du compte:
          inventoryRepository.addInventoryItem(accountToModify.getId(), DinoparcConstants.CHAMPIFUZ, fusionSummary.getChampifuzzAmount());
          if (fusionSummary.getEternityPillsAmount() != null && fusionSummary.getEternityPillsAmount() > 0) {
            inventoryRepository.addInventoryItem(accountToModify.getId(), DinoparcConstants.ETERNITY_PILL, fusionSummary.getEternityPillsAmount());
          }
          if (fusionSummary.getSpecialChampifuzzAmount() != null && fusionSummary.getSpecialChampifuzzAmount() > 0) {
            inventoryRepository.addInventoryItem(accountToModify.getId(), DinoparcConstants.SPECIAL_CHAMPIFUZ, fusionSummary.getSpecialChampifuzzAmount());
          }

          //10. Sauvegarder l'historique (new event):
          History history = new History();
          history.setPlayerId(accountToModify.getId());
          history.setIcon("hist_reset.gif");
          history.setType("fusion");
          history.setBuyAmount(fusionPrice);
          history.setFromDinozName(dinozToSave.getName());
          history.setToDinozName(dinozToDelete.getName());
          historyRepository.save(history);
          
          //11. Sauvegarder le Dinoz A:
          dinozRepository.processFusion(dinozToSave.getId(), dinozToSave.getAppearanceCode(), dinozToSave.getLevel(), dinozToSave.getElementsValues());
          
          //12. Supprimer le Dinoz B:
          dinozRepository.delete(dinozToDelete.getId());
          
          //13. Sauvegarder le compte:
          var newNbPoints = accountToModify.getNbPoints() + newLevel - oldLevelsTotal;
          var newAverage = newNbPoints / playerRepository.countDinoz(accountToModify.getId());
          playerRepository.updatePoints(accountToModify.getId(), newNbPoints, newAverage);

          if (newLevel >= 350) {
            sendDiscordMessageForFusion(dinoz1, fusionPrice, newLevel, resultingAppCode, accountToModify);
          }

          return fusionSummary;
          
        } else {
          throw new Exception("No match error!");
        }
        
      } catch (Exception e) {
        return null;
      }
    }
    
    return null;
  }

  private void sendDiscordMessageForFusion(Dinoz dinoz1, Integer fusionPrice, Integer newLevel, String resultingAppCode, Player accountToModify) {
    String discordMessage = "Two " + dinoz1.getRace() + "s of player " + accountToModify.getName()
            + " for a total of " + fusionPrice + " golds. The new level is " + newLevel + "!";
    String openMessage = "To view the Dinoz, right-click on the link in blue, 'Copy link', and open it in a private browsing tab.";
    String moddedAppCode = resultingAppCode.replace("#", "%23");
    EmbedCreateSpec embedDiscordMessage = EmbedCreateSpec
            .builder()
            .color(Color.GREEN)
            .thumbnail("https://i.ibb.co/5cyj7n4/raymond.gif")
            .title("Link to Dinoz (private tab)")
            .url("https://neoparc.eternaltwin.org/canvas?&appCode=" + moddedAppCode + "&size=500")
            .author("Fusion from " + accountToModify.getName(), "https://neoparc.eternaltwin.org/canvas?&appCode=" + moddedAppCode + "&size=200", "https://i.ibb.co/FqM1hyz/anim-new.gif")
            .description(discordMessage)
            .addField("See Dinoz", openMessage, false)
            .footer("Generated by the Neoparc Server", "https://i.ibb.co/bB74rVG/hist-reset.gif")
            .build();

    discordService.postEmbedMessage(DinoparcConstants.CHANNEL_CERBERE, embedDiscordMessage);
  }

  public List<String> simulateTwoDinozTogether(Dinoz dinoz1, Dinoz dinoz2) {
    List<String> resultingAppCodes = new ArrayList<>();

    Dinoz dinozToSave = null;
    Dinoz dinozToDelete = null;
    if (isFirstDinozBiggestOfBoth(dinoz1, dinoz2)) {
      dinozToSave = dinoz1;
      dinozToDelete = dinoz2;

    } else {
      dinozToSave = dinoz2;
      dinozToDelete = dinoz1;
    }

    Boolean mainDinozIsDark = (dinozToSave.getAppearanceCode().endsWith("%23")
            || dinozToSave.getAppearanceCode().endsWith("#")
            || dinozToSave.getSkillsMap().containsKey(DinoparcConstants.APPRENTI_FEU)
            || dinozToSave.isDark());

    Boolean mainDinozIsShiny = (dinozToSave.getAppearanceCode().endsWith("$"));
    Boolean mainDinozIsGrey = (dinozToSave.getAppearanceCode().endsWith("&"));

    //Tirer 9 apparences au hasard:
    for (int i = 0; i <= 9; i++) {
      String resultingAppCode = "";
      String paddedAppCode1 = String.format("%-21s", dinozToSave.getAppearanceCode()
              .replace("#", "")
              .replace("%23", "")
                      .replace("$", "")
      ).replace(' ', '0');

      String paddedAppCode2 = String.format("%-21s", dinozToDelete.getAppearanceCode()
              .replace("#", "")
              .replace("%23", "")
                      .replace("$", "")
              ).replace(' ', '0');

      for (int j = 0; j < 21; j++) {
        resultingAppCode += selectRandomCharFromEitherDinozAtRandomIndice(j, paddedAppCode1, paddedAppCode2);
      }

      if (mainDinozIsDark) {
        resultingAppCode += "#";
      } else if (mainDinozIsShiny) {
        resultingAppCode += "$";
      } else if (mainDinozIsGrey) {
        resultingAppCode += "&";
      }
      resultingAppCodes.add(resultingAppCode);
    }

    Optional<Player> account = playerRepository.findById(dinoz1.getMasterId());
    Player accountToModify = account.get();
    playerRepository.updateCash(accountToModify.getId(), -4900);

    return resultingAppCodes;
  }

  private char selectRandomCharFromEitherDinozAtRandomIndice(Integer indice, String appearanceCode1, String appearanceCode2) {
    if (ThreadLocalRandom.current().nextInt(0, 1 + 1) == 0) {
      return appearanceCode1.charAt(indice);
    } else {
      return appearanceCode2.charAt(indice);
    }
  }

  private Boolean validateFusionParameters(Dinoz dinoz1, Dinoz dinoz2, Integer fusionPrice, Integer resultLevel) {
    Integer levelBig = Math.max(dinoz1.getLevel(), dinoz2.getLevel());
    Integer resultLevelCheck = computeResultLevel((double)dinoz1.getLevel(), (double)dinoz2.getLevel());
    Integer fusionPriceCheck = (1000 + (75 * levelBig)) * (resultLevelCheck - levelBig);

    if (fusionPriceCheck.equals(fusionPrice) && resultLevelCheck.equals(resultLevel)) {
      return true;
    }
    
    return false;
  }
  
  private Integer computeResultLevel(Double dinoz1Level, Double dinoz2Level) {
    Double resultLevel = Double.valueOf(0);
    if (dinoz1Level >= dinoz2Level) {
      resultLevel = Math.ceil((dinoz1Level) + ((dinoz2Level / 2)));

    } else {
      resultLevel = Math.ceil((dinoz2Level) + ((dinoz1Level / 2)));
    }

    return resultLevel.intValue();
  }
 
  private Boolean isFirstDinozBiggestOfBoth(Dinoz dinoz1, Dinoz dinoz2) {
    if (dinoz1.getLevel() >= dinoz2.getLevel()) {
      return true;
    }
    
    return false;
  }
}
