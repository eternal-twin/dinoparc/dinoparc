package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.*;

import java.util.Set;

/**
 * The remote user owns some dinoz that were already imported.
 * This may happen following an exchange.
 */
public final class UserOwnsAlreadyImportedDinozException extends ImportCheckException {
  public final DinoparcServer server;
  public final DinoparcUserId dparcUserId;
  public final DinoparcUsername username;
  public final Set<DinoparcDinozId> dinoz;

  public UserOwnsAlreadyImportedDinozException(final DinoparcServer server, final DinoparcUserId dparcUserId, final DinoparcUsername username, final Set<DinoparcDinozId> dinoz) {
    this.server = server;
    this.dparcUserId = dparcUserId;
    this.username = username;
    this.dinoz = dinoz;
  }

  public UserOwnsAlreadyImportedDinozException(final ShortDinoparcUser dparcUser, final Set<DinoparcDinozId> dinoz) {
    this(dparcUser.getServer(), dparcUser.getId(), dparcUser.getUsername(), dinoz);
  }
}
