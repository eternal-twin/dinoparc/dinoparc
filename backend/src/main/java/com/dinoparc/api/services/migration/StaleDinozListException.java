package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.dinoparc.DinoparcUserId;
import net.eternaltwin.dinoparc.DinoparcUsername;
import net.eternaltwin.dinoparc.ShortDinoparcUser;

/**
 * The archive contained a dinoz list that may be stale.
 */
public final class StaleDinozListException extends ImportCheckException {
  public final DinoparcServer server;
  public final DinoparcUserId dparcUserId;
  public final DinoparcUsername username;

  public StaleDinozListException(final DinoparcServer server, final DinoparcUserId dparcUserId, final DinoparcUsername username) {
    this.server = server;
    this.dparcUserId = dparcUserId;
    this.username = username;
  }

  public StaleDinozListException(final ShortDinoparcUser dparcUser) {
    this(dparcUser.getServer(), dparcUser.getId(), dparcUser.getUsername());
  }
}
