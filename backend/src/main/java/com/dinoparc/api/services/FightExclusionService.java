package com.dinoparc.api.services;

import com.dinoparc.api.controllers.dto.FightExclusionRequestDto;
import com.dinoparc.api.domain.account.FightExclusion;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class FightExclusionService {
    private PgFightExclusionRepository fightExclusionRepository;
    private PgPlayerRepository playerRepository;

    @Autowired
    public FightExclusionService(PgFightExclusionRepository fightExclusionRepository, PgPlayerRepository playerRepository) {
        this.fightExclusionRepository = fightExclusionRepository;
        this.playerRepository = playerRepository;
    }

    public String addFightExclusion(FightExclusionRequestDto fightExclusionRequestDto) {
        Player fromPlayer = playerRepository.findByName(fightExclusionRequestDto.getFromPlayerName());
        if (fromPlayer == null) {
            return "Request failed : unknown fromPlayer";
        }

        Player toPlayer = playerRepository.findByName(fightExclusionRequestDto.getToPlayerName());
        if (toPlayer == null) {
            return "Request failed : unknown toPlayer";
        }

        UUID fromPlayerId = UUID.fromString(fromPlayer.getId());
        UUID toPlayerId = UUID.fromString(toPlayer.getId());

        Optional<FightExclusion> existingExclusionOpt1 = fightExclusionRepository.getFightExclusion(fromPlayerId, toPlayerId);
        Optional<FightExclusion> existingExclusionOpt2 = fightExclusionRepository.getFightExclusion(toPlayerId, fromPlayerId);
        if (existingExclusionOpt1.isPresent() || existingExclusionOpt2.isPresent()) {
            return "Request failed : exclusion already exists";
        }

        FightExclusion fightExclusion1 = new FightExclusion();
        fightExclusion1.setFromPlayerId(fromPlayerId);
        fightExclusion1.setToPlayerId(toPlayerId);
        fightExclusion1.setReason(fightExclusionRequestDto.getReason());

        FightExclusion fightExclusion2 = new FightExclusion();
        fightExclusion2.setFromPlayerId(toPlayerId);
        fightExclusion2.setToPlayerId(fromPlayerId);
        fightExclusion2.setReason(fightExclusionRequestDto.getReason());

        fightExclusionRepository.createFightExclusion(fightExclusion1);
        fightExclusionRepository.createFightExclusion(fightExclusion2);

        return "Fight exclusions created";
    }

    public String deleteFightExclusion(FightExclusionRequestDto fightExclusionRequestDto) {
        Player fromPlayer = playerRepository.findByName(fightExclusionRequestDto.getFromPlayerName());
        if (fromPlayer == null) {
            return "Request failed : unknown fromPlayer";
        }

        Player toPlayer = playerRepository.findByName(fightExclusionRequestDto.getToPlayerName());
        if (toPlayer == null) {
            return "Request failed : unknown toPlayer";
        }

        UUID fromPlayerId = UUID.fromString(fromPlayer.getId());
        UUID toPlayerId = UUID.fromString(toPlayer.getId());

        Optional<FightExclusion> existingExclusionOpt1 = fightExclusionRepository.getFightExclusion(fromPlayerId, toPlayerId);
        Optional<FightExclusion> existingExclusionOpt2 = fightExclusionRepository.getFightExclusion(toPlayerId, fromPlayerId);
        if (existingExclusionOpt1.isEmpty() && existingExclusionOpt2.isEmpty()) {
            return "Request failed : unknown exclusions";
        }

        if (existingExclusionOpt1.isPresent()) {
            fightExclusionRepository.deleteFightExclusion(existingExclusionOpt1.get().getId());
        }

        if (existingExclusionOpt2.isPresent()) {
            fightExclusionRepository.deleteFightExclusion(existingExclusionOpt2.get().getId());
        }
        return "Fight exclusions removed";
    }
}
