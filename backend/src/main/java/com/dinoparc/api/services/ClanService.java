package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.dto.CreateClanRequest;
import com.dinoparc.api.controllers.dto.PublicClanDto;
import com.dinoparc.api.controllers.dto.RankingClanDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.clan.*;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;
import com.dinoparc.api.domain.dinoz.NameUtils;
import com.dinoparc.api.repository.PgClanRepository;
import com.dinoparc.api.repository.PgDinozRepository;
import com.dinoparc.api.repository.PgInventoryRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import org.apache.commons.collections4.map.ListOrderedMap;
import org.jetbrains.annotations.NotNull;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Component
@EnableScheduling
public class ClanService {

  public OccupationSummary occupationSummary;
  private final TotemService totemService;
  private final PgPlayerRepository playerRepository;
  private final PgDinozRepository dinozRepository;
  private final PgInventoryRepository inventoryRepository;
  private final PgClanRepository clanRepository;
  private final NeoparcConfig neoparcConfig;

  @Autowired
  public ClanService(
          TotemService totemService,
          PgPlayerRepository playerRepository,
          PgDinozRepository dinozRepository,
          PgInventoryRepository inventoryRepository,
          PgClanRepository clanRepository,
          NeoparcConfig neoparcConfig) {
    this.totemService = totemService;
    this.playerRepository = playerRepository;
    this.dinozRepository = dinozRepository;
    this.inventoryRepository = inventoryRepository;
    this.clanRepository = clanRepository;
    this.neoparcConfig = neoparcConfig;
  }

  public Boolean createClanForUser(CreateClanRequest createClanRequest, Player clanCreator) {
    //1. Remove the 100k from creator accounts.
    playerRepository.updateCash(createClanRequest.getAccountId(), -100000);

    //2. Create the Clan in the database.
    Clan newClan = new Clan();
    newClan.setId(UUID.randomUUID().toString());
    newClan.setCreatorId(createClanRequest.getAccountId());
    newClan.setName(createClanRequest.getClanName());
    newClan.setNbDinozTotal(playerRepository.countDinoz(clanCreator.getId()));
    newClan.setNbDinozWarriors(0);
    newClan.setNbPointsOccupation(0);
    newClan.setNbPointsFights(0);
    newClan.setNbPointsTotem(0);
    newClan.setPosition(0);
    newClan.setFaction(getRandomFaction());
    newClan.setCreationDate(ZonedDateTime.now(ZoneId.of("Europe/Paris")).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    newClan.getMembers().add(clanCreator.getId());

    Page principalPage = new Page();
    principalPage.setPageTitle(createClanRequest.getClanName());
    principalPage.setRawContent(createClanRequest.getClanDescription());
    principalPage.setPrivate(false);
    principalPage.setPageId(UUID.randomUUID().toString());
    Page secretPage = new Page();
    secretPage.setPageTitle(createClanRequest.getClanName() + " (Pvt.)");
    secretPage.setRawContent("");
    secretPage.setPrivate(true);
    secretPage.setPageId(UUID.randomUUID().toString());
    newClan.getPages().put(secretPage.getPageId(), secretPage);
    newClan.getPages().put("PRINCIPAL-" + principalPage.getPageId(), principalPage);

    clanRepository.createClan(newClan);
    return true;
  }

  public Clan getClan(String playerId) {
    Clan clan = clanRepository.getClanOfPlayer(playerId);

    if (clan.getCandidates().size() > 0) {
      List<String> beautifyCandidates = new ArrayList<>();
      for (String candidatId : clan.getCandidates()) {
        beautifyCandidates.add((playerRepository.findById(candidatId).get().getName() + "=" + candidatId));
      }
      clan.setCandidates(beautifyCandidates);
    }

    Map.Entry<String, Page> principalPage = null;
    for (Map.Entry<String, Page> pageEntry : clan.getPages().entrySet()) {
      if (pageEntry.getKey().startsWith("PRINCIPAL-")) {
        principalPage = pageEntry;
      }
    }

    ListOrderedMap clanPagesOrdered = ListOrderedMap.listOrderedMap(clan.getPages());
    clanPagesOrdered.remove(principalPage.getKey());
    clanPagesOrdered.put(0, principalPage.getKey(), principalPage.getValue());
    clan.setPages(clanPagesOrdered);

    if (null == clan.getTotem().getTotemNumber() && DinoparcConstants.WAR_MODE) {
      clan.setTotem(totemService.generateNewTotem(neoparcConfig.getRngFactory(), 1));
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
    }

    if (null == clan.getChest() || clan.getChest().isEmpty()) {
      HashMap<String, Integer> basicChest = new HashMap<String, Integer>();
      basicChest.putIfAbsent(DinoparcConstants.POTION_IRMA, 0);
      basicChest.putIfAbsent(DinoparcConstants.CHARME_PRISMATIK, 0);
      basicChest.putIfAbsent(DinoparcConstants.POTION_GUERRE, 0);

      clan.setChest(basicChest);
      clanRepository.updateChest(clan.getId(), clan.getChest());
    }

    if (DinoparcConstants.WAR_MODE && isTotemFull(clan)) {
      clan.getTotem().setFull(true);
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
    }

    if (DinoparcConstants.WAR_MODE) {
      refreshClanPositionInRankings(clan);
    }

    clan.setAllies(refreshClanAllies(clan).getAllies());

    return clan;
  }

  public boolean isTotemFull(Clan clan) {
    return clan.getTotem().getTotalBuzeNeeded() <= clan.getTotem().getCurrentBuze()
            && clan.getTotem().getTotalPlumesNeeded() <= clan.getTotem().getCurrentPlumes()
            && clan.getTotem().getTotalDentsNeeded() <= clan.getTotem().getCurrentDents()
            && clan.getTotem().getTotalCrinsNeeded() <= clan.getTotem().getCurrentCrins()
            && clan.getTotem().getTotalBoisNeeded() <= clan.getTotem().getCurrentBois();
  }

  public ClanMessage postMessageToClanForum(Player player, String message) {
    return clanRepository.postMessageToClanForum(player.getName(), this.getClan(player.getId()).getId(), message);
  }

  public ClanMessage getClanMessages(String accountId) {
    return clanRepository.getClanMessages(this.getClan(accountId).getId());
  }

  public Boolean checkRightsOnClanForUser(String accountId) {
    Clan actualClan = this.getClan(accountId);
    if (actualClan.getMembers().contains(accountId)) {
      Optional<PlayerClan> optPlayerClan = clanRepository.getPlayerClan(actualClan.getId(), accountId);
      if (optPlayerClan.isPresent()) {
        return optPlayerClan.get().getRole().equalsIgnoreCase(DinoparcConstants.CLAN_CREATOR) || optPlayerClan.get().getRole().equalsIgnoreCase(DinoparcConstants.CLAN_ADMIN);
      }
    }
    return false;
  }

  public Boolean modifyOrCreateClanPage(CreateClanRequest createClanRequest) {
    Clan actualClan = this.getClan(createClanRequest.getAccountId());
    Optional<PlayerClan> playerClan = clanRepository.getPlayerClan(actualClan.getId(), createClanRequest.getAccountId());
    if (actualClan.getMembers().contains(createClanRequest.getAccountId())
            && playerClan.isPresent()
            && playerClan.get().getRole().equalsIgnoreCase(DinoparcConstants.CLAN_ADMIN)
            || playerClan.get().getRole().equalsIgnoreCase(DinoparcConstants.CLAN_CREATOR)) {

        Page page = null;
        if (createClanRequest.getPageId() != null && !createClanRequest.getPageId().isEmpty()) {
          if (actualClan.getPages().containsKey("PRINCIPAL-" + createClanRequest.getPageId())) {
            page = actualClan.getPages().get("PRINCIPAL-" + createClanRequest.getPageId());
          } else {
            page = actualClan.getPages().get(createClanRequest.getPageId());
          }

          if (createClanRequest.getClanName() != null && !createClanRequest.getClanName().isEmpty()) {
            page.setPageTitle(createClanRequest.getClanName());
          }

          if (createClanRequest.getClanDescription() != null && !createClanRequest.getClanDescription().isEmpty()) {
            String securedHtmlInnerContent = createClanRequest.getClanDescription().replaceAll("(?i)<(?!img|/img).*?>", "");
            page.setRawContent(securedHtmlInnerContent);
          }

          actualClan.getPages().replace(page.getPageId(), page);

        } else if (createClanRequest.getPageId() == null || createClanRequest.getPageId().isEmpty()) {
          page = new Page();
          page.setPageId(UUID.randomUUID().toString());
          page.setPrivate(createClanRequest.getPrivate());
          page.setPageTitle(createClanRequest.getClanName());
          String securedHtmlInnerContent = createClanRequest.getClanDescription().replaceAll("(?i)<(?!img|/img).*?>", "");
          page.setRawContent(securedHtmlInnerContent);
          actualClan.getPages().put(page.getPageId(), page);
        }
        clanRepository.modifyClanPage(actualClan);
        return true;
    }
    return false;
  }

  public Boolean deleteClanPage(CreateClanRequest createClanRequest) {
    Clan actualClan = this.getClan(createClanRequest.getAccountId());
    Optional<PlayerClan> playerClan = clanRepository.getPlayerClan(actualClan.getId(), createClanRequest.getAccountId());
    if (actualClan.getMembers().contains(createClanRequest.getAccountId())
            && playerClan.isPresent()
            && playerClan.get().getRole().equalsIgnoreCase(DinoparcConstants.CLAN_ADMIN)
            || playerClan.get().getRole().equalsIgnoreCase(DinoparcConstants.CLAN_CREATOR)) {

      actualClan.getPages().remove(createClanRequest.getPageId());
      clanRepository.modifyClanPage(actualClan);
      return true;
    }
    return false;
  }

  public List<RankingClanDto> getLatestPublicClansDisplayDataForRankings() {
    List<RankingClanDto> allClanDisplays = new ArrayList<>();
    for (Clan clan : clanRepository.getLatest50ClansCreated()) {
      RankingClanDto clanDto = new RankingClanDto();
      clanDto.setClanId(clan.getId());
      clanDto.setClanName(clan.getName());
      clanDto.setCreationDate(clan.getCreationDate());
      clanDto.setNbPlayers(clan.getMembers().size());

      Optional<Player> clanCreator = playerRepository.findById(clan.getCreatorId());
      if (clanCreator.isPresent()) {
        clanDto.setCreatorName(playerRepository.findById(clan.getCreatorId()).get().getName());
        allClanDisplays.add(clanDto);
      } else {
        if (clanRepository.getClan(clan.getId()).get().getMembers().size() == 0) {
          clanRepository.deleteGhostClan(clan.getId());
        } else {
          clanCreator = playerRepository.findById(clanRepository.getClan(clan.getId()).get().getMembers().get(0));
          clanDto.setCreatorName(playerRepository.findById(clanCreator.get().getId()).get().getName());
          allClanDisplays.add(clanDto);
        }
      }
    }

    return getOrderedLatestClansCreatedByDate(allClanDisplays);
  }

  @NotNull
  private static List<RankingClanDto> getOrderedLatestClansCreatedByDate(List<RankingClanDto> allClanDisplays) {
    List<RankingClanDto> orderedRankings = allClanDisplays
            .stream()
            .sorted(Comparator.comparingLong(RankingClanDto::getCreationDateAsLongEpoch))
            .toList();

    return orderedRankings;
  }

  public List<RankingClanDto> getAllPublicClansDisplayDataForRankings() {
    List<RankingClanDto> allClanDisplays = new ArrayList<>();
    for (Clan clan : clanRepository.getAllClans()) {
      RankingClanDto clanDto = new RankingClanDto();
      clanDto.setClanId(clan.getId());
      clanDto.setClanName(clan.getName());
      clanDto.setCreatorName(playerRepository.findById(clan.getCreatorId()).get().getName());
      clanDto.setPointsTotal(clan.getNbPointsFights() + clan.getNbPointsOccupation() + clan.getNbPointsTotem());
      clanDto.setFaction(clan.getFaction());
      clanDto.setPosition(clan.getPosition());
      allClanDisplays.add(clanDto);
    }

    if (DinoparcConstants.WAR_MODE) {
      allClanDisplays.sort(Comparator.comparingDouble(RankingClanDto::getPointsTotal).reversed());
    } else {
      allClanDisplays.sort(Comparator.comparingDouble(RankingClanDto::getPosition));
    }
    return allClanDisplays;
  }

  public PublicClanDto getPublicClanData(String clanId) {
    Optional<Clan> clan = clanRepository.getClan(clanId);
    if (DinoparcConstants.WAR_MODE) {
      refreshClanPositionInRankings(clan.get());
    }
    clan.get().getAllies().clear();
    List<String> allies = refreshClanAllies(clan.get()).getAllies();

    PublicClanDto publicClanDto = new PublicClanDto();
    clan = clanRepository.getClan(clanId);
    if (clan.isPresent()) {
      publicClanDto.setClanId(clan.get().getId());
      publicClanDto.setName(clan.get().getName());
      publicClanDto.setNbDinozTotal(clan.get().getNbDinozTotal());
      publicClanDto.setNbDinozWarriors(clan.get().getNbDinozWarriors());
      publicClanDto.setNbPointsOccupation(clan.get().getNbPointsOccupation());
      publicClanDto.setNbPointsFights(clan.get().getNbPointsFights());
      publicClanDto.setNbPointsTotem(clan.get().getNbPointsTotem());
      publicClanDto.setPosition(clan.get().getPosition());
      publicClanDto.setFaction(clan.get().getFaction());
      publicClanDto.setCreationDate(clan.get().getCreationDate());
      publicClanDto.setMembers(clan.get().getMembers());
      publicClanDto.setAllies(allies);

      for (Map.Entry<String, Page> entry : clan.get().getPages().entrySet()) {
        if (!entry.getValue().getPrivate()) {
          publicClanDto.getPublicPages().putIfAbsent(entry.getKey(), entry.getValue());
        }
      }

      return publicClanDto;
    }

    return new PublicClanDto();
  }

  public List<PlayerClan> getClansMembers(String clanId) {
    List<PlayerClan> allClanPlayers = new ArrayList<>();
    Optional<Clan> clan = clanRepository.getClan(clanId);
    if (clan.isPresent()) {
      for (String rawMemberId : clan.get().getMembers()) {
        if (playerRepository.checkIfPlayerIsInAClan(rawMemberId).getInAClan() && clanRepository.getClanOfPlayer(rawMemberId).getId().equals(clan.get().getId())) {
          PlayerClan playerClan = clanRepository.getPlayerClan(clanId, rawMemberId).get();
          playerClan.setPlayerName(playerRepository.findById(playerClan.getPlayerId()).get().getName());
          allClanPlayers.add(playerClan);
        }
      }
    }

    return allClanPlayers;
  }

  public Boolean applyToClan(String clanId, Player player) {
    Optional<Clan> clan = clanRepository.getClan(clanId);
    if (player.getCash() >= 10000 && clan.isPresent() && clan.get().getMembers().size() < DinoparcConstants.CLAN_PLAYER_LIMIT && !DinoparcConstants.WAR_MODE) {
        for (Clan otherClan : clanRepository.getAllClans()) {
          if (!otherClan.getId().equalsIgnoreCase(clan.get().getId()) && otherClan.getCandidates().contains(player.getId())) {
            return false;
          }
        }

      if (!clan.get().getCandidates().contains(player.getId())) {
        playerRepository.updateCash(player.getId(), -10000);
        clan.get().getCandidates().add(player.getId());
        clanRepository.updateApplicationsToClan(clan.get().getId(), clan.get().getCandidates());
        return true;
      }
    }
    return false;
  }

  public Boolean removeApplicationFromClan(Player player) {
    Clan clanWithApplication = null;
    for (Clan clan : clanRepository.getAllClans()) {
      if (clan.getCandidates().contains(player.getId())) {
        clanWithApplication = clan;
      }
    }
    if (clanWithApplication != null) {
      clanWithApplication.getCandidates().remove(player.getId());
      clanRepository.updateApplicationsToClan(clanWithApplication.getId(), clanWithApplication.getCandidates());
      return true;
    }
    return false;
  }

  public Boolean acceptPlayerIntoClan(String clanId, String candidatId) {
    Optional<Clan> clan = clanRepository.getClan(clanId);
    if (clan.isPresent() && clan.get().getMembers().size() < DinoparcConstants.CLAN_PLAYER_LIMIT) {
        clanRepository.addPlayerToClan(clan.get(), candidatId);
        return true;
    } else if (clan.isPresent() && clan.get().getMembers().size() >= DinoparcConstants.CLAN_PLAYER_LIMIT) {
      return this.removePlayerFromClan(clanId, candidatId);
    }
    return false;
  }

  public Boolean removePlayerFromClan(String clanId, String candidatId) {
    Optional<Clan> clan = clanRepository.getClan(clanId);
    Optional<PlayerClan> playerClan = clanRepository.getPlayerClan(clanId, candidatId);
    if (clan.isPresent() && playerClan.isPresent() && clan.get().getMembers().contains(playerClan.get().getPlayerId())) {
      clanRepository.expellPlayerFromClan(clan.get(), candidatId);
      return true;
    } else if (clan.isPresent() && clan.get().getCandidates().contains(candidatId)) {
      clanRepository.refusePlayerApplication(clan.get(), candidatId);
      return true;
    }
    return false;
  }

  public Boolean quitClan(String accountId) {
    clanRepository.quitClan(accountId);
    return true;
  }

  public Boolean changeAdminFlagForUser(String clanId, String requesterId, String userToEdit) {
    Optional<Clan> clan = clanRepository.getClan(clanId);
    Optional<PlayerClan> requesterPlayer = clanRepository.getPlayerClan(clanId, requesterId);
    Optional<PlayerClan> userToEditPlayer = clanRepository.getPlayerClan(clanId, userToEdit);

    if (clan.isPresent() && clan.get().getMembers().contains(requesterId) && clan.get().getMembers().contains(userToEdit)
            && requesterPlayer.isPresent() && requesterPlayer.get().getRole().equals(DinoparcConstants.CLAN_CREATOR)
            || requesterPlayer.get().getRole().equals(DinoparcConstants.CLAN_ADMIN)
            && userToEditPlayer.isPresent()) {

      PlayerClan playerClanUpdated = userToEditPlayer.get();
      if (playerClanUpdated.getRole().equals(DinoparcConstants.CLAN_ADMIN) || playerClanUpdated.getRole().equals(DinoparcConstants.CLAN_CREATOR)) {
        playerClanUpdated.setRole(DinoparcConstants.CLAN_MEMBER);
      } else {
        playerClanUpdated.setRole(DinoparcConstants.CLAN_ADMIN);
      }

      clanRepository.updatePlayerClan(playerClanUpdated);
      return true;
    }
    return false;
  }

  public Boolean changeTitleForUser(String clanId, String requesterId, String userToEdit, String title) {
    Optional<Clan> clan = clanRepository.getClan(clanId);
    Optional<PlayerClan> requesterPlayer = clanRepository.getPlayerClan(clanId, requesterId);
    Optional<PlayerClan> userToEditPlayer = clanRepository.getPlayerClan(clanId, userToEdit);

    if (clan.isPresent() && clan.get().getMembers().contains(requesterId) && clan.get().getMembers().contains(userToEdit)
            && requesterPlayer.isPresent() && requesterPlayer.get().getRole().equals(DinoparcConstants.CLAN_CREATOR)
            || requesterPlayer.get().getRole().equals(DinoparcConstants.CLAN_ADMIN)
            && userToEditPlayer.isPresent()) {

      PlayerClan playerClanUpdated = userToEditPlayer.get();
      playerClanUpdated.setTitle(title);

      clanRepository.updatePlayerClan(playerClanUpdated);
      return true;
    }
    return false;
  }

  public Integer giveBuzeToTotem(String playerId, Integer buzeQuantity) {
    var inventoryQty = inventoryRepository.getQty(playerId, DinoparcConstants.BUZE);
    if (buzeQuantity == Integer.MAX_VALUE) {
      buzeQuantity = inventoryQty;
    }

    if (inventoryQty >= buzeQuantity) {
      Clan clan = clanRepository.getClanOfPlayer(playerId);
      Integer missing = clan.getTotem().getTotalBuzeNeeded() - clan.getTotem().getCurrentBuze();
      if (buzeQuantity > missing) {
        buzeQuantity = missing;
        clan.getTotem().setCurrentBuze(clan.getTotem().getTotalBuzeNeeded());
      } else {
        clan.getTotem().setCurrentBuze(clan.getTotem().getCurrentBuze() + buzeQuantity);
      }

      if (isTotemFull(clan)) {
        clan.getTotem().setFull(true);
      }
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
      inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.BUZE, buzeQuantity);

      if (buzeQuantity >= 0) {
        PlayerClan playerClan = clanRepository.getPlayerClan(playerId).get();
        if (playerClan.getClanId().equals(clan.getId())) {
          playerClan.setNbPointsTotem(playerClan.getNbPointsTotem() + buzeQuantity);
          clanRepository.updatePlayerClan(playerClan);
          clanRepository.addClanPointsFouilles(playerClan.getClanId(), buzeQuantity);
        }
      }

      return buzeQuantity;
    }
    return 0;
  }

  public Integer givePlumesToTotem(String playerId, Integer plumesQuantity) {
    var inventoryQty = inventoryRepository.getQty(playerId, DinoparcConstants.PLUME);
    if (plumesQuantity == Integer.MAX_VALUE) {
      plumesQuantity = inventoryQty;
    }

    if (inventoryQty >= plumesQuantity) {
      Clan clan = clanRepository.getClanOfPlayer(playerId);
      Integer missing = clan.getTotem().getTotalPlumesNeeded() - clan.getTotem().getCurrentPlumes();
      if (plumesQuantity > missing) {
        plumesQuantity = missing;
        clan.getTotem().setCurrentPlumes(clan.getTotem().getTotalPlumesNeeded());
      } else {
        clan.getTotem().setCurrentPlumes(clan.getTotem().getCurrentPlumes() + plumesQuantity);
      }

      if (isTotemFull(clan)) {
        clan.getTotem().setFull(true);
      }
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
      inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PLUME, plumesQuantity);

      if (plumesQuantity >= 0) {
        PlayerClan playerClan = clanRepository.getPlayerClan(playerId).get();
        if (playerClan.getClanId().equals(clan.getId())) {
          playerClan.setNbPointsTotem(playerClan.getNbPointsTotem() + plumesQuantity);
          clanRepository.updatePlayerClan(playerClan);
          clanRepository.addClanPointsFouilles(playerClan.getClanId(), plumesQuantity);
        }
      }

      return plumesQuantity;
    }
    return 0;
  }

  public Integer giveDentsToTotem(String playerId, Integer dentsQuantity) {
    var inventoryQty = inventoryRepository.getQty(playerId, DinoparcConstants.DENT);
    if (dentsQuantity == Integer.MAX_VALUE) {
      dentsQuantity = inventoryQty;
    }

    if (inventoryQty >= dentsQuantity) {
      Clan clan = clanRepository.getClanOfPlayer(playerId);
      Integer missing = clan.getTotem().getTotalDentsNeeded() - clan.getTotem().getCurrentDents();
      if (dentsQuantity > missing) {
        dentsQuantity = missing;
        clan.getTotem().setCurrentDents(clan.getTotem().getTotalDentsNeeded());
      } else {
        clan.getTotem().setCurrentDents(clan.getTotem().getCurrentDents() + dentsQuantity);
      }

      if (isTotemFull(clan)) {
        clan.getTotem().setFull(true);
      }
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
      inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.DENT, dentsQuantity);

      if (dentsQuantity >= 0) {
        PlayerClan playerClan = clanRepository.getPlayerClan(playerId).get();
        if (playerClan.getClanId().equals(clan.getId())) {
          playerClan.setNbPointsTotem(playerClan.getNbPointsTotem() + dentsQuantity);
          clanRepository.updatePlayerClan(playerClan);
          clanRepository.addClanPointsFouilles(playerClan.getClanId(), dentsQuantity);
        }
      }

      return dentsQuantity;
    }
    return 0;
  }

  public Integer giveCrinsToTotem(String playerId, Integer crinsQuantity) {
    var inventoryQty = inventoryRepository.getQty(playerId, DinoparcConstants.CRIN);
    if (crinsQuantity == Integer.MAX_VALUE) {
      crinsQuantity = inventoryQty;
    }

    if (inventoryQty >= crinsQuantity) {
      Clan clan = clanRepository.getClanOfPlayer(playerId);
      Integer missing = clan.getTotem().getTotalCrinsNeeded() - clan.getTotem().getCurrentCrins();
      if (crinsQuantity > missing) {
        crinsQuantity = missing;
        clan.getTotem().setCurrentCrins(clan.getTotem().getTotalCrinsNeeded());
      } else {
        clan.getTotem().setCurrentCrins(clan.getTotem().getCurrentCrins() + crinsQuantity);
      }

      if (isTotemFull(clan)) {
        clan.getTotem().setFull(true);
      }
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
      inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.CRIN, crinsQuantity);

      if (crinsQuantity >= 0) {
        PlayerClan playerClan = clanRepository.getPlayerClan(playerId).get();
        if (playerClan.getClanId().equals(clan.getId())) {
          playerClan.setNbPointsTotem(playerClan.getNbPointsTotem() + crinsQuantity);
          clanRepository.updatePlayerClan(playerClan);
          clanRepository.addClanPointsFouilles(playerClan.getClanId(), crinsQuantity);
        }
      }

      return crinsQuantity;
    }
    return 0;
  }

  public Integer giveBoisToTotem(String playerId, Integer boisQuantity) {
    var inventoryQty = inventoryRepository.getQty(playerId, DinoparcConstants.BOIS);
    if (boisQuantity == Integer.MAX_VALUE) {
      boisQuantity = inventoryQty;
    }

    if (inventoryQty >= boisQuantity) {
      Clan clan = clanRepository.getClanOfPlayer(playerId);
      Integer missing = clan.getTotem().getTotalBoisNeeded() - clan.getTotem().getCurrentBois();
      if (boisQuantity > missing) {
        boisQuantity = missing;
        clan.getTotem().setCurrentBois(clan.getTotem().getTotalBoisNeeded());
      } else {
        clan.getTotem().setCurrentBois(clan.getTotem().getCurrentBois() + boisQuantity);
      }

      if (isTotemFull(clan)) {
        clan.getTotem().setFull(true);
      }
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
      inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.BOIS, boisQuantity);

      if (boisQuantity >= 0) {
        PlayerClan playerClan = clanRepository.getPlayerClan(playerId).get();
        if (playerClan.getClanId().equals(clan.getId())) {
          playerClan.setNbPointsTotem(playerClan.getNbPointsTotem() + boisQuantity);
          clanRepository.updatePlayerClan(playerClan);
          clanRepository.addClanPointsFouilles(playerClan.getClanId(), boisQuantity);
        }
      }

      return boisQuantity;
    }
    return 0;
  }

  public void redeemTotemRewards(Player player) {
    Clan clan = clanRepository.getClanOfPlayer(player.getId());
    if (clan != null && isTotemFull(clan)) {
      for (Map.Entry<String, Integer> reward : clan.getTotem().getTotemRewards().entrySet()) {
          clan.getChest().putIfAbsent(reward.getKey(), 0);
          clan.getChest().put(reward.getKey(), clan.getChest().get(reward.getKey()) + reward.getValue());
      }
      clan.setTotem(totemService.generateNewTotem(neoparcConfig.getRngFactory(), clan.getTotem().getTotemNumber() + 1));
      clanRepository.updateTotem(clan.getId(), clan.getTotem());
      clanRepository.updateChest(clan.getId(), clan.getChest());
    }
  }

  public List<RankingClanDto> getClansRanking() {
    List<RankingClanDto> orderedRankings = new ArrayList<>();
    if (DinoparcConstants.WAR_MODE) {
      orderedRankings = this.getAllPublicClansDisplayDataForRankings()
              .stream()
              .sorted(Comparator.comparingInt(RankingClanDto::getPointsTotal))
              .collect(Collectors.toList());

      Collections.reverse(orderedRankings);
    } else {
      orderedRankings = this.getAllPublicClansDisplayDataForRankings()
              .stream()
              .sorted(Comparator.comparingInt(RankingClanDto::getPosition))
              .collect(Collectors.toList());
    }
    return orderedRankings;
  }

  public String getRandomFaction() {
    List<String> allThreeFactions = new ArrayList<>();
    allThreeFactions.add(DinoparcConstants.FACTION_1);
    allThreeFactions.add(DinoparcConstants.FACTION_2);
    allThreeFactions.add(DinoparcConstants.FACTION_3);

    return allThreeFactions.get(ThreadLocalRandom.current().nextInt(3));
  }

  public Boolean tryToRedeemIrmasFromClanChest(Player player, Integer quantity) {
    Clan playerClan = clanRepository.getClanOfPlayer(player.getId());
    if (playerClan != null && quantity > 0) {
      if (playerClan.getChest().get(DinoparcConstants.POTION_IRMA) != null && playerClan.getChest().get(DinoparcConstants.POTION_IRMA) >= quantity) {
        playerClan.getChest().put(DinoparcConstants.POTION_IRMA, playerClan.getChest().get(DinoparcConstants.POTION_IRMA) - quantity);
        clanRepository.updateChest(playerClan.getId(), playerClan.getChest());
        inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.POTION_IRMA, quantity);
        return true;
      }
    }
    return false;
  }

  public Boolean tryToRedeemPrismatiksFromClanChest(Player player, Integer quantity) {
    Clan playerClan = clanRepository.getClanOfPlayer(player.getId());
    if (playerClan != null && quantity > 0) {
      if (playerClan.getChest().get(DinoparcConstants.CHARME_PRISMATIK) != null && playerClan.getChest().get(DinoparcConstants.CHARME_PRISMATIK) >= quantity) {
        playerClan.getChest().put(DinoparcConstants.CHARME_PRISMATIK, playerClan.getChest().get(DinoparcConstants.CHARME_PRISMATIK) - quantity);
        clanRepository.updateChest(playerClan.getId(), playerClan.getChest());
        inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.CHARME_PRISMATIK, quantity);
        return true;
      }
    }
    return false;
  }

  public Boolean giveWarriorPotionToDinoz(String playerId, Dinoz dinoz) {
    Clan playerClan = clanRepository.getClanOfPlayer(playerId);
    if (playerClan != null && !dinoz.getMalusList().contains(getWarriorStatusFromClanFaction(playerClan.getFaction()))) {
      if (playerClan.getChest().get(DinoparcConstants.POTION_GUERRE) != null && playerClan.getChest().get(DinoparcConstants.POTION_GUERRE) > 0) {
        playerClan.getChest().put(DinoparcConstants.POTION_GUERRE, playerClan.getChest().get(DinoparcConstants.POTION_GUERRE) - 1);
        clanRepository.updateChest(playerClan.getId(), playerClan.getChest());
        dinoz.getMalusList().add(getWarriorStatusFromClanFaction(playerClan.getFaction()));
        dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
        return true;
      }
    }
    return false;
  }

  public Boolean removeWarriorPotionFromDinoz(String playerId, Dinoz dinoz) {
    Clan playerClan = clanRepository.getClanOfPlayer(playerId);
    if (playerClan != null && dinoz.getMalusList().contains(getWarriorStatusFromClanFaction(playerClan.getFaction()))) {
      if (playerClan.getChest().get(DinoparcConstants.POTION_GUERRE) != null && playerClan.getChest().get(DinoparcConstants.POTION_GUERRE) > 0) {
        playerClan.getChest().put(DinoparcConstants.POTION_GUERRE, playerClan.getChest().get(DinoparcConstants.POTION_GUERRE) - 1);
        clanRepository.updateChest(playerClan.getId(), playerClan.getChest());
        dinoz.getMalusList().removeAll(Collections.singleton(getWarriorStatusFromClanFaction(playerClan.getFaction())));
        dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
        return true;
      }
    }
    return false;
  }

  private String getWarriorStatusFromClanFaction(String faction) {
    switch (faction) {
      case DinoparcConstants.FACTION_1:
        return DinoparcConstants.FACTION_1_GUERRIER;
      case DinoparcConstants.FACTION_2:
        return DinoparcConstants.FACTION_2_GUERRIER;
      case DinoparcConstants.FACTION_3:
        return DinoparcConstants.FACTION_3_GUERRIER;
      default :
        return null;
    }
  }

  private void refreshClanPositionInRankings(Clan clan) {
    List<RankingClanDto> allClans = getAllPublicClansDisplayDataForRankings();
    Integer counter = 1;
    for (RankingClanDto pbClan : allClans) {
      if (pbClan.getClanId().equals(clan.getId())) {
        clan.setPosition(counter);
        clanRepository.updatePosition(clan.getId(), clan.getPosition());
      }
      counter++;
    }
  }

  private Clan refreshClanAllies(Clan clan) {
    clan.setAllies(new ArrayList<>());
    for (RankingClanDto pbClan : getAllPublicClansDisplayDataForRankings()) {
      if (pbClan.getFaction().equalsIgnoreCase(clan.getFaction()) && !pbClan.getClanId().equalsIgnoreCase(clan.getId())) {
        clan.getAllies().add(pbClan.getClanName());
      }
    }
    return clan;
  }

  public Collection<Dinoz> getEnnemyDinozInThatLocation(String playerId, int placeNumber, String lastFledEnnemy, Integer homeDinozLevel) {
    List<Dinoz> validEnnemyWarriorDinozInThatLocation = new ArrayList<>();
    Clan playerClan = clanRepository.getClanOfPlayer(playerId);
    if (playerClan != null) {
      for (Clan ennemyClan : clanRepository.getAllClans()) {
        if (!ennemyClan.getFaction().equalsIgnoreCase(playerClan.getFaction())) {
          for (Dinoz dinoz : clanRepository.getAllWarriorDinozOfClan(ennemyClan.getId())) {
            if (!dinoz.getId().equals(lastFledEnnemy)
                    && dinoz.getPlaceNumber() == placeNumber
                    && dinoz.getLife() > 0
                    && !dinoz.isInTourney()
                    && isLevelGapValid(homeDinozLevel, dinoz.getLevel())) {

              dinoz.setDinozIsActive(true);
              validEnnemyWarriorDinozInThatLocation.add(dinoz);
            }
          }
        }
      }
    }

    if (Objects.nonNull(occupationSummary) && Objects.nonNull(occupationSummary.getAlivePVEWarEnnemies())) {
      for (Dinoz pveWarriorDinoz : occupationSummary.getAlivePVEWarEnnemies()) {
        if (pveWarriorDinoz.getPlaceNumber() == placeNumber && pveWarriorDinoz.getLife() > 0 && isLevelGapValid(homeDinozLevel, pveWarriorDinoz.getLevel())) {
          validEnnemyWarriorDinozInThatLocation.add(pveWarriorDinoz);
        }
      }
    }
    return validEnnemyWarriorDinozInThatLocation;
  }

  private boolean isLevelGapValid(Integer homeDinozLevel, Integer foreignDinozLevel) {
    if (homeDinozLevel < 100) {
      return true;
    } else if (homeDinozLevel <= (4 * foreignDinozLevel)) {
      return true;
    }
    return false;
  }

  public Map<Dinoz, String> getAllWarriorDinozInThatLocation(Integer placeNumber) {
    HashMap<Dinoz, String> validEnnemyWarriorDinozInThatLocation = new HashMap<>();
    if (locationIsNotProtectedFromWar(placeNumber)) {
      for (Dinoz warriorDinozFound : clanRepository.getAllDinozWarriorAliveInThatLocation(placeNumber)) {
        validEnnemyWarriorDinozInThatLocation.put(warriorDinozFound, getFactionFromMalusList(warriorDinozFound.getMalusList()));
      }
    }
    for (Dinoz alivePVEWarrior : occupationSummary.alivePVEWarEnnemies.stream().filter(pveWarrior -> pveWarrior.getPlaceNumber() == placeNumber).toList()) {
      validEnnemyWarriorDinozInThatLocation.put(alivePVEWarrior, DinoparcConstants.PVE_FACTION);
    }
    return validEnnemyWarriorDinozInThatLocation;
  }

  private String getFactionFromMalusList(List<String> malusList) {
    if (malusList.contains(DinoparcConstants.FACTION_1_GUERRIER)) {
      return DinoparcConstants.FACTION_1;
    } else if (malusList.contains(DinoparcConstants.FACTION_2_GUERRIER)) {
      return DinoparcConstants.FACTION_2;
    } else if (malusList.contains(DinoparcConstants.FACTION_3_GUERRIER)) {
      return DinoparcConstants.FACTION_3;
    }
    return StringUtils.EMPTY;
  }

  private boolean locationIsNotProtectedFromWar(int placeNumber) {
    if (placeNumber == 6 || placeNumber == 12 || placeNumber == 18) {
      return false;
    }
    return true;
  }

  public boolean checkIfDinozIsWarrior(List<String> malusList) {
    return (malusList.contains(DinoparcConstants.FACTION_1_GUERRIER)
            || malusList.contains(DinoparcConstants.FACTION_2_GUERRIER)
            || malusList.contains(DinoparcConstants.FACTION_3_GUERRIER)
    );
  }

  public void computeOccupationSummary(List<Dinoz> newPVEWarriors) {
    if (occupationSummary == null) {
      occupationSummary = new OccupationSummary();
    }
    if (newPVEWarriors.size() > 0) {
      occupationSummary.setAlivePVEWarEnnemies(newPVEWarriors);
    }
    occupationSummary.setNextRefresh(ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusMinutes(1).truncatedTo(ChronoUnit.MINUTES).toEpochSecond());

    List<OccupationData> occupationData = new ArrayList<>();

    //For all locations in the game :
    for (Integer locationNumber = 0; locationNumber <= 37; locationNumber++) {
      if (locationNumberIsValid(locationNumber)) {
        OccupationData locData = new OccupationData();
        locData.setLocation(String.valueOf(locationNumber));
        Map<Dinoz, String> allWarriorsOfLocation = getAllWarriorDinozInThatLocation(locationNumber);

        float nbOfWarriorFromFaction1 = 0;
        float nbOfWarriorFromFaction2 = 0;
        float nbOfWarriorFromFaction3 = 0;
        float nbOfWarriorFromFactionPVE = 0;
        for (Map.Entry<Dinoz, String> warriorDinoz : allWarriorsOfLocation.entrySet()) {
          if (warriorDinoz.getValue().equalsIgnoreCase(DinoparcConstants.FACTION_1)) {
            nbOfWarriorFromFaction1++;
            if (DinoparcConstants.FEROSS.equalsIgnoreCase(warriorDinoz.getKey().getRace())
                    && warriorDinoz.getKey().getSkillsMap().containsKey(DinoparcConstants.FORTERESSE)
                    && warriorDinoz.getKey().getSkillsMap().get(DinoparcConstants.FORTERESSE) >= 5) {
              nbOfWarriorFromFaction1++;
            }
          } else if (warriorDinoz.getValue().equalsIgnoreCase(DinoparcConstants.FACTION_2)) {
            nbOfWarriorFromFaction2++;
            if (DinoparcConstants.FEROSS.equalsIgnoreCase(warriorDinoz.getKey().getRace())
                    && warriorDinoz.getKey().getSkillsMap().containsKey(DinoparcConstants.FORTERESSE)
                    && warriorDinoz.getKey().getSkillsMap().get(DinoparcConstants.FORTERESSE) >= 5) {
              nbOfWarriorFromFaction2++;
            }
          } else if (warriorDinoz.getValue().equalsIgnoreCase(DinoparcConstants.FACTION_3)) {
            nbOfWarriorFromFaction3++;
            if (DinoparcConstants.FEROSS.equalsIgnoreCase(warriorDinoz.getKey().getRace())
                    && warriorDinoz.getKey().getSkillsMap().containsKey(DinoparcConstants.FORTERESSE)
                    && warriorDinoz.getKey().getSkillsMap().get(DinoparcConstants.FORTERESSE) >= 5) {
              nbOfWarriorFromFaction3++;
            }
          } else if (warriorDinoz.getValue().equalsIgnoreCase(DinoparcConstants.PVE_FACTION)) {
            nbOfWarriorFromFactionPVE++;
            if (DinoparcConstants.FEROSS.equalsIgnoreCase(warriorDinoz.getKey().getRace())
                    && warriorDinoz.getKey().getSkillsMap().containsKey(DinoparcConstants.FORTERESSE)
                    && warriorDinoz.getKey().getSkillsMap().get(DinoparcConstants.FORTERESSE) >= 5) {
              nbOfWarriorFromFactionPVE++;
            }
          }
        }

        float dominantFaction = Collections.max(Arrays.asList(nbOfWarriorFromFaction1, nbOfWarriorFromFaction2, nbOfWarriorFromFaction3, nbOfWarriorFromFactionPVE));
        if (dominantFaction > 0) {
          locData.setFaction(getDominantFactionFromOccupationValue(dominantFaction, nbOfWarriorFromFaction1, nbOfWarriorFromFaction2, nbOfWarriorFromFaction3, nbOfWarriorFromFactionPVE));
          locData.setPercentage(String.valueOf(dominantFaction / (nbOfWarriorFromFaction1 + nbOfWarriorFromFaction2 + nbOfWarriorFromFaction3 + nbOfWarriorFromFactionPVE) * 100));
        }
        occupationData.add(locData);

      } else if (locationNumber == 6 || locationNumber == 12 || locationNumber == 18){
        OccupationData locData = new OccupationData();
        locData.setLocation(String.valueOf(locationNumber));
        locData.setFaction("QG");
        occupationData.add(locData);
      }
    }
    occupationSummary.setOccupationData(occupationData);
  }

  private String getDominantFactionFromOccupationValue(
          float dominantFaction,
          float nbOfWarriorFromFaction1,
          float nbOfWarriorFromFaction2,
          float nbOfWarriorFromFaction3,
          float nbOfWarriorFromFactionPVE) {

    if (dominantFaction == nbOfWarriorFromFaction1) {
      return DinoparcConstants.FACTION_1;
    } else if (dominantFaction == nbOfWarriorFromFaction2) {
      return DinoparcConstants.FACTION_2;
    } else if (dominantFaction == nbOfWarriorFromFaction3) {
      return DinoparcConstants.FACTION_3;
    } else if (dominantFaction == nbOfWarriorFromFactionPVE) {
      return DinoparcConstants.PVE_FACTION;
    }
    return null;
  }

  private boolean locationNumberIsValid(int locationNumber) {
    if (locationNumber >= 0 && locationNumber < 6) {
      return true;
    } else if (locationNumber > 6 && locationNumber < 12) {
      return true;
    } else if (locationNumber > 12 && locationNumber < 18) {
      return true;
    } else if (locationNumber > 18 && locationNumber < 23) {
      return true;
    } else if (locationNumber == 37) {
      return true;
    }
    return false;
  }

  public Dinoz createRandomPVEWarriorDinoz() {
    Dinoz dinoz = new Dinoz();
    dinoz.setId(DinoparcConstants.PVE_FACTION + UUID.randomUUID().toString());
    dinoz.setInBazar(false);
    dinoz.setMasterId("admin_" + DinoparcConstants.PVE_FACTION);
    dinoz.setMasterName("???");
    dinoz.setName(NameUtils.getRandomName());
    dinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
    dinoz.setEndMessage(DinoparcConstants.ENDMESSAGE1);
    dinoz.setLife(getRandomLifeForPVWBot());
    dinoz.setExperience(0);
    dinoz.setDanger(0);
    dinoz.setPlaceNumber(DinozUtils.getRandomPlaceNumberThatIsNotAFactionBase());
    dinoz.setDark(ThreadLocalRandom.current().nextBoolean());

    //Decide race & app Code :
    String generatedRace = DinozUtils.getRandomRace();
    dinoz.setRace(Dinoz.getRaceFromAppearanceCode(generatedRace));
    dinoz.setAppearanceCode(generatedRace + DinozUtils.getRandomHexString());

    //Decide level + elements + skills :
    DinozUtils.initializeNPCDinoz(dinoz);
    if (dinoz.isDark()) {
      dinoz.setAppearanceCode(dinoz.getAppearanceCode() + "#");
      dinoz.getSkillsMap().put(DinoparcConstants.POUVOIR_SOMBRE, 5);
      dinoz.getElementsValues().put(DinoparcConstants.FEU, dinoz.getElementsValues().get(DinoparcConstants.FEU) + 5);
      dinoz.getElementsValues().put(DinoparcConstants.TERRE, dinoz.getElementsValues().get(DinoparcConstants.TERRE) + 5);
      dinoz.getElementsValues().put(DinoparcConstants.EAU, dinoz.getElementsValues().get(DinoparcConstants.EAU) + 5);
      dinoz.getElementsValues().put(DinoparcConstants.FOUDRE, dinoz.getElementsValues().get(DinoparcConstants.FOUDRE) + 5);
      dinoz.getElementsValues().put(DinoparcConstants.AIR, dinoz.getElementsValues().get(DinoparcConstants.AIR) + 5);
    } else if (ThreadLocalRandom.current().nextInt(100) == 1) {
      dinoz.setAppearanceCode(dinoz.getAppearanceCode() + "$");
    }

    int[] randomCompetences = new Random().ints(1, 6).distinct().limit(3).toArray();
    dinoz.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[0]), 3);
    dinoz.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[1]), 2);
    dinoz.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[2]), 1);
    DinozUtils.concatElementWithCompetence(dinoz);

    Integer randomDrawnLevel = DinozUtils.getRandomLevelForPVEWarrior();
    dinoz.setLevel(randomDrawnLevel);
    dinoz.setDinozIsActive(true);
    dinoz.getElementsValues().replace(DinoparcConstants.FEU, dinoz.getElementsValues().get(DinoparcConstants.FEU) + Math.floorDiv(randomDrawnLevel, 4));
    dinoz.getElementsValues().replace(DinoparcConstants.TERRE, dinoz.getElementsValues().get(DinoparcConstants.TERRE) + Math.floorDiv(randomDrawnLevel, 4));
    dinoz.getElementsValues().replace(DinoparcConstants.EAU, dinoz.getElementsValues().get(DinoparcConstants.EAU) + Math.floorDiv(randomDrawnLevel, 4));
    dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, dinoz.getElementsValues().get(DinoparcConstants.FOUDRE) + Math.floorDiv(randomDrawnLevel, 4));
    dinoz.getElementsValues().replace(DinoparcConstants.AIR, dinoz.getElementsValues().get(DinoparcConstants.AIR) + Math.floorDiv(randomDrawnLevel, 4));

    return dinoz;
  }

  private Integer getRandomLifeForPVWBot() {
    if (ThreadLocalRandom.current().nextInt(1, 100 + 1) == 1) {
      //1% chance to generate insane amount of HPs :
      return ThreadLocalRandom.current().nextInt(1000, 5000 + 1);
    }
    return 100;
  }

}