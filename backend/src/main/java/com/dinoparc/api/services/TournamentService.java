package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.Tournament;
import com.dinoparc.api.domain.dinoz.TournamentRanking;
import com.dinoparc.api.repository.PgClanRepository;
import com.dinoparc.api.repository.PgDinozRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Component
@SuppressWarnings("all")
public class TournamentService {

  private final PgDinozRepository dinozRepository;
  private final PgPlayerRepository playerRepository;
  private final PgClanRepository clanRepository;
  private final ClanService clanService;

  @Autowired
  public TournamentService(PgDinozRepository dinozRepository, PgPlayerRepository playerRepository, ClanService clanService, PgClanRepository clanRepository) {
    this.dinozRepository = dinozRepository;
    this.playerRepository = playerRepository;
    this.clanRepository = clanRepository;
    this.clanService = clanService;
  }

  public void addGlobalStatsToTournamentInstance(Tournament tournament, Dinoz dinoz) {
    Iterable<Dinoz> allContestants = getAllContestants(dinoz.getPlaceNumber());
    List<TournamentRanking> sortedRankings = new ArrayList<TournamentRanking>();

    for (Dinoz contestant : allContestants) {
      TournamentRanking ranking = new TournamentRanking();
      ranking.setDinozName(contestant.getName());
      ranking.setMasterName(contestant.getMasterName());
      ranking.setNbPoints(contestant.getTournaments().get(Tournament.getTournamentByPlaceNumber(dinoz.getPlaceNumber())).getActualPoints());
      ranking.setLevel(contestant.getLevel());
      ranking.setAlive(contestant.getLife() > 0);
      sortedRankings.add(ranking);
    }

    Collections.sort(sortedRankings);

    Integer counter = 1;
    for (TournamentRanking ranking : sortedRankings) {
      if (ranking.getDinozName().equalsIgnoreCase(dinoz.getName())) {
        tournament.setPosition(counter);
      }

      ranking.setPosition(counter);
      counter++;
    }

    if (sortedRankings.size() > 50) {
      tournament.setRankings(sortedRankings.subList(0, 50));

    } else {
      tournament.setRankings(sortedRankings);
    }

    tournament.setNbInscrits(sortedRankings.size());
  }

  public Dinoz findTournamentOpponent(Dinoz homeDinoz, String tournamentName) {
    if (tournamentName.equalsIgnoreCase(Tournament.getTournamentByPlaceNumber(homeDinoz.getPlaceNumber()))) {
      Iterable<Dinoz> allContestants = getAllContestants(homeDinoz.getPlaceNumber());
      List<Dinoz> validContestants = new ArrayList<Dinoz>();

      for (Dinoz contestant : allContestants) {
        Integer contestantPoint = contestant.getTournaments().get(tournamentName).getActualPoints();
        Integer homeDinozPoint = homeDinoz.getTournaments().get(tournamentName).getActualPoints();
        Integer pointsDifference = homeDinozPoint - contestantPoint;

        if (!(contestant.getMasterId().equalsIgnoreCase(homeDinoz.getMasterId()))
            && pointsDifferenceIsValid(pointsDifference)
            && contestant.getLife() > 0) {
          validContestants.add(contestant);
        }
      }

      if (!validContestants.isEmpty()) {
        return randomlyFindAValidContestant(validContestants, homeDinoz);
      }
    }

    return null;
  }

  private Iterable<Dinoz> getAllContestants(Integer placeNumber) {
    return dinozRepository.getTournamentContestants(placeNumber);
  }

  private Dinoz randomlyFindAValidContestant(List<Dinoz> validContestants, Dinoz homeDinoz) {
    //Condition de l'oeil du tigre en Tournoi :
    if (homeDinoz.getMalusList().contains(DinoparcConstants.OEIL_DU_TIGRE)
            && DinoparcConstants.WAR_MODE
            && clanService.checkIfDinozIsWarrior(homeDinoz.getMalusList())
            && playerRepository.checkIfPlayerIsInAClan(homeDinoz.getMasterId()).getInAClan()) {

      for (Dinoz foreignDinoz : validContestants) {
        if (clanService.checkIfDinozIsWarrior(foreignDinoz.getMalusList())) {
          Clan homeDinozClan = clanRepository.getClanOfPlayer(homeDinoz.getMasterId());
          Clan foreignDinozClan = clanRepository.getClanOfPlayer(foreignDinoz.getMasterId());
          if (!homeDinozClan.getFaction().equals(foreignDinozClan.getFaction())) {
            return foreignDinoz;
          }
        }
      }
    }
    final int min = 0;
    final int max = validContestants.size() - 1;
    final int random = new Random().nextInt((max - min) + 1) + min;
    return validContestants.get(random);
  }

  private boolean pointsDifferenceIsValid(Integer pointsDifference) {
    if (pointsDifference == -2 || pointsDifference == -1 || pointsDifference == 0
        || pointsDifference == 1 || pointsDifference == 2) {
      return true;
    }
    return false;
  }
}
