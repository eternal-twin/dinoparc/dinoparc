package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.user.UserId;

/**
 * The user asks to import an account from a server where he is not linked.
 */
public final class MissingDinoparcLinkException extends ImportCheckException {
  public final UserId userId;
  public final DinoparcServer server;

  public MissingDinoparcLinkException(final UserId userId, final DinoparcServer server) {
    this.userId = userId;
    this.server = server;
  }
}
