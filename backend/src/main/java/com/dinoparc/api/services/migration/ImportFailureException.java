package com.dinoparc.api.services.migration;

/**
 * Represents an unexpected fatal exception to import a user.
 */
public class ImportFailureException extends Exception {
}
