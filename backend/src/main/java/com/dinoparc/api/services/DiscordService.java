package com.dinoparc.api.services;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.spec.EmbedCreateSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DiscordService {

    private final GatewayDiscordClient discordClient;

    @Value("${server.discord-disable:#{false}}")
    public Boolean discordDisable;

    @Autowired
    public DiscordService(GatewayDiscordClient discordClient) {
        this.discordClient = discordClient;
    }

    public void postMessage(Snowflake channel, String message) {
        if (!discordDisable) {
            if (Objects.isNull(discordClient)) {
                return;
            }

            try {
                discordClient.getChannelById(channel)
                        .ofType(MessageChannel.class)
                        .flatMap(ch -> ch.createMessage(message))
                        .subscribe();
            } catch (Exception e) {}
        }
    }

    public void postEmbedMessage(Snowflake channel, EmbedCreateSpec embedCreateSpec) {
        if (!discordDisable) {
            if (Objects.isNull(discordClient)) {
                return;
            }

            try {
                discordClient.getChannelById(channel)
                        .ofType(MessageChannel.class)
                        .flatMap(ch -> ch.createMessage(embedCreateSpec))
                        .subscribe();
            } catch (Exception e) {}
        }
    }
}
