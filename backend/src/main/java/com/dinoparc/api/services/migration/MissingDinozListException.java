package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.dinoparc.DinoparcUserId;
import net.eternaltwin.dinoparc.DinoparcUsername;
import net.eternaltwin.dinoparc.ShortDinoparcUser;

/**
 * The archive did not contain the Dinoz list.
 */
public final class MissingDinozListException extends ImportCheckException {
  public final DinoparcServer server;
  public final DinoparcUserId dparcUserId;
  public final DinoparcUsername username;

  public MissingDinozListException(final DinoparcServer server, final DinoparcUserId dparcUserId, final DinoparcUsername username) {
    this.server = server;
    this.dparcUserId = dparcUserId;
    this.username = username;
  }

  public MissingDinozListException(final ShortDinoparcUser dparcUser) {
    this(dparcUser.getServer(), dparcUser.getId(), dparcUser.getUsername());
  }
}
