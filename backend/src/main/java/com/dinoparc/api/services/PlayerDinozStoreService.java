package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.PlayerDinozStore;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.repository.*;
import kotlin.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class PlayerDinozStoreService {

    private final PgPlayerRepository playerRepository;
    private final PgPlayerDinozStoreRepository playerDinozStoreRepository;
    private final PgPlayerStatRepository playerStatRepository;
    private final PgDinozRepository dinozRepository;
    private final PgHistoryRepository historyRepository;
    private final NeoparcConfig neoparcConfig;

    @Autowired
    public PlayerDinozStoreService(
            PgPlayerRepository playerRepository,
            PgPlayerDinozStoreRepository playerDinozStoreRepository,
            PgPlayerStatRepository playerStatRepository,
            PgDinozRepository dinozRepository,
            PgHistoryRepository historyRepository,
            NeoparcConfig neoparcConfig) {

        this.playerRepository = playerRepository;
        this.playerDinozStoreRepository = playerDinozStoreRepository;
        this.playerStatRepository = playerStatRepository;
        this.dinozRepository = dinozRepository;
        this.historyRepository = historyRepository;
        this.neoparcConfig = neoparcConfig;
    }

    public boolean buyDinoz(Player buyerAccount, PlayerDinozStore playerDinozStore, String name) {
        if (buyerAccount.getId().equals(playerDinozStore.getPlayerId().toString())
            && buyerAccount.getCash() >= playerDinozStore.getPrice()) {
            // 1. Créer le dinoz
            Dinoz dinoz = new Dinoz();
            dinoz.setId(playerDinozStore.getId().toString());
            dinoz.setMasterId(buyerAccount.getId());
            dinoz.setMasterName(buyerAccount.getName());
            if (name == null || name.isEmpty() || name.isBlank()) {
                name = buyerAccount.getName();
            }
            dinoz.setName(name);
            dinoz.setRace(playerDinozStore.getRace());
            dinoz.setLife(100);
            dinoz.setLevel(1);
            dinoz.setExperience(0);
            dinoz.setDanger(0);
            dinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
            dinoz.setEndMessage(DinoparcConstants.ENDMESSAGE1);
            dinoz.getElementsValues().put(DinoparcConstants.FEU, playerDinozStore.getInitialElementsValues().get(DinoparcConstants.FEU));
            dinoz.getElementsValues().put(DinoparcConstants.TERRE, playerDinozStore.getInitialElementsValues().get(DinoparcConstants.TERRE));
            dinoz.getElementsValues().put(DinoparcConstants.EAU, playerDinozStore.getInitialElementsValues().get(DinoparcConstants.EAU));
            dinoz.getElementsValues().put(DinoparcConstants.FOUDRE, playerDinozStore.getInitialElementsValues().get(DinoparcConstants.FOUDRE));
            dinoz.getElementsValues().put(DinoparcConstants.AIR, playerDinozStore.getInitialElementsValues().get(DinoparcConstants.AIR));

            dinoz.getSkillsMap().put(DinoparcConstants.FORCE, playerDinozStore.getInitialCompList().get(DinoparcConstants.FORCE));
            dinoz.getSkillsMap().put(DinoparcConstants.ENDURANCE, playerDinozStore.getInitialCompList().get(DinoparcConstants.ENDURANCE));
            dinoz.getSkillsMap().put(DinoparcConstants.PERCEPTION, playerDinozStore.getInitialCompList().get(DinoparcConstants.PERCEPTION));
            dinoz.getSkillsMap().put(DinoparcConstants.INTELLIGENCE, playerDinozStore.getInitialCompList().get(DinoparcConstants.INTELLIGENCE));
            dinoz.getSkillsMap().put(DinoparcConstants.AGILITÉ, playerDinozStore.getInitialCompList().get(DinoparcConstants.AGILITÉ));
            dinoz.getSkillsMap().put(DinoparcConstants.FOUILLER, playerDinozStore.getInitialCompList().get(DinoparcConstants.FOUILLER));
            dinoz.getSkillsMap().put(DinoparcConstants.ROCK, playerDinozStore.getInitialCompList().get(DinoparcConstants.ROCK));
            dinoz.getSkillsMap().put(DinoparcConstants.PROTECTIONDUFEU, playerDinozStore.getInitialCompList().get(DinoparcConstants.PROTECTIONDUFEU));
            dinoz.getSkillsMap().put(DinoparcConstants.SOLUBILITE, playerDinozStore.getInitialCompList().get(DinoparcConstants.SOLUBILITE));
            dinoz.getSkillsMap().put(DinoparcConstants.FORTERESSE, playerDinozStore.getInitialCompList().get(DinoparcConstants.FORTERESSE));

            dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
            dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
            dinoz.getActionsMap().put(DinoparcConstants.TournoiDinoville, true);
            dinoz.getActionsMap().put(DinoparcConstants.POSTEMISSIONS, true);

            if (dinoz.getRace().equalsIgnoreCase(DinoparcConstants.PTEROZ)) {
                dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
            }

            if (dinoz.getRace().equalsIgnoreCase(DinoparcConstants.ROKKY)) {
                dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
            }

            dinoz.setPlaceNumber(0);
            dinoz.setAppearanceCode(playerDinozStore.getAppearanceCode());
            dinoz.setInTourney(false);
            dinoz.setDark(false);
            dinoz.setDinozIsActive(true);

            // 2. Ajouter à l'historique
            History buyEvent = new History();
            buyEvent.setPlayerId(buyerAccount.getId());
            buyEvent.setType("buyDinoz");
            buyEvent.setIcon("hist_xp.gif");
            buyEvent.setBuyAmount(playerDinozStore.getPrice());

            historyRepository.save(buyEvent);

            // 3. Indexer la transaction
            dinozRepository.create(dinoz);
            playerRepository.updateCash(buyerAccount.getId(), -playerDinozStore.getPrice());
            playerRepository.updatePoints(buyerAccount.getId(), buyerAccount.getNbPoints() + 1, (buyerAccount.getNbPoints()) / (playerRepository.countDinoz(buyerAccount.getId()) + 1));

            // 4. Creer dinoz et associer au joueur
            playerRepository.addDinoz(buyerAccount.getId(), dinoz.getId());

            // 5. Ajouter aux stats
            playerStatRepository.addStats(UUID.fromString(buyerAccount.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_DINOZ_BUY, 1)));

            // 6. Reset store
            renewShopForUser(buyerAccount.getId());

            // 7. Update last generated store date
            playerRepository.updateLastGeneratedStore(buyerAccount.getId(), OffsetDateTime.now());
            return false;
        }
        return true;
    }

    public List<PlayerDinozStore> renewShopForUser(String accountId) {
        Player account = playerRepository.findById(accountId).get();

        if (account != null) {
            playerDinozStoreRepository.deleteByPlayerId(UUID.fromString(accountId));
            List<PlayerDinozStore> shopDinoz = new ArrayList<PlayerDinozStore>();
            int currentRank = 0;

            while (shopDinoz.size() < 15) {
                PlayerDinozStore playerDinozStore = PlayerDinozStore.generateOneDinozStore(UUID.fromString(accountId), account.getUnlockedDinoz(), currentRank);
                if (playerDinozStore != null) {
                    playerDinozStoreRepository.create(playerDinozStore);
                    shopDinoz.add(playerDinozStore);
                    currentRank++;
                }
            }

            playerRepository.updateLastGeneratedStore(accountId, OffsetDateTime.now());
            return shopDinoz;
        }
        return Collections.emptyList();
    }

    public List<PlayerDinozStore> renewShopForUserWithOnlyOneRace(String accountId, String chosenRace) {
        Player account = playerRepository.findById(accountId).get();

        if (account != null) {
            playerDinozStoreRepository.deleteByPlayerId(UUID.fromString(accountId));
            List<PlayerDinozStore> shopDinoz = new ArrayList<>();
            int currentRank = 0;

            while (shopDinoz.size() < 15) {
                PlayerDinozStore playerDinozStore = PlayerDinozStore.generateOneDinozSingleRaceStore(chosenRace, UUID.fromString(accountId), currentRank);
                if (playerDinozStore != null) {
                    playerDinozStoreRepository.create(playerDinozStore);
                    shopDinoz.add(playerDinozStore);
                    currentRank++;
                }
            }

            playerRepository.updateLastGeneratedStore(accountId, OffsetDateTime.now());
            return shopDinoz;
        }
        return Collections.emptyList();
    }

    public List<PlayerDinozStore> renewShopForUserWithOnlyOneColor(String accountId) {
        Player account = playerRepository.findById(accountId).get();

        if (account != null) {
            playerDinozStoreRepository.deleteByPlayerId(UUID.fromString(accountId));
            List<PlayerDinozStore> shopDinoz = new ArrayList<>();
            int currentRank = 0;

            Rng rng = neoparcConfig.getRngFactory()
                    .string(String.valueOf(ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond()))
                    .string(account.getId())
                    .toRng();

            String randomColor = String.valueOf("0123456789ABC".charAt(rng.randIntBetween(0, 13)));
            while (shopDinoz.size() < 15) {
                PlayerDinozStore playerDinozStore = PlayerDinozStore.generateOneDinozSingleColorStore(randomColor, UUID.fromString(accountId), currentRank, account.getUnlockedDinoz());
                if (playerDinozStore != null) {
                    playerDinozStoreRepository.create(playerDinozStore);
                    shopDinoz.add(playerDinozStore);
                    currentRank++;
                }
            }

            playerRepository.updateLastGeneratedStore(accountId, OffsetDateTime.now());
            return shopDinoz;
        }
        return Collections.emptyList();
    }

    public List<PlayerDinozStore> renewShopForUserWithMagikTicket(String accountId) {
        Player account = playerRepository.findById(accountId).get();

        if (account != null) {
            String randomColor = null;
            playerDinozStoreRepository.deleteByPlayerId(UUID.fromString(accountId));
            List<PlayerDinozStore> shopDinoz = new ArrayList<PlayerDinozStore>();
            int currentRank = 0;

            if (ThreadLocalRandom.current().nextInt(1, 5 + 1) == 1) {
                Rng rng = neoparcConfig.getRngFactory()
                        .string(String.valueOf(ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond()))
                        .string(account.getId())
                        .toRng();
                randomColor = String.valueOf("0123456789ABC".charAt(rng.randIntBetween(0, 13)));
            }
            while (shopDinoz.size() < 15) {
                PlayerDinozStore playerDinozStore = PlayerDinozStore.generateOneDinozStoreForMagikTicket(randomColor, UUID.fromString(accountId), currentRank);
                if (playerDinozStore != null) {
                    playerDinozStoreRepository.create(playerDinozStore);
                    shopDinoz.add(playerDinozStore);
                    currentRank++;
                }
            }

            playerRepository.updateLastGeneratedStore(accountId, OffsetDateTime.now());
            return shopDinoz;
        }
        return Collections.emptyList();
    }

    public Optional<PlayerDinozStore> getById(UUID playerDinozStoreId) {
        return playerDinozStoreRepository.findById(playerDinozStoreId);
    }

    public List<PlayerDinozStore> getAllByPlayerId(UUID playerId) {
        return playerDinozStoreRepository.findAllByPlayerId(playerId);
    }
}
