package com.dinoparc.api.services;

import com.dinoparc.api.controllers.dto.ClientInformationDetailRequestDto;
import com.dinoparc.api.domain.account.ClientInformation;
import com.dinoparc.api.domain.account.ClientInformationBis;
import com.dinoparc.api.domain.account.ClientInformationBisDetail;
import com.dinoparc.api.domain.account.ClientInformationDetail;
import com.dinoparc.api.repository.PgClientInformationBisRepository;
import com.dinoparc.api.repository.PgClientInformationRepository;
import com.dinoparc.api.repository.PgClientInformationTraceRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Component
public class ClientInformationService {
    @Value("${server.salt}")
    public String salt;

    private final PgClientInformationRepository clientInformationRepository;

    private final PgClientInformationBisRepository clientInformationBisRepository;

    private final PgClientInformationTraceRepository clientInformationTraceRepository;

    public ClientInformationService(PgClientInformationRepository clientInformationRepository,
                                    PgClientInformationBisRepository clientInformationBisRepository,
                                    PgClientInformationTraceRepository clientInformationTraceRepository) {
        this.clientInformationRepository = clientInformationRepository;
        this.clientInformationBisRepository = clientInformationBisRepository;
        this.clientInformationTraceRepository = clientInformationTraceRepository;
    }

    public void checkClientInfos(String username, HttpServletRequest request) {
        ZonedDateTime neoparcTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));

        StringBuilder sb = new StringBuilder(neoparcTime.format(DateTimeFormatter.ISO_LOCAL_DATE));
        sb.append(" ");
        sb.append(neoparcTime.format(DateTimeFormatter.ISO_LOCAL_TIME).substring(0, 8));

        ClientInformation infos = new ClientInformation();
        infos.setUsername(username);
        infos.setFirst(encrypt(salt, getClientFirstInfo(request)));
        infos.setSecond(encrypt(salt, getClientSecondInfo(request)));
        infos.setThird(encrypt(salt, getClientThirdInfo(request)));
        infos.setFourth(encrypt(salt, getClientFourthInfo(request)));
        infos.setRetrievedDate(neoparcTime.toOffsetDateTime());
        infos.setRetrievedDateTxt(sb.toString());

        clientInformationRepository.register(infos);
    }

    private byte[] encrypt(String salt, String text) {
        if (text == null || text.isEmpty() || text.isBlank()) {
            return null;
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes());
            byte[] encryptedText = md.digest(text.getBytes(StandardCharsets.UTF_8));
            return encryptedText;
        } catch (Exception e) {
            return null;
        }
    }

    private String getClientFirstInfo(HttpServletRequest request) {
        String firstInfo = request.getHeader("X-Forwarded-For");
        if (firstInfo == null || firstInfo.length() == 0 || "unknown".equalsIgnoreCase(firstInfo)) {
            firstInfo = request.getHeader("Proxy-Client-IP");
        }
        if (firstInfo == null || firstInfo.length() == 0 || "unknown".equalsIgnoreCase(firstInfo)) {
            firstInfo = request.getHeader("WL-Proxy-Client-IP");
        }
        if (firstInfo == null || firstInfo.length() == 0 || "unknown".equalsIgnoreCase(firstInfo)) {
            firstInfo = request.getHeader("HTTP_CLIENT_IP");
        }
        if (firstInfo == null || firstInfo.length() == 0 || "unknown".equalsIgnoreCase(firstInfo)) {
            firstInfo = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (firstInfo == null || firstInfo.length() == 0 || "unknown".equalsIgnoreCase(firstInfo)) {
            firstInfo = request.getRemoteAddr();
        }
        return firstInfo;
    }

    private String getClientSecondInfo(HttpServletRequest request) {
        final String secondInfo = request.getHeader("User-Agent");

        final String lowerCaseBrowser = secondInfo.toLowerCase();
        if (lowerCaseBrowser.contains("windows")) {
            return "Windows";
        }
        if (lowerCaseBrowser.contains("mac")) {
            return "Mac";
        }
        if (lowerCaseBrowser.contains("x11")) {
            return "Unix";
        }
        if (lowerCaseBrowser.contains("android")) {
            return "Android";
        }
        if (lowerCaseBrowser.contains("iphone")) {
            return "IPhone";
        }
        return secondInfo;
    }

    private String getClientThirdInfo(HttpServletRequest request) {
        final String userAgentHeader = request.getHeader("User-Agent");
        final String user = userAgentHeader.toLowerCase();

        if (user.contains("msie")) {
            String substring = userAgentHeader.substring(userAgentHeader.indexOf("MSIE")).split(";")[0];
            return substring.split(" ")[0].replace("MSIE", "IE") + "-" + substring.split(" ")[1];
        }
        if (user.contains("safari") && user.contains("version")) {
            return (userAgentHeader.substring(userAgentHeader.indexOf("Safari")).split(" ")[0]).split(
                    "/")[0] + "-" + (userAgentHeader.substring(
                    userAgentHeader.indexOf("Version")).split(" ")[0]).split("/")[1];
        }
        if (user.contains("opr") || user.contains("opera")) {
            if (user.contains("opera"))
                return (userAgentHeader.substring(userAgentHeader.indexOf("Opera")).split(" ")[0]).split(
                        "/")[0] + "-" + (userAgentHeader.substring(
                        userAgentHeader.indexOf("Version")).split(" ")[0]).split("/")[1];
            if (user.contains("opr"))
                return ((userAgentHeader.substring(userAgentHeader.indexOf("OPR")).split(" ")[0]).replace("/",
                        "-")).replace(
                        "OPR", "Opera");
        }
        if (user.contains("chrome")) {
            return (userAgentHeader.substring(userAgentHeader.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        }
        if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1) || (user.indexOf(
                "mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf(
                "mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1)) {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            return "Netscape-?";
        }
        if (user.contains("firefox")) {
            return (userAgentHeader.substring(userAgentHeader.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        }
        if (user.contains("rv")) {
            return "IE";
        }
        return userAgentHeader;
    }

    private String getClientFourthInfo(HttpServletRequest request) {
        return request.getHeader("User-Agent");
    }

    public void addClientInformationDetail(ClientInformationDetailRequestDto clientInformationDetailRequest, String type, OffsetDateTime retrievedDate) {
        var toInsert = new ClientInformationDetail();
        toInsert.setAccountId(clientInformationDetailRequest.getAccountId());
        toInsert.setDinozId(clientInformationDetailRequest.getDinozId());
        toInsert.setFirst(clientInformationDetailRequest.getFirst());
        toInsert.setSecond(clientInformationDetailRequest.getSecond());
        toInsert.setDetailType(type);
        toInsert.setRetrievedDate(retrievedDate);
        clientInformationRepository.addDetail(toInsert);
    }

    public void manageClientInformationBis(HttpServletRequest request, String accountId, String dinozId, Integer life, Integer xp, String client) {
        var details = new ClientInformationBisDetail();
        details.setLife(life);
        details.setXp(xp);
        details.setClient(client);

        var toInsert = new ClientInformationBis();
        toInsert.setPlayerId(UUID.fromString(accountId));
        toInsert.setDinozId(dinozId);
        toInsert.setRetrievedDate(OffsetDateTime.now());
        toInsert.setFirst(encrypt(salt, getClientFirstInfo(request)));
        toInsert.setFourth(getClientFourthInfo(request));
        toInsert.setRank(0);
        toInsert.setSkipInfo(details);

        clientInformationBisRepository.incrementRank(accountId);
        clientInformationBisRepository.addClientInformationBis(toInsert);
    }

    public void cleanSkipHistory(String accountId) {
        clientInformationBisRepository.cleanUpAccount(accountId);
    }

    public void cleanClientInformationTrace() {
        clientInformationTraceRepository.cleanUp();
    }
}
