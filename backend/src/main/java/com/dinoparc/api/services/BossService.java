package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.dinoz.*;
import com.dinoparc.api.repository.PgArmyDinozRepository;
import com.dinoparc.api.repository.PgBossRepository;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class BossService {

    private final PgArmyDinozRepository armyDinozRepository;
    private final PgBossRepository bossRepository;
    private final AccountService accountService;

    public BossService(PgArmyDinozRepository armyDinozRepository, PgBossRepository bossRepository, AccountService accountService) {
        this.armyDinozRepository = armyDinozRepository;
        this.bossRepository = bossRepository;
        this.accountService = accountService;
    }

    public ArmyDinoz addArmyDinoz(Player player, ArmyDinozInputDTO armyDinozInput) {
        Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(player, armyDinozInput.getDinozId());
        Optional<ArmyDinoz> optExistingArmyDinoz = armyDinozRepository.getArmyDinozFromMaster(armyDinozInput.getBossId(), UUID.fromString(player.getId()));
        Optional<EventDinozDto> boss = bossRepository.getBoss(armyDinozInput.getBossId());

        if (canAddArmyDinoz(dinoz, optExistingArmyDinoz, boss)) {
            UUID bossId = UUID.fromString(boss.get().getBossId());
            int nextRank = armyDinozRepository.getNextRank(bossId);

            ArmyDinoz armyDinozToCreate = new ArmyDinoz();
            armyDinozToCreate.setArmyDinozId(UUID.randomUUID().toString());
            armyDinozToCreate.setDinozId(dinoz.get().getId());
            armyDinozToCreate.setBossId(bossId);
            armyDinozToCreate.setBossLifeLost(0);
            armyDinozToCreate.setNbAttacks(0);
            armyDinozToCreate.setMaxNbAttacks(DinoparcConstants.RAID_BOSS_ID.equals(bossId) ? DinoparcConstants.RAID_BOSS_ARMY_DINOZ_MAX_NB_ATTACK : DinoparcConstants.BOSS_ARMY_DINOZ_MAX_NB_ATTACK);
            armyDinozToCreate.setMasterId(UUID.fromString(player.getId()));
            armyDinozToCreate.setRank(nextRank);

            return armyDinozRepository.createArmyDinoz(armyDinozToCreate);
        }

        return null;
    }

    public boolean deleteArmyDinozAndUpdateRanks(ArmyDinoz armyDinoz) {
        var optBoss = bossRepository.getBoss(armyDinoz.getBossId());

        if (canDeleteArmyDinoz(optBoss)) {
            armyDinozRepository.deleteArmyDinozAndUpdateRanks(UUID.fromString(armyDinoz.getArmyDinozId()), armyDinoz.getBossId(), armyDinoz.getRank());
            return true;
        }
        return false;
    }

    private boolean canDeleteArmyDinoz(Optional<EventDinozDto> optBoss) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        return optBoss.isPresent() &&
            (now.getHour() < optBoss.get().getHourOfDay() - 1 || (now.getHour() == optBoss.get().getHourOfDay() - 1 && now.getMinute() < 50));
    }

    private boolean canAddArmyDinoz(Optional<Dinoz> dinoz, Optional<ArmyDinoz> optExistingArmyDinoz, Optional<EventDinozDto> boss) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        return boss.isPresent()
                && boss.get().getLife() > 0
                && !boss.get().getArmyAttacking()
                && optExistingArmyDinoz.isEmpty()
                && dinoz.isPresent()
                && dinoz.get().getPlaceNumber() == boss.get().getPlaceNumber()
                && dinoz.get().getLife() > 0
                && boss.get().getDayOfWeek() == now.getDayOfWeek().getValue()
                && (now.getHour() < boss.get().getHourOfDay() - 1 || (now.getHour() == boss.get().getHourOfDay() - 1 && now.getMinute() < 50));
    }

    public List<ArmyDinoz> getPlayerArmyDinozs(String playerId) {
        return armyDinozRepository.getArmyDinozsFromMaster(UUID.fromString(playerId));
    }

    public Optional<ArmyDinoz> getArmyDinoz(String bossId, String dinozId) {
        return armyDinozRepository.getArmyDinoz(UUID.fromString(bossId), dinozId);
    }

    public Optional<ArmyDinozOverview> getOverview(String bossId) {
        var boss = bossRepository.getBoss(UUID.fromString(bossId));

        if (boss.isEmpty()) {
            return Optional.empty();
        }

        var armyDinozs = armyDinozRepository.getArmyDinozsDto(UUID.fromString(bossId));

        var ret = new ArmyDinozOverview();
        ret.setBoss(Dinoz.generateBoss(boss.get()));
        ret.getBoss().setMaxLife(DinoparcConstants.RAID_BOSS_MAX_LIFE);
        ret.setArmyDinozs(armyDinozs);
        return Optional.of(ret);
    }

    public boolean isInArmy(Dinoz dinoz) {
        return armyDinozRepository.isInArmy(dinoz.getId());
    }

    public Optional<EventDinozDto> getBossById(UUID bossId) {
        return bossRepository.getBoss(bossId);
    }
}
