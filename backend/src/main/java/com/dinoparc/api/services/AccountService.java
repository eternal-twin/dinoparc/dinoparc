package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.AccountController;
import com.dinoparc.api.controllers.dto.*;
import com.dinoparc.api.domain.account.Collection;
import com.dinoparc.api.domain.account.*;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.dinoz.*;
import com.dinoparc.api.domain.misc.ChaudronCraftTryResult;
import com.dinoparc.api.domain.misc.EggUtils;
import com.dinoparc.api.domain.mission.GranitMissionDao;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.migration.ImportOptions;
import com.dinoparc.api.services.migration.MigrationUtils;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;
import kotlin.Pair;
import net.eternaltwin.dinoparc.*;
import net.eternaltwin.user.UserId;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jetbrains.annotations.NotNull;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import static com.dinoparc.api.domain.dinoz.Dinoz.isDinozMaxed;
import static com.dinoparc.tables.PlayerStat.PLAYER_STAT;

@Component
public class AccountService {

  private static final List<Integer> basicDinozList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 16);
  public static final List<String> racesValidesPourSacrifice = Arrays.asList("Goupignon", "Santaz", "Ouistiti", "Serpantin", "Soufflet", "Feross", "Mahamuti");
  public static List<String> wistitiCheckers = new ArrayList<>();

  private final PgArmyDinozRepository armyDinozRepository;
  private final PgPlayerRepository playerRepository;
  private final PgPlayerMissionRepository playerMissionRepository;
  private final PgPlayerStatRepository playerStatRepository;
  private final PgDinozRepository dinozRepository;
  private final PgImportRepository importRepository;
  private final PgHistoryRepository historyRepository;
  private final PgBazarRepository bazarRepository;
  private final PgCollectionRepository collectionRepository;
  private final PgInventoryRepository inventoryRepository;
  private final PgBossRepository raidBossRepository;
  private final PgWildWistitiRepository wildWistitiRepository;
  private final PgClanRepository clanRepository;
  private final MigrationUtils migrationUtils;
  private final NeoparcConfig neoparcConfig;
  private final ChaudronService chaudronService;
  private final ClanService clanService;
  private final PlayerDinozStoreService playerDinozStoreService;
  private final DiscordService discordService;
  private final PgGranitMissionRepository granitMissionRepository;

  public static Map<Integer, Integer[]> locationsCompatibility;
  private Map<Integer, HunterGroup> hunterGroups;
  public Integer merchantLocation;

  //public static Integer halloweenHordesLocation;
  private Integer baseShinyOdds = 60; //60 is the normal value; 40 is for the event
  public List<String> availableEnnemies = new ArrayList<>();
  public List<String> hosts = new ArrayList<>();
  public List<String> labyrintheValidRaces = new ArrayList<>();

  @Autowired
  public AccountService(
          PgArmyDinozRepository armyDinozRepository,
          PgPlayerRepository playerRepository,
          PgPlayerMissionRepository playerMissionRepository, PgPlayerStatRepository playerStatRepository,
          PgBazarRepository bazarRepository,
          PgDinozRepository dinozRepository,
          PgImportRepository importRepository,
          PgHistoryRepository historyRepository,
          PgCollectionRepository collectionRepository,
          PgInventoryRepository inventoryRepository,
          PgBossRepository raidBossRepository,
          PgWildWistitiRepository wildWistitiRepository,
          PgClanRepository clanRepository,
          MigrationUtils migrationUtils,
          NeoparcConfig neoparcConfig,
          ChaudronService chaudronService,
          PlayerDinozStoreService playerDinozStoreService,
          ClanService clanService,
          DiscordService discordService,
          PgGranitMissionRepository granitMissionRepository) {

    this.armyDinozRepository = armyDinozRepository;
    this.playerRepository = playerRepository;
    this.playerMissionRepository = playerMissionRepository;
    this.playerStatRepository = playerStatRepository;
    this.bazarRepository = bazarRepository;
    this.dinozRepository = dinozRepository;
    this.importRepository = importRepository;
    this.historyRepository = historyRepository;
    this.collectionRepository = collectionRepository;
    this.inventoryRepository = inventoryRepository;
    this.raidBossRepository = raidBossRepository;
    this.wildWistitiRepository = wildWistitiRepository;
    this.clanRepository = clanRepository;
    this.migrationUtils = migrationUtils;
    this.neoparcConfig = neoparcConfig;
    this.chaudronService = chaudronService;
    this.playerDinozStoreService = playerDinozStoreService;
    this.clanService = clanService;
    this.discordService = discordService;
    this.granitMissionRepository = granitMissionRepository;

    merchantLocation = ThreadLocalRandom.current().nextInt(0, 37 + 1);
    //halloweenHordesLocation = ThreadLocalRandom.current().nextInt(0, 37 + 1);
    initHunterGroups();

    locationsCompatibility = new HashMap<Integer, Integer[]>();
    locationsCompatibility.put(0, new Integer[] {1, 4});
    locationsCompatibility.put(1, new Integer[] {0, 2});
    locationsCompatibility.put(2, new Integer[] {1, 3});
    locationsCompatibility.put(3, new Integer[] {2, 4});
    locationsCompatibility.put(4, new Integer[] {0, 3});
    locationsCompatibility.put(5, new Integer[] {4, 6, 7});
    locationsCompatibility.put(6, new Integer[] {5});
    locationsCompatibility.put(7, new Integer[] {5});
    locationsCompatibility.put(8, new Integer[] {7, 9, 11});
    locationsCompatibility.put(9, new Integer[] {8, 10, 11, 37});
    locationsCompatibility.put(10, new Integer[] {9});
    locationsCompatibility.put(11, new Integer[] {8, 9});
    locationsCompatibility.put(12, new Integer[] {11, 13});
    locationsCompatibility.put(13, new Integer[] {0, 12, 14});
    locationsCompatibility.put(14, new Integer[] {13});
    locationsCompatibility.put(15, new Integer[] {16});
    locationsCompatibility.put(16, new Integer[] {15, 17, 19});
    locationsCompatibility.put(17, new Integer[] {16, 20});
    locationsCompatibility.put(18, new Integer[] {16, 21});
    locationsCompatibility.put(19, new Integer[] {16, 21});
    locationsCompatibility.put(20, new Integer[] {17});
    locationsCompatibility.put(21, new Integer[] {19});
    locationsCompatibility.put(22, new Integer[] {11});
    locationsCompatibility.put(23, new Integer[] {24});
    locationsCompatibility.put(24, new Integer[] {23, 25, 26});
    locationsCompatibility.put(25, new Integer[] {24});
    locationsCompatibility.put(26, new Integer[] {24, 27, 29});
    locationsCompatibility.put(27, new Integer[] {26, 28});
    locationsCompatibility.put(28, new Integer[] {27});
    locationsCompatibility.put(29, new Integer[] {26});
    locationsCompatibility.put(30, new Integer[] {31, 33, 39});
    locationsCompatibility.put(31, new Integer[] {30, 32});
    locationsCompatibility.put(32, new Integer[] {31});
    locationsCompatibility.put(33, new Integer[] {30, 34, 35});
    locationsCompatibility.put(34, new Integer[] {33, 35});
    locationsCompatibility.put(35, new Integer[] {33, 34, 36});
    locationsCompatibility.put(36, new Integer[] {35});
    locationsCompatibility.put(37, new Integer[] {9});

    for (int race = 0; race <= 21; race++) {
      if (ThreadLocalRandom.current().nextInt(0, 7) == 0 || race == 11) {
        labyrintheValidRaces.add(String.valueOf(race));
      }
    }
  }

  private void initHunterGroups() {
    this.hunterGroups = new HashMap<Integer, HunterGroup>();
    this.hunterGroups.put(1, new HunterGroup());
    this.hunterGroups.put(2, new HunterGroup());
    this.hunterGroups.put(3, new HunterGroup());
    this.hunterGroups.put(4, new HunterGroup());
    this.hunterGroups.put(5, new HunterGroup());
    this.hunterGroups.put(6, new HunterGroup());

    for (Player account : playerRepository.findAll()) {
      if (account.getHunterGroup() != null && account.getHunterGroup() != 0) {
        var hunterGroup = this.hunterGroups.get(account.getHunterGroup());
        hunterGroup.getAccountIds().add(account.getId());
      }
    }

    this.getHunterGroups().get(1).setHour(ThreadLocalRandom.current().nextInt(0, 6 + 1)); //7 creneaux au total en G1
    this.getHunterGroups().get(2).setHour(ThreadLocalRandom.current().nextInt(7, 13 + 1)); //7 creneaux au total en G2
    this.getHunterGroups().get(3).setHour(ThreadLocalRandom.current().nextInt(14, 18 + 1)); //5 creneaux au total en G3
    this.getHunterGroups().get(4).setHour(ThreadLocalRandom.current().nextInt(19, 21 + 1)); //3 creneaux au total en G4
    this.getHunterGroups().get(5).setHour(ThreadLocalRandom.current().nextInt(22, 23 + 1)); //2 creneaux au total en G5
    this.getHunterGroups().get(6).setHour(99);
  }

  public Optional<Dinoz> refreshStateAndGetDinoz(Player account, String dinozId) {
    var optPlayerDinoz = dinozRepository.findByIdAndPlayerId(dinozId, account.getId());

    if (optPlayerDinoz.isPresent()) {
      Dinoz dinoz = optPlayerDinoz.get();
      dinoz.getActionsMap().put(DinoparcConstants.HERO_SIBILIN, false);

      if (dinoz.getPlaceNumber() == this.getMerchantLocation()) {
        dinoz.getActionsMap().put("Merchant", true);
      } else {
        dinoz.getActionsMap().put("Merchant", false);
      }

      if (dinoz.getExperience() >= 100 && isDinozMaxed(dinoz, !getAvailableLearnings(dinoz).isEmpty())) {
        dinoz.setExperience(0);
        dinoz.getActionsMap().put(DinoparcConstants.LEVELUP, false);
      }

      if (dinoz.getMalusList().contains(DinoparcConstants.COULIS_CERISE)) {
        Long minutesLeft = ChronoUnit.MINUTES.between(ZonedDateTime.now(ZoneId.of("Europe/Paris")), ZonedDateTime.ofInstant(Instant.ofEpochSecond(dinoz.getEpochSecondsEndOfCherryEffect()),ZoneId.of("Europe/Paris")));
        dinoz.setCherryEffectMinutesLeft(minutesLeft);
        if (minutesLeft < 0) {
          dinoz.getMalusList().remove(DinoparcConstants.COULIS_CERISE);
          dinoz.setCherryEffectMinutesLeft(0);
        }
      }

      if (dinoz.getMalusList().contains(DinoparcConstants.BIERE)) {
        Integer beerEquipped = Collections.frequency(dinoz.getMalusList(), DinoparcConstants.BIERE);
        if (beerEquipped > 1) {
          dinoz.getMalusList().removeAll(Collections.singleton(DinoparcConstants.BIERE));
          dinoz.getMalusList().add(DinoparcConstants.BIERE);
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BIERE, (beerEquipped - 1));
        }
      }

      if (dinoz.getPlaceNumber() == 19 && dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
        dinoz.getActionsMap().put(DinoparcConstants.DINOFOUNTAIN, true);
      }

      if (dinoz.getPlaceNumber() == 19 && accountHasEggs(account)) {
        dinoz.getActionsMap().put(DinoparcConstants.EGG_HATCHER, true);
      } else {
        dinoz.getActionsMap().put(DinoparcConstants.EGG_HATCHER, false);
      }

      if (dinoz.getPlaceNumber() == 18) {
        dinoz.getActionsMap().put(DinoparcConstants.TELESCOPE, true);
      } else {
        dinoz.getActionsMap().put(DinoparcConstants.TELESCOPE, false);
      }

      if (dinoz.getPlaceNumber() == 7) {
        Optional<GranitMissionDao> granitMissionData = granitMissionRepository.getMissionData(UUID.fromString(account.getId()));
        if (granitMissionData.isEmpty()) {
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, true);

        } else if (!granitMissionData.get().getCompleteAndValidated()) {
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT_MISSION_STATUS, true);
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, false);

        } else if (granitMissionData.get().getCompleteAndValidated()) {
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT_MISSION_STATUS, false);
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, false);
        }
      }

      EventDinozDto raidBoss = raidBossRepository.getRaidBoss();
      Boolean isRaidBossKO = raidBoss.getLife() <= 0;
      if (dinoz.getPlaceNumber() == 37 && isRaidBossKO) {
        dinoz.getActionsMap().put(DinoparcConstants.PLAINS_RAID_EXCHANGE, true);
      } else if (dinoz.getPlaceNumber() == 37 && !isRaidBossKO) {
        dinoz.getActionsMap().put(DinoparcConstants.PLAINS_RAID_EXCHANGE, false);
      }

      //For Halloween Event only :
//      if (dinoz.getPlaceNumber() == getHalloweenHordesLocation()) {
//        dinoz.getActionsMap().put(DinoparcConstants.HORDES, true);
//      } else {
//        dinoz.getActionsMap().put(DinoparcConstants.HORDES, false);
//        if (ThreadLocalRandom.current().nextInt(1, 5000) == 1) {
//          dinoz.getActionsMap().put(DinoparcConstants.HERO_SIBILIN, true);
//        }
//      }
      dinoz.getActionsMap().put(DinoparcConstants.HORDES, false);

      //For XMAS Event only :
      if (dinoz.getPlaceNumber() == 0) {
        var giftQty = inventoryRepository.getQty(account.getId(), DinoparcConstants.GIFT);
        if (giftQty > 0) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_GIFT, true);
        } else if (giftQty == 0) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_GIFT, false);
        }
      }

      if (dinoz.getPlaceNumber() == 12 && inventoryRepository.getQty(account.getId(), DinoparcConstants.PRUNIAC) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ENTER_DEMON, true);
      } else {
        dinoz.getActionsMap().put(DinoparcConstants.ENTER_DEMON, false);
      }

      if (!AccountController.connectedToday.contains(account.getName())) {
        AccountController.connectedToday.add(account.getName());
      }

      loadBossInfos(dinoz);
      manageArmyDinozActions(dinoz, dinoz.getPlaceNumber());
      manageClanActionsIfPlayerInClan(dinoz);

      return Optional.of(dinoz);
    }
    return null;
  }

  private void manageClanActionsIfPlayerInClan(Dinoz dinoz) {
    Player player = playerRepository.findById(dinoz.getMasterId()).get();
    dinoz.getActionsMap().put(DinoparcConstants.FACTION_1_ACTION, false);
    dinoz.getActionsMap().put(DinoparcConstants.FACTION_2_ACTION, false);
    dinoz.getActionsMap().put(DinoparcConstants.FACTION_3_ACTION, false);
    if (playerRepository.checkIfPlayerIsInAClan(player.getId()).getInAClan() && DinoparcConstants.WAR_MODE) {
      Clan playerClan = clanRepository.getClanOfPlayer(player.getId());
      if (playerClan != null
              && playerClan.getFaction() != null
              && !playerClan.getFaction().equals(DinoparcConstants.NO_FACTION)) {

        if (dinoz.getPlaceNumber() == 18 && (DinoparcConstants.FACTION_1.equalsIgnoreCase(playerClan.getFaction()))) {
          dinoz.getActionsMap().put(DinoparcConstants.FACTION_1_ACTION, true);
        } else if (dinoz.getPlaceNumber() == 6 && (DinoparcConstants.FACTION_2.equalsIgnoreCase(playerClan.getFaction()))) {
          dinoz.getActionsMap().put(DinoparcConstants.FACTION_2_ACTION, true);
        } else if (dinoz.getPlaceNumber() == 12 && (DinoparcConstants.FACTION_3.equalsIgnoreCase(playerClan.getFaction()))) {
          dinoz.getActionsMap().put(DinoparcConstants.FACTION_3_ACTION, true);
        }
      }
    }
  }

  private void loadBossInfos(Dinoz dinoz) {
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    List<EventDinozDto> bossList = raidBossRepository.findByPlaceNumberAndDayOfWeek(dinoz.getPlaceNumber(), now.getDayOfWeek().getValue());

    if (bossList.isEmpty()) {
      dinoz.setBossInfos(null);
    } else {
      DinozBossInfosDto bossInfos = new DinozBossInfosDto();
      bossInfos.setBossCategory("RAID");
      bossInfos.setBossId(bossList.get(0).getBossId());
      bossInfos.setArmyDinoz(armyDinozRepository.getArmyDinoz(UUID.fromString(bossList.get(0).getBossId()), dinoz.getId()).orElse(null));
      dinoz.setBossInfos(bossInfos);
    }
  }

  private void toggleSpecificLocationActions(Dinoz dinoz) {
    checkForCherryEffectsInteractionWithSpecialMove(dinoz);
    manageArmyDinozActions(dinoz, dinoz.getPlaceNumber());

    switch (dinoz.getPlaceNumber()) {
      case 0:
        // Dinoville
        dinoz.getActionsMap().put(DinoparcConstants.TournoiDinoville, true);
        dinoz.getActionsMap().put(DinoparcConstants.POSTEMISSIONS, true);

        //For XMAS Event only :
        if (dinoz.getPlaceNumber() == 0 && inventoryRepository.getQty(dinoz.getMasterId(), DinoparcConstants.GIFT) > 0) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_GIFT, true);
        }
        break;

      case 1:
        // Caverne de Madame Irma
        dinoz.getActionsMap().put(DinoparcConstants.MADAME_IRMA, true);
        break;

      case 2:
        // Clairière
        dinoz.getActionsMap().put(DinoparcConstants.CATAPULTE, true);
        break;

      case 3:
        // Dinoplage
        dinoz.getActionsMap().put(DinoparcConstants.TournoiDinoplage, true);
        dinoz.getActionsMap().put(DinoparcConstants.DINOBATH, true);
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)
                && dinoz.getSkillsMap().containsKey(DinoparcConstants.NATATION)
                && dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == 5) {
          dinoz.getActionsMap().put(DinoparcConstants.PASSAGEBALEINE, true);
        }
        break;

      case 4:
        // Barrage
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, true);
        }
        break;

      case 5:
        // Falaises
        dinoz.getActionsMap().put(DinoparcConstants.ARENA, true);
        break;

      case 6:
        // Mont Dino
        dinoz.getActionsMap().put(DinoparcConstants.TournoiMontDino, true);
        break;

      case 7:
        // Granit
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
        }

        Optional<GranitMissionDao> granitMissionData = granitMissionRepository.getMissionData(UUID.fromString(dinoz.getMasterId()));
        if (granitMissionData.isEmpty()) {
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, true);
        } else if (!granitMissionData.get().getCompleteAndValidated()) {
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT_MISSION_STATUS, true);
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, false);
        } else if (granitMissionData.get().getCompleteAndValidated()) {
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT_MISSION_STATUS, false);
          dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, false);
        }

        break;

      case 8:
        // Gredins
        dinoz.getActionsMap().put(DinoparcConstants.CASINO, true);
        break;

      case 9:
        // Forêt
        dinoz.getActionsMap().put(DinoparcConstants.NPCDinozBuyer, true);
        break;

      case 10:
        // Temple
        dinoz.getActionsMap().put(DinoparcConstants.TournoiTemple, true);
        break;

      case 11:
        // PortPrune
        dinoz.getActionsMap().put(DinoparcConstants.PRUNE_SHOP, true);
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
          dinoz.getActionsMap().put(DinoparcConstants.ACT_NAVIGUER, true);
        }
        break;

      case 12:
        // Pitié
        if (dinoz.getLevel() >= 25
                && dinoz.getLevel() <= 117
                && racesValidesPourSacrifice.contains(dinoz.getRace())
                && inventoryRepository.getQty(dinoz.getMasterId(), DinoparcConstants.POTION_SOMBRE) == 0
                && !playerHasAtLeastOneActiveListingWithDarkPotion(dinoz.getMasterId())) {
          dinoz.getActionsMap().put(DinoparcConstants.SACRIFICE, true);
        }
        break;

      case 13:
        // Mayinca
        dinoz.getActionsMap().put(DinoparcConstants.TournoiRuines, true);
        dinoz.getActionsMap().put(DinoparcConstants.CERBERE, false);
        if (dinoz.getSkillsMap().get(DinoparcConstants.CUISINE) != null && dinoz.getSkillsMap().get(DinoparcConstants.CUISINE) >= 4) {
          dinoz.getActionsMap().put(DinoparcConstants.CHAUDRON, true);
        }
        break;

      case 14:
        // Crédit
        dinoz.getActionsMap().put(DinoparcConstants.CREDIT, true);
        break;

      case 15:
        // Bazar
        dinoz.getActionsMap().put(DinoparcConstants.BAZAR, true);
        break;

      case 16:
        // Marais
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        }
        break;

      case 17:
        // Jungle
        break;

      case 18:
        // Bordeciel
        break;

      case 19:
        // Source
        if (dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
          dinoz.getActionsMap().put(DinoparcConstants.DINOFOUNTAIN, true);
        }
        if (accountHasEggs(getAccountById(dinoz.getMasterId()).get())) {
          dinoz.getActionsMap().put(DinoparcConstants.EGG_HATCHER, true);
        }
        break;

      case 20:
        // CaverneAnomalie
        dinoz.getActionsMap().put(DinoparcConstants.ACT_DELTAPLANER, true);
        dinoz.getActionsMap().put(DinoparcConstants.ANOMALY_SHOP, true);
        dinoz.getActionsMap().put(DinoparcConstants.FUSION_CENTER, true);
        break;

      case 21:
        // Hutte
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        }
        dinoz.getActionsMap().put(DinoparcConstants.ACT_DELTAPLANER, true);
        break;

      case 22:
        // ToutChaud
        dinoz.getActionsMap().put(DinoparcConstants.CRATER_SHOP, true);
        break;

      case 23:
        // Bastion de Défense :
        dinoz.getActionsMap().put(DinoparcConstants.TAVERNE, true);
        break;

      case 24:
        // Port Royal :
        dinoz.getActionsMap().put(DinoparcConstants.PAMAGUERRE, true);
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) != null && dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) >= 5) {
          dinoz.getActionsMap().put(DinoparcConstants.MARCHÉPIRATE, true);
        }
        break;

      case 25:
        // Sun Tzu :

        break;

      case 26:
        // Labyrinthe :
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, true);
        }
        break;

      case 27:
        // Bosquet Baveux :

        break;

      case 28:
        // Stiou :

        break;

      case 29:
        // Université :

        break;

      case 30:
        // Labo Dangalf :

        break;

      case 31:
        // Puits d'Igor :

        break;

      case 32:
        // Mont Everouest :

        break;

      case 33:
        // Lac Céleste :
        Player player = playerRepository.findPlayerByDinozId(dinoz.getId());
        if (!player.getHasClaimedTradeFromFishermanToday() && inventoryRepository.getQty(player.getId(), DinoparcConstants.RAMENS) > 0) {
          dinoz.getActionsMap().put(DinoparcConstants.INUIT, true);
        }
        break;

      case 34:
        // Repaire Mahamuti :

        break;

      case 35:
        // Village Fantôme :

        break;

      case 36:
        // Dinocropole :
        if (dinoz.getActionsMap().get(DinoparcConstants.COMBAT)) {
          dinoz.getActionsMap().put(DinoparcConstants.COMBAT, false);
        }
        break;

      case 37:
        // Plaines du Roi Rokky
        EventDinozDto raidBoss = raidBossRepository.getRaidBoss();

        dinoz.getActionsMap().put(DinoparcConstants.PLAINS_SHOP, true);

        // Exchange is available only if boss is KO
        if (raidBoss.getLife() <= 0) {
          dinoz.getActionsMap().put(DinoparcConstants.PLAINS_RAID_EXCHANGE, true);
        }
        break;

      default:
        break;
    }

    if (dinoz.getExperience() >= 100 && isDinozMaxed(dinoz, !getAvailableLearnings(dinoz).isEmpty())) {
      dinoz.getActionsMap().put(DinoparcConstants.LEVELUP, true);
    }

    if (dinoz.getPlaceNumber() == this.getMerchantLocation()) {
      dinoz.getActionsMap().put("Merchant", true);
    }

//    if (dinoz.getPlaceNumber() == getHalloweenHordesLocation()) {
//      dinoz.getActionsMap().put(DinoparcConstants.HORDES, true);
//    }

    manageClanActionsIfPlayerInClan(dinoz);
  }

  private void manageArmyDinozActions(Dinoz dinoz, int placeNumber) {
    dinoz.getActionsMap().put(DinoparcConstants.ACT_ARMY_JOIN_ARMY, false);
    dinoz.getActionsMap().put(DinoparcConstants.ACT_ARMY_QUIT_ARMY, false);
    dinoz.getActionsMap().put(DinoparcConstants.ACT_ARMY_SEE_ARMY, false);

    if (dinoz.getBossInfos() != null) {
      var boss = raidBossRepository.getBoss(UUID.fromString(dinoz.getBossInfos().getBossId())).get();
      ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));

      // Army can be seen on raid day at good location
      dinoz.getActionsMap().put(DinoparcConstants.ACT_ARMY_SEE_ARMY, true);

      // Join or quit army is possible raid day at location until the end of raid if player didn't fight the boss
      long dailyRaidFights = playerStatRepository.getCounter(UUID.fromString(dinoz.getMasterId()), "nb_daily_raid_fight");

      if (dailyRaidFights == 0 &&
              (now.getHour() < boss.getHourOfDay()
                || (boss.getHourOfDay() == now.getHour() && now.getMinute() < 30))) {
        if (dinoz.getBossInfos().getArmyDinoz() != null) {
          dinoz.getActionsMap().put(DinoparcConstants.ACT_ARMY_QUIT_ARMY, true);
        } else if (armyDinozRepository.getArmyDinozsFromMaster(UUID.fromString(dinoz.getMasterId())).isEmpty()){
          dinoz.getActionsMap().put(DinoparcConstants.ACT_ARMY_JOIN_ARMY, true);
        }
      }
    }
  }

  public Boolean playerHasAtLeastOneActiveListingWithDarkPotion(String playerId) {
    return bazarRepository.playerHasAtLeastOneActiveListingWithDarkPotion(playerId);
  }

  public List<Dinoz> availableEnnemiesFromTelescope(Player player, Integer location, Dinoz myDinoz) {
    int myDinozLvl = myDinoz.getLevel();
    int minLvl = 0;
    int maxLvl = 0;

    if (myDinoz.getLevel() <= 25) {
      minLvl = myDinozLvl - 4;
      maxLvl = myDinozLvl + 9;
    } else if (myDinoz.getLevel() > 25 && myDinoz.getLevel() <= 100) {
      minLvl = myDinozLvl - 7;
      maxLvl = myDinozLvl + 15;
    } else {
      minLvl = myDinozLvl - 20;
      maxLvl = myDinozLvl + 200;
    }

    Integer myPerceptionLevel = myDinoz.getSkillsMap().get(DinoparcConstants.PERCEPTION);
    if (myPerceptionLevel == null) {
      myPerceptionLevel = 0;
    }

    List<Dinoz> possibleFighters = new ArrayList<Dinoz>();
    if (!player.isBanned() && !player.isBannedPvp()) {
      //Display clan ennemies :
      if (DinoparcConstants.WAR_MODE
              && clanService.checkIfDinozIsWarrior(myDinoz.getMalusList())
              && locationIsNotProtectedFromWar(location)
              && playerRepository.checkIfPlayerIsInAClan(player.getId()).getInAClan()) {
        possibleFighters.addAll(clanService.getEnnemyDinozInThatLocation(player.getId(), location, myDinoz.getLastFledEnnemy(), myDinoz.getLevel()));
        possibleFighters
                .stream()
                .forEach(ennemyFromClan -> ennemyFromClan.setTaggedAsClanEnnemy(true));

      } else {
        //Display normal Dinoz in that location :
        possibleFighters.addAll(
                dinozRepository.getFighters(player.getId(), location, minLvl, maxLvl, myPerceptionLevel + 2, myDinoz.getLastFledEnnemy())
                        .stream()
                        .filter(opponent -> !clanService.checkIfDinozIsWarrior(opponent.getMalusList()))
                        .toList());
      }
    }

    playerRepository.updateCash(player.getId(), -100);
    return possibleFighters;
  }

  public List<Dinoz> getAvailableEnnemies(Player account, String dinozId, Integer life, Integer xp) {
    Dinoz myDinoz = dinozRepository.findById(dinozId).get();
    EventDinozDto raidBoss = raidBossRepository.getRaidBoss();
    playerStatRepository.addStats(UUID.fromString(account.getId()), dinozId, List.of(new Pair(PgPlayerStatRepository.NB_SKIP, 1)));

    if (!account.isBanned()) {
      if (raidBossIsVisible(account, myDinoz, raidBoss)) {
        return List.of(Dinoz.generateActualRaidBoss(raidBoss));
      }

      EventDinozDto wildWistiti = wildWistitiRepository.getWildWistiti();
      if (myDinoz.getPlaceNumber() == 17 && wildWistitiIsVisible(account, wildWistiti, false)) {
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_WISTITI_SKIP, 1)));
        return List.of(Dinoz.generateActualW(wildWistiti));
      }
    }

    int myDinozLvl = myDinoz.getLevel();
    int minLvl = 0;
    int maxLvl = 0;

    if (myDinoz.getLevel() <= 25) {
      minLvl = myDinozLvl - 4;
      maxLvl = myDinozLvl + 9;
    } else if (myDinoz.getLevel() > 25 && myDinoz.getLevel() <= 100) {
      minLvl = myDinozLvl - 7;
      maxLvl = myDinozLvl + 15;
    } else {
      minLvl = myDinozLvl - 20;
      maxLvl = myDinozLvl + 200;
    }

    Integer myPerceptionLevel = myDinoz.getSkillsMap().get(DinoparcConstants.PERCEPTION);
    if (myPerceptionLevel == null) {
      myPerceptionLevel = 0;
    }

    List<Dinoz> possibleFighters = new ArrayList<Dinoz>();

    if (!kabukiQuestCompleted(account)) {
      Optional<Dinoz> wildKabuki = checkToSpawnWildKabukiFromQuest(myDinoz);
      if (wildKabuki.isPresent()) {
        possibleFighters.add(wildKabuki.get());
      }
    }

    if (shouldSpawnManny(account)) {
      Optional<Dinoz> wildManny = checkToSpawnWildMannyFromQuest(myDinoz);
      if (wildManny.isPresent()) {
        possibleFighters.add(wildManny.get());
      }
    }

    if (!account.isBanned() && !account.isBannedPvp() && !myDinoz.isBotOnly()) {
      //Display clan ennemies :
      if (DinoparcConstants.WAR_MODE
              && clanService.checkIfDinozIsWarrior(myDinoz.getMalusList())
              && locationIsNotProtectedFromWar(myDinoz.getPlaceNumber())
              && playerRepository.checkIfPlayerIsInAClan(account.getId()).getInAClan()) {
        possibleFighters.addAll(clanService.getEnnemyDinozInThatLocation(account.getId(), myDinoz.getPlaceNumber(), myDinoz.getLastFledEnnemy(), myDinoz.getLevel()));
        possibleFighters
                .stream()
                .forEach(ennemyFromClan -> ennemyFromClan.setTaggedAsClanEnnemy(true));

      } else {
        //Display normal Dinoz in that location :
        possibleFighters.addAll(
                dinozRepository.getFighters(account.getId(), myDinoz.getPlaceNumber(), minLvl, maxLvl, myPerceptionLevel + 2, myDinoz.getLastFledEnnemy())
                        .stream()
                        .filter(opponent -> !clanService.checkIfDinozIsWarrior(opponent.getMalusList()))
                        .toList());
      }
    }

    addARandomBotDinoz(account, myDinoz, possibleFighters, playerRepository.countDinoz(account.getId()));
    if (hosts.contains(myDinoz.getId())) {
      availableEnnemies.add("Life " + life + ", XP " + xp);
    }

    return possibleFighters;
  }

  private boolean locationIsNotProtectedFromWar(int placeNumber) {
    if (placeNumber == 6 || placeNumber == 12 || placeNumber == 18) {
      return false;
    }
    return true;
  }

  public boolean wildWistitiIsVisible(Player account, EventDinozDto wildWistiti, boolean isFightCall) {
    Integer hunterGroup = account.getHunterGroup();
    Long statWistitiSkip = playerStatRepository.getCounter(UUID.fromString(account.getId()), "nb_wistiti_skip");
    OffsetDateTime lastWistitiFight = account.getLastWistitiFight();

    boolean lastFightTooRecent = isFightCall
            && lastWistitiFight != null
            && lastWistitiFight.isAfter(OffsetDateTime.now(ZoneId.of("Europe/Paris")).minusNanos(900000000));

    return !account.isBannedWistiti()
      && hunterGroup != null
      && wildWistiti.getLife() > 0
      && isValidHunterGroup(hunterGroup, account)
      && statWistitiSkip <= 5
      && !lastFightTooRecent;
  }

  public boolean isValidHunterGroup(Integer hunterGroup, Player account) {
    ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    Boolean minuteIsValid = currentTime.getMinute() <= 4;

    if (minuteIsValid && !wistitiCheckers.contains(account.getName())) {
      wistitiCheckers.add(account.getName());
    }

    if (hunterGroups.get(hunterGroup).getHour() == currentTime.getHour() && minuteIsValid && groupIsNotExcluded(hunterGroup)) {
      return true;
    }

    return false;
  }

  public boolean groupIsNotExcluded(Integer hunterGroup) {
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    //Group #5 is never skipped (Only Groups 1-2-3-4 can be skipped) :
    if (hunterGroup != null && hunterGroup < 5) {
      Rng rng = neoparcConfig.getRngFactory()
              .string("groupIsNotExcludedV6")
              .string(hunterGroup.toString())
              .string(String.valueOf(now.getDayOfYear()) + String.valueOf(now.getYear()))
              .toRng();
      switch (hunterGroup) {
        case 1 :
          return (rng.randIntBetween(1, 100) < (100 - 50));
        case 2 :
          return (rng.randIntBetween(1, 100) < (100 - 50));
        case 3 :
          return (rng.randIntBetween(1, 100) < (100 - 45));
        case 4 :
          return (rng.randIntBetween(1, 100) < (100 - 40));
        default :
          return true;
      }
    }
    return true;
  }

  public boolean raidBossIsVisible(Player account, Dinoz myDinoz, EventDinozDto raidBoss) {
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    var optArmyDinoz = armyDinozRepository.getArmyDinozFromMaster(DinoparcConstants.RAID_BOSS_ID, UUID.fromString(account.getId()));

    return (!account.isBannedRaid()
            && optArmyDinoz.isEmpty()
            && myDinoz.getPlaceNumber() == raidBoss.getPlaceNumber()
            && raidBoss.getDayOfWeek() == now.getDayOfWeek().getValue()
            && raidBoss.getHourOfDay() == now.getHour()
            && now.getMinute() < 30
            && raidBoss.getLife() > 0);
  }

  private void addARandomBotDinoz(Player account, Dinoz myDinoz, List<Dinoz> possibleFighters, int playerNbDinoz) {
    Integer shinyDinozOddsDenominator = baseShinyOdds;
    if (myDinoz.getSkillsMap().get(DinoparcConstants.CHASSEUR) != null && myDinoz.getSkillsMap().get(DinoparcConstants.CHASSEUR) >= 1) {
      shinyDinozOddsDenominator = baseShinyOdds - ((myDinoz.getSkillsMap().get(DinoparcConstants.CHASSEUR)) * 2);
    }

    Dinoz lastValidBot = Dinoz.generateARandomDinoz(myDinoz, playerNbDinoz, shinyDinozOddsDenominator, account.isBanned() || account.isBannedShiny());
    BotDinozDto bdt = new BotDinozDto();
    bdt.setFeu(lastValidBot.getElementsValues().get(DinoparcConstants.FEU));
    bdt.setTerre(lastValidBot.getElementsValues().get(DinoparcConstants.TERRE));
    bdt.setEau(lastValidBot.getElementsValues().get(DinoparcConstants.EAU));
    bdt.setFoudre(lastValidBot.getElementsValues().get(DinoparcConstants.FOUDRE));
    bdt.setAir(lastValidBot.getElementsValues().get(DinoparcConstants.AIR));
    bdt.setAppearanceCode(lastValidBot.getAppearanceCode());

    dinozRepository.setLastValidBotHash(myDinoz.getId(), bdt.hashCode());
    possibleFighters.add(lastValidBot);
  }

  private Optional<Dinoz> checkToSpawnWildKabukiFromQuest(Dinoz myDinoz) {
    Dinoz wildKabuki = new Dinoz();
    wildKabuki.setDanger(Integer.MAX_VALUE);

    if (myDinoz.getPlaceNumber() == 4 && myDinoz.getKabukiProgression() == 0) {
      //Spawn Jeune Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_0_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_0_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 7 && myDinoz.getKabukiProgression() == 1) {
      //Spawn Apprenti Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_1_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_1_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 16 && myDinoz.getKabukiProgression() == 2) {
      //Spawn Novice Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_2_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_2_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 6 && myDinoz.getKabukiProgression() == 3) {
      //Spawn Guerrier Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_3_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_3_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 3 && myDinoz.getKabukiProgression() == 4) {
      //Spawn Soldat Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_4_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_4_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 20 && myDinoz.getKabukiProgression() == 5) {
      //Spawn Commando Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_5_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_5_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 21 && myDinoz.getKabukiProgression() == 6) {
      //Spawn Mage Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_6_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_6_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 2 && myDinoz.getKabukiProgression() == 7) {
      //Spawn Sorcier Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_7_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_7_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 19 && myDinoz.getKabukiProgression() == 8) {
      //Spawn General Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_8_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_8_APPCODE);
      return Optional.of(wildKabuki);

    } else if (myDinoz.getPlaceNumber() == 0 && myDinoz.getKabukiProgression() == 9) {
      //Spawn Boss Kabuki;
      wildKabuki.setId(DinoparcConstants.KABUKI_9_ID);
      wildKabuki.setAppearanceCode(DinoparcConstants.KABUKI_9_APPCODE);
      return Optional.of(wildKabuki);
    }

    return Optional.empty();
  }

  private Optional<Dinoz> checkToSpawnWildMannyFromQuest(Dinoz myDinoz) {
    Dinoz wildManny = new Dinoz();
    wildManny.setDanger(Integer.MAX_VALUE);
    GranitMissionDao granitMissionData = granitMissionRepository.getMissionData(UUID.fromString(myDinoz.getMasterId())).get();

    if (myDinoz.getPlaceNumber() == granitMissionData.getMannyLocation() && myDinoz.getLevel() <= 5 && granitMissionData.getMannyLife() > 0) {
      wildManny.setId(DinoparcConstants.MANNY_ID);
      wildManny.setAppearanceCode(DinoparcConstants.MANNY_APPCODE);
      wildManny.setLife(granitMissionData.getMannyLife());
      wildManny.setPlaceNumber(granitMissionData.getMannyLocation());
      return Optional.of(wildManny);
    }
    return Optional.empty();
  }

  public Player createAndInitializeAccount(CreateAccountDto createDto, String accountId) throws Exception {
    Player newAccount = new Player();
    newAccount.setId(accountId);
    newAccount.setName(createDto.getUsernameInscription());
    newAccount.setCash(DinoparcConstants.INITIALMONEY);

    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.POTION_IRMA, 3);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.POTION_ANGE, 1);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.NUAGE_BURGER, 3);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.PAIN_CHAUD, 1);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.TARTE_VIANDE, 1);

    newAccount.setUnlockedDinoz(basicDinozList);
    newAccount.setNbPoints(0);
    newAccount.setWistitiCaptured(0);
    newAccount.setHermitStage(1);
    newAccount.setHermitStageCurrentWins(0);
    newAccount.setDailyShiniesFought(0);
    newAccount.setAverageLevelPoints(0);
    newAccount.setBanned(false);
    newAccount.setBlocked(false);
    newAccount.setHelpToggle(true);
    newAccount.setHasClaimedTradeFromFishermanToday(false);
    playerRepository.create(newAccount);
    playerRepository.updateImgMode(newAccount.getId(), PlayerImgModeEnum.BASE_64.name());
    sendDiscordMessageForNewPlayerArrival(newAccount.getName());

    return newAccount;
  }

  public Player createAndInitializeAccount(String username, String accountId) {
    Player newAccount = new Player();
    newAccount.setId(accountId);
    newAccount.setName(username);
    newAccount.setCash(DinoparcConstants.INITIALMONEY);
    newAccount.setLastLogin(ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond());

    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.POTION_IRMA, 3);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.POTION_ANGE, 1);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.NUAGE_BURGER, 3);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.PAIN_CHAUD, 1);
    inventoryRepository.addInventoryItem(accountId, DinoparcConstants.TARTE_VIANDE, 1);

    newAccount.setUnlockedDinoz(basicDinozList);
    newAccount.setNbPoints(0);
    newAccount.setWistitiCaptured(0);
    newAccount.setHermitStage(1);
    newAccount.setHermitStageCurrentWins(0);
    newAccount.setDailyShiniesFought(0);
    newAccount.setNbPoints(0);
    newAccount.setAverageLevelPoints(0);
    newAccount.setBanned(false);
    newAccount.setBlocked(false);
    newAccount.setHelpToggle(true);
    newAccount.setHasClaimedTradeFromFishermanToday(false);
    newAccount.setLastPirateBuy("never");
    playerRepository.create(newAccount);
    playerRepository.updateImgMode(newAccount.getId(), PlayerImgModeEnum.BASE_64.name());
    sendDiscordMessageForNewPlayerArrival(newAccount.getName());

    return newAccount;
  }

  private void sendDiscordMessageForNewPlayerArrival(String playerName) {
    EmbedCreateSpec embedDiscordMessage = EmbedCreateSpec
            .builder()
            .color(Color.PINK)
            .author("Welcome " + playerName + "!", "https://neoparc.eternaltwin.org", "https://i.ibb.co/FqM1hyz/anim-new.gif")
            .thumbnail("https://i.ibb.co/Tg3VBCj/vieux2.jpg")
            .title("A new player has just registered on Neoparc! " + "Welcome " + playerName + "!")
            .footer("Generated by the Neoparc Server", "https://i.ibb.co/bB74rVG/hist-reset.gif")
            .build();

    discordService.postEmbedMessage(DinoparcConstants.CHANNEL_CERBERE, embedDiscordMessage);
  }

  public List<Player> getAllAccounts() {
    Iterable<Player> allAccounts = playerRepository.findAll();
    List<Player> result = new ArrayList<Player>();
    allAccounts.forEach(result::add);

    return result;
  }

  public DigResultDto processDiggingForDinoz(Player account, Dinoz dinoz) {
    DigResultDto result = new DigResultDto();
    Integer collectionObjectRelatedToLocation = getObjectForLocation(dinoz.getPlaceNumber());
    Collection collection = collectionRepository.getPlayerCollection(UUID.fromString(account.getId()));
    var playerCollection = collection.getCollection();

    if (collectionObjectRelatedToLocation == 36
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == 5
        && !playerCollection.contains("36")) {

      // Étoile Laba
      result.setDigCode(1);
      result.setDigObject(collectionObjectRelatedToLocation);
      playerCollection.add(String.valueOf(collectionObjectRelatedToLocation));
      collectionRepository.updateCollection(collection.getId(), playerCollection);
      checkForJazzQuestCompletion(collection, playerCollection, account);
    }

    else if (collectionObjectRelatedToLocation == 37 && dinoz.getExperience() >= 100
        && !playerCollection.contains("37")) {

      // Étoile Du Fond
      result.setDigCode(1);
      result.setDigObject(collectionObjectRelatedToLocation);
      playerCollection.add(String.valueOf(collectionObjectRelatedToLocation));
      collectionRepository.updateCollection(collection.getId(), playerCollection);
      checkForJazzQuestCompletion(collection, playerCollection, account);
    }

    else if (collectionObjectRelatedToLocation == 38
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FEU) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FEU) > 0
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) > 0
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_EAU) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_EAU) > 0
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FOUDRE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FOUDRE) > 0
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_AIR) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_AIR) > 0
        && !playerCollection.contains("38")) {

      // Étoile Des Neiges
      result.setDigCode(1);
      result.setDigObject(collectionObjectRelatedToLocation);
      playerCollection.add(String.valueOf(collectionObjectRelatedToLocation));
      collectionRepository.updateCollection(collection.getId(), playerCollection);
      checkForJazzQuestCompletion(collection, playerCollection, account);
    }

    else if (collectionObjectRelatedToLocation == 39 && dinoz.getLife() > 100
        && !playerCollection.contains("39")) {

      // Étoile De Mer
      result.setDigCode(1);
      result.setDigObject(collectionObjectRelatedToLocation);
      playerCollection.add(String.valueOf(collectionObjectRelatedToLocation));
      collectionRepository.updateCollection(collection.getId(), playerCollection);
      checkForJazzQuestCompletion(collection, playerCollection, account);
    }

    else if (collectionObjectRelatedToLocation == 40
        && dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)
        && dinoz.getRace().equalsIgnoreCase(DinoparcConstants.PTEROZ)
        && !playerCollection.contains("40")) {

      // Étoile D'Araignées
      result.setDigCode(1);
      result.setDigObject(collectionObjectRelatedToLocation);
      playerCollection.add(String.valueOf(collectionObjectRelatedToLocation));
      collectionRepository.updateCollection(collection.getId(), playerCollection);
      checkForJazzQuestCompletion(collection, playerCollection, account);

    } else {
      Integer random = ThreadLocalRandom.current().nextInt(1, 101 + 1);
      Integer digPowerFactor = (5 * (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER)));

      if (random <= 10) {
        //10% Trouver un objet de collection :
        digCollectionObject(account, dinoz, collection, playerCollection, result, collectionObjectRelatedToLocation);

      } else if (random == 101) {
        //Une chance sur 101 ; Trouver un objet inusité :
        processDiggingForExtraRareItem(account, result, dinoz.getPlaceNumber());

      } else if (random <= (40 - digPowerFactor)
              && (!dinoz.getRace().equalsIgnoreCase(DinoparcConstants.PTEROZ))
              && (!dinoz.getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU))) {
        //Échouer une fouille selon ton niveau de fouille et ta race :
        result.setDigCode(3);

      } else if ((random > (40 - digPowerFactor) && random <= 100)
              || (dinoz.getRace().equalsIgnoreCase(DinoparcConstants.PTEROZ))
              || (dinoz.getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU))) {
        //Trouver un élément du Totem :
        processDiggingForTotemItem(account, dinoz, result);

      } else {
        //Échouer une fouille selon ton niveau de fouille et ta race :
        result.setDigCode(3);
      }
    }

    playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_DIG, 1)));
    dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, false);
    dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());

    return result;
  }

  private void processDiggingForExtraRareItem(Player account, DigResultDto result, Integer placeNumber) {
    Integer playerNbDinoz = playerRepository.countDinoz(account.getId());
    if (playerNbDinoz >= 750 && ThreadLocalRandom.current().nextBoolean()) {
      result.setDigCode(3);

    } else {       //Rare objets diggable by location or special conditions are starting at ID 101.
      if (placeNumber == 23 ) {
        // Canne à pêche au Bastion de Défense :
        result.setDigCode(4);
        result.setDigObject(101);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CANNEAPECHE, 1);

      } else {
        // Fouilles rares normales :
        Integer utraRareFound = ThreadLocalRandom.current().nextInt(1, 3 + 1);
        result.setDigCode(4);
        result.setDigObject(utraRareFound);

        switch (utraRareFound) {
          case 1 :
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BAVE_LOUPI, 1);
            break;
          case 2 :
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.MONO_CHAMPIFUZ, 1);
            break;
          case 3 :
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PRUNIAC, 1);
            break;
          default :
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BAVE_LOUPI, 1);
            break;
        }
      }
    }
  }

  private void processDiggingForTotemItem(Player account, Dinoz dinoz, DigResultDto result) {
    if (dinoz.getRace().equalsIgnoreCase(DinoparcConstants.PTEROZ)) {
      result.setDigCode(2);
      Integer haz = ThreadLocalRandom.current().nextInt(1, 32 + 1);
      if (haz <= 16) {
        result.setDigObject(1);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
      } else if (haz > 16 && haz <= 25) {
        result.setDigObject(2);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PLUME, 1);
      } else if (haz > 25 && haz <= 29) {
        result.setDigObject(3);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.DENT, 1);
      } else if (haz > 29 && haz <= 31) {
        result.setDigObject(4);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CRIN, 1);
      } else {
        result.setDigObject(5);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BOIS, 1);
      }

    } else {
      Integer digLevel = dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER);

      switch (digLevel) {
        case 1:
          result.setDigCode(2);
          result.setDigObject(1);
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
          break;

        case 2:
          result.setDigCode(2);
          Integer haz1 = ThreadLocalRandom.current().nextInt(1, 25 + 1);
          if (haz1 <= 16) {
            result.setDigObject(1);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
          } else if (haz1 > 16 && haz1 <= 25) {
            result.setDigObject(2);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PLUME, 1);
          }
          break;

        case 3:
          result.setDigCode(2);
          Integer haz2 = ThreadLocalRandom.current().nextInt(1, 29 + 1);
          if (haz2 <= 16) {
            result.setDigObject(1);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
          } else if (haz2 > 16 && haz2 <= 25) {
            result.setDigObject(2);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PLUME, 1);
          } else if (haz2 > 25 && haz2 <= 29) {
            result.setDigObject(3);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.DENT, 1);
          }
          break;

        case 4:
          result.setDigCode(2);
          result.setDigObject(ThreadLocalRandom.current().nextInt(1, 4));
          Integer haz3 = ThreadLocalRandom.current().nextInt(1, 31 + 1);
          if (haz3 <= 16) {
            result.setDigObject(1);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
          } else if (haz3 > 16 && haz3 <= 25) {
            result.setDigObject(2);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PLUME, 1);
          } else if (haz3 > 25 && haz3 <= 29) {
            result.setDigObject(3);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.DENT, 1);
          } else if (haz3 > 29 && haz3 <= 31) {
            result.setDigObject(4);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CRIN, 1);
          }
          break;

        case 5:
          result.setDigCode(2);
          result.setDigObject(ThreadLocalRandom.current().nextInt(1, 4));
          Integer haz4 = ThreadLocalRandom.current().nextInt(1, 32 + 1);
          if (haz4 <= 16) {
            result.setDigObject(1);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
          } else if (haz4 > 16 && haz4 <= 25) {
            result.setDigObject(2);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PLUME, 1);
          } else if (haz4 > 25 && haz4 <= 29) {
            result.setDigObject(3);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.DENT, 1);
          } else if (haz4 > 29 && haz4 <= 31) {
            result.setDigObject(4);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CRIN, 1);
          } else {
            result.setDigObject(5);
            inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BOIS, 1);
          }
          break;

        default:
          result.setDigCode(2);
          result.setDigObject(1);
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BUZE, 1);
      }
    }
  }

  public void startGranitMission(Player player) {
    if (granitMissionRepository.getMissionData(UUID.fromString(player.getId())).isEmpty()) {
      granitMissionRepository.create(UUID.fromString(player.getId()));
    }
  }

  public GranitMissionDao getGranitMissionData(UUID playerId) {
    return granitMissionRepository.getMissionData(playerId).get();
  }

  public void claimGranitMission(Player player) {
    UUID playerId = UUID.fromString(player.getId());
    granitMissionRepository.setCompletedAndValidated(playerId);
    granitMissionRepository.setCompleted(playerId);

    var collection = collectionRepository.getPlayerCollection(playerId);
    var epicCollection = collection.getEpicCollection();

    if (!epicCollection.contains("33")) {
      epicCollection.add("33");
      collectionRepository.updateEpicCollection(collection.getId(), epicCollection);
    }
  }

  private void checkForJazzQuestCompletion(Collection collection, List<String> playerCollection, Player account) {
    if (playerCollection.contains("36") && playerCollection.contains("37")
        && playerCollection.contains("38") && playerCollection.contains("39")
        && playerCollection.contains("40")) {

      collection.getEpicCollection().add("1");
      collectionRepository.updateEpicCollection(collection.getId(), collection.getEpicCollection());
    }
  }

  private Integer getObjectForLocation(int placeNumber) {
    switch (placeNumber) {
      case 0: //Dinoville
        return 48;
      case 1: //Irma
        return 17;
      case 2: //Clairière
        return 27;
      case 3: //Dinoplage
        return 18;
      case 4: //Barrage
        return 29;
      case 5: //Falaises
        return 26;
      case 6: //Mont Dino
        return 49;
      case 7: //Granit
        return 16;
      case 8: //Gredins
        return 20;
      case 9: //Forêt
        return 30;
      case 10: //Temple
        return 32;
      case 11: //Port
        return 24;
      case 12: //Pitié
        return 47;
      case 13: //Ruines
        return 43;
      case 14: //Crédit
        return 44;
      case 15: //Bazar
        return 21;
      case 16: //Marais
        return 22;
      case 17: //Jungle
        return 36;
      case 18: //Bordeciel
        return 37;
      case 19: //Source
        return 38;
      case 20: //Anomalie
        return 39;
      case 21: //Hutte
        return 40;
      case 22: //Grand Tout-Chaud
        return 23;
      case 37: //Roi Rokky
        return 45;

      default:
        return 0;
    }
  }

  private void digCollectionObject(Player player, Dinoz dinoz, Collection collection, List<String> playerCollection, DigResultDto result, Integer collectionObjectRelatedToLocation) {
    if (collectionObjectRelatedToLocation != 0) {
      if (playerCollection.contains(String.valueOf(collectionObjectRelatedToLocation))) {
        processDiggingForTotemItem(player, dinoz, result);

      } else {
        if (collectionObjectRelatedToLocation != 36 && collectionObjectRelatedToLocation != 37
            && collectionObjectRelatedToLocation != 38 && collectionObjectRelatedToLocation != 39
            && collectionObjectRelatedToLocation != 40) {
          result.setDigCode(1);
          result.setDigObject(collectionObjectRelatedToLocation);
          playerCollection.add(String.valueOf(collectionObjectRelatedToLocation));
          collectionRepository.updateCollection(collection.getId(), playerCollection);
        } else {
          processDiggingForTotemItem(player, dinoz, result);
        }
      }

    } else {
      processDiggingForTotemItem(player, dinoz, result);
    }
  }

  public void respawnAllDinozAtDinotown(Player account, List<String> playerArmyDinozIds) {
    for (Dinoz dinoz : getAllDinozOfAccount(account)) {
      if (dinoz.getLife() < 1 && !playerArmyDinozIds.contains(dinoz.getId())) {
        respawnAtDinotown(dinoz);
      }
    }
  }

  public void respawnAtDinotown(Dinoz dinoz) {
    if (dinoz.isInTourney()) {
      String actualTourneyName = Tournament.getTournamentByPlaceNumber(dinoz.getPlaceNumber());
      Tournament tourneyInstance = dinoz.getTournaments().get(actualTourneyName);
      tourneyInstance.setActualPoints(0);
      dinozRepository.setTournaments(dinoz.getId(), dinoz.getTournaments(), false);
    }

    Boolean combat = false;
    Boolean deplace = false;
    Boolean fouiller = false;
    Boolean rock = false;

    dinozRepository.setLife(dinoz.getId(), 30);
    dinoz.setLife(30);
    dinozRepository.setPlaceNumber(dinoz.getId(), 0);
    dinoz.setPlaceNumber(0);
    dinozRepository.setExperience(dinoz.getId(), 0);
    dinoz.setExperience(0);

    if (dinoz.getMalusList() != null && dinoz.getMalusList().contains(DinoparcConstants.KABUKI_QUETE)) {
      dinoz.getMalusList().remove(DinoparcConstants.KABUKI_QUETE);
      dinozRepository.setKabukiProgression(dinoz.getId(), dinoz.getMalusList(), 0);
    }

    if (dinoz.getActionsMap().get(DinoparcConstants.COMBAT) != null && dinoz.getActionsMap().get(DinoparcConstants.COMBAT)) {
      combat = true;
    }

    if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT) != null && dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
      deplace = true;
    }

    if (dinoz.getActionsMap().get(DinoparcConstants.FOUILLER) != null && dinoz.getActionsMap().get(DinoparcConstants.FOUILLER)) {
      fouiller = true;
    }

    if (dinoz.getActionsMap().get(DinoparcConstants.ROCK) != null && dinoz.getActionsMap().get(DinoparcConstants.ROCK)) {
      rock = true;
    }

    dinoz.getActionsMap().clear();
    loadBossInfos(dinoz);
    toggleSpecificLocationActions(dinoz);

    if (combat) {
      dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    } else {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    }

    if (deplace) {
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
    } else {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    }

    if (fouiller) {
      dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    } else if (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) != null && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) >= 1) {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    }

    if (rock) {
      dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
    } else if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) >= 1) {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    }

    if (dinoz.getActionsMap().get(DinoparcConstants.BAIN_FLAMMES) != null && dinoz.getActionsMap().get(DinoparcConstants.BAIN_FLAMMES)) {
      dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, false);
    }

    dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());

    if (dinoz.getDanger() > 1) {
      dinozRepository.setDanger(dinoz.getId(), dinoz.getDanger() / 2);
    } else if (dinoz.getDanger() < 0) {
      dinozRepository.setDanger(dinoz.getId(), 0);
    }

    if (dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM) && ThreadLocalRandom.current().nextInt(1, 10 + 1) == 1) {
      dinoz.getMalusList().remove(DinoparcConstants.CHIKABUM);
      dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
    }
  }

  public boolean buyObjects(Player account, BuyRequestDto buyRequest) throws IllegalAccessException {
    if (account.getCash() >= Integer.parseInt(buyRequest.getTotalPrice())) {
      Integer totalPriceCheck = Integer.valueOf(0);

      var accountPricesMap = getShop(account).getPricesMap();

      totalPriceCheck += (Integer.parseInt(buyRequest.getIrmaBuy()) * accountPricesMap.get(DinoparcConstants.POTION_IRMA).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getAngeBuy()) * accountPricesMap.get(DinoparcConstants.POTION_ANGE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getBurgerBuy()) * accountPricesMap.get(DinoparcConstants.NUAGE_BURGER).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getPainBuy()) * accountPricesMap.get(DinoparcConstants.PAIN_CHAUD).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getTarteBuy()) * accountPricesMap.get(DinoparcConstants.TARTE_VIANDE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getMedailleBuy()) * accountPricesMap.get(DinoparcConstants.MEDAILLE_CHOCOLAT).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getFeuBuy()) * accountPricesMap.get(DinoparcConstants.CHARME_FEU).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getTerreBuy()) * accountPricesMap.get(DinoparcConstants.CHARME_TERRE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getEauBuy()) * accountPricesMap.get(DinoparcConstants.CHARME_EAU).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getFoudreBuy()) * accountPricesMap.get(DinoparcConstants.CHARME_FOUDRE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getAirBuy()) * accountPricesMap.get(DinoparcConstants.CHARME_AIR).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getGriffesBuy()) * accountPricesMap.get(DinoparcConstants.GRIFFES_EMPOISONNÉES).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getLoupiBuy()) * accountPricesMap.get(DinoparcConstants.BAVE_LOUPI).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getRamensBuy()) * accountPricesMap.get(DinoparcConstants.RAMENS).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getTigerBuy()) * accountPricesMap.get(DinoparcConstants.OEIL_DU_TIGRE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getAntidoteBuy()) * accountPricesMap.get(DinoparcConstants.ANTIDOTE).intValue());

      if (totalPriceCheck.intValue() == Integer.parseInt(buyRequest.getTotalPrice()) && buyRequest.isValid()) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA, Integer.parseInt(buyRequest.getIrmaBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_ANGE, Integer.parseInt(buyRequest.getAngeBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.NUAGE_BURGER, Integer.parseInt(buyRequest.getBurgerBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PAIN_CHAUD, Integer.parseInt(buyRequest.getPainBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.TARTE_VIANDE, Integer.parseInt(buyRequest.getTarteBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.MEDAILLE_CHOCOLAT, Integer.parseInt(buyRequest.getMedailleBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_FEU, Integer.parseInt(buyRequest.getFeuBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_TERRE, Integer.parseInt(buyRequest.getTerreBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_EAU, Integer.parseInt(buyRequest.getEauBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_FOUDRE, Integer.parseInt(buyRequest.getFoudreBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_AIR, Integer.parseInt(buyRequest.getAirBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.GRIFFES_EMPOISONNÉES, Integer.parseInt(buyRequest.getGriffesBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BAVE_LOUPI, Integer.parseInt(buyRequest.getLoupiBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.RAMENS, Integer.parseInt(buyRequest.getRamensBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.OEIL_DU_TIGRE, Integer.parseInt(buyRequest.getTigerBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.ANTIDOTE, Integer.parseInt(buyRequest.getAntidoteBuy()));

        playerRepository.updateCash(account.getId(), -totalPriceCheck);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPriceCheck)));

        History buyEvent = new History();
        buyEvent.setPlayerId(account.getId());
        buyEvent.setType("buyObject");
        buyEvent.setIcon("hist_buy.gif");
        buyEvent.setBuyAmount(totalPriceCheck);
        historyRepository.save(buyEvent);

        return true;
      }
    }
    return false;
  }

  public Dinoz getDinozById(String dinozId) {
    Optional<Dinoz> dinoz = dinozRepository.findById(dinozId);
    if (dinoz.isPresent()) {
      return dinoz.get();

    } else {
      return null;
    }
  }

  public List<Dinoz> getAllDinozOfAccount(Player account) {
    return dinozRepository.findByPlayerId(account.getId());
  }

  public List<Dinoz> getAllDinozOfAccount(String playerId) {
    return dinozRepository.findByPlayerId(playerId);
  }

  public List<RestrictedDinozDto> getAllRestrictedDinozOfAccountFromGivenPage(Player account, Integer pageNumber, boolean isOwner) {
    return dinozRepository.findRestrictedByRank(account.getId(), (pageNumber * 30) + 1, (pageNumber + 1) * 30, isOwner);
  }

  public Integer getInventoryIrmaQty(Player account) {
    return inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA) + inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS);
  }

  public List<Integer> getInventoryBothIrmasTypesQty(Player account) {
    List<Integer> irmasQty = new ArrayList<>();
    irmasQty.add(inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA));
    irmasQty.add(inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS));
    return irmasQty;
  }

  public InventoryDto getInventory(Player account) {
    InventoryDto inventory = new InventoryDto();
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      DinozId dinozId = new DinozId();
      dinozId.setDinozId(dinoz.getId());
      dinozId.setDinozName(dinoz.getName());
      dinozId.setDinozLife(dinoz.getLife());
      inventory.getDinozList().add(dinozId);
    }

    inventory.setInventoryItemsMap(inventoryRepository.getAll(account.getId()));
    return inventory;
  }

  public Optional<Dinoz> getNextDinozInList(Player account, String dinozId) {
    var optPlayerDinoz = dinozRepository.findByIdAndPlayerId(dinozId, account.getId());

    if (optPlayerDinoz.isPresent()) {
      Integer indexOfCurrentDinoz = playerRepository.findPlayerDinozRank(account.getId(), dinozId);
      var optNextDinoz = dinozRepository.findByRankAndPlayerId(indexOfCurrentDinoz + 1, account.getId());

      return optNextDinoz.isPresent() ? optNextDinoz : optPlayerDinoz;
    }

    return null;
  }

  public Optional<Dinoz> getPreviousDinozInList(Player account, String dinozId) {
    var optPlayerDinoz = dinozRepository.findByIdAndPlayerId(dinozId, account.getId());

    if (optPlayerDinoz.isPresent()) {
      Integer indexOfCurrentDinoz = playerRepository.findPlayerDinozRank(account.getId(), dinozId);
      var optNextDinoz = dinozRepository.findByRankAndPlayerId(indexOfCurrentDinoz - 1, account.getId());

      return optNextDinoz.isPresent() ? optNextDinoz : optPlayerDinoz;
    }

    return null;
  }

  public String spendOneChocolateEgg(Player player) {
    String prizeInsideEgg = DinoparcConstants.MEDAILLE_CHOCOLAT;
    Integer qtyOfInstances = 1;
    Integer draw = ThreadLocalRandom.current().nextInt(1, 100 + 1);

    if (draw <= 40) {
      //40% chance medals (1 to 5 medals)
      prizeInsideEgg = DinoparcConstants.MEDAILLE_CHOCOLAT;
      qtyOfInstances = ThreadLocalRandom.current().nextInt(1, 5 + 1);
      inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.MEDAILLE_CHOCOLAT, qtyOfInstances);

    } else if (draw <= 70) {
      //30% chance golds (1 to 99,000 gold coins)
      prizeInsideEgg = "GOLD";
      qtyOfInstances = getRandomAmountOfGoldWonForEgg();
      playerRepository.updateCash(player.getId(), qtyOfInstances);

    } else if (draw <= 91) {
      //20% chance champifuz entre 5 et 15
      prizeInsideEgg = DinoparcConstants.CHAMPIFUZ;
      qtyOfInstances = ThreadLocalRandom.current().nextInt(5, 15 + 1);
      inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.CHAMPIFUZ, qtyOfInstances);

    } else if (draw <= 96) {
      //5% chance Ticket Monochrome x1
      prizeInsideEgg = DinoparcConstants.MONO_CHAMPIFUZ;
      qtyOfInstances = 1;
      inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.MONO_CHAMPIFUZ, qtyOfInstances);

    } else if (draw <= 98) {
      //2% chance eternity pills x1
      prizeInsideEgg = DinoparcConstants.ETERNITY_PILL;
      qtyOfInstances = 1;
      inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.ETERNITY_PILL, qtyOfInstances);

    } else if (draw == 99) {
      //1% chance Coulis x1
      prizeInsideEgg = DinoparcConstants.COULIS_CERISE;
      qtyOfInstances = 1;
      inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.COULIS_CERISE, 1);

    } else if (draw == 100) {
      //1% chance Oeuf Cobalt x1
      prizeInsideEgg = DinoparcConstants.OEUF_COBALT;
      qtyOfInstances = 1;
      inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.OEUF_COBALT, 1);
    }

    inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.OEUF_CHOCOLAT, 1);
    return prizeInsideEgg + ";" + String.valueOf(qtyOfInstances);
  }

  public Integer getRandomAmountOfGoldWonForEgg() {
    Integer amount = ThreadLocalRandom.current().nextInt(2500, 9000 + 1);
    Integer cashDraw = ThreadLocalRandom.current().nextInt(1, 100 + 1);
    if (cashDraw <= 50) {
      return ThreadLocalRandom.current().nextInt(amount, amount * 3);
    } else if (cashDraw <= 90) {
      return ThreadLocalRandom.current().nextInt(amount, amount * 6);
    } else if (cashDraw < 100) {
      //Lucky draw
      return ThreadLocalRandom.current().nextInt(amount * 6, amount * 11);
    } else if (cashDraw == 100) {
      //Unlucky draw :
      return ThreadLocalRandom.current().nextInt(1, 5);
    }
    return amount;
  }

  public String hatchEgg(Player account, String eggType, String name) {
    Dinoz hatchedDinoz = new Dinoz();
    hatchedDinoz.setId(UUID.randomUUID().toString());
    hatchedDinoz.setMasterId(account.getId());
    hatchedDinoz.setMasterName(account.getName());
    hatchedDinoz.setName(name);
    hatchedDinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
    hatchedDinoz.setEndMessage(DinoparcConstants.ENDMESSAGE1);
    hatchedDinoz.setLife(100);
    hatchedDinoz.setExperience(0);
    hatchedDinoz.setLevel(1);
    hatchedDinoz.setDanger(0);
    hatchedDinoz.setPlaceNumber(19);
    hatchedDinoz.setDark(false);
    hatchedDinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    hatchedDinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
    String race = EggUtils.getDinozRaceFromEggType(eggType);
    hatchedDinoz.setRace(race);
    String generatedRaceCharCode = EggUtils.getDinozRaceCharCodeFromEggType(race);

    String ceriseFlag = "";
    if (eggType.equalsIgnoreCase(DinoparcConstants.OEUF_COBALT)) {
      ceriseFlag += "$";
    }

    hatchedDinoz.setAppearanceCode(generatedRaceCharCode + DinozUtils.getRandomHexString() + ceriseFlag);
    initializeHatchedDinoz(hatchedDinoz);
    dinozRepository.create(hatchedDinoz);

    inventoryRepository.substractInventoryItem(account.getId(), eggType, 1);
    inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PRUNIAC, 1);

    playerRepository.addDinoz(account.getId(), hatchedDinoz.getId());
    playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_EGG_HATCH, 1)));

    return hatchedDinoz.getAppearanceCode();
  }

  public void initializeHatchedDinoz(Dinoz hatchedDinoz) {
    switch (String.valueOf(hatchedDinoz.getAppearanceCode().charAt(0))) {
      case "B":
        hatchedDinoz.getElementsValues().put(DinoparcConstants.FEU, 1);
        hatchedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 2);
        hatchedDinoz.getElementsValues().put(DinoparcConstants.EAU, 3);
        hatchedDinoz.getSkillsMap().put(DinoparcConstants.SOLUBILITE, 1);
        break;

      case "E":
        hatchedDinoz.getElementsValues().put(DinoparcConstants.FEU, 1);
        hatchedDinoz.getElementsValues().put(DinoparcConstants.EAU, 2);
        hatchedDinoz.getElementsValues().put(DinoparcConstants.AIR, 1);
        break;

      case "I":
        hatchedDinoz.getElementsValues().put(DinoparcConstants.FEU, 5);
        hatchedDinoz.getSkillsMap().put(DinoparcConstants.PROTECTIONDUFEU, 1);
        break;

      case "K":
        hatchedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 4);
        hatchedDinoz.getElementsValues().put(DinoparcConstants.EAU, 2);
        hatchedDinoz.getSkillsMap().put(DinoparcConstants.FORTERESSE, 1);
        break;

      default:
        break;
    }

    hatchedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.FEU, 0);
    hatchedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.TERRE, 0);
    hatchedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.EAU, 0);
    hatchedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.FOUDRE, 0);
    hatchedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.AIR, 0);
  }

  public ShopDto getShop(Player account) {
    ShopDto shopDto = new ShopDto();
    shopDto.setAccountCashAmount(account.getCash());
    shopDto.setQuantityMap(inventoryRepository.getAll(account.getId()));

    Integer combinedMerchantPoints = 0;
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.COMMERCE) && dinoz.getLife() > 0) {
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) > 0) {
          combinedMerchantPoints += dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE);
        }
      }
    }

    if (combinedMerchantPoints > 10) {
      combinedMerchantPoints = 10;
    }

    double discount = 0;
    switch (combinedMerchantPoints) {
      case 1:
        discount = 0.99;
        applyDiscount(shopDto, discount);
        break;

      case 2:
        discount = 0.98;
        applyDiscount(shopDto, discount);
        break;

      case 3:
        discount = 0.97;
        applyDiscount(shopDto, discount);
        break;

      case 4:
        discount = 0.96;
        applyDiscount(shopDto, discount);
        break;

      case 5:
        discount = 0.95;
        applyDiscount(shopDto, discount);
        break;

      case 6:
        discount = 0.94;
        applyDiscount(shopDto, discount);
        break;

      case 7:
        discount = 0.93;
        applyDiscount(shopDto, discount);
        break;

      case 8:
        discount = 0.92;
        applyDiscount(shopDto, discount);
        break;

      case 9:
        discount = 0.91;
        applyDiscount(shopDto, discount);
        break;

      case 10:
        discount = 0.90;
        applyDiscount(shopDto, discount);
        break;

      default:
        discount = 1;
        applyDiscount(shopDto, discount);
        break;
    }

    return shopDto;
  }

  private Boolean accountHasEggs(Player account) {
    Integer og = inventoryRepository.getQty(account.getId(), DinoparcConstants.OEUF_GLUON);
    Integer ost = inventoryRepository.getQty(account.getId(), DinoparcConstants.OEUF_SANTAZ);
    Integer osp = inventoryRepository.getQty(account.getId(), DinoparcConstants.OEUF_SERPANTIN);
    Integer ox = inventoryRepository.getQty(account.getId(), DinoparcConstants.OEUF_COBALT);
    Integer of = inventoryRepository.getQty(account.getId(), DinoparcConstants.OEUF_FEROSS);

    if (og + ost + osp + ox + of == 0) {
      return false;
    }

    return true;
  }

  private void applyDiscount(ShopDto shopDto, double discount) {
    shopDto.getPricesMap().put(DinoparcConstants.POTION_IRMA, 1000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.POTION_ANGE, 3000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.NUAGE_BURGER, 700 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.PAIN_CHAUD, 4500 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.TARTE_VIANDE, 1000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.MEDAILLE_CHOCOLAT, 2000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_FEU, 300 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_TERRE, 300 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_EAU, 300 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_FOUDRE, 300 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_AIR, 300 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.GRIFFES_EMPOISONNÉES, 700 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.BAVE_LOUPI, 1000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.RAMENS, 2000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.OEIL_DU_TIGRE, 3000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.ANTIDOTE, 400 * discount);
  }

  public Dinoz getEnnemyDinozByIds(String trace, Dinoz myDinoz) {
    Optional<Dinoz> ennemyDinoz = dinozRepository.findById(trace);

    if (!ennemyDinoz.isPresent() && trace.startsWith("BOT")) {
      Dinoz wantedBot = rebuildBotDinoz(trace.replace("#", "%23"));
      BotDinozDto wantedBotDto = new BotDinozDto();
      wantedBotDto.setFeu(wantedBot.getElementsValues().get(DinoparcConstants.FEU));
      wantedBotDto.setTerre(wantedBot.getElementsValues().get(DinoparcConstants.TERRE));
      wantedBotDto.setEau(wantedBot.getElementsValues().get(DinoparcConstants.EAU));
      wantedBotDto.setFoudre(wantedBot.getElementsValues().get(DinoparcConstants.FOUDRE));
      wantedBotDto.setAir(wantedBot.getElementsValues().get(DinoparcConstants.AIR));
      wantedBotDto.setAppearanceCode(wantedBot.getAppearanceCode());

      if (wantedBotDto.hashCode() == myDinoz.getLastValidBotHash() && myDinoz.getLastValidBotHash() != 0) {
        dinozRepository.setLastValidBotHash(myDinoz.getId(), 0);
        // Uncomment following line if shinies rate is not static
        //dinozRepository.setLastValidBotHashByMasterName(myDinoz.getMasterName(), 0);
        wantedBot.setPlaceNumber(myDinoz.getPlaceNumber());
        return wantedBot;

      } else {
        return null;
      }

    } else if (!ennemyDinoz.isPresent() && trace.startsWith("@") && !trace.startsWith("@MANNY")) {
      return rebuildWildKabuki(trace, myDinoz);

    } else if (!ennemyDinoz.isPresent() && trace.startsWith("@MANNY")) {
      return rebuildManny(trace, myDinoz);

    } else if (!ennemyDinoz.isPresent() && clanService.checkIfDinozIsWarrior(myDinoz.getMalusList())) {
      return clanService.occupationSummary.getAlivePVEWarEnnemies().stream().filter(dinoz -> dinoz.getId().equals(trace)).findFirst().get();

    } else {
      Dinoz realEnnemyPvpDinoz = ennemyDinoz.get();
      if (realEnnemyPvpDinoz.getSkillsMap().get(DinoparcConstants.AGILITÉ) != null && realEnnemyPvpDinoz.getSkillsMap().get(DinoparcConstants.AGILITÉ) >= 1) {
          Integer agilityLevel = realEnnemyPvpDinoz.getSkillsMap().get(DinoparcConstants.AGILITÉ);
        if (ThreadLocalRandom.current().nextInt(1, 100) <= agilityLevel) {
          dinozRepository.setLastFledEnnemy(myDinoz.getId(), realEnnemyPvpDinoz.getId());

          History fledHistoryEvent = new History();
          fledHistoryEvent.setPlayerId(realEnnemyPvpDinoz.getMasterId());
          fledHistoryEvent.setFromDinozName(myDinoz.getName());
          fledHistoryEvent.setFromUserName(myDinoz.getMasterName());
          fledHistoryEvent.setToDinozName(realEnnemyPvpDinoz.getName());
          fledHistoryEvent.setToUserName(realEnnemyPvpDinoz.getMasterName());
          fledHistoryEvent.setIcon("hist_defense.gif");
          fledHistoryEvent.setType("fled");
          historyRepository.save(fledHistoryEvent);

          return null;
        }
      }

      return realEnnemyPvpDinoz;
    }
  }

  private Dinoz rebuildWildKabuki(String trace, Dinoz myDinoz) {
    Optional<Dinoz> wildKabuki = checkToSpawnWildKabukiFromQuest(myDinoz);
    if (wildKabuki.isPresent() && wildKabuki.get().getId().equals(trace)) {
      Dinoz kabuki = wildKabuki.get();
      decideElementsOfTheWildKabuki(myDinoz, kabuki);
      kabuki.setBeginMessage(DinoparcConstants.BEGINMESSAGEBOT);
      kabuki.setEndMessage(DinoparcConstants.ENDMESSAGEBOT);
      kabuki.setLife(100);
      kabuki.setName(kabuki.getId());
      kabuki.setPlaceNumber(myDinoz.getPlaceNumber());
      return kabuki;
    }

    return null;
  }

  private Dinoz rebuildManny(String trace, Dinoz myDinoz) {
    Optional<Dinoz> wildManny = checkToSpawnWildMannyFromQuest(myDinoz);
    if (wildManny.isPresent() && trace.equals(wildManny.get().getId())) {
      Dinoz manny = wildManny.get();
      manny.setRace(DinoparcConstants.MAHAMUTI);
      manny.getElementsValues().put(DinoparcConstants.FEU, 0);
      manny.getElementsValues().put(DinoparcConstants.TERRE, 5);
      manny.getElementsValues().put(DinoparcConstants.EAU, 4);
      manny.getElementsValues().put(DinoparcConstants.FOUDRE, 0);
      manny.getElementsValues().put(DinoparcConstants.AIR, 3);
      manny.setBeginMessage(DinoparcConstants.BEGINMESSAGEBOT);
      manny.setEndMessage(DinoparcConstants.ENDMESSAGEBOT);
      manny.setName("Manny");
      manny.setLevel(1);
      return manny;
    }
    return null;
  }

  public Integer applyFireBathToDinoz(Dinoz dinoz) {
    Integer lifeWon = 0;
    if (dinoz.getLife() < 10) {
      Integer bathLevel = dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES);
      lifeWon = ThreadLocalRandom.current().nextInt(5, (bathLevel * 5) + 1);
      dinoz.setLife(dinoz.getLife() + lifeWon);
    }

    dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, false);
    dinozRepository.processFireBath(dinoz.getId(), dinoz.getLife(), dinoz.getActionsMap());
    return lifeWon;
  }

  public Long getDailyRaidFightsCountOfAccount(String accountId) {
    return playerStatRepository.getCounter(UUID.fromString(accountId), "nb_daily_raid_fight");
  }

  public RockDto processRockAction(Player account, Dinoz rokky, String language) {
    RockDto rockResult = new RockDto();
    Integer dangerEarnedByRokky = 0;
    Integer dangerLossByAll = 0;

    switch (rokky.getSkillsMap().get(DinoparcConstants.ROCK)) {
      case 1:
        dangerEarnedByRokky = ThreadLocalRandom.current().nextInt(100, 300 + 1);
        dangerLossByAll = ThreadLocalRandom.current().nextInt(1, 3 + 1);
        break;
      case 2:
        dangerEarnedByRokky = ThreadLocalRandom.current().nextInt(100, 280 + 1);
        dangerLossByAll = ThreadLocalRandom.current().nextInt(1, 4 + 1);
        break;
      case 3:
        dangerEarnedByRokky = ThreadLocalRandom.current().nextInt(100, 250 + 1);
        dangerLossByAll = ThreadLocalRandom.current().nextInt(1, 5 + 1);
        break;
      case 4:
        dangerEarnedByRokky = ThreadLocalRandom.current().nextInt(100, 220 + 1);
        dangerLossByAll = ThreadLocalRandom.current().nextInt(1, 6 + 1);
        break;
      case 5:
        dangerEarnedByRokky = ThreadLocalRandom.current().nextInt(100, 200 + 1);
        dangerLossByAll = ThreadLocalRandom.current().nextInt(1, 7 + 1);
        break;
      default:
        break;
    }

    switch (language) {
      case "fr":
        rockResult.setRockSummary(
            "Votre Rokky a bien attiré l'attention sur lui en rockant! Tous vos dinoz perdent "
                + dangerLossByAll + " de danger et votre Rokky en gagne " + dangerEarnedByRokky + "!");
        break;

      case "es":
        rockResult.setRockSummary(
            "¡Tu Rokky llamó la atención sobre él balanceándose! ¡Todos tus dinoz pierden "
                + dangerLossByAll + " ante el peligro y tu Rokky obtiene " + dangerEarnedByRokky + "!");
        break;

      case "en":
        rockResult
            .setRockSummary("Your Rokky drew attention to him by rocking! All your dinoz lose "
                + dangerLossByAll + " of danger and your Rokky gains " + dangerEarnedByRokky + "!");
        break;

      default:
        rockResult.setRockSummary(
            "Votre Rokky a bien attiré l'attention sur lui en rockant! Tous vos dinoz perdent "
                + dangerLossByAll + " de danger et votre Rokky en gagne " + dangerEarnedByRokky + "!");
        break;
    }

    rokky.setDanger(rokky.getDanger() + dangerEarnedByRokky);
    rokky.getActionsMap().put(DinoparcConstants.ROCK, false);
    rokky.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);

    if (rokky.getActionsMap().containsKey(DinoparcConstants.BARRAGE) && rokky.getActionsMap().get(DinoparcConstants.BARRAGE)) {
      rokky.getActionsMap().put(DinoparcConstants.BARRAGE, false);
    }

    if (rokky.getActionsMap().containsKey(DinoparcConstants.PASSAGEGRANIT) && rokky.getActionsMap().get(DinoparcConstants.PASSAGEGRANIT)) {
      rokky.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, false);
    }

    if (rokky.getActionsMap().containsKey(DinoparcConstants.PASSAGEBALEINE) && rokky.getActionsMap().get(DinoparcConstants.PASSAGEBALEINE)) {
      rokky.getActionsMap().put(DinoparcConstants.PASSAGEBALEINE, false);
    }

    if (rokky.getActionsMap().containsKey(DinoparcConstants.PASSAGELABYRINTHE) && rokky.getActionsMap().get(DinoparcConstants.PASSAGELABYRINTHE)) {
      rokky.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, false);
    }

    rokky.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    dinozRepository.processRock(rokky.getMasterId(), rokky.getId(), rokky.getDanger(), rokky.getActionsMap(), dangerLossByAll);
    rockResult.setNewDangerValue(rokky.getDanger());

    return rockResult;
  }

  public Map<Integer, String> getAvailableLocations(Player account, String dinozId) {
    Optional<Dinoz> dinoz = dinozRepository.findById(dinozId);
    HashMap<Integer, String> locations = new HashMap<Integer, String>();
    if (dinoz.isPresent()) {
      Dinoz movingDinoz = dinoz.get();
      for (Integer availableLocation : this.locationsCompatibility.get(movingDinoz.getPlaceNumber())) {
        locations.put(availableLocation, StringUtils.EMPTY);
      }
      return locations;
    }
    return null;
  }

  public Dinoz moveDinozToRequestedLocation(Dinoz dinoz, Map<Integer, String> availableLocations, String requestPlaceNumber) {
    Integer originPlaceNumber = dinoz.getPlaceNumber();

    if (availableLocations.containsKey(Integer.parseInt(requestPlaceNumber))) {
      patchMaraisStateForNewLocation(dinoz, originPlaceNumber);

      if (requestPlaceNumber.equalsIgnoreCase("39")) {
        dinoz.setPlaceNumber(Integer.parseInt("26"));
      } else {
        dinoz.setPlaceNumber(Integer.parseInt(requestPlaceNumber));
      }

      applyMaraisStateByDay(dinoz);
      applyRandomInfectionChancesIfMarais(dinoz);

      for (String actionString : dinoz.getActionsMap().keySet()) {
        if (!(actionString.equalsIgnoreCase(DinoparcConstants.COMBAT)
                || actionString.equalsIgnoreCase(DinoparcConstants.ROCK)
                || actionString.equalsIgnoreCase(DinoparcConstants.BAIN_FLAMMES)
                || actionString.equalsIgnoreCase(DinoparcConstants.DÉPLACERVERT)
                || actionString.equalsIgnoreCase(DinoparcConstants.FOUILLER)
                || actionString.equalsIgnoreCase(DinoparcConstants.LEVELUP))) {
          dinoz.getActionsMap().replace(actionString, false);
        }
      }

      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);

      if (dinoz.getSkillsMap().get(DinoparcConstants.COURSE) != null && dinoz.getSkillsMap().get(DinoparcConstants.COURSE) > 0
              && ThreadLocalRandom.current().nextInt(1, 30 + 1) <= dinoz.getSkillsMap().get(DinoparcConstants.COURSE)) {
        resetMovementForRunningDinoz(dinoz);

      } else {
        dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
        dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
      }

      //Pour les Rokkys, on enlève la possibilité de Rocker si l'on se déplace.
      if (dinoz.getActionsMap().containsKey(DinoparcConstants.ROCK) && dinoz.getActionsMap().get(DinoparcConstants.ROCK)) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      //Pour l'effet du Nectar de cerises :
      if (dinoz.getMalusList().contains(DinoparcConstants.COULIS_CERISE)) {
        Long minutesLeft = ChronoUnit.MINUTES.between(ZonedDateTime.now(ZoneId.of("Europe/Paris")), ZonedDateTime.ofInstant(Instant.ofEpochSecond(dinoz.getEpochSecondsEndOfCherryEffect()),ZoneId.of("Europe/Paris")));
        dinoz.setCherryEffectMinutesLeft(minutesLeft);
        if (minutesLeft < 0) {
          dinoz.getMalusList().remove(DinoparcConstants.COULIS_CERISE);
          dinoz.setCherryEffectMinutesLeft(0);

        } else {
          dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
        }
      }

      if (dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null
              && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) >= 1
              && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {
        dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
      }

      if (dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) != null
              && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) >= 1
              && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {
        dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
      }

      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      return dinozRepository.setActionsAndMalusAndPassiveAndPlaceNumber(dinoz.getId(), dinoz.getActionsMap(), dinoz.getMalusList(), dinoz.getPassiveList(), dinoz.getPlaceNumber());
    }

    return null;
  }

  private void resetMovementForRunningDinoz(Dinoz dinoz) {
    if (dinoz.getActionsMap().get(DinoparcConstants.COMBAT) == null || !dinoz.getActionsMap().get(DinoparcConstants.COMBAT)) {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    }
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);

    if (dinoz.getPlaceNumber() == 4) {
      dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, true);
    }

    if (dinoz.getPlaceNumber() == 7) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
    }

    if (dinoz.getPlaceNumber() == 3
            && dinoz.getSkillsMap().containsKey(DinoparcConstants.NATATION)
            && dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == 5) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEBALEINE, true);
    }

    if (dinoz.getPlaceNumber() == 26) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, true);
    }

    if (dinoz.getPlaceNumber() == 11) {
      dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
      dinoz.getActionsMap().put(DinoparcConstants.ACT_NAVIGUER, true);
    }

    if (dinoz.getPlaceNumber() == 16) {
      dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
    }
  }

  private void patchMaraisStateForNewLocation(Dinoz dinoz, Integer originPlaceNumber) {
    if (originPlaceNumber == 16) {
      LocalDate localDate = LocalDate.now();
      switch (localDate.getDayOfWeek().getValue()) {
        case 1:
          // Lundi
          dinoz.getActionsMap().replace(DinoparcConstants.DÉPLACERVERT, true);
          break;
        case 2:
          // Mardi
          break;
        case 3:
          // Mercredi
          dinoz.getActionsMap().replace(DinoparcConstants.COMBAT, true);
          break;
        case 4:
          // Jeudi
          break;
        case 5:
          // Vendredi
          dinoz.getActionsMap().replace(DinoparcConstants.COMBAT, true);
          break;
        case 6:
          // Samedi
          break;
        case 7:
          // Dimanche
          break;
        default:
          break;
      }
    }
  }

  public static void applyMaraisStateByDay(Dinoz dinoz) {
    if (dinoz.getPlaceNumber() == 16 && !dinoz.getRace().equalsIgnoreCase(DinoparcConstants.GOUPIGNON)) {
      ZoneId zid = ZoneId.of("Europe/Paris");
      LocalDate localDate = LocalDate.now(zid);

      switch (localDate.getDayOfWeek().getValue()) {
        case 1:
          // Lundi
          dinoz.getActionsMap().replace(DinoparcConstants.DÉPLACERVERT, false);
          dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
          break;
        case 2:
          // Mardi
          break;
        case 3:
          // Mercredi
          if (dinoz.getActionsMap().get(DinoparcConstants.COMBAT)) {
            dinoz.getActionsMap().replace(DinoparcConstants.COMBAT, null);
          }
          break;
        case 4:
          // Jeudi
          break;
        case 5:
          // Vendredi
          dinoz.getActionsMap().replace(DinoparcConstants.COMBAT, null);
          break;
        case 6:
          // Samedi
          break;
        case 7:
          // Dimanche
          break;
        default:
          break;
      }
    }
  }

  private void applyRandomInfectionChancesIfMarais(Dinoz dinoz) {
    if (dinoz.getPlaceNumber() == 16 && ThreadLocalRandom.current().nextInt(1, 6 + 1) == 1 && !dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
      dinoz.getMalusList().add(DinoparcConstants.CHIKABUM);
      if (dinoz.getPassiveList().containsKey(DinoparcConstants.ANTIDOTE) && dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) >= 1) {
        dinoz.getPassiveList().replace(DinoparcConstants.ANTIDOTE, dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) - 1);
        dinoz.getMalusList().remove(DinoparcConstants.CHIKABUM);
      }
    }
  }

  public Optional<Player> getAccountById(String id) {
    return playerRepository.findById(id);
  }

  public void deleteAccount(String accountId) {
    Optional<PlayerClan> playerClan = clanRepository.getPlayerClan(accountId);
    if (playerClan.isPresent()) {
      //Player has a clan, so therefore, expell him from the clan :
      clanRepository.quitClan(playerClan.get().getPlayerId());
    }

    //Delete the Dinoz of the account in question :
    for (var dinoz : dinozRepository.findByPlayerId(accountId)){
      dinozRepository.deleteById(dinoz.getId());
    }

    //Delete the inventory important objects :
    inventoryRepository.substractInventoryItem(accountId, DinoparcConstants.POTION_IRMA, 99999);
    inventoryRepository.substractInventoryItem(accountId, DinoparcConstants.BONS, 99999);

    //Delete the gold :
    playerRepository.updateCash(accountId, -99999);

    //Delete the missions :
    playerStatRepository.deletePlayerStats(UUID.fromString(accountId));
    playerMissionRepository.deletePlayerMission(UUID.fromString(accountId));

    //Delete the bazar offers :
    for (BazarListing listing : bazarRepository.findAllActive()) {
      if (listing.getLastBiderId() != null && listing.getLastBiderId().equalsIgnoreCase(accountId)) {
        bazarRepository.deleteById(listing.getId());
      } else if (listing.getSellerId().equalsIgnoreCase(accountId)) {
        bazarRepository.deleteById(listing.getId());
      }
    }

    playerRepository.deleteById(accountId);
  }

  public void deleteDinoz(String dinozId) {
    dinozRepository.deleteById(dinozId);
    playerStatRepository.deleteDinozStats(dinozId);
    playerMissionRepository.deletePlayerDinozMission(dinozId);
  }

  public void removeAllDinoz(List<Dinoz> allDinoz) {
    allDinoz.forEach(dinoz -> {
      dinozRepository.deleteById(dinoz.getId());
      playerStatRepository.deleteDinozStats(dinoz.getId());
      playerMissionRepository.deletePlayerDinozMission(dinoz.getId());
    });
  }

  public void clearImports() {
    importRepository.deleteAll();
  }

  public Map<Integer, HunterGroup> getHunterGroups() {
    return hunterGroups;
  }

  private Dinoz rebuildBotDinoz(String dinozId) {
    String[] dataParts = dinozId.split("-");
    HashMap<String, Integer> elements = new HashMap<String, Integer>();
    elements.put(DinoparcConstants.FEU, Integer.parseInt(dataParts[1]));
    elements.put(DinoparcConstants.TERRE, Integer.parseInt(dataParts[2]));
    elements.put(DinoparcConstants.EAU, Integer.parseInt(dataParts[3]));
    elements.put(DinoparcConstants.FOUDRE, Integer.parseInt(dataParts[4]));
    elements.put(DinoparcConstants.AIR, Integer.parseInt(dataParts[5]));

    Dinoz botDinoz = new Dinoz();
    botDinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGEBOT);
    botDinoz.setEndMessage(DinoparcConstants.ENDMESSAGEBOT);
    botDinoz.setAppearanceCode(dataParts[6]);
    botDinoz.setLife(100);
    botDinoz.setName("BOT");
    botDinoz.setElementsValues(elements);
    botDinoz.setPassiveList(new HashMap<String, Integer>());

    return botDinoz;
  }

  public void levelUp(Dinoz dinoz, String skill, Boolean fromBazarNpc) {
    if (dinoz.getSkillsMap().get(skill) != null || dinoz.getSkillsMap().get(skill) != 0 && dinoz.getSkillsMap().get(skill) < 5) {
      dinoz.getSkillsMap().put(skill, dinoz.getSkillsMap().get(skill) + 1);
      dinoz.getElementsValues().put(getElementNameFromSkill(skill), dinoz.getElementsValues().get(getElementNameFromSkill(skill)) + 1);
      dinoz.setExperience(0);
      dinoz.setLevel(dinoz.getLevel() + 1);
      dinoz.getActionsMap().put(DinoparcConstants.LEVELUP, false);

      dinozRepository.processLvlUp(dinoz.getId(), dinoz.getSkillsMap(), dinoz.getElementsValues(), 0, dinoz.getLevel(), dinoz.getActionsMap());

      if (!fromBazarNpc) {
        History historyEvent = new History();
        historyEvent.setPlayerId(dinoz.getMasterId());
        historyEvent.setIcon("hist_level.gif");
        historyEvent.setType("level_up");
        historyEvent.setBuyAmount(dinoz.getLevel());
        historyEvent.setFromDinozName(dinoz.getName());
        historyEvent.setObject(skill);
        historyRepository.save(historyEvent);
      }

      if (dinoz.getLevel() == 20 && dinoz.getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI)) {
        EmbedCreateSpec embedDiscordMessage = EmbedCreateSpec
                .builder()
                .color(Color.BLACK)
                .thumbnail("https://i.ibb.co/vx7zxnp5/stiti-eye.png")
                .title("Level Up!")
                .description(dinoz.getMasterName() + " vient de sécuriser " + dinoz.getName() + " en atteignant le niveau 20!")
                .footer("Généré par le serveur Neoparc", "https://i.ibb.co/bB74rVG/hist-reset.gif")
                .build();

        discordService.postEmbedMessage(DinoparcConstants.CHANNEL_CERBERE, embedDiscordMessage);
      }
    }
  }

  public void learnNewSkill(Dinoz dinoz, String skill, Boolean fromBazarNpc) {
    if (dinoz.getSkillsMap().get(skill) == null || dinoz.getSkillsMap().get(skill) == 0 && getAvailableLearnings(dinoz).keySet().contains(skill)) {
      dinoz.getSkillsMap().put(skill, 1);
      dinoz.getElementsValues().put(getElementNameFromSkill(skill), dinoz.getElementsValues().get(getElementNameFromSkill(skill)) + 1);
      dinoz.setExperience(0);
      dinoz.setLevel(dinoz.getLevel() + 1);
      dinoz.getActionsMap().put(DinoparcConstants.LEVELUP, false);

      if (skill.equalsIgnoreCase(DinoparcConstants.FOUILLER)) {
        dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
      }

      if (skill.equalsIgnoreCase(DinoparcConstants.BAIN_FLAMMES) && dinoz.getLife() < 10) {
        dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, true);
      }

      if (skill.equalsIgnoreCase(DinoparcConstants.PÊCHE) && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {
        dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
      }

      if (skill.equalsIgnoreCase(DinoparcConstants.CUEILLETTE) && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {
        dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
      }

      dinozRepository.processLvlUp(dinoz.getId(), dinoz.getSkillsMap(), dinoz.getElementsValues(), 0, dinoz.getLevel(), dinoz.getActionsMap());

      if (!fromBazarNpc) {
        History historyEvent = new History();
        historyEvent.setPlayerId(dinoz.getMasterId());
        historyEvent.setIcon("hist_level.gif");
        historyEvent.setType("level_up");
        historyEvent.setFromDinozName(dinoz.getName());
        historyEvent.setBuyAmount(dinoz.getLevel());
        historyEvent.setObject(skill);
        historyRepository.save(historyEvent);
      }
    }
  }

  public String getElementNameFromSkill(String competence) {
    if (competence.equalsIgnoreCase(DinoparcConstants.FORCE)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.ENDURANCE)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.PERCEPTION)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.INTELLIGENCE)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.AGILITÉ)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.FOUILLER)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.ROCK)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.ARTS_MARTIAUX)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.CAMOUFLAGE)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.MEDECINE)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.NATATION)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.ESCALADE)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.SURVIE)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.STRATEGIE)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.COMMERCE)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.NAVIGATION)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.COURSE)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.CONTRE_ATTAQUE)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.CHANCE)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.MUSIQUE)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.CUISINE)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.SAUT)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.VOLER)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.TAUNT)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.APPRENTI_FEU)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.APPRENTI_TERRE)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.APPRENTI_EAU)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.APPRENTI_FOUDRE)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.POUVOIR_SOMBRE)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.PROTECTIONDUFEU)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.SOLUBILITE)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.BAIN_FLAMMES)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.RESILIENCE)) {
      return DinoparcConstants.FEU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.CUEILLETTE)) {
      return DinoparcConstants.TERRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.PÊCHE)) {
      return DinoparcConstants.EAU;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.INGENIEUR)) {
      return DinoparcConstants.FOUDRE;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.COUP_CRITIQUE)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.CHASSEUR)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.PIQURE)) {
      return DinoparcConstants.AIR;

    } else if (competence.equalsIgnoreCase(DinoparcConstants.FORTERESSE)) {
      return DinoparcConstants.TERRE;
    }
    return null;
  }

  public static Map<String, Integer> getAvailableLearnings(Dinoz dinoz) {
    HashMap<String, Integer> availableLearnings = new HashMap<String, Integer>();

    for (String skill : dinoz.getSkillsMap().keySet()) {
      if (dinoz.getSkillsMap().get(skill) != null && dinoz.getSkillsMap().get(skill) >= 4) {
        addPossibleNewSkills(dinoz, availableLearnings, skill);
      }
    }

    if (!dinoz.getSkillsMap().containsKey(DinoparcConstants.FORCE) || dinoz.getSkillsMap().get(DinoparcConstants.FORCE) == null) {
      availableLearnings.put(DinoparcConstants.FORCE, 1);
    }

    if (!dinoz.getSkillsMap().containsKey(DinoparcConstants.ENDURANCE) || dinoz.getSkillsMap().get(DinoparcConstants.ENDURANCE) == null) {
      availableLearnings.put(DinoparcConstants.ENDURANCE, 1);
    }

    if (!dinoz.getSkillsMap().containsKey(DinoparcConstants.PERCEPTION) || dinoz.getSkillsMap().get(DinoparcConstants.PERCEPTION) == null) {
      availableLearnings.put(DinoparcConstants.PERCEPTION, 1);
    }

    if (!dinoz.getSkillsMap().containsKey(DinoparcConstants.INTELLIGENCE) || dinoz.getSkillsMap().get(DinoparcConstants.INTELLIGENCE) == null) {
      availableLearnings.put(DinoparcConstants.INTELLIGENCE, 1);
    }

    if (!dinoz.getSkillsMap().containsKey(DinoparcConstants.AGILITÉ) || dinoz.getSkillsMap().get(DinoparcConstants.AGILITÉ) == null) {
      availableLearnings.put(DinoparcConstants.AGILITÉ, 1);
    }

    if (isDinozMaxed(dinoz, false)
            && availableLearnings.isEmpty()
            && dinoz.getRace().equals(DinoparcConstants.SOUFFLET)
            && dinoz.isDark()
            && !dinoz.getSkillsMap().containsKey(DinoparcConstants.PIQURE)) {
      availableLearnings.put(DinoparcConstants.PIQURE, 1);
    }

    return availableLearnings;
  }

  private static void addPossibleNewSkills(Dinoz dinoz, HashMap<String, Integer> availableLearnings, String skill) {
    switch (skill) {
      case DinoparcConstants.FORCE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) == 0) {
          availableLearnings.put(DinoparcConstants.ARTS_MARTIAUX, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE) == 0) {
          availableLearnings.put(DinoparcConstants.CAMOUFLAGE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == 0) {
          availableLearnings.put(DinoparcConstants.FOUILLER, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE) == 0) {
          availableLearnings.put(DinoparcConstants.RESILIENCE, 1);
        }
        break;

      case DinoparcConstants.AGILITÉ:
        if (dinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE) == 0) {
          availableLearnings.put(DinoparcConstants.CAMOUFLAGE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.MEDECINE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.MEDECINE) == 0) {
          availableLearnings.put(DinoparcConstants.MEDECINE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == 0) {
          availableLearnings.put(DinoparcConstants.FOUILLER, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE) == 0) {
          availableLearnings.put(DinoparcConstants.RESILIENCE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) == 0) {
          availableLearnings.put(DinoparcConstants.COUP_CRITIQUE, 1);
        }
        break;

      case DinoparcConstants.PERCEPTION:
        if (dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == 0) {
          availableLearnings.put(DinoparcConstants.NATATION, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) == 0) {
          availableLearnings.put(DinoparcConstants.TAUNT, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == 0) {
          availableLearnings.put(DinoparcConstants.FOUILLER, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.VOLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.VOLER) == 0) {
          availableLearnings.put(DinoparcConstants.VOLER, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.ESCALADE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.ESCALADE) == 0) {
          availableLearnings.put(DinoparcConstants.ESCALADE, 1);
        }
        break;

      case DinoparcConstants.INTELLIGENCE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == 0) {
          availableLearnings.put(DinoparcConstants.NATATION, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.MEDECINE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.MEDECINE) == 0) {
          availableLearnings.put(DinoparcConstants.MEDECINE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == 0) {
          availableLearnings.put(DinoparcConstants.FOUILLER, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.VOLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.VOLER) == 0) {
          availableLearnings.put(DinoparcConstants.VOLER, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) == 0) {
          availableLearnings.put(DinoparcConstants.COUP_CRITIQUE, 1);
        }
        break;

      case DinoparcConstants.ENDURANCE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) == 0) {
          availableLearnings.put(DinoparcConstants.TAUNT, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) == 0) {
          availableLearnings.put(DinoparcConstants.ARTS_MARTIAUX, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.ESCALADE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.ESCALADE) == 0) {
          availableLearnings.put(DinoparcConstants.ESCALADE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) == 0) {
          availableLearnings.put(DinoparcConstants.FOUILLER, 1);
        }
        break;

      case DinoparcConstants.ARTS_MARTIAUX:
        if (dinoz.getSkillsMap().get(DinoparcConstants.SURVIE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.SURVIE) == 0) {
          availableLearnings.put(DinoparcConstants.SURVIE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) == 0) {
          availableLearnings.put(DinoparcConstants.COMMERCE, 1);
        }
        break;

      case DinoparcConstants.CAMOUFLAGE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.STRATEGIE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.STRATEGIE) == 0) {
          availableLearnings.put(DinoparcConstants.STRATEGIE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.NAVIGATION) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.NAVIGATION) == 0) {
          availableLearnings.put(DinoparcConstants.NAVIGATION, 1);
        }
        break;

      case DinoparcConstants.MEDECINE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) == 0) {
          availableLearnings.put(DinoparcConstants.COMMERCE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.COURSE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.COURSE) == 0) {
          availableLearnings.put(DinoparcConstants.COURSE, 1);
        }
        break;

      case DinoparcConstants.NATATION:
        if (dinoz.getSkillsMap().get(DinoparcConstants.SURVIE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.SURVIE) == 0) {
          availableLearnings.put(DinoparcConstants.SURVIE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.NAVIGATION) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.NAVIGATION) == 0) {
          availableLearnings.put(DinoparcConstants.NAVIGATION, 1);
        }
        break;

      case DinoparcConstants.ESCALADE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.COURSE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.COURSE) == 0) {
          availableLearnings.put(DinoparcConstants.COURSE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.STRATEGIE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.STRATEGIE) == 0) {
          availableLearnings.put(DinoparcConstants.STRATEGIE, 1);
        }
        break;

      case DinoparcConstants.SURVIE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.CHANCE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CHANCE) == 0) {
          availableLearnings.put(DinoparcConstants.CHANCE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.CONTRE_ATTAQUE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CONTRE_ATTAQUE) == 0) {
          availableLearnings.put(DinoparcConstants.CONTRE_ATTAQUE, 1);
        }
        break;

      case DinoparcConstants.STRATEGIE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.CHANCE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CHANCE) == 0) {
          availableLearnings.put(DinoparcConstants.CHANCE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.MUSIQUE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.MUSIQUE) == 0) {
          availableLearnings.put(DinoparcConstants.MUSIQUE, 1);
        }
        break;

      case DinoparcConstants.COMMERCE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.MUSIQUE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.MUSIQUE) == 0) {
          availableLearnings.put(DinoparcConstants.MUSIQUE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.CUISINE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CUISINE) == 0) {
          availableLearnings.put(DinoparcConstants.CUISINE, 1);
        }
        break;

      case DinoparcConstants.NAVIGATION:
        if (dinoz.getSkillsMap().get(DinoparcConstants.CUISINE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CUISINE) == 0) {
          availableLearnings.put(DinoparcConstants.CUISINE, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.SAUT) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.SAUT) == 0) {
          availableLearnings.put(DinoparcConstants.SAUT, 1);
        }
        break;

      case DinoparcConstants.COURSE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.SAUT) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.SAUT) == 0) {
          availableLearnings.put(DinoparcConstants.SAUT, 1);
        }
        if (dinoz.getSkillsMap().get(DinoparcConstants.CONTRE_ATTAQUE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.CONTRE_ATTAQUE) == 0) {
          availableLearnings.put(DinoparcConstants.CONTRE_ATTAQUE, 1);
        }
        break;

      case DinoparcConstants.CONTRE_ATTAQUE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) == 0) {
          availableLearnings.put(DinoparcConstants.BAIN_FLAMMES, 1);
        }
        break;

      case DinoparcConstants.SAUT:
        if (dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) == 0) {
          availableLearnings.put(DinoparcConstants.CUEILLETTE, 1);
        }
        break;

      case DinoparcConstants.CUISINE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) == 0) {
          availableLearnings.put(DinoparcConstants.PÊCHE, 1);
        }
        break;

      case DinoparcConstants.MUSIQUE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) == 0) {
          availableLearnings.put(DinoparcConstants.INGENIEUR, 1);
        }
        break;

      case DinoparcConstants.CHANCE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.CHASSEUR) == null
                || dinoz.getSkillsMap().get(DinoparcConstants.CHASSEUR) == 0) {
          availableLearnings.put(DinoparcConstants.CHASSEUR, 1);
        }
        break;

      case DinoparcConstants.APPRENTI_FEU:
        if (dinoz.getSkillsMap().get(DinoparcConstants.APPRENTI_TERRE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.APPRENTI_TERRE) == 0) {
          availableLearnings.put(DinoparcConstants.APPRENTI_TERRE, 1);
        }
        break;

      case DinoparcConstants.APPRENTI_TERRE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.APPRENTI_EAU) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.APPRENTI_EAU) == 0) {
          availableLearnings.put(DinoparcConstants.APPRENTI_EAU, 1);
        }
        break;

      case DinoparcConstants.APPRENTI_EAU:
        if (dinoz.getSkillsMap().get(DinoparcConstants.APPRENTI_FOUDRE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.APPRENTI_FOUDRE) == 0) {
          availableLearnings.put(DinoparcConstants.APPRENTI_FOUDRE, 1);
        }
        break;

      case DinoparcConstants.APPRENTI_FOUDRE:
        if (dinoz.getSkillsMap().get(DinoparcConstants.POUVOIR_SOMBRE) == null
            || dinoz.getSkillsMap().get(DinoparcConstants.POUVOIR_SOMBRE) == 0) {
          availableLearnings.put(DinoparcConstants.POUVOIR_SOMBRE, 1);
        }
        break;

      default:
        break;
    }
  }

  public GiveObjectDto giveIrmaToDinoz(Dinoz dinoz, Player account, List<Integer> irmasQtys) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);
    giveObjectDto.setInutileError(false);
    giveObjectDto.setSuccessMessageFr("Votre Dinoz peut gambader à nouveau à travers Dinoland. Merci Madame Irma!");
    giveObjectDto.setSuccessMessageEs("Tu Dinoz puede moverse a través de Dinoland nuevamente. ¡Gracias Bruja Lola!");
    giveObjectDto.setSuccessMessageEn("Your Dinoz can move through Dinoland again once again. Thank you Sparky!");

    dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, false);
    dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.FOUILLER)
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.ROCK)
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.BAIN_FLAMMES)
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) > 0
            && dinoz.getLife() < 10) {

      dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.PÊCHE)
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0
            && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.CUEILLETTE)
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) > 0
            && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
    }

    loadBossInfos(dinoz);
    toggleSpecificLocationActions(dinoz);
    applyMaraisStateByDay(dinoz);

    dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());

    // Consume the surplus potions first if possible:
    if (irmasQtys.get(1) > 0) {
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS, 1);
    } else {
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA, 1);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveAngeToDinoz(Dinoz dinoz, Player account) {
    if (dinoz.getMalusList().contains(DinoparcConstants.KABUKI_QUETE)) {
      dinoz.getMalusList().remove(DinoparcConstants.KABUKI_QUETE);
      dinoz.setKabukiProgression(0);
      dinozRepository.setKabukiProgression(dinoz.getId(), dinoz.getMalusList(), 0);
    }

    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);
    giveObjectDto.setInutileError(false);
    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant en pleine forme! Cette potion d'ange n'a pas été un luxe pour lui.");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " está ahora en excelente forma! Esta poción de ángel no era un lujo para él.");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now in great shape! This angel potion was no luxury for him.");

    dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, false);
    dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
    applyMaraisStateByDay(dinoz);

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.FOUILLER)
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.ROCK)
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.BAIN_FLAMMES)
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, false);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.PÊCHE)
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0
            && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.CUEILLETTE)
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) > 0
            && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
    }

    if (dinoz.getPlaceNumber() == 4) {
      dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, true);
    }

    if (dinoz.getPlaceNumber() == 7) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
    }

    if (dinoz.getPlaceNumber() == 3) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
    }

    if (dinoz.getPlaceNumber() == 11) {
      dinoz.getActionsMap().put(DinoparcConstants.ACT_NAVIGUER, true);
      dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
    }

    if (dinoz.getPlaceNumber() == 21) {
      dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
    }

    dinozRepository.processAnge(dinoz.getId(), 30, dinoz.getActionsMap());
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.POTION_ANGE, 1);

    return giveObjectDto;
  }

  public GiveObjectDto giveBurgerToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    Integer maxLife = 100;
    if (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) != null && dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) >= 1) {
      maxLife = maxLife + (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) * 10);
    }

    GiveObjectDto giveObjectDto = new GiveObjectDto();
    Integer finalLife = dinoz.getLife() + (10 * quantity);

    if (dinoz.getLife() >= maxLife) {
      giveObjectDto.setInutileError(true);
      giveObjectDto.setSuccess(false);

    } else {
      if (finalLife > maxLife) {
        dinoz.setLife(maxLife);
      } else {
        dinoz.setLife(finalLife);
      }

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " a adoré son petit Nuage-Burger! Il regagne + 10% de vie!");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " le encantaba su pequeño Nube-Hamburguesa! ¡Recupera + 10% de salud!");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " loved his tasty Cloud-Burger! He regains + 10% of life on the go!");

      dinozRepository.setLife(dinoz.getId(), dinoz.getLife());

      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.NUAGE_BURGER, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto givePainChaudToDinoz(Dinoz dinoz, Player account) {
    Integer maxLife = 100;
    if (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) != null && dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) >= 1) {
      maxLife = maxLife + (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) * 10);
    }

    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);
    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " a adoré sa grande collation authentique! Il regagne tous ses points de vie!");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " le encantó su gran bocadillo auténtico! ¡Recupera todos sus puntos de vida!");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " loved his great authentic snack! He regains all his life points!");

    if (dinoz.getLife() >= maxLife) {
      giveObjectDto.setInutileError(true);
      giveObjectDto.setSuccess(false);

    } else {
      if (dinoz.getActionsMap().get(DinoparcConstants.BAIN_FLAMMES) != null && dinoz.getActionsMap().get(DinoparcConstants.BAIN_FLAMMES)) {
        dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, false);
        dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());
      }
      dinozRepository.setLife(dinoz.getId(), maxLife);
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.PAIN_CHAUD, 1);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveTarteToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    Integer maxLife = 100;
    if (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) != null && dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) >= 1) {
      maxLife = maxLife + (dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) * 10);
    }

    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);
    Integer lifeWon = 0;

    for (int i = 0; i < quantity; i++) {
      lifeWon += ThreadLocalRandom.current().nextInt(12, 20 + 1);
    }

    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " a savouré sa tarte à la viande! Il regagne " + lifeWon + "% de vie.");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " disfrutó de su pastel de carne! El recupera " + lifeWon + "% de vida.");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " enjoyed his Shepherd's Pie! He regains " + lifeWon + "% of life.");

    if (dinoz.getLife() >= maxLife) {
      giveObjectDto.setInutileError(true);
      giveObjectDto.setSuccess(false);

    } else {
      Integer finalLife = dinoz.getLife() + lifeWon;
      if (finalLife > maxLife) {
        finalLife = maxLife;
      }
      dinozRepository.setLife(dinoz.getId(), finalLife);
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.TARTE_VIANDE, 1);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveMedalToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);

    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " va porter fièrement sa nouvelle médaille en chocolat. Du moins, jusqu'à ce qu'elle fonde... (+ 10% d'expérience!)");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " lucirá con orgullo su nueva medalla de chocolate. Al menos hasta que se derrita ... (¡+ 10% de experiencia!)");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will proudly wear his new chocolate medal. At least until it melts ... (+ 10% experience!)");

    if (!isDinozMaxed(dinoz, !getAvailableLearnings(dinoz).isEmpty()) && dinoz.getExperience() < (100 - (10 * quantity))) {
      dinozRepository.setExperience(dinoz.getId(), dinoz.getExperience() + (10 * quantity));
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.MEDAILLE_CHOCOLAT, quantity);
    } else {
      if (!isDinozMaxed(dinoz, !getAvailableLearnings(dinoz).isEmpty())) {
        dinoz.getActionsMap().put(DinoparcConstants.LEVELUP, true);
        dinozRepository.setExperienceWithActionMap(dinoz.getId(), 100, dinoz.getActionsMap());
        inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.MEDAILLE_CHOCOLAT, quantity);
      } else {
        giveObjectDto.setInutileError(true);
        giveObjectDto.setSuccess(false);
      }
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveCargouMilkToDinoz(Dinoz dinoz, Player account) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);

    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant maître en fouille, pêche et cueillette pour une durée indéterminée!");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " es un maestro en excavar, pescar y recolectar por tiempo indefinido!");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now a master in digging, fishing and gathering for an indefinite period!");

    List<String> newMalusList = dinoz.getMalusList();
    newMalusList.add(DinoparcConstants.LAIT_DE_CARGOU);
    dinozRepository.setMalusList(dinoz.getId(), newMalusList);
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.LAIT_DE_CARGOU, 1);

    return giveObjectDto;
  }

  public GiveObjectDto giveFishingRodToDinoz(Dinoz dinoz, Player account) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);

    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " peut maintenant effectuer des actions de pêches jusqu'à ce que la canne se brise!");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " puedes realizar acciones de pesca hasta que la caña se rompa!");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " can now perform fishing actions until the fishing rod breaks!");

    List<String> newMalusList = dinoz.getMalusList();
    newMalusList.add(DinoparcConstants.CANNEAPECHE);
    dinozRepository.setMalusList(dinoz.getId(), newMalusList);
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CANNEAPECHE, 1);

    return giveObjectDto;
  }

  public GiveObjectDto giveDarkPotionToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);
    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " s'est transformé! Il se sent beaucoup plus fort et est devenu un Dinoz Sombre!");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + " se ha transformado! ¡Se siente mucho más fuerte y se ha convertido en un Dark Dinoz!");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + " has transformed! He feels much stronger and has become a Dark Dinoz!");

    dinoz.getElementsValues().put(DinoparcConstants.FEU, dinoz.getElementsValues().get(DinoparcConstants.FEU) + 1);
    dinoz.getSkillsMap().put(DinoparcConstants.APPRENTI_FEU, 1);
    dinozRepository.processDarkPotion(dinoz.getId(), true, dinoz.getAppearanceCode() + "#", dinoz.getElementsValues(), dinoz.getSkillsMap());
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.POTION_SOMBRE, quantity);

    playerStatRepository.addStats(UUID.fromString(account.getId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_DARK_DINOZ, 1)));

    return giveObjectDto;
  }

  public GiveObjectDto giveCherryLiquorToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);

    if (dinoz.isDark() || dinoz.getAppearanceCode().endsWith("#")) {
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant inarrêtable! Il peut combattre et se déplacer à volonté pendant 30 minutes! L'intensité du nectar n'a pas eu d'effets secondaires sur lui. Ne perdez plus une seconde de plus et allez combattre!");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora es imparable! ¡Él puede luchar y moverse a voluntad durante 30 minutos! La intensidad del néctar no tenía ningún efecto secundario en él. ¡No pierdas un segundo más y voy a luchar!");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now unstoppable! He can fight and move at will for 30 minutes! The intensity of the Nectar did not have any side effects on it. Do not lose a second more and go fighting!");

    } else {
      if (dinoz.getAppearanceCode().endsWith("$")) {
        dinoz.setAppearanceCode(dinoz.getAppearanceCode().replace("$", ""));
      } else {
        dinoz.setAppearanceCode(dinoz.getAppearanceCode() + "$");
      }

      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant inarrêtable! Il peut combattre et se déplacer à volonté pendant 30 minutes! L'intensité du nectar a eu quelques effets secondaires sur lui, rien de grave. Ne perdez plus une seconde de plus et allez combattre!");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora es imparable! ¡Él puede luchar y moverse a voluntad durante 30 minutos! La intensidad del néctar tenía algunos efectos secundarios en él, nada serio. ¡No pierdas un segundo más y voy a luchar!");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now unstoppable! He can fight and move at will for 30 minutes! The intensity of the Nectar had some side effects on it, nothing serious. Do not lose a second more and go fighting!");
    }

    dinoz.getMalusList().add(DinoparcConstants.COULIS_CERISE);
    dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);

    if (dinoz.getPlaceNumber() == 2) {
      dinoz.getActionsMap().put(DinoparcConstants.CATAPULTE, true);
    }

    if (dinoz.getPlaceNumber() == 3
            && dinoz.getSkillsMap().containsKey(DinoparcConstants.NATATION)
            && dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == 5) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEBALEINE, true);
    }

    if (dinoz.getPlaceNumber() == 26) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, true);
    }

    if (dinoz.getPlaceNumber() == 4) {
      dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, true);
    }

    if (dinoz.getPlaceNumber() == 7) {
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
    }

    dinoz.setEpochSecondsEndOfCherryEffect(ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusMinutes(30).toEpochSecond());
    dinozRepository.processCherry(dinoz.getId(), dinoz.getAppearanceCode(), dinoz.getMalusList(), dinoz.getActionsMap(), dinoz.getEpochSecondsEndOfCherryEffect(), 30);
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.COULIS_CERISE, quantity);

    return giveObjectDto;
  }

  public GiveObjectDto giveLvlUpPillToDinoz(Player account, Dinoz dinoz, String chosenElement, Integer pillQty) {
    dinoz.getElementsValues().put(chosenElement, dinoz.getElementsValues().get(chosenElement) + pillQty);
    dinozRepository.processLvlUp(dinoz.getId(), dinoz.getSkillsMap(), dinoz.getElementsValues(), dinoz.getExperience(), dinoz.getLevel() + pillQty, dinoz.getActionsMap());
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.ETERNITY_PILL, pillQty);

    GiveObjectDto giveObjectDto = new GiveObjectDto();
    giveObjectDto.setSuccess(true);
    giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est passé au niveau " + (dinoz.getLevel() + pillQty) + "! Vous avez choisi de lui donner un bonus en " + chosenElement + ".");
    giveObjectDto.setSuccessMessageEs(dinoz.getName() + "  ha pasado al nivel " +  (dinoz.getLevel() + pillQty) + "! Has elegido darle un extra en " + getSpanishElementTrad(chosenElement) + ".");
    giveObjectDto.setSuccessMessageEn(dinoz.getName() + "  rose to level " +  (dinoz.getLevel() + pillQty) + "! You have chosen to give him a bonus in " + getEnglishElementTrad(chosenElement) + ".");
    return giveObjectDto;
  }

  private String getSpanishElementTrad(String chosenElement) {
    switch (chosenElement) {
      case DinoparcConstants.FEU :
        return "Fuego";
      case DinoparcConstants.TERRE :
        return "Tierra";
      case DinoparcConstants.EAU :
        return "Agua";
      case DinoparcConstants.FOUDRE :
        return "Trueno";
      case DinoparcConstants.AIR :
        return "Viento";
      default :
        return "Null";
    }
  }

  private String getEnglishElementTrad(String chosenElement) {
    switch (chosenElement) {
      case DinoparcConstants.FEU :
        return "Fire";
      case DinoparcConstants.TERRE :
        return "Earth";
      case DinoparcConstants.EAU :
        return "Water";
      case DinoparcConstants.FOUDRE :
        return "Thunder";
      case DinoparcConstants.AIR :
        return "Wind";
      default :
        return "Null";
    }
  }

  public GiveObjectDto giveFireCharmToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FEU) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FEU) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_FEU) != null && charmLimitReachedForSpecificDinoz(dinoz, DinoparcConstants.CHARME_FEU)) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.CHARME_FEU, 0);
      dinoz.getPassiveList().put(DinoparcConstants.CHARME_FEU, dinoz.getPassiveList().get(DinoparcConstants.CHARME_FEU) + quantity);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " sera protégé contre les dégâts de la prochaine attaque Feu qu'il subira.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " estará protegido contra el daño del próximo ataque de fuego que reciba.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will tank any damage done by the next Fire attack that it receives.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHARME_FEU, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveWoodCharmToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) != null && charmLimitReachedForSpecificDinoz(dinoz, DinoparcConstants.CHARME_TERRE)) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.CHARME_TERRE, 0);
      dinoz.getPassiveList().put(DinoparcConstants.CHARME_TERRE, dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) + quantity);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " sera protégé contre les dégâts de la prochaine attaque Terre qu'il subira.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " estará protegido contra el daño del próximo ataque de Tierra que reciba.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will tank any damage done by the next Wood attack that it receives.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHARME_TERRE, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveWaterCharmToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_EAU) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_EAU) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_EAU) != null && charmLimitReachedForSpecificDinoz(dinoz, DinoparcConstants.CHARME_EAU)) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.CHARME_EAU, 0);
      dinoz.getPassiveList().put(DinoparcConstants.CHARME_EAU, dinoz.getPassiveList().get(DinoparcConstants.CHARME_EAU) + quantity);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " sera protégé contre les dégâts de la prochaine attaque Eau qu'il subira.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " estará protegido contra el daño del próximo ataque de Agua que reciba.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will tank any damage done by the next Water attack that it receives.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHARME_EAU, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveThunderCharmToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FOUDRE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_FOUDRE) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_FOUDRE) != null && charmLimitReachedForSpecificDinoz(dinoz, DinoparcConstants.CHARME_FOUDRE)) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.CHARME_FOUDRE, 0);
      dinoz.getPassiveList().put(DinoparcConstants.CHARME_FOUDRE, dinoz.getPassiveList().get(DinoparcConstants.CHARME_FOUDRE) + quantity);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " sera protégé contre les dégâts de la prochaine attaque Foudre qu'il subira.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " estará protegido contra el daño del próximo ataque de Rayo que reciba.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will tank any damage done by the next Thunder attack that it receives.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHARME_FOUDRE, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveAirCharmToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_AIR) != null
        && dinoz.getPassiveList().get(DinoparcConstants.CHARME_AIR) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_AIR) != null && charmLimitReachedForSpecificDinoz(dinoz, DinoparcConstants.CHARME_AIR)) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.CHARME_AIR, 0);
      dinoz.getPassiveList().put(DinoparcConstants.CHARME_AIR, dinoz.getPassiveList().get(DinoparcConstants.CHARME_AIR) + quantity);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " sera protégé contre les dégâts de la prochaine attaque Air qu'il subira.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " estará protegido contra el daño del próximo ataque de Aire que reciba.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will tank any damage done by the next Wind attack that it receives.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHARME_AIR, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto givePrismatikCharmToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
            && dinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
            && dinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null && dinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) >= DinoparcConstants.CHARMS_LIMIT) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.CHARME_PRISMATIK, 0);
      dinoz.getPassiveList().put(DinoparcConstants.CHARME_PRISMATIK, dinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) + quantity);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " sera protégé de tous les éléments des prochaines attaques qu'il déclenchera! (Durée aléatoire)");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " estará protegido de todos los elementos de los próximos ataques que desate! (Duración aleatoria)");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will be protected from all the elements of the next attacks it unleashes! (Random duration)");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHARME_PRISMATIK, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveGriffesToDinoz(Dinoz dinoz, Player account) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) != null
        && dinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) == 0) {
      giveObjectDto.setInutileError(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.GRIFFES_EMPOISONNÉES, 0);
      dinoz.getPassiveList().put(DinoparcConstants.GRIFFES_EMPOISONNÉES, dinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) + 1);
      dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant équipé de longues griffes empoisonnées! Le prochain Dinoz qui s'y frottera n'aura pas de chance.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora está equipado con largas garras venenosas! El próximo Dino que se encuentre con él no tendrá ninguna oportunidad.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now equipped with long poisoned claws! The next Dino who runs into it won't have a chance.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.GRIFFES_EMPOISONNÉES, 1);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveSpitToDinoz(Dinoz dinoz, Player account) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE)) {
      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant soigné de son statut de Dinoz empoisonné.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora está curado de su estado de Dinoz envenenado.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now healed of his poisoned status.");
      dinoz.getMalusList().remove(DinoparcConstants.EMPOISONNE);
      dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.BAVE_LOUPI, 1);
    } else {
      giveObjectDto.setInutileError(true);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveRamensToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    if (dinoz.getDanger() == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getDanger() > 0) {
      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " a dévoré son bol de ramens. Son score de danger a été réduit de moitié.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " devoró su plato de ramen. Su puntuación de peligro se ha reducido a la mitad.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " devoured his bowl of ramen. His Danger Score has been halved.");

      Integer finalDanger = dinoz.getDanger();

      for (int i = 0; i < quantity; i++) {
        finalDanger /= 2;
      }

      dinozRepository.setDanger(dinoz.getId(), finalDanger);
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.RAMENS, 1);
    } else {
      giveObjectDto.setInutileError(true);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveHerbalTeaToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    if (dinoz.getDanger() == -200) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getDanger() > -200) {
      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " a adoré sa tisane des bois. Son score de danger a été mis au minimum possible! (-200)");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " amaba su té de hierbas. ¡Su puntuación de peligro se ha ajustado al mínimo posible! (-200)");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " loved his herbal tea. His danger score has been set to the minimum possible! (-200)");
      dinozRepository.setDanger(dinoz.getId(), -200);
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.TISANE, 1);
    } else {
      giveObjectDto.setInutileError(true);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveDinojakBeerToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    if (!dinoz.getMalusList().contains(DinoparcConstants.BIERE)) {
      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " va gagner plus de pièces d'or pendant ses prochains combats! (Durée aléatoire)");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ganará más de oro durante sus próximas batallas. (Duración aleatoria)");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " will earn more gold during his next battles! (Random duration)");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.BIERE, 1);

      List<String> malusList = dinoz.getMalusList();
      malusList.add(DinoparcConstants.BIERE);
      dinozRepository.setMalusList(dinoz.getId(), malusList);
    } else {
      giveObjectDto.setSuccess(false);
      giveObjectDto.setInutileError(true);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveTigerEyesToDinoz(Dinoz dinoz, Player account) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinoz.getMalusList().contains(DinoparcConstants.OEIL_DU_TIGRE)) {
      giveObjectDto.setInutileError(true);

    } else {
      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant muni d'un Oeil du Tigre! Il pourra dénicher ses ennemis en tournois lors de la Guerre des Clans!");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora tiene un ojo de tigre! ¡Podrá encontrar a sus enemigos en torneos durante las Guerras de Clanes!");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " now has a Tiger Eye! He will be able to find his enemies in tournaments during the Clan Wars!");
      dinoz.getMalusList().add(DinoparcConstants.OEIL_DU_TIGRE);

      dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.OEIL_DU_TIGRE, 1);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveChikabumPillToDinoz(Dinoz dinoz, Player account) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) == 0) {
      giveObjectDto.setInutileError(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.ANTIDOTE, 0);
      dinoz.getPassiveList().put(DinoparcConstants.ANTIDOTE, dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) + 1);

      if (dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
        dinoz.getMalusList().remove(DinoparcConstants.CHIKABUM);
      }

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " est maintenant immunisé contre la prochaine infection au Virus Chikabum.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora es inmune a la próxima infección por el virus Chikabum.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " is now immune to the next Chikabum Virus infection.");

      dinozRepository.setPassiveAndMalusList(dinoz.getId(), dinoz.getPassiveList(), dinoz.getMalusList());
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.ANTIDOTE, 1);
    }

    return giveObjectDto;
  }

  public GiveObjectDto givePruniacToDinoz(Dinoz dinoz, Player account, String newName) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();
    if (newName == null || newName.isEmpty() || newName.isBlank() || newName.length() > 15 || newName.equalsIgnoreCase("null")) {
      giveObjectDto.setBadNameError(true);
      return giveObjectDto;

    } else {
      dinozRepository.setName(dinoz.getId(), newName);
      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr("Votre Dinoz a été renommé! Désormais, il s'appelle " + newName + ".");
      giveObjectDto.setSuccessMessageEs("Your Dinoz has been renamed! It is now called " + newName + ".");
      giveObjectDto.setSuccessMessageEn("¡Tu Dinoz ha sido renombrado! Ahora se llama " + newName + ".");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.PRUNIAC, 1);

      return giveObjectDto;
    }
  }

  public GiveObjectDto giveFocusAggroToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) != null
            && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) >= DinoparcConstants.FOCUS_LIMIT) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.FOCUS_AGGRESIVITE, 0);
      dinoz.getElementsValues().replace(DinoparcConstants.FEU, dinoz.getElementsValues().get(DinoparcConstants.FEU) + (2 * quantity));
      dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, dinoz.getElementsValues().get(DinoparcConstants.FOUDRE) + (2 * quantity));
      dinoz.getPassiveList().put(DinoparcConstants.FOCUS_AGGRESIVITE, dinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) + (1 * quantity));
      dinozRepository.processFocus(dinoz.getId(), dinoz.getPassiveList(), dinoz.getElementsValues());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " bénéficie maintenant d'un bonus (+2 feu & +2 foudre) sur ses points d'éléments! Ce bonus a une durée aléatoire.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora se beneficia de una bonificación (+2 fuego y +2 rayo) en sus puntos de elemento. Esta bonificación tiene una duración aleatoria.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " now benefits from a bonus (+2 fire & +2 thunder) on its element points! This bonus has a random duration.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.FOCUS_AGGRESIVITE, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveFocusNatureToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) != null
        && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) != null
            && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) >= DinoparcConstants.FOCUS_LIMIT) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.FOCUS_NATURE, 0);
      dinoz.getElementsValues().replace(DinoparcConstants.TERRE, dinoz.getElementsValues().get(DinoparcConstants.TERRE) + (2 * quantity));
      dinoz.getElementsValues().replace(DinoparcConstants.AIR, dinoz.getElementsValues().get(DinoparcConstants.AIR) + (2 * quantity));
      dinoz.getPassiveList().put(DinoparcConstants.FOCUS_NATURE, dinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) + (1 * quantity));
      dinozRepository.processFocus(dinoz.getId(), dinoz.getPassiveList(), dinoz.getElementsValues());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " bénéficie maintenant d'un bonus (+2 terre & +2 air) sur ses points d'éléments! Ce bonus a une durée aléatoire.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora se beneficia de una bonificación (+2 tierra y +2 aire) en sus puntos de elemento. Esta bonificación tiene una duración aleatoria.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " now benefits from a bonus (+2 earth & +2 air) on its element points! This bonus has a random duration.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.FOCUS_NATURE, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveFocusSirainsToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
        && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) != null
        && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) != null
            && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) >= DinoparcConstants.FOCUS_LIMIT) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.FOCUS_SIRAINS, 0);
      dinoz.getElementsValues().replace(DinoparcConstants.EAU, dinoz.getElementsValues().get(DinoparcConstants.EAU) + (5 * quantity));
      dinoz.getPassiveList().put(DinoparcConstants.FOCUS_SIRAINS, dinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) + (1 * quantity));
      dinozRepository.processFocus(dinoz.getId(), dinoz.getPassiveList(), dinoz.getElementsValues());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " bénéficie maintenant d'un bonus (+5 eau) sur ses points d'éléments! Ce bonus a une durée aléatoire.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora se beneficia de una bonificación (+5 aqua) en sus puntos de elemento. Esta bonificación tiene una duración aleatoria.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " now benefits from a bonus (+5 water) on its element points! This bonus has a random duration.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.FOCUS_SIRAINS, quantity);
    }

    return giveObjectDto;
  }

  public GiveObjectDto giveFocusMahamutiToDinoz(Dinoz dinoz, Player account, Integer quantity) {
    GiveObjectDto giveObjectDto = new GiveObjectDto();

    if (dinozHasNoMorePassiveSpace(dinoz)
            && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) != null
            && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) == 0) {
      giveObjectDto.setInutileError(true);

    } else if (dinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) != null
            && dinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) >= DinoparcConstants.FOCUS_LIMIT) {
      giveObjectDto.setLimitReached(true);

    } else {
      dinoz.getPassiveList().putIfAbsent(DinoparcConstants.FOCUS_MAHAMUTI, 0);
      dinoz.getElementsValues().replace(DinoparcConstants.TERRE, dinoz.getElementsValues().get(DinoparcConstants.TERRE) + (5));
      dinoz.getPassiveList().put(DinoparcConstants.FOCUS_MAHAMUTI, dinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) + (1 * quantity));
      dinozRepository.processFocus(dinoz.getId(), dinoz.getPassiveList(), dinoz.getElementsValues());

      giveObjectDto.setSuccess(true);
      giveObjectDto.setSuccessMessageFr(dinoz.getName() + " bénéficie maintenant d'un bonus (+5 " + "Terre" + ") sur ses points d'éléments! Ce bonus a une durée aléatoire.");
      giveObjectDto.setSuccessMessageEs(dinoz.getName() + " ahora se beneficia de una bonificación (+5 " + "Earth" + ") en sus puntos de elemento. Esta bonificación tiene una duración aleatoria.");
      giveObjectDto.setSuccessMessageEn(dinoz.getName() + " now benefits from a bonus (+5 " + "Árbol" + ") on its element points! This bonus has a random duration.");
      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.FOCUS_MAHAMUTI, quantity);
    }

    return giveObjectDto;
  }

  public Map<String, Integer> getChaudronCraftableObjects(Player player) {
    Map<String, Integer> chaudronCraftableObjects = new HashMap<String, Integer>();
    var accountId = player.getId();

    var plant1 = inventoryRepository.getQty(accountId, DinoparcConstants.FEUILLE_PACIFIQUE);
    var plant2 = inventoryRepository.getQty(accountId, DinoparcConstants.OREADE_BLANC);
    var plant3 = inventoryRepository.getQty(accountId, DinoparcConstants.TIGE_RONCIVORE);
    var plant4 = inventoryRepository.getQty(accountId, DinoparcConstants.ANÉMONE_SOLITAIRE);

    var fish1 = inventoryRepository.getQty(accountId, DinoparcConstants.PERCHE_PERLEE);
    var fish2 = inventoryRepository.getQty(accountId, DinoparcConstants.GREMILLE_GRELOTTANTE);
    var fish3 = inventoryRepository.getQty(accountId, DinoparcConstants.CUBE_GLU);

    if (plant1 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.FEUILLE_PACIFIQUE, plant1);
    }
    if (plant2 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.OREADE_BLANC, plant2);
    }
    if (plant3 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.TIGE_RONCIVORE, plant3);
    }
    if (plant4 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.ANÉMONE_SOLITAIRE, plant4);
    }
    if (fish1 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.PERCHE_PERLEE, fish1);
    }
    if (fish2 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.GREMILLE_GRELOTTANTE, fish2);
    }
    if (fish3 > 0) {
      chaudronCraftableObjects.put(DinoparcConstants.CUBE_GLU, fish3);
    }

    return chaudronCraftableObjects;
  }

  public boolean buyFromIrmaDirectly(Integer irmaBuy, Player account) {
    if (irmaBuy > 0) {
      Integer totalPrice = irmaBuy * 700;
      if (account.getCash() >= totalPrice) {
        playerRepository.updateCash(account.getId(), -totalPrice);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPrice)));

        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA, irmaBuy);

        History historyEvent = new History();
        historyEvent.setPlayerId(account.getId());
        historyEvent.setIcon("hist_buy.gif");
        historyEvent.setType("buy_irma");
        historyEvent.setBuyAmount(irmaBuy);

        historyRepository.save(historyEvent);
        return true;
      }
    }

    return false;
  }

  public boolean sellBonsDirectly(Integer bonsQty, Player account, Integer bonsValue) {
    if (bonsQty > 0) {
      Integer totalBonsEngaged = getEngagedBonsAmountInBazarForUser(account);
      Integer itemQty = inventoryRepository.getQty(account.getId(), DinoparcConstants.BONS);

      if (itemQty < bonsQty) {
        return false;
      }

      if (bonsQty > itemQty - totalBonsEngaged) {
        return false;
      }

      Integer totalPrice = bonsQty * bonsValue;
      playerRepository.updateCash(account.getId(), totalPrice);
      account.setCash(account.getCash() + totalPrice);

      inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.BONS, bonsQty);
      playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_BONS_SELL, bonsQty), new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPrice)));

      History historyEvent = new History();
      historyEvent.setPlayerId(account.getId());
      historyEvent.setIcon("hist_buy.gif");
      historyEvent.setType("sell_bons");
      historyEvent.setBuyAmount(bonsQty);
      historyEvent.setLifeLost(totalPrice);

      historyRepository.save(historyEvent);
      return true;
    }

    return false;
  }

  public Integer getEngagedBonsAmountInBazarForUser(Player account) {
    List<BazarListing> ownBids = bazarRepository.findActiveByLastBiderId(account.getId());

    Integer totalBonsEngaged = 0;
    for (BazarListing listing : ownBids) {
      totalBonsEngaged = totalBonsEngaged + listing.getLastPrice();
    }

    return totalBonsEngaged;
  }

  public boolean buyBonsDirectly(Integer bonsQty, Player account, Integer bonsValue) {
    if (bonsQty > 0) {
      Integer totalPrice = bonsQty * bonsValue;
      if (account.getCash() >= totalPrice) {
        playerRepository.updateCash(account.getId(), -totalPrice);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.BONS, bonsQty);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_BONS_BUY, bonsQty)));

        History historyEvent = new History();
        historyEvent.setPlayerId(account.getId());
        historyEvent.setIcon("hist_buy.gif");
        historyEvent.setType("buy_bons");
        historyEvent.setBuyAmount(bonsQty);
        historyEvent.setLifeLost(totalPrice);
        historyRepository.save(historyEvent);

        return true;
      }
    }

    return false;
  }

  public boolean buyPruniacDirectly(Integer pruniacBuy, Player account) {
    if (pruniacBuy > 0) {
      Integer totalPrice = pruniacBuy * 3500;
      if (account.getCash() >= totalPrice) {
        account.setCash(account.getCash() - totalPrice);
        playerRepository.updateCash(account.getId(), -totalPrice);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPrice)));

        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PRUNIAC, pruniacBuy);

        History historyEvent = new History();
        historyEvent.setPlayerId(account.getId());
        historyEvent.setIcon("hist_buy.gif");
        historyEvent.setType("buy_pruniac");
        historyEvent.setBuyAmount(pruniacBuy);

        historyRepository.save(historyEvent);
        return true;
      }
    }

    return false;
  }

  private boolean charmLimitReachedForSpecificDinoz(Dinoz dinoz, String charm) {
    if (checkIfDinozIsWarrior(dinoz.getMalusList())) {
      return dinoz.getPassiveList().get(charm) >= DinoparcConstants.CHARMS_LIMIT + 15;
    } else {
      return dinoz.getPassiveList().get(charm) >= DinoparcConstants.CHARMS_LIMIT;
    }
  }

  public boolean catapultDinozToJazzIsland(Dinoz dinoz, Player account) {
    if (account.getCash() >= 50 && dinoz.getPlaceNumber() == 2) {
      account.setCash(account.getCash() - 50);
      playerRepository.updateCash(account.getId(), -50);
      playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_CATAPULT, 1)));
      playerStatRepository.addStats(UUID.fromString(account.getId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_CLIMB, 1)));

      dinoz.getActionsMap().replace(DinoparcConstants.CATAPULTE, false);
      dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, false);
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
      dinoz.getActionsMap().put(DinoparcConstants.BAZAR, true);

      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      dinozRepository.processMove(dinoz.getId(), 15, dinoz.getActionsMap());

      return true;

    } else {
      return false;
    }
  }

  public boolean passTheGranitDoorDinoz(Dinoz dinoz, Player account) {
    var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(account.getId())).getCollection();

    if (playerCollection.contains("6") && dinoz.getPlaceNumber() == 7) {
      dinoz.setPlaceNumber(8);
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, false);
      dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT, false);
      dinoz.getActionsMap().put(DinoparcConstants.GARDE_GRANIT_MISSION_STATUS, false);
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);

      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      dinozRepository.processMove(dinoz.getId(), 8, dinoz.getActionsMap());

      return true;
    }
    return false;
  }

  public boolean passTheBaleineDinoz(Dinoz dinoz, Player account) {
    var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(account.getId())).getCollection();
    var playerCollectionEpic = collectionRepository.getPlayerCollection(UUID.fromString(account.getId())).getEpicCollection();

    if (isCollectionFull(playerCollection) //Full Collection
            && playerCollectionEpic.contains("1") //Jazz
            && playerCollectionEpic.contains("3") //Kabukis
            && playerCollectionEpic.contains("7") //Arena Gold
            && playerCollectionEpic.contains("33") //P.D.A
            && dinoz.getPlaceNumber() == 3
            && dinoz.getSkillsMap().containsKey(DinoparcConstants.NATATION)
            && dinoz.getSkillsMap().get(DinoparcConstants.NATATION) == 5) {
      dinoz.setPlaceNumber(23);
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGEBALEINE, false);
      dinoz.getActionsMap().put(DinoparcConstants.TournoiDinoplage, false);
      dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, false);
      dinoz.getActionsMap().put(DinoparcConstants.DINOBATH, false);
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);

      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      dinozRepository.processMove(dinoz.getId(), 23, dinoz.getActionsMap());
      return true;
    }
    return false;
  }

  public boolean passTheLabyrintheDinoz(Dinoz dinoz) {
    if (dinoz.getPlaceNumber() == 26
            && (labyrintheValidRaces.contains(String.valueOf(DinozUtils.getRaceNumber(dinoz.getRace())))
                  || ZonedDateTime.now(ZoneId.of("Europe/Paris")).getDayOfWeek().equals(DayOfWeek.SUNDAY))) {
      dinoz.setPlaceNumber(30);
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);
      dinoz.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, false);
      dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, false);
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);

      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      dinozRepository.processMove(dinoz.getId(), 30, dinoz.getActionsMap());
      return true;
    }

    dinoz.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, false);

    Integer sort = ThreadLocalRandom.current().nextInt(1, 5 + 1);
    if (sort == 1) {
      dinoz.getMalusList().add(DinoparcConstants.EMPOISONNE);
    } else if (sort == 2) {
      dinoz.getMalusList().add(DinoparcConstants.CHIKABUM);
      if (dinoz.getPassiveList().containsKey(DinoparcConstants.ANTIDOTE) && dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) >= 1) {
        dinoz.getPassiveList().replace(DinoparcConstants.ANTIDOTE, dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) - 1);
        dinoz.getMalusList().remove(DinoparcConstants.CHIKABUM);
      }
    }

    dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());
    dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
    return false;
  }

  public boolean takeReturnBoatToMainLand(Dinoz dinoz) {
    if (dinoz.getPlaceNumber() == 24 && dinoz.getSkillsMap().containsKey(DinoparcConstants.NAVIGATION) && dinoz.getSkillsMap().get(DinoparcConstants.NAVIGATION) == 5) {
      dinoz.setPlaceNumber(11);
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);
      dinoz.getActionsMap().put(DinoparcConstants.PAMAGUERRE, false);
      dinoz.getActionsMap().put(DinoparcConstants.MARCHÉPIRATE, false);
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);

      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      dinozRepository.processMove(dinoz.getId(), 11, dinoz.getActionsMap());
      return true;
    }
    return false;
  }

  private Boolean isCollectionFull(List<String> playerCollection) {
    Boolean isCollectionFull = true;
    for (Integer collectionItemNb = 1; collectionItemNb <= 49; collectionItemNb++) {
      if (!playerCollection.contains(collectionItemNb.toString())) {
        isCollectionFull = false;
      }
    }
    return isCollectionFull;
  }

  public boolean controlDinoz(Dinoz dinoz) {
    if (dinoz.getLevel() >= 5 && dinozIsCleanFromEffects(dinoz.getMalusList())) {
      dinoz.setPlaceNumber(5);
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);
      dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, false);
      dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);

      if (dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
      }

      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }

      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
      dinozRepository.processMove(dinoz.getId(), 5, dinoz.getActionsMap());

      return true;

    } else {
      return false;
    }
  }

  public Boolean hasAccessToTournamentForDinoz(String tournament, Dinoz dinoz, List<String> collection) {
    Boolean hasAccess = true;
    if (tournament.equalsIgnoreCase(DinoparcConstants.TournoiDinoville)) {
      if (!(collection.contains("3") && collection.contains("6") && collection.contains("9")
          && collection.contains("12"))) {
        hasAccess = false;
      }
    }

    return hasAccess;
  }

  public Tournament getTournamentInfoForDinoz(String tournamentName, Dinoz dinoz, List<String> collection) {
    Tournament wantedTournament = dinoz.getTournaments().get(tournamentName);
    if (hasAccessToTournamentForDinoz(tournamentName, dinoz, collection)) {
      dinozRepository.setTournaments(dinoz.getId(), dinoz.getTournaments(), true);
    }

    return wantedTournament;
  }

  public Boolean attemptToQuitTourney(Dinoz dinoz, String tournament) {
    Boolean success = false;
    if (dinoz.isInTourney()) {
      Tournament modifiedTournament = dinoz.getTournaments().get(tournament);
      modifiedTournament.setActualPoints(0);
      modifiedTournament.setPosition(0);
      modifiedTournament.setNbInscrits(0);
      modifiedTournament.setCurrentWins(0);
      modifiedTournament.setCurrentLosses(0);
      dinoz.getTournaments().replace(tournament, modifiedTournament);
      dinoz.setInTourney(false);
      dinozRepository.setTournaments(dinoz.getId(), dinoz.getTournaments(), false);
      success = true;
    }

    return success;
  }

  public Boolean getHelpFlagValueForPlayer(String playerId) {
    return playerRepository.getPlayerHelpToggleValue(playerId);
  }

  public void switchHelpFlagValueForPlayer(String playerId, Boolean newValue) {
    playerRepository.updateHelpToggleFlag(playerId, newValue);
  }

  public List<PlayerDinozStore> spendOneChampifuzTicket(Player account) {
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.CHAMPIFUZ, 1);
    playerRepository.updateLastGeneratedStore(account.getId(), OffsetDateTime.now());
    playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_CHAMPIFUZ, 1)));
    return playerDinozStoreService.renewShopForUser(account.getId());
  }

  public List<PlayerDinozStore> spendOneSpecialChampifuzTicket(Player account, String chosenRace) {
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.SPECIAL_CHAMPIFUZ, 1);
    playerRepository.updateLastGeneratedStore(account.getId(), OffsetDateTime.now());
    return playerDinozStoreService.renewShopForUserWithOnlyOneRace(account.getId(), chosenRace);
  }

  public List<PlayerDinozStore> spendOneChampifuzMonochromeTicket(Player account) {
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.MONO_CHAMPIFUZ, 1);
    playerRepository.updateLastGeneratedStore(account.getId(), OffsetDateTime.now());
    playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_CHAMPIFUZ, 1)));
    return playerDinozStoreService.renewShopForUserWithOnlyOneColor(account.getId());
  }

  public List<PlayerDinozStore> spendOneMagikTicket(Player account) {
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.MAGIK_CHAMPIFUZ, 1);
    playerRepository.updateLastGeneratedStore(account.getId(), OffsetDateTime.now());
    playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_CHAMPIFUZ, 1)));
    return playerDinozStoreService.renewShopForUserWithMagikTicket(account.getId());
  }

  public Boolean trySpecialMoveWithDinoz(Dinoz dinoz, String specialMove) {
    Boolean success = false;
    switch (specialMove) {
      case DinoparcConstants.ACT_ESCALADER:
        if (dinoz.getSkillsMap().containsKey(DinoparcConstants.ESCALADE)) {
          Integer random = ThreadLocalRandom.current().nextInt(1, 5 + 1);
          if (random <= dinoz.getSkillsMap().get(DinoparcConstants.ESCALADE)) {
            playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), null, List.of(new Pair(PgPlayerStatRepository.NB_CLIMB, 1)));
            playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_CLIMB, 1)));
            success = true;
            if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
              dinoz.getActionsMap().replace(DinoparcConstants.DÉPLACERVERT, false);
              dinoz.getActionsMap().replace(DinoparcConstants.POTION_IRMA, true);
            }

            switch (dinoz.getPlaceNumber()) {
              case 11:
                dinoz.setPlaceNumber(22);
                dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, false);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_NAVIGUER, false);
                dinoz.getActionsMap().replace(DinoparcConstants.PRUNE_SHOP, false);
                break;

              case 16:
                dinoz.setPlaceNumber(18);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_DELTAPLANER, false);
                // Restoring combat action if coming from Marais in no combat day
                if (dinoz.getActionsMap().get(DinoparcConstants.COMBAT) == null) {
                  dinoz.getActionsMap().replace(DinoparcConstants.COMBAT, true);
                }
                break;

              case 21:
                dinoz.setPlaceNumber(18);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_DELTAPLANER, false);
                break;
            }
            playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
          } else {
            dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
          }
        } else {
          dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
        }
        break;

      case DinoparcConstants.ACT_NAVIGUER:
        if (dinoz.getSkillsMap().containsKey(DinoparcConstants.NAVIGATION)) {
          Integer random = ThreadLocalRandom.current().nextInt(1, 5 + 1);
          if (random <= dinoz.getSkillsMap().get(DinoparcConstants.NAVIGATION)) {
            success = true;
            playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), null, List.of(new Pair(PgPlayerStatRepository.NB_PRUNAYER, 1)));
            playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_PRUNAYER, 1)));
            if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
              dinoz.getActionsMap().replace(DinoparcConstants.DÉPLACERVERT, false);
              dinoz.getActionsMap().replace(DinoparcConstants.POTION_IRMA, true);
            }

            switch (dinoz.getPlaceNumber()) {
              case 11:
                dinoz.setPlaceNumber(12);
                dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, false);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
                dinoz.getActionsMap().replace(DinoparcConstants.ACT_NAVIGUER, false);
                dinoz.getActionsMap().replace(DinoparcConstants.PRUNE_SHOP, false);
                break;
            }
            playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
          } else {
            dinoz.getActionsMap().replace(DinoparcConstants.ACT_NAVIGUER, false);
          }
        } else {
          dinoz.getActionsMap().replace(DinoparcConstants.ACT_NAVIGUER, false);
        }
        break;

      default:
        success = true;
        if (dinoz.getActionsMap().get(DinoparcConstants.DÉPLACERVERT)) {
          dinoz.getActionsMap().replace(DinoparcConstants.DÉPLACERVERT, false);
          dinoz.getActionsMap().replace(DinoparcConstants.POTION_IRMA, true);
        }

        Integer random = ThreadLocalRandom.current().nextInt(0, 10 + 1);
        String whereToGo = specialMove.substring(18);
        if (whereToGo.length() > 0) {
          dinoz.setPlaceNumber(Integer.parseInt(whereToGo));
          dinoz.getActionsMap().replace(DinoparcConstants.ACT_DELTAPLANER, false);
          dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
          dinoz.getActionsMap().replace(DinoparcConstants.ANOMALY_SHOP, false);
          dinoz.getActionsMap().replace(DinoparcConstants.FUSION_CENTER, false);

          if (dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0 && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {
            dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
          }

          if (dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) != null && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) > 0 && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {
            dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
          }
        } else {
          switch (dinoz.getPlaceNumber()) {
            case 20:
              if (random <= 5) {
                dinoz.setPlaceNumber(1);//irma

              } else if (random > 5 && random < 10) {
                dinoz.setPlaceNumber(3);//dinoplage
                if (dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0) {
                  dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
                }

              } else {
                dinoz.setPlaceNumber(7);//porte
              }

              dinoz.getActionsMap().replace(DinoparcConstants.ACT_DELTAPLANER, false);
              dinoz.getActionsMap().replace(DinoparcConstants.ANOMALY_SHOP, false);
              dinoz.getActionsMap().replace(DinoparcConstants.FUSION_CENTER, false);
              break;

            case 21:
              if (random <= 5) {
                dinoz.setPlaceNumber(4);//barrage

              } else if (random > 5 && random < 10) {
                dinoz.setPlaceNumber(0);//dinoville

              } else {
                dinoz.setPlaceNumber(8);//gredins
              }

              dinoz.getActionsMap().replace(DinoparcConstants.ACT_DELTAPLANER, false);
              dinoz.getActionsMap().replace(DinoparcConstants.ACT_ESCALADER, false);
              break;
          }
        }
        playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_MOVE, 1)));
        break;
    }

    if (success) {
      loadBossInfos(dinoz);
      toggleSpecificLocationActions(dinoz);
      //Pour les Rokkys, on enlève la possibilité de Rocker si l'on se déplace.
      if (dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {
        dinoz.getActionsMap().put(DinoparcConstants.ROCK, false);
      }
      checkForCherryEffectsInteractionWithSpecialMove(dinoz);
    }

    dinozRepository.processMove(dinoz.getId(), dinoz.getPlaceNumber(), dinoz.getActionsMap());
    return success;
  }

  private void checkForCherryEffectsInteractionWithSpecialMove(Dinoz dinoz) {
    if (dinoz.getMalusList().contains(DinoparcConstants.COULIS_CERISE)) {
      Long minutesLeft = ChronoUnit.MINUTES.between(ZonedDateTime.now(ZoneId.of("Europe/Paris")), ZonedDateTime.ofInstant(Instant.ofEpochSecond(dinoz.getEpochSecondsEndOfCherryEffect()),ZoneId.of("Europe/Paris")));
      dinoz.setCherryEffectMinutesLeft(minutesLeft);
      if (minutesLeft < 0) {
        dinoz.getMalusList().remove(DinoparcConstants.COULIS_CERISE);
        dinoz.setCherryEffectMinutesLeft(0);
        dinozRepository.processCurrentCherry(dinoz.getId(), dinoz.getMalusList(), 0);
      } else {
        dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
      }
    }
  }

  private boolean kabukiQuestCompleted(Player account) {
    var playerEpicCollection = collectionRepository.getPlayerCollection(UUID.fromString(account.getId())).getEpicCollection();
    return playerEpicCollection.contains("3");
  }

  private boolean shouldSpawnManny(Player account) {
    Optional<GranitMissionDao> granitMissionDao = granitMissionRepository.getMissionData(UUID.fromString(account.getId()));
    if (granitMissionDao.isPresent() && granitMissionDao.get().getComplete() && !granitMissionDao.get().getCompleteAndValidated()) {
      return true;
    }
    return false;
  }

  private void decideElements(Dinoz myDinoz, Dinoz ww) {
    int elementAttacker = Dinoz.getHighestElementValue(myDinoz.getElementsValues());
    ww.getElementsValues().put(DinoparcConstants.FEU, ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
    ww.getElementsValues().put(DinoparcConstants.TERRE, ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
    ww.getElementsValues().put(DinoparcConstants.EAU, ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
    ww.getElementsValues().put(DinoparcConstants.FOUDRE, ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
    ww.getElementsValues().put(DinoparcConstants.AIR, ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
  }

  private void decideElementsOfTheWildKabuki(Dinoz myDinoz, Dinoz wildKabuki) {
    wildKabuki.setRace(DinoparcConstants.KABUKI);
    int elementAttacker = Dinoz.getHighestElementValue(myDinoz.getElementsValues());
    wildKabuki.getElementsValues().put(DinoparcConstants.FEU, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    wildKabuki.getElementsValues().put(DinoparcConstants.TERRE, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    wildKabuki.getElementsValues().put(DinoparcConstants.EAU, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    wildKabuki.getElementsValues().put(DinoparcConstants.FOUDRE, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    wildKabuki.getElementsValues().put(DinoparcConstants.AIR, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
  }

  public List<String> getLabyrintheValidRaces() {
    return labyrintheValidRaces;
  }

  public void setLabyrintheValidRaces(List<String> labyrintheValidRaces) {
    this.labyrintheValidRaces = labyrintheValidRaces;
  }

  public ShopDto getCraterShop(Player account, Dinoz actualDinoz) {
    ShopDto shopDto = new ShopDto();
    shopDto.setAccountCashAmount(account.getCash());
    shopDto.setQuantityMap(inventoryRepository.getAll(account.getId()));

    Integer combinedMerchantPoints = 0;
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.COMMERCE) && dinoz.getLife() > 0) {
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) > 0) {
          combinedMerchantPoints += dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE);
        }
      }
    }

    if (combinedMerchantPoints > 10) {
      combinedMerchantPoints = 10;
    }

    double discount = 0;
    switch (combinedMerchantPoints) {
      case 1:
        discount = 0.99;
        applyCraterDiscount(shopDto, discount);
        break;

      case 2:
        discount = 0.98;
        applyCraterDiscount(shopDto, discount);
        break;

      case 3:
        discount = 0.97;
        applyCraterDiscount(shopDto, discount);
        break;

      case 4:
        discount = 0.96;
        applyCraterDiscount(shopDto, discount);
        break;

      case 5:
        discount = 0.95;
        applyCraterDiscount(shopDto, discount);
        break;

      case 6:
        discount = 0.94;
        applyCraterDiscount(shopDto, discount);
        break;

      case 7:
        discount = 0.93;
        applyCraterDiscount(shopDto, discount);
        break;

      case 8:
        discount = 0.92;
        applyCraterDiscount(shopDto, discount);
        break;

      case 9:
        discount = 0.91;
        applyCraterDiscount(shopDto, discount);
        break;

      case 10:
        discount = 0.90;
        applyCraterDiscount(shopDto, discount);
        break;

      default:
        discount = 1;
        applyCraterDiscount(shopDto, discount);
        break;
    }

    return shopDto;
  }

  private void applyCraterDiscount(ShopDto shopDto, double discount) {
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_FEU, 200 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_TERRE, 200 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_EAU, 200 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_FOUDRE, 200 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_AIR, 200 * discount);
  }

  public boolean buyObjectsFromCrater(Player account, Dinoz dinoz, BuyRequestDto buyRequest) throws IllegalAccessException {
    if (account.getCash() >= Integer.parseInt(buyRequest.getTotalPrice())) {
      Integer totalPriceCheck = Integer.valueOf(0);

      totalPriceCheck += (Integer.parseInt(buyRequest.getFeuBuy())
          * getCraterShop(account, dinoz).getPricesMap().get(DinoparcConstants.CHARME_FEU).intValue());

      totalPriceCheck += (Integer.parseInt(buyRequest.getTerreBuy())
          * getCraterShop(account, dinoz).getPricesMap().get(DinoparcConstants.CHARME_TERRE).intValue());

      totalPriceCheck += (Integer.parseInt(buyRequest.getEauBuy())
          * getCraterShop(account, dinoz).getPricesMap().get(DinoparcConstants.CHARME_EAU).intValue());

      totalPriceCheck += (Integer.parseInt(buyRequest.getFoudreBuy())
          * getCraterShop(account, dinoz).getPricesMap().get(DinoparcConstants.CHARME_FOUDRE).intValue());

      totalPriceCheck += (Integer.parseInt(buyRequest.getAirBuy())
          * getCraterShop(account, dinoz).getPricesMap().get(DinoparcConstants.CHARME_AIR).intValue());

      if (totalPriceCheck.intValue() == Integer.parseInt(buyRequest.getTotalPrice()) && buyRequest.isValid()) {
        playerRepository.updateCash(account.getId(), -totalPriceCheck);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPriceCheck)));

        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_FEU, Integer.parseInt(buyRequest.getFeuBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_TERRE, Integer.parseInt(buyRequest.getTerreBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_EAU, Integer.parseInt(buyRequest.getEauBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_FOUDRE, Integer.parseInt(buyRequest.getFoudreBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_AIR, Integer.parseInt(buyRequest.getAirBuy()));

        // Ajouter à l'historique
        History buyEvent = new History();
        buyEvent.setPlayerId(account.getId());
        buyEvent.setType("buyObjectCrater");
        buyEvent.setIcon("hist_buy.gif");
        buyEvent.setBuyAmount(totalPriceCheck);
        historyRepository.save(buyEvent);

        return true;
      }
    }

    return false;
  }

  public ShopDto getAnomalyShop(Player account) {
    ShopDto shopDto = new ShopDto();
    shopDto.setAccountCashAmount(account.getCash());
    shopDto.setQuantityMap(inventoryRepository.getAll(account.getId()));

    Integer combinedMerchantPoints = 0;
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.COMMERCE) && dinoz.getLife() > 0) {
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) > 0) {
          combinedMerchantPoints += dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE);
        }
      }
    }

    if (combinedMerchantPoints > 10) {
      combinedMerchantPoints = 10;
    }

    double discount = 0;
    switch (combinedMerchantPoints) {
      case 1:
        discount = 0.99;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 2:
        discount = 0.98;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 3:
        discount = 0.97;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 4:
        discount = 0.96;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 5:
        discount = 0.95;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 6:
        discount = 0.94;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 7:
        discount = 0.93;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 8:
        discount = 0.92;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 9:
        discount = 0.91;
        applyAnomalyDiscount(shopDto, discount);
        break;

      case 10:
        discount = 0.90;
        applyAnomalyDiscount(shopDto, discount);
        break;

      default:
        discount = 1;
        applyAnomalyDiscount(shopDto, discount);
        break;
    }

    return shopDto;
  }

  public ShopDto getPlainsShop(Player account) {
    ShopDto shopDto = new ShopDto();
    shopDto.setAccountCashAmount(account.getCash());
    shopDto.setQuantityMap(inventoryRepository.getAll(account.getId()));

    Integer combinedMerchantPoints = 0;
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.COMMERCE) && dinoz.getLife() > 0) {
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) != null && dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) > 0) {
          combinedMerchantPoints += dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE);
        }
      }
    }

    if (combinedMerchantPoints > 10) {
      combinedMerchantPoints = 10;
    }

    double discount = 0;
    switch (combinedMerchantPoints) {
      case 1:
        discount = 0.99;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 2:
        discount = 0.98;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 3:
        discount = 0.97;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 4:
        discount = 0.96;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 5:
        discount = 0.95;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 6:
        discount = 0.94;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 7:
        discount = 0.93;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 8:
        discount = 0.92;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 9:
        discount = 0.91;
        applyPlainsDiscount(shopDto, discount);
        break;

      case 10:
        discount = 0.90;
        applyPlainsDiscount(shopDto, discount);
        break;

      default:
        discount = 1;
        applyPlainsDiscount(shopDto, discount);
        break;
    }

    return shopDto;
  }

  public ShopDto getPlainsSecretShop(Player account) {
    ShopDto shopDto = new ShopDto();
    var allInventory = inventoryRepository.getAll(account.getId());
    shopDto.setQuantityMap(allInventory);
    shopDto.setAccountCashAmount(allInventory.containsKey(DinoparcConstants.GOUTTE_GLU) ? allInventory.get(DinoparcConstants.GOUTTE_GLU) : 0);

    Integer combinedMerchantPoints = 0;
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.COMMERCE) && dinoz.getLife() > 0) {
        if (dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) != null && dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE) > 0) {
          combinedMerchantPoints += dinoz.getSkillsMap().get(DinoparcConstants.COMMERCE);
        }
      }
    }

    if (combinedMerchantPoints > 10) {
      combinedMerchantPoints = 10;
    }

    double discount = 0;
    switch (combinedMerchantPoints) {
      case 1:
        discount = 0.99;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 2:
        discount = 0.98;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 3:
        discount = 0.97;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 4:
        discount = 0.96;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 5:
        discount = 0.95;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 6:
        discount = 0.94;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 7:
        discount = 0.93;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 8:
        discount = 0.92;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 9:
        discount = 0.91;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      case 10:
        discount = 0.90;
        applySecretPlainsDiscount(shopDto, discount);
        break;

      default:
        discount = 1;
        applySecretPlainsDiscount(shopDto, discount);
        break;
    }

    return shopDto;
  }

  public ShopDto getPirateShop(Player account) {
    ShopDto shopDto = new ShopDto();
    var allInventory = inventoryRepository.getAll(account.getId());
    shopDto.setQuantityMap(allInventory);
    shopDto.setAccountCashAmount(inventoryRepository.getQty(account.getId(), DinoparcConstants.BONS));

    return getWeeklyPiratesItemForSale(shopDto);
  }

  private ShopDto getWeeklyPiratesItemForSale(ShopDto shopDto) {
    Calendar calendar = Calendar.getInstance(Locale.FRANCE);

    Integer weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
    Rng rng = neoparcConfig.getRngFactory()
            .string("PiratesV1")
            .string(String.valueOf(weekOfYear))
            .toRng();

    //There are 14 possibles results :
    Integer result = rng.randIntBetween(1, 14 + 1);

    switch (result) {
      case 1 :
        shopDto.getPricesMap().put(DinoparcConstants.BIERE, 10.0);
        return shopDto;
      case 2 :
        shopDto.getPricesMap().put(DinoparcConstants.FOCUS_MAHAMUTI, 1.0);
        return shopDto;
      case 3 :
        shopDto.getPricesMap().put(DinoparcConstants.LAIT_DE_CARGOU, 5.0);
        return shopDto;
      case 4 :
        shopDto.getPricesMap().put(DinoparcConstants.COULIS_CERISE, 150.0);
        return shopDto;
      case 5 :
        shopDto.getPricesMap().put(DinoparcConstants.OEUF_GLUON, 150.0);
        return shopDto;
      case 6 :
        shopDto.getPricesMap().put(DinoparcConstants.OEUF_SERPANTIN, 125.0);
        return shopDto;
      case 7 :
        shopDto.getPricesMap().put(DinoparcConstants.OEUF_COBALT, 200.0);
        return shopDto;
      case 8 :
        shopDto.getPricesMap().put(DinoparcConstants.ETERNITY_PILL, 30.0);
        return shopDto;
      case 9 :
        shopDto.getPricesMap().put(DinoparcConstants.POTION_SOMBRE, 25.0);
        return shopDto;
      case 10 :
        shopDto.getPricesMap().put(DinoparcConstants.GIFT, 1.0);
        return shopDto;
      case 11 :
        shopDto.getPricesMap().put(DinoparcConstants.SPECIAL_CHAMPIFUZ, 1.0);
        return shopDto;
      case 12 :
        shopDto.getPricesMap().put(DinoparcConstants.MONO_CHAMPIFUZ, 1.0);
        return shopDto;
      case 13 :
        shopDto.getPricesMap().put(DinoparcConstants.MAGIK_CHAMPIFUZ, 200.0);
        return shopDto;
      case 14 :
        shopDto.getPricesMap().put(DinoparcConstants.TISANE, 1.0);
        return shopDto;

      default :
        shopDto.getPricesMap().put(DinoparcConstants.BIERE, 10.0);
        return shopDto;
    }
  }

  private void applySecretPlainsDiscount(ShopDto shopDto, double discount) {
    shopDto.getPricesMap().put(DinoparcConstants.ETERNITY_PILL, 1100 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.OEUF_GLUON, 3300 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.OEUF_SERPANTIN, 2200 * discount);
  }

  private void applyPlainsDiscount(ShopDto shopDto, double discount) {
    shopDto.getPricesMap().put(DinoparcConstants.CHARME_PRISMATIK, 2200 * discount);
  }

  private void applyAnomalyDiscount(ShopDto shopDto, double discount) {
    shopDto.getPricesMap().put(DinoparcConstants.FOCUS_AGGRESIVITE, 2000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.FOCUS_NATURE, 2000 * discount);
    shopDto.getPricesMap().put(DinoparcConstants.FOCUS_SIRAINS, 2000 * discount);
  }

  public boolean buyObjectsFromAnomaly(Player account, BuyRequestDto buyRequest) throws IllegalAccessException {
    if (account.getCash() >= Integer.parseInt(buyRequest.getTotalPrice())) {
      Integer totalPriceCheck = Integer.valueOf(0);

      totalPriceCheck += (Integer.parseInt(buyRequest.getFocusAggroBuy()) * getAnomalyShop(account).getPricesMap().get(DinoparcConstants.FOCUS_AGGRESIVITE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getFocusNatureBuy()) * getAnomalyShop(account).getPricesMap().get(DinoparcConstants.FOCUS_NATURE).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getFocusWaterBuy()) * getAnomalyShop(account).getPricesMap().get(DinoparcConstants.FOCUS_SIRAINS).intValue());

      if (totalPriceCheck.intValue() == Integer.parseInt(buyRequest.getTotalPrice()) && buyRequest.isValid()) {
        playerRepository.updateCash(account.getId(), -totalPriceCheck);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPriceCheck)));

        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.FOCUS_AGGRESIVITE, Integer.parseInt(buyRequest.getFocusAggroBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.FOCUS_NATURE, Integer.parseInt(buyRequest.getFocusNatureBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.FOCUS_SIRAINS, Integer.parseInt(buyRequest.getFocusWaterBuy()));

        // Ajouter à l'historique
        History buyEvent = new History();
        buyEvent.setPlayerId(account.getId());
        buyEvent.setType("buyObjectAnomaly");
        buyEvent.setIcon("hist_buy.gif");
        buyEvent.setBuyAmount(totalPriceCheck);
        historyRepository.save(buyEvent);

        return true;
      }
    }

    return false;
  }

  public boolean buyObjectsFromSecretLair(Player account, BuyRequestDto buyRequest) throws IllegalAccessException {
    if (inventoryRepository.getQty(account.getId(), DinoparcConstants.GOUTTE_GLU) >= Integer.parseInt(buyRequest.getTotalPrice())) {
      Integer totalPriceCheck = Integer.valueOf(0);

      totalPriceCheck += (Integer.parseInt(buyRequest.getEternityPillBuy()) * getPlainsSecretShop(account).getPricesMap().get(DinoparcConstants.ETERNITY_PILL).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getEgg11Buy()) * getPlainsSecretShop(account).getPricesMap().get(DinoparcConstants.OEUF_GLUON).intValue());
      totalPriceCheck += (Integer.parseInt(buyRequest.getEgg18Buy()) * getPlainsSecretShop(account).getPricesMap().get(DinoparcConstants.OEUF_SERPANTIN).intValue());

      if (totalPriceCheck.intValue() == Integer.parseInt(buyRequest.getTotalPrice()) && buyRequest.isValid()) {
        inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.GOUTTE_GLU, totalPriceCheck);
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.ETERNITY_PILL, Integer.parseInt(buyRequest.getEternityPillBuy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.OEUF_GLUON, Integer.parseInt(buyRequest.getEgg11Buy()));
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.OEUF_SERPANTIN, Integer.parseInt(buyRequest.getEgg18Buy()));

        // Ajouter à l'historique
        History buyEvent = new History();
        buyEvent.setPlayerId(account.getId());
        buyEvent.setType("buyObjectSecretLair");
        buyEvent.setIcon("hist_buy.gif");
        buyEvent.setBuyAmount(totalPriceCheck);
        historyRepository.save(buyEvent);

        return true;
      }
    }

    return false;
  }

  public boolean buyObjectsFromPlains(Player account, BuyRequestDto buyRequest) throws IllegalAccessException {
    if (account.getCash() >= Integer.parseInt(buyRequest.getTotalPrice())) {
      Integer totalPriceCheck = 0;
      Integer prismatikBuy = Integer.parseInt(buyRequest.getPrismatikBuy());
      Integer prismatikPrice = getPlainsShop(account).getPricesMap().get(DinoparcConstants.CHARME_PRISMATIK).intValue();
      totalPriceCheck += (prismatikBuy * prismatikPrice);

      if (totalPriceCheck == Integer.parseInt(buyRequest.getTotalPrice()) && buyRequest.isValid()) {
        playerRepository.updateCash(account.getId(), -totalPriceCheck);
        playerStatRepository.addStats(UUID.fromString(account.getId()), null, List.of(new Pair(PgPlayerStatRepository.SUM_GOLD_SHOPS, totalPriceCheck)));

        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CHARME_PRISMATIK, prismatikBuy);

        // Ajouter à l'historique
        History buyEvent = new History();
        buyEvent.setPlayerId(account.getId());
        buyEvent.setType("buyObjectPlains");
        buyEvent.setIcon("hist_buy.gif");
        buyEvent.setBuyAmount(totalPriceCheck);
        historyRepository.save(buyEvent);
        return true;
      }
    }

    return false;
  }

  public boolean buyObjectsFromPirates(Player account, BuyRequestDto buyRequest) throws IllegalAccessException {
    Integer bonsNumber = inventoryRepository.getQty(account.getId(), DinoparcConstants.BONS);
    if (bonsNumber >= Integer.parseInt(buyRequest.getTotalPrice())) {
      Entry<String, Double> objectWithPrice = getPirateShop(account).getPricesMap().entrySet().stream().findFirst().get();
      Integer totalPriceCheck = objectWithPrice.getValue().intValue();

      Integer weekOfYear = Calendar.getInstance(Locale.FRANCE).get(Calendar.WEEK_OF_YEAR);
      Integer year = Calendar.getInstance(Locale.FRANCE).get(Calendar.YEAR);
      String lastPirateBuy = String.valueOf(year) + "-" + String.valueOf(weekOfYear);

      if (totalPriceCheck == Integer.parseInt(buyRequest.getTotalPrice()) && !account.getLastPirateBuy().equalsIgnoreCase(lastPirateBuy)) {
        inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.BONS, totalPriceCheck);
        inventoryRepository.addInventoryItem(account.getId(), objectWithPrice.getKey(), 1);
        playerRepository.updateLastPirateBuy(account.getId(), lastPirateBuy);
        return true;
      }
    }
    return false;
  }

  private boolean dinozHasNoMorePassiveSpace(Dinoz dinoz) {
    Integer counter = 0;
    for (String passive : dinoz.getPassiveList().keySet()) {
      if (dinoz.getPassiveList().get(passive) != null && dinoz.getPassiveList().get(passive) > 0) {
        counter++;
      }
    }

    return (counter >= 8);
  }

  public long getNumberOfDinozOnServer() {
    return dinozRepository.count();
  }

  public List<Dinoz> fetchFusionableDinozForOne(Player account, Dinoz dinoz) {
    List<Dinoz> allDinoz = getAllDinozOfAccount(account);
    List<Dinoz> fusionableDinoz = new ArrayList<Dinoz>();

    for (Dinoz fusionCandidate : allDinoz) {
      if (fusionCandidate.getRace().equals(dinoz.getRace())
          && !fusionCandidate.getId().equals(dinoz.getId())) {
        fusionableDinoz.add(fusionCandidate);
      }
    }

    return fusionableDinoz;
  }

  public Boolean sellDinozToJeanBambois(Player account, Dinoz dinoz) {
    Integer price = (2955 + (dinoz.getLevel() * 631));
    dinozRepository.deleteById(dinoz.getId());
    playerStatRepository.deleteDinozStats(dinoz.getId());
    playerMissionRepository.deletePlayerDinozMission(dinoz.getId());
    playerRepository.updateCash(account.getId(), price);
    playerRepository.deleteDinoz(account.getId(), dinoz.getId());

    // Ajouter à l'historique
    History sellEvent = new History();
    sellEvent.setPlayerId(account.getId());
    sellEvent.setType("sellToJean");
    sellEvent.setIcon("hist_buy.gif");
    sellEvent.setBuyAmount(price);
    sellEvent.setFromDinozName(dinoz.getName());
    historyRepository.save(sellEvent);

    return true;
  }

  public SacrificeResultDto sacrificeDinoz(Player account, Dinoz dinoz) {
    SacrificeResultDto sacrifice = new SacrificeResultDto();
    Integer valeur = (int) (Math.floor(dinoz.getLevel() + Math.sqrt(dinoz.getLevel()) - 19) * 1.86);
    Integer newValeur = account.getSacrificePoints() + valeur;

    if (newValeur >= 200) {
      sacrifice.setPotionsWon(2);
      account.setSacrificePoints(newValeur - 200);
      sacrifice.setNewPercentage(account.getSacrificePoints().doubleValue() / 100);
      inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_SOMBRE, 2);
    } else if (newValeur < 200 && newValeur >= 100) {
      sacrifice.setPotionsWon(1);
      account.setSacrificePoints(newValeur - 100);
      sacrifice.setNewPercentage(account.getSacrificePoints().doubleValue() / 100);
      inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_SOMBRE, 1);
    } else {
      sacrifice.setPotionsWon(0);
      account.setSacrificePoints(newValeur);
      sacrifice.setNewPercentage(account.getSacrificePoints().doubleValue() / 100);
    }

    //Ajouter à l'historique
    History sellEvent = new History();
    sellEvent.setPlayerId(account.getId());
    sellEvent.setType("sacrifice");
    sellEvent.setIcon("hist_death.gif");
    sellEvent.setFromDinozName(dinoz.getName());
    historyRepository.save(sellEvent);

    //Supprimer le Dinoz
    dinozRepository.deleteById(dinoz.getId());
    playerStatRepository.deleteDinozStats(dinoz.getId());
    playerMissionRepository.deletePlayerDinozMission(dinoz.getId());
    playerRepository.deleteDinoz(account.getId(), dinoz.getId());
    playerRepository.updateSacrificePoints(account.getId(), account.getSacrificePoints());

    return sacrifice;
  }

  public Integer takeBath(Player account, Dinoz dinoz) {
    if (account.getCash() < 1700) {
      return -2;

    } else {
      Integer tauntLevel = 0;
      Integer maxThLife = 100;
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.TAUNT) && dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) != null && dinoz.getSkillsMap().get(DinoparcConstants.TAUNT) > 0) {
        tauntLevel = dinoz.getSkillsMap().get(DinoparcConstants.TAUNT);
        maxThLife = maxThLife + (10 * tauntLevel);
      }
      Integer lifeMissing = (maxThLife - dinoz.getLife());
      Integer min = Math.floorDiv(lifeMissing, 4);

      if (lifeMissing == 2 || lifeMissing == 1) {
        min = 1;
      }

      Integer lifeWon = ThreadLocalRandom.current().nextInt(min, lifeMissing + 1);
      playerRepository.updateCash(account.getId(), -1700);

      dinozRepository.setLife(dinoz.getId(), dinoz.getLife() + lifeWon);
      return lifeWon;
    }
  }

  public void drinkFromFountain(Dinoz dinoz) {
    if (dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
      dinoz.getMalusList().remove(DinoparcConstants.CHIKABUM);
      dinoz.getActionsMap().put(DinoparcConstants.DINOFOUNTAIN, false);
      dinozRepository.setActionsAndMalus(dinoz.getId(), dinoz.getActionsMap(), dinoz.getMalusList());
    } else {
      return;
    }
  }

  /**
   * Add item here if you invent a new item and you want it to be sellable at the bazar offers.
   * @return Sellables instance
   */
  public Sellables getAllSellables(Player account, Dinoz dinoz) {
    Sellables sellables = new Sellables();
    sellables.setSellableDinoz(dinoz);

    var accountId = account.getId();
    var prismatikQty = inventoryRepository.getQty(accountId, DinoparcConstants.CHARME_PRISMATIK);
    var sombreQty = inventoryRepository.getQty(accountId, DinoparcConstants.POTION_SOMBRE);
    var champiQty = inventoryRepository.getQty(accountId, DinoparcConstants.CHAMPIFUZ);
    var stEggQty = inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SANTAZ);
    var spEggQty = inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_SERPANTIN);
    var ferEggQty = inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_FEROSS);
    var gEggQty = inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_GLUON);
    var cherryQty = inventoryRepository.getQty(accountId, DinoparcConstants.COULIS_CERISE);
    var giftQty = inventoryRepository.getQty(accountId, DinoparcConstants.GIFT);
    //var eternityQty = inventoryRepository.getQty(accountId, DinoparcConstants.ETERNITY_PILL);
    var pruniacQty = inventoryRepository.getQty(accountId, DinoparcConstants.PRUNIAC);
    var tisaneQty = inventoryRepository.getQty(accountId, DinoparcConstants.TISANE);
    var biereQty = inventoryRepository.getQty(accountId, DinoparcConstants.BIERE);
    var eggXQty = inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_COBALT);
    var specialChampifuzQty = inventoryRepository.getQty(accountId, DinoparcConstants.SPECIAL_CHAMPIFUZ);
    var eggChocolateQty = inventoryRepository.getQty(accountId, DinoparcConstants.OEUF_CHOCOLAT);
    var monochromeChampifuzQty = inventoryRepository.getQty(accountId, DinoparcConstants.MONO_CHAMPIFUZ);
    var magikChampifuzQty = inventoryRepository.getQty(accountId, DinoparcConstants.MAGIK_CHAMPIFUZ);
    var bronzeCasinoTokenQty = inventoryRepository.getQty(accountId, DinoparcConstants.JETON_CASINO_BRONZE);
    var silverCasinoTokenQty = inventoryRepository.getQty(accountId, DinoparcConstants.JETON_CASINO_ARGENT);
    var goldCasinoTokenQty = inventoryRepository.getQty(accountId, DinoparcConstants.JETON_CASINO_OR);

    if (eggChocolateQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.OEUF_CHOCOLAT, eggChocolateQty);
    }
    if (eggXQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.OEUF_COBALT, eggXQty);
    }
    if (biereQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.BIERE, biereQty);
    }
    if (tisaneQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.TISANE, tisaneQty);
    }
    if (pruniacQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.PRUNIAC, pruniacQty);
    }
    if (prismatikQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.CHARME_PRISMATIK, prismatikQty);
    }
    if (sombreQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.POTION_SOMBRE, sombreQty);
    }
    if (champiQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.CHAMPIFUZ, champiQty);
    }
    if (stEggQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.OEUF_SANTAZ, stEggQty);
    }
    if (spEggQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.OEUF_SERPANTIN, spEggQty);
    }
    if (ferEggQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.OEUF_FEROSS, ferEggQty);
    }
    if (gEggQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.OEUF_GLUON, gEggQty);
    }
    if (cherryQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.COULIS_CERISE, cherryQty);
    }
    if (giftQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.GIFT, giftQty);
    }
//    if (eternityQty > 0) {
//      sellables.getSellableObjects().put(DinoparcConstants.ETERNITY_PILL, eternityQty);
//    }

    if (specialChampifuzQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.SPECIAL_CHAMPIFUZ, specialChampifuzQty);
    }

    if (monochromeChampifuzQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.MONO_CHAMPIFUZ, monochromeChampifuzQty);
    }

    if (magikChampifuzQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.MAGIK_CHAMPIFUZ, magikChampifuzQty);
    }

    if (bronzeCasinoTokenQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.JETON_CASINO_BRONZE, bronzeCasinoTokenQty);
    }

    if (silverCasinoTokenQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.JETON_CASINO_ARGENT, silverCasinoTokenQty);
    }

    if (goldCasinoTokenQty > 0) {
      sellables.getSellableObjects().put(DinoparcConstants.JETON_CASINO_OR, goldCasinoTokenQty);
    }

    return sellables;
  }

  public void enterAtDemonsHideout(Dinoz dinoz) {
    if (!dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
      if (dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) != null && dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) >= 1) {
        dinoz.getPassiveList().put(DinoparcConstants.ANTIDOTE, dinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) - 1);
        dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());
      } else {
        dinoz.getMalusList().add(DinoparcConstants.CHIKABUM);
      }
    }

    if (!dinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE)) {
      dinoz.getMalusList().add(DinoparcConstants.EMPOISONNE);
    }

    dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
    dinozRepository.setLife(dinoz.getId(), 1);

    Integer newDanger = dinoz.getDanger() * 2;
    if (newDanger > 666666) {
      dinozRepository.setDanger(dinoz.getId(), 666666);
    } else if (newDanger < 1) {
      dinozRepository.setDanger(dinoz.getId(), 666);
    } else {
      dinozRepository.setDanger(dinoz.getId(), dinoz.getDanger() * 2);
    }
  }

  public Integer exchangePruniacWithDemons(Player player) {
    Random rand = new Random();
    inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.PRUNIAC, 1);
    Collection playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(player.getId()));

    List<String> lootables = Arrays.asList("19", "25", "28", "31", "33", "34", "35", "41", "42", "46");
    List<String> differences = new ArrayList<>(lootables);
    differences.removeAll(playerCollection.getCollection());

    if (!differences.isEmpty() && ThreadLocalRandom.current().nextInt(1, 5) == 1) {
      String objectWon = differences.get(rand.nextInt(differences.size()));
      playerCollection.getCollection().add(objectWon);
      collectionRepository.updateCollection(playerCollection.getId(), playerCollection.getCollection());
      return Integer.parseInt(objectWon);
    } else {
      return 0;
    }
  }

  public Integer getGeneralRankingsNumberOfActives() {
    Integer numberOfActivePlayers = 0;
    for (Player player : playerRepository.findAll()) {
      if (Objects.nonNull(player.getLastLogin()) && this.isPlayerActiveSince7Days(player.getLastLogin())) {
        numberOfActivePlayers++;
      }
    }
    return numberOfActivePlayers;
  }

  private Boolean isPlayerActiveSince7Days(Long timestamp) {
    return (ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond() - timestamp) < 604800;
  }

  public Long getPlayerEventStats(String playerId) {
    return playerStatRepository.getCounter(UUID.fromString(playerId), PLAYER_STAT.EVENT_COUNTER.getName());
  }

  /**
   * _________________________________________________________________________________________________________
   * SECTION : DINOZ IMPORTS!
   * _________________________________________________________________________________________________________
   */
  public boolean hasAlreadyImported(String accountId, String server) {
    Iterable<ImportTraceDto> allImports = importRepository.findAll();

    for (ImportTraceDto it : allImports) {
      if (it.getImportedAccount().equalsIgnoreCase(accountId)
          && it.getOriginServer().equalsIgnoreCase(server)) {
        return true;
      }
    }

    return false;
  }

  public void deleteImportByIdAndServer(String accountId, String server) {
    Iterable<ImportTraceDto> allImports = importRepository.findAll();

    for (ImportTraceDto it : allImports) {
      if (it.getImportedAccount().equalsIgnoreCase(accountId)
          && it.getOriginServer().equalsIgnoreCase(server)) {
        importRepository.delete(it);
      }
    }
  }

  public boolean tryToImportAccount(Player account, String server) {
    ImportOptions options = buildOptionsFromServer(account, server);

    if (options != null) {
      List<String> allImportedDinozIds = new ArrayList<String>();
      Iterable<ImportTraceDto> allImports = importRepository.findAll();

      for (ImportTraceDto it : allImports) {
        allImportedDinozIds.addAll(it.getImportedDinoz());
      }

      ImportTraceDto it = initImportTrace(account.getId(), server);

      try {
        Map<DinoparcDinozId, EtwinDinoparcDinoz> rawDinozMap = migrationUtils.importUser(options);
        List<Dinoz> importedDinoz = new ArrayList<Dinoz>();
        for (EtwinDinoparcDinoz rawDinoz : rawDinozMap.values()) {
          if (!allImportedDinozIds.contains(rawDinoz.getId().toString())) {
            Dinoz dinoz = mapDinoz(rawDinoz, account.getId(), account.getName(), server);
            importedDinoz.add(dinoz);
            it.getImportedDinoz().add(dinoz.getId());
          }
        }

        Comparator<String> idOrder = (idA, idB) -> {
          try {
            return Integer.valueOf(idA) - Integer.valueOf(idB);

          } catch (Exception e) {
            return idA.length() - idB.length();
          }
        };

        Collections.sort(it.getImportedDinoz(), idOrder);

        importedDinoz.forEach(dinoz -> this.dinozRepository.create(dinoz));
        this.playerRepository.save(account);
        this.playerRepository.addAllDinoz(account.getId(), it.getImportedDinoz());
        this.refreshPointsAndCollection(account);
        this.importRepository.save(it);
        return true;

      } catch (Exception e) {
        String stacktrace = ExceptionUtils.getStackTrace(e);
        System.out.println(stacktrace);
        return false;
      }
    }

    return false;
  }

  private void refreshPointsAndCollection(Player account) {
    Integer sumOfPoints = 0;
    for (Dinoz dinoz : this.getAllDinozOfAccount(account)) {
      sumOfPoints += dinoz.getLevel();
    }

    playerRepository.updatePoints(account.getId(), sumOfPoints, sumOfPoints / playerRepository.countDinoz(account.getId()));
    var collection = collectionRepository.getPlayerCollection(UUID.fromString(account.getId()));
    var epicCollection = collection.getEpicCollection();

    if (!epicCollection.contains("14")) {
      epicCollection.add("14");
      collectionRepository.updateEpicCollection(collection.getId(), epicCollection);
    }
  }

  private ImportTraceDto initImportTrace(String accountId, String server) {
    ImportTraceDto importTrace = new ImportTraceDto();
    importTrace.setId(UUID.randomUUID().toString());
    importTrace.setImportationDate(OffsetDateTime.now(ZoneId.of("Europe/Paris")));
    importTrace.setImportedAccount(accountId);
    importTrace.setNewHostAccountId(accountId);
    importTrace.setOriginServer(server);

    return importTrace;
  }

  private ImportOptions buildOptionsFromServer(Player account, String server) {
    switch (server) {
      case "fr":
        return new ImportOptions(new UserId(account.getId()), DinoparcServer.DinoparcCom, false,
            true);

      case "es":
        return new ImportOptions(new UserId(account.getId()), DinoparcServer.SpDinoparcCom, false,
            true);

      case "en":
        return new ImportOptions(new UserId(account.getId()), DinoparcServer.EnDinoparcCom, false,
            true);

      default:
        return null;
    }
  }

  private Dinoz mapDinoz(EtwinDinoparcDinoz rawDinoz, String masterId, String masterName,
      String server) {
    Dinoz dinoz = new Dinoz();

    switch (server) {
      case "fr":
        dinoz.setId(rawDinoz.getId().toString());
        break;
      case "es":
        dinoz.setId("999" + rawDinoz.getId().toString());
        break;
      case "en":
        dinoz.setId("888" + rawDinoz.getId().toString());
        break;
      default:
        dinoz.setId(rawDinoz.getId().toString());
        break;
    }

    dinoz.setMasterId(masterId);
    dinoz.setMasterName(masterName);
    dinoz.setName(rawDinoz.getName().getLatest().getValue().toString());
    dinoz.setRace(getDinozRace(rawDinoz.getRace().getLatest().getValue()));
    dinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
    dinoz.setEndMessage(DinoparcConstants.ENDMESSAGE1);
    dinoz.setLife(100);
    dinoz.setLevel(rawDinoz.getLevel().getLatest().getValue().toInt());
    dinoz.setExperience(0);
    dinoz.setDanger(0);
    dinoz.setPlaceNumber(0);
    dinoz.setAppearanceCode(rawDinoz.getSkin().getLatest().getValue().getInner().toString());

    if (dinoz.getAppearanceCode().endsWith("#") || dinoz.getAppearanceCode().endsWith("%23")) {
      dinoz.setDark(true);
    }

    dinoz.setInTourney(false);
    initImportedDinozActions(rawDinoz, dinoz);
    initImportedDinozElements(rawDinoz, dinoz);
    initImportedDinozSkills(rawDinoz, dinoz);

    return dinoz;
  }

  private void initImportedDinozSkills(EtwinDinoparcDinoz rawDinoz, Dinoz dinoz) {
    for (Entry<DinoparcSkill, DinoparcSkillLevel> skill : rawDinoz.getSkills().getLatest().getValue().entrySet()) {
      if (skillIsTranfered(skill)) {
        dinoz.getSkillsMap().put(getSkillConstantFromEtwinName(skill.getKey()), skill.getValue().toInt());
      }
    }
  }

  private boolean skillIsTranfered(Entry<DinoparcSkill, DinoparcSkillLevel> skill) {
    if (!skill.getKey().equals(DinoparcSkill.TotemThief)
        && !skill.getKey().equals(DinoparcSkill.Saboteur)
        && !skill.getKey().equals(DinoparcSkill.Spy)
        && !skill.getKey().equals(DinoparcSkill.Mercenary)) {
      return true;
    }
    return false;
  }

  private void initImportedDinozElements(EtwinDinoparcDinoz rawDinoz, Dinoz dinoz) {
    dinoz.getElementsValues().put(DinoparcConstants.FEU,
        rawDinoz.getElements().getLatest().getValue().getFire().toInt());
    dinoz.getElementsValues().put(DinoparcConstants.TERRE,
        rawDinoz.getElements().getLatest().getValue().getEarth().toInt());
    dinoz.getElementsValues().put(DinoparcConstants.EAU,
        rawDinoz.getElements().getLatest().getValue().getWater().toInt());
    dinoz.getElementsValues().put(DinoparcConstants.FOUDRE,
        rawDinoz.getElements().getLatest().getValue().getThunder().toInt());
    dinoz.getElementsValues().put(DinoparcConstants.AIR,
        rawDinoz.getElements().getLatest().getValue().getAir().toInt());
  }

  private void initImportedDinozActions(EtwinDinoparcDinoz rawDinoz, Dinoz dinoz) {
    dinoz.getActionsMap().put(DinoparcConstants.TournoiDinoville, true);
    dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);

    if (rawDinoz.getSkills().getLatest().getValue().containsKey(DinoparcSkill.Dig)
        && rawDinoz.getSkills().getLatest().getValue().get(DinoparcSkill.Dig).toInt() > 0) {
      dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    }

    if (rawDinoz.getSkills().getLatest().getValue().containsKey(DinoparcSkill.Juggle)
        && rawDinoz.getSkills().getLatest().getValue().get(DinoparcSkill.Juggle).toInt() > 0) {
      dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
    }
  }

  private String getDinozRace(DinoparcDinozRace value) {
    switch (value) {
      case Moueffe:
        return DinoparcConstants.MOUEFFE;
      case Picori:
        return DinoparcConstants.PICORI;
      case Castivore:
        return DinoparcConstants.CASTIVORE;
      case Sirain:
        return DinoparcConstants.SIRAIN;
      case Winks:
        return DinoparcConstants.WINKS;
      case Gorilloz:
        return DinoparcConstants.GORILLOZ;
      case Cargou:
        return DinoparcConstants.CARGOU;
      case Hippoclamp:
        return DinoparcConstants.HIPPOCLAMP;
      case Rokky:
        return DinoparcConstants.ROKKY;
      case Pigmou:
        return DinoparcConstants.PIGMOU;
      case Wanwan:
        return DinoparcConstants.WANWAN;
      case Gluon:
        return DinoparcConstants.GOUPIGNON;
      case Kump:
        return DinoparcConstants.KUMP;
      case Pteroz:
        return DinoparcConstants.PTEROZ;
      case Santaz:
        return DinoparcConstants.SANTAZ;
      case Ouistiti:
        return DinoparcConstants.OUISTITI;
      case Korgon:
        return DinoparcConstants.KORGON;
      case Kabuki:
        return DinoparcConstants.KABUKI;
      case Serpantin:
        return DinoparcConstants.SERPANTIN;

      default:
        return null;
    }
  }

  private String getSkillConstantFromEtwinName(DinoparcSkill dinoparcSkill) {
    switch (dinoparcSkill) {
      case Dexterity:
        return DinoparcConstants.AGILITÉ;
      case Intelligence:
        return DinoparcConstants.INTELLIGENCE;
      case Perception:
        return DinoparcConstants.PERCEPTION;
      case Stamina:
        return DinoparcConstants.ENDURANCE;
      case Strength:
        return DinoparcConstants.FORCE;
      case Dig:
        return DinoparcConstants.FOUILLER;
      case Medicine:
        return DinoparcConstants.MEDECINE;
      case Swim:
        return DinoparcConstants.NATATION;
      case Camouflage:
        return DinoparcConstants.CAMOUFLAGE;
      case Climb:
        return DinoparcConstants.ESCALADE;
      case MartialArts:
        return DinoparcConstants.ARTS_MARTIAUX;
      case Steal:
        return DinoparcConstants.VOLER;
      case Provoke:
        return DinoparcConstants.TAUNT;
      case Bargain:
        return DinoparcConstants.COMMERCE;
      case Navigation:
        return DinoparcConstants.NAVIGATION;
      case Run:
        return DinoparcConstants.COURSE;
      case Survival:
        return DinoparcConstants.SURVIE;
      case Strategy:
        return DinoparcConstants.STRATEGIE;
      case Music:
        return DinoparcConstants.MUSIQUE;
      case Jump:
        return DinoparcConstants.SAUT;
      case Cook:
        return DinoparcConstants.CUISINE;
      case Luck:
        return DinoparcConstants.CHANCE;
      case Counterattack:
        return DinoparcConstants.CONTRE_ATTAQUE;
      case Juggle:
        return DinoparcConstants.ROCK;
      case FireProtection:
        return DinoparcConstants.PROTECTIONDUFEU;
      case FireApprentice:
        return DinoparcConstants.APPRENTI_FEU;
      case EarthApprentice:
        return DinoparcConstants.APPRENTI_TERRE;
      case WaterApprentice:
        return DinoparcConstants.APPRENTI_EAU;
      case ThunderApprentice:
        return DinoparcConstants.APPRENTI_FOUDRE;
      case ShadowPower:
        return DinoparcConstants.POUVOIR_SOMBRE;

      default:
        return null;
    }
  }

  public void moveDinozInList(Player account, String dinozId, Integer direction, Integer count) {
    playerRepository.moveDinoz(account.getId(), dinozId, direction.equals(1), count);
  }

  /**
   *
   * _________________________________________________________________________________________________________
   *
   * SECTION : MESSAGING!
   * _________________________________________________________________________________________________________
   *
   *
   */
  public boolean markMessagesAsViewed(Player account) {
    playerRepository.updateMessages(account.getId(), false);
    return true;
  }

  public HashMap<String, String> getBlackList(Player account) {
    return account.getBlackList();
  }

  public boolean addToBlackList(Player account, String accountToAddId) {
    HashMap<String, String> newBlackList = account.getBlackList();
    String accountToAddName = playerRepository.findById(accountToAddId).get().getName();
    newBlackList.put(accountToAddId, accountToAddName);
    playerRepository.updateBlackList(account.getId(), newBlackList);
    return true;
  }

  public boolean removeFromBlackList(Player account, String accountToRemoveId) {
    HashMap<String, String> newBlackList = account.getBlackList();
    newBlackList.remove(accountToRemoveId);
    playerRepository.updateBlackList(account.getId(), newBlackList);
    return true;
  }

  public Double getSacrificeProgress(Player account) {
    if (account.getSacrificePoints() == null) {
      playerRepository.updateSacrificePoints(account.getId(), 0);
      return 0D;
    }
    return Double.parseDouble(account.getSacrificePoints().toString()) / 100;
  }

  public ChaudronCraftTryResult tryChaudronCraft(Player player, Integer recipeNumber) {
    return chaudronService.findMatchingRecipe(player.getId(), inventoryRepository, recipeNumber);
  }

  public ChaudronCraftTryResult tradeRamenWithFisherman(Player player, Dinoz dinoz) {
    ChaudronCraftTryResult result = new ChaudronCraftTryResult();

    if (inventoryRepository.getQty(player.getId(), DinoparcConstants.RAMENS) > 0 && !player.getHasClaimedTradeFromFishermanToday()) {
      inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.RAMENS, 1);

      //Draw result :
      RandomCollection<Object> randomElement = new RandomCollection<>()
              .add(10, DinoparcConstants.ANTIDOTE)
              .add(10, DinoparcConstants.CHARME_FEU)
              .add(10, DinoparcConstants.CHARME_TERRE)
              .add(10, DinoparcConstants.CHARME_EAU)
              .add(10, DinoparcConstants.CHARME_FOUDRE)
              .add(10, DinoparcConstants.CHARME_AIR)
              .add(20, DinoparcConstants.JETON_CASINO_BRONZE)
              .add(7, DinoparcConstants.JETON_CASINO_ARGENT)
              .add(3, DinoparcConstants.JETON_CASINO_OR);

      String drawnElement = (String) randomElement.next();
      result.setGeneratedObject(drawnElement);
      inventoryRepository.addInventoryItem(player.getId(), drawnElement, 1);

      result.setSuccess(true);
      result.setGeneratedObjectQty(1);
      dinoz.getActionsMap().put(DinoparcConstants.INUIT, false);
      dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());
      playerRepository.updateHasClaimedTradeFromFishermanToday(player.getId(), true);
    }

    return result;
  }

  public Integer getMerchantLocation() {
    return this.merchantLocation;
  }

  public void changeMerchantLocation(Integer newLocation) {
    this.merchantLocation = newLocation;
  }

//  public static Integer getHalloweenHordesLocation() {
//    return halloweenHordesLocation;
//  }
//
//  public void changeHalloweenHordesLocation(Integer newLocation) {
//    this.halloweenHordesLocation = newLocation;
//  }

  public boolean sellToMerchant(Player account, String dinozId, HashMap<String, Integer> ingredientsMap) {
    Optional<Dinoz> dinoz = dinozRepository.findById(dinozId);
    if (!dinoz.isPresent() || dinoz.get().getPlaceNumber() != this.getMerchantLocation()) {
      return false;
    }

    Integer amount = 0;
    Map<String, Integer> currentIngredients = inventoryRepository.getAll(account.getId());
    for (var ingredient : ingredientsMap.entrySet()) {
      String ingredientName = ingredient.getKey();
      Integer quantity = ingredient.getValue();
      Integer currentQuantity = currentIngredients.get(ingredientName);
      Integer price = DinoparcConstants.PRICES.get(ingredientName);

      if (quantity > currentQuantity || quantity < 0) {
        return false;
      }

      amount = quantity * price;
      inventoryRepository.substractInventoryItem(account.getId(), ingredientName, quantity);
      playerRepository.updateCash(account.getId(), amount);
    }
    return true;
  }

  public boolean sellAllIngredientsToMerchant(Player account, String dinozId) {
    Optional<Dinoz> dinoz = dinozRepository.findById(dinozId);
    if (!dinoz.isPresent() || dinoz.get().getPlaceNumber() != this.getMerchantLocation()) {
      return false;
    }

    Map<String, Integer> ingredientsMap = new HashMap<>();
    Integer amount = 0;
    Map<String, Integer> currentIngredients = inventoryRepository.getAll(account.getId());
    ingredientsMap.put(DinoparcConstants.BUZE, currentIngredients.get(DinoparcConstants.BUZE));
    ingredientsMap.put(DinoparcConstants.PLUME, currentIngredients.get(DinoparcConstants.PLUME));
    ingredientsMap.put(DinoparcConstants.DENT, currentIngredients.get(DinoparcConstants.DENT));
    ingredientsMap.put(DinoparcConstants.CRIN, currentIngredients.get(DinoparcConstants.CRIN));
    ingredientsMap.put(DinoparcConstants.BOIS, currentIngredients.get(DinoparcConstants.BOIS));

    ingredientsMap.put(DinoparcConstants.PERCHE_PERLEE, currentIngredients.get(DinoparcConstants.PERCHE_PERLEE));
    ingredientsMap.put(DinoparcConstants.GREMILLE_GRELOTTANTE, currentIngredients.get(DinoparcConstants.GREMILLE_GRELOTTANTE));
    ingredientsMap.put(DinoparcConstants.CUBE_GLU, currentIngredients.get(DinoparcConstants.CUBE_GLU));
    ingredientsMap.put(DinoparcConstants.ANGUILLE_CELESTE, currentIngredients.get(DinoparcConstants.ANGUILLE_CELESTE));

    ingredientsMap.put(DinoparcConstants.FEUILLE_PACIFIQUE, currentIngredients.get(DinoparcConstants.FEUILLE_PACIFIQUE));
    ingredientsMap.put(DinoparcConstants.OREADE_BLANC, currentIngredients.get(DinoparcConstants.OREADE_BLANC));
    ingredientsMap.put(DinoparcConstants.TIGE_RONCIVORE, currentIngredients.get(DinoparcConstants.TIGE_RONCIVORE));
    ingredientsMap.put(DinoparcConstants.ANÉMONE_SOLITAIRE, currentIngredients.get(DinoparcConstants.ANÉMONE_SOLITAIRE));

    for (var ingredient : ingredientsMap.entrySet()) {
      String ingredientName = ingredient.getKey();
      Integer quantity = ingredient.getValue();
      Integer currentQuantity = currentIngredients.get(ingredientName);
      Integer price = DinoparcConstants.PRICES.get(ingredientName);

      if (quantity > currentQuantity || quantity < 0) {
        return false;
      }

      amount = quantity * price;
      inventoryRepository.substractInventoryItem(account.getId(), ingredientName, quantity);
      playerRepository.updateCash(account.getId(), amount);
    }
    return true;
  }

  public Boolean isCollectionFullOfTradableItems(String playerId) {
    Collection collection = collectionRepository.getPlayerCollection(UUID.fromString(playerId));
    ArrayList<String> removableCollectionItems = getTradableCollectionObjectList();
    return collection.getCollection().containsAll(removableCollectionItems);
  }

//  public TradeCollectionResponseDto tradeCollection(Player player, String dinozId) {
//    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
//    Collection collection = collectionRepository.getPlayerCollection(UUID.fromString(player.getId()));
//    ArrayList<String> removableCollectionItems = getTradableCollectionObjectList();
//    Long eventCounter = playerStatRepository.getCounter(UUID.fromString(player.getId()), PLAYER_STAT.EVENT_COUNTER.getName());
//
//    if (now.getMonthValue() == 12 && eventCounter < now.getDayOfMonth() && isCollectionFullOfTradableItems(player.getId())) {
//      Optional<Dinoz> dinoz = dinozRepository.findById(dinozId);
//      if (!dinoz.isPresent() || dinoz.get().getPlaceNumber() != this.getMerchantLocation()) {
//        return new TradeCollectionResponseDto(0, 0, false);
//
//      } else {
//        inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.GIFT, 10);
//        playerStatRepository.addStats(UUID.fromString(player.getId()), null, List.of(new Pair(PgPlayerStatRepository.EVENT_COUNTER, 1)));
//        Integer magikTickets = 0;
//        if (eventCounter % 5 == 0) {
//          magikTickets = 1;
//          inventoryRepository.addInventoryItem(player.getId(), DinoparcConstants.MAGIK_CHAMPIFUZ, magikTickets);
//        }
//
//        collection.getCollection().removeAll(removableCollectionItems);
//        collectionRepository.updateCollection(collection.getId(), collection.getCollection());
//        return new TradeCollectionResponseDto(10, magikTickets, true);
//      }
//    }
//    return new TradeCollectionResponseDto(0, 0, false);
//  }

  @NotNull
  private static ArrayList<String> getTradableCollectionObjectList() {
    ArrayList<String> removableCollectionItems = new ArrayList<>();
    for (int i = 16; i <= 35; i++) {
      removableCollectionItems.add(String.valueOf(i));
    }
    for (int i = 41; i <= 49; i++) {
      removableCollectionItems.add(String.valueOf(i));
    }
    return removableCollectionItems;
  }

  public String getRaidCluesPieces(String dinozId) {
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    Rng rng = neoparcConfig.getRngFactory()
            .string("getRaidCluesPiecesV2")
            .string(dinozId)
            .string(String.valueOf(now.getDayOfYear()))
            .toRng();

    EventDinozDto raidBoss = raidBossRepository.getRaidBoss();
    switch (rng.randIntBetween(0, 6)) {
      case 0 :
        return String.valueOf(raidBoss.getDayOfWeek());
      case 1 :
        return String.valueOf(raidBoss.getHourOfDay());
      case 2 :
        return String.valueOf(raidBoss.getPlaceNumber());
      case 3 :
        return "1 - " + raidBoss.getFirstElement();
      case 4 :
        return "2 - " + raidBoss.getSecondElement();
      case 5 :
        return "3 - " + raidBoss.getThirdElement();
      default :
        return String.valueOf(raidBoss.getDayOfWeek());
    }
  }

  public Integer processFishingForDinoz(Player account, Dinoz dinoz) {
      Integer fishingLevel = dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE);
      Integer fishingResult = 0;

    if (ThreadLocalRandom.current().nextBoolean()
            || (Arrays.asList(
                    DinoparcConstants.WINKS,
                    DinoparcConstants.SIRAIN,
                    DinoparcConstants.KUMP,
                    DinoparcConstants.CARGOU)
            .contains(dinoz.getRace()) && ThreadLocalRandom.current().nextInt(1, 100 + 1) <= 80)
            || dinoz.getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU)) {
        if (fishingLevel < 3) {
          fishingResult = 1;
        } else {
          fishingResult = getRandomFish(dinoz.getPlaceNumber());
        }

        if (fishingResult == 1) {
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.PERCHE_PERLEE, 1);
        } else if (fishingResult == 2) {
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.GREMILLE_GRELOTTANTE, 1);
        } else if (fishingResult == 3) {
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.CUBE_GLU, 1);
        } else if (fishingResult == 4) {
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.ANGUILLE_CELESTE, 1);
        } else if (fishingResult == 5) {
          inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.OEUF_CHOCOLAT, 1);
        }
      }

      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), null, List.of(new Pair(PgPlayerStatRepository.NB_FISHING, 1)));
      playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_FISHING, 1)));

    if (!dinoz.getMalusList().contains(DinoparcConstants.CANNEAPECHE)) {
      dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, false);
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
      dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());
    } else {
      checkForCanneAPecheVanishing(dinoz);
    }

    return fishingResult;
  }

  public void checkForCanneAPecheVanishing(Dinoz dinoz) {
    if (dinoz.getMalusList().contains(DinoparcConstants.CANNEAPECHE)) {
      Integer vanishOdds = ThreadLocalRandom.current().nextInt(1, 100 + 1);
      Integer durability = 9;
      if (dinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && dinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= dinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }
      if (vanishOdds < durability) {
        dinoz.getMalusList().remove(DinoparcConstants.CANNEAPECHE);
        dinozRepository.setMalusList(dinoz.getId(), dinoz.getMalusList());
      }
    }
  }

  public Integer processPickingForDinoz(Player account, Dinoz dinoz) {
    Integer pickingLevel = dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE);
    Integer pickingResult = 0;

    if (ThreadLocalRandom.current().nextBoolean()
            || (Arrays.asList(
                    DinoparcConstants.GORILLOZ,
                    DinoparcConstants.KORGON,
                    DinoparcConstants.CASTIVORE,
                    DinoparcConstants.CARGOU)
            .contains(dinoz.getRace()) && ThreadLocalRandom.current().nextInt(1, 100 + 1) <= 80)
            || dinoz.getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU)) {
      if (pickingLevel < 3) {
        pickingResult = 1;
      } else {
        pickingResult = getRandomPlant();
      }

      if (pickingResult == 1) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.FEUILLE_PACIFIQUE, 1);
      } else if (pickingResult == 2) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.OREADE_BLANC, 1);
      } else if (pickingResult == 3) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.TIGE_RONCIVORE, 1);
      } else if (pickingResult == 4) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.ANÉMONE_SOLITAIRE, 1);
      } else if (pickingResult == 5) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.OEUF_CHOCOLAT, 1);
      }
    }

    playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), null, List.of(new Pair(PgPlayerStatRepository.NB_PICKING, 1)));
    playerStatRepository.addStats(UUID.fromString(dinoz.getMasterId()), dinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_PICKING, 1)));

    dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, false);
    dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
    dinozRepository.setActionsMap(dinoz.getId(), dinoz.getActionsMap());

    return pickingResult;
  }

  private Integer getRandomFish(Integer placeNumber) {
    RandomCollection<Object> randomElement = null;
    if (placeNumber == 33) {
      randomElement = new RandomCollection<>()
              .add(55, DinoparcConstants.PERCHE_PERLEE)
              .add(35, DinoparcConstants.GREMILLE_GRELOTTANTE)
              .add(5, DinoparcConstants.CUBE_GLU)
              .add(1, DinoparcConstants.ANGUILLE_CELESTE);
              //.add(5, DinoparcConstants.OEUF_CHOCOLAT);
    } else {
      randomElement = new RandomCollection<>()
              .add(55, DinoparcConstants.PERCHE_PERLEE)
              .add(35, DinoparcConstants.GREMILLE_GRELOTTANTE)
              .add(5, DinoparcConstants.CUBE_GLU);
              //.add(5, DinoparcConstants.OEUF_CHOCOLAT);
    }

    String drawnElement = (String) randomElement.next();
    if (drawnElement.equalsIgnoreCase(DinoparcConstants.PERCHE_PERLEE)) {
      return 1;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.GREMILLE_GRELOTTANTE)) {
      return 2;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.CUBE_GLU)) {
      return 3;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.ANGUILLE_CELESTE)) {
      return 4;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.OEUF_CHOCOLAT)) {
      return 5;
    }
    return 0;
  }

  private Integer getRandomPlant() {
    RandomCollection<Object> randomElement;
    String drawnElement;

    randomElement = new RandomCollection<>()
            .add(50, DinoparcConstants.FEUILLE_PACIFIQUE)
            .add(30, DinoparcConstants.OREADE_BLANC)
            .add(15, DinoparcConstants.TIGE_RONCIVORE)
            .add(5, DinoparcConstants.ANÉMONE_SOLITAIRE);
            //.add(5, DinoparcConstants.OEUF_CHOCOLAT);

    drawnElement = (String) randomElement.next();
    if (drawnElement.equalsIgnoreCase(DinoparcConstants.FEUILLE_PACIFIQUE)) {
      return 1;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.OREADE_BLANC)) {
      return 2;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.TIGE_RONCIVORE)) {
      return 3;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.ANÉMONE_SOLITAIRE)) {
      return 4;
    } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.OEUF_CHOCOLAT)) {
      return 5;
    }
    return 0;
  }

  private boolean dinozIsCleanFromEffects(List<String> malusList) {
    if (malusList.contains(DinoparcConstants.CHIKABUM) || malusList.contains(DinoparcConstants.EMPOISONNE)) {
      return false;
    }
    return true;
  }

  public boolean checkIfDinozIsWarrior(List<String> malusList) {
    return (malusList.contains(DinoparcConstants.FACTION_1_GUERRIER)
            || malusList.contains(DinoparcConstants.FACTION_2_GUERRIER)
            || malusList.contains(DinoparcConstants.FACTION_3_GUERRIER)
    );
  }

  public InputStream downloadDinozData(String accountId) {
    List<Dinoz> allMyDinoz = dinozRepository.findByPlayerId(accountId);
    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
         CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format)) {
      csvPrinter.printRecord(Arrays.asList(
              "Name",
              "Level",
              "Race",
              "Fire",
              "Earth",
              "Water",
              "Lightning",
              "Wind",
              "Major Element",
              "2nd Element",
              "3rd Element",
              "Maxed"
      ));

      for (Dinoz dinoz : allMyDinoz) {
        FightElements fightElements = new FightElements(dinoz);
        List<String> data = Arrays.asList(
                dinoz.getName(),
                String.valueOf(dinoz.getLevel()),
                dinoz.getRace(),
                String.valueOf(dinoz.getElementsValues().get(DinoparcConstants.FEU)),
                String.valueOf(dinoz.getElementsValues().get(DinoparcConstants.TERRE)),
                String.valueOf(dinoz.getElementsValues().get(DinoparcConstants.EAU)),
                String.valueOf(dinoz.getElementsValues().get(DinoparcConstants.FOUDRE)),
                String.valueOf(dinoz.getElementsValues().get(DinoparcConstants.AIR)),
                String.valueOf(fightElements.getHighestLeft()),
                String.valueOf(fightElements.getSecondHighestLeft()),
                String.valueOf(fightElements.getThirdHighestLeft()),
                String.valueOf(isDinozMaxed(dinoz, !getAvailableLearnings(dinoz).isEmpty()))
        );
        csvPrinter.printRecord(data);
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());

    } catch (IOException e) {
      throw new RuntimeException("Failed to export data to CSV file: " + e.getMessage());
    }
  }

  //Méthode pour empêcher que les Dinoz de deux joueurs spécifiques se croisent quand y'a de l'acharnement qui fait en sorte que le jeu n'est plus un jeu.
  private boolean checkIfBothPlayersAreNotExcludedFromOneAnother(Dinoz homeDinoz, Dinoz foreignDinoz) {
    //Case #1 : Arminius & Dragoon29
    if (("46c65b80-93ac-49cd-8e72-c3d69edffea9".equals(homeDinoz.getMasterId()) && "b7dc60ca-c8d3-4df3-aad0-db9f2b95024f".equals(foreignDinoz.getMasterId()))
        || ("b7dc60ca-c8d3-4df3-aad0-db9f2b95024f".equals(homeDinoz.getMasterId()) && "46c65b80-93ac-49cd-8e72-c3d69edffea9".equals(foreignDinoz.getMasterId()))) {
      return false;
    }

    return true;
  }
}
