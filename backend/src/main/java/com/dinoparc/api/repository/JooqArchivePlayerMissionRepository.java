package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.PlayerMission;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.dinoparc.tables.ArchivePlayerMission.ARCHIVE_PLAYER_MISSION;

@Repository
public class JooqArchivePlayerMissionRepository implements PgArchivePlayerMissionRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqArchivePlayerMissionRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }
    
    @Override
    public void create(PlayerMission playerMission) {
        jooqContext.insertInto(ARCHIVE_PLAYER_MISSION).set(jooqContext.newRecord(ARCHIVE_PLAYER_MISSION, playerMission)).execute();
    }
}
