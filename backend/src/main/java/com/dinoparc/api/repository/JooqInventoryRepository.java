package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.tables.Inventory;
import com.dinoparc.tables.records.InventoryRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.dinoparc.tables.Inventory.INVENTORY;

@Repository
public class JooqInventoryRepository implements PgInventoryRepository {

    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private DSLContext jooqContext;

    @Autowired
    public JooqInventoryRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
    }

    @Override
    public Map<String, Integer> getAll(String accountId) {
        var records = jooqContext.selectFrom(INVENTORY).where(INVENTORY.PLAYER_ID.eq(UUID.fromString(accountId))).fetch();
        return this.fromRecords(records);
    }

    @Override
    public Integer getQty(String accountId, String name) {
        Optional<InventoryRecord> optRecord = jooqContext.selectFrom(INVENTORY)
                .where(INVENTORY.PLAYER_ID.eq(UUID.fromString(accountId)).and(INVENTORY.NAME.eq(name)))
                .fetchOptional();

        return optRecord.isPresent() ? optRecord.get().getQuantity() : 0;
    }

    @Override
    public Long getTotalQtyOnServer(String name) {
        return jooqContext.selectFrom(INVENTORY)
                .where(INVENTORY.NAME.eq(name))
                .stream()
                .mapToLong(InventoryRecord::getQuantity)
                .sum();
    }

    @Override
    public void fixBondsIssues() {
        jooqContext.update(INVENTORY).set(INVENTORY.QUANTITY, 0)
                .where(INVENTORY.QUANTITY.eq(99999))
                .and(INVENTORY.NAME.eq(DinoparcConstants.BONS))
                .execute();
    }

    @Override
    public void addInventoryItem(String accountId, String name, int qty) {
        var playerId = UUID.fromString(accountId);
        Optional<InventoryRecord> optRecord = jooqContext.selectFrom(INVENTORY)
                .where(INVENTORY.PLAYER_ID.eq(playerId).and(INVENTORY.NAME.eq(name)))
                .fetchOptional();

        if (optRecord.isPresent()) {
            jooqContext.update(INVENTORY).set(INVENTORY.QUANTITY, INVENTORY.QUANTITY.add(qty))
                    .where(INVENTORY.PLAYER_ID.eq(UUID.fromString(accountId)))
                    .and(INVENTORY.NAME.eq(name))
                    .execute();
        } else {
            jooqContext.insertInto(INVENTORY).set(toRecord(playerId, name, qty)).execute();
        }
    }

    @Override
    public void substractInventoryItem(String accountId, String name, int qty) {
        var playerId = UUID.fromString(accountId);
        Optional<InventoryRecord> optRecord = jooqContext.selectFrom(INVENTORY)
                .where(INVENTORY.PLAYER_ID.eq(playerId).and(INVENTORY.NAME.eq(name)))
                .fetchOptional();

        if (optRecord.isPresent()) {
            jooqContext.update(INVENTORY).set(INVENTORY.QUANTITY, INVENTORY.QUANTITY.sub(qty))
                    .where(INVENTORY.PLAYER_ID.eq(playerId).and(INVENTORY.NAME.eq(name)))
                    .execute();
        } else {
            jooqContext.insertInto(INVENTORY).set(toRecord(playerId, name, -qty)).execute();
        }
    }

    private InventoryRecord toRecord(UUID playerId, String itemName, Integer quantity) {
        var ret = jooqContext.newRecord(INVENTORY);
        ret.setPlayerId(playerId);
        ret.setName(itemName);
        ret.setQuantity(quantity);
        return ret;
    }

    private Map<String, Integer> fromRecords(List<InventoryRecord> records) {
        Map<String, Integer> ret = new HashMap<>();

        records.forEach(record -> {
            ret.put(record.getName(), record.getQuantity());
        });

        this.completeInventoryMap(ret);

        return ret;
    }

    private void completeInventoryMap(Map<String, Integer> ret) {
        if(!ret.containsKey(DinoparcConstants.POTION_IRMA)) { ret.put(DinoparcConstants.POTION_IRMA, 0); }
        if(!ret.containsKey(DinoparcConstants.POTION_ANGE)) { ret.put(DinoparcConstants.POTION_ANGE, 0); }
        if(!ret.containsKey(DinoparcConstants.NUAGE_BURGER)) { ret.put(DinoparcConstants.NUAGE_BURGER, 0); }
        if(!ret.containsKey(DinoparcConstants.PAIN_CHAUD)) { ret.put(DinoparcConstants.PAIN_CHAUD, 0); }
        if(!ret.containsKey(DinoparcConstants.TARTE_VIANDE)) { ret.put(DinoparcConstants.TARTE_VIANDE, 0); }
        if(!ret.containsKey(DinoparcConstants.MEDAILLE_CHOCOLAT)) { ret.put(DinoparcConstants.MEDAILLE_CHOCOLAT, 0); }
        if(!ret.containsKey(DinoparcConstants.BAVE_LOUPI)) { ret.put(DinoparcConstants.BAVE_LOUPI, 0); }
        if(!ret.containsKey(DinoparcConstants.RAMENS)) { ret.put(DinoparcConstants.RAMENS, 0); }
        if(!ret.containsKey(DinoparcConstants.POTION_SOMBRE)) { ret.put(DinoparcConstants.POTION_SOMBRE, 0); }
        if(!ret.containsKey(DinoparcConstants.COULIS_CERISE)) { ret.put(DinoparcConstants.COULIS_CERISE, 0); }
        if(!ret.containsKey(DinoparcConstants.ETERNITY_PILL)) { ret.put(DinoparcConstants.ETERNITY_PILL, 0); }
        if(!ret.containsKey(DinoparcConstants.OEUF_GLUON)) { ret.put(DinoparcConstants.OEUF_GLUON, 0); }
        if(!ret.containsKey(DinoparcConstants.OEUF_SANTAZ)) { ret.put(DinoparcConstants.OEUF_SANTAZ, 0); }
        if(!ret.containsKey(DinoparcConstants.OEUF_SERPANTIN)) { ret.put(DinoparcConstants.OEUF_SERPANTIN, 0); }
        if(!ret.containsKey(DinoparcConstants.CHARME_FEU)) { ret.put(DinoparcConstants.CHARME_FEU, 0); }
        if(!ret.containsKey(DinoparcConstants.CHARME_TERRE)) { ret.put(DinoparcConstants.CHARME_TERRE, 0); }
        if(!ret.containsKey(DinoparcConstants.CHARME_EAU)) { ret.put(DinoparcConstants.CHARME_EAU, 0); }
        if(!ret.containsKey(DinoparcConstants.CHARME_FOUDRE)) { ret.put(DinoparcConstants.CHARME_FOUDRE, 0); }
        if(!ret.containsKey(DinoparcConstants.CHARME_AIR)) { ret.put(DinoparcConstants.CHARME_AIR, 0); }
        if(!ret.containsKey(DinoparcConstants.CHARME_PRISMATIK)) { ret.put(DinoparcConstants.CHARME_PRISMATIK, 0); }
        if(!ret.containsKey(DinoparcConstants.FOCUS_AGGRESIVITE)) { ret.put(DinoparcConstants.FOCUS_AGGRESIVITE, 0); }
        if(!ret.containsKey(DinoparcConstants.FOCUS_NATURE)) { ret.put(DinoparcConstants.FOCUS_NATURE, 0); }
        if(!ret.containsKey(DinoparcConstants.FOCUS_SIRAINS)) { ret.put(DinoparcConstants.FOCUS_SIRAINS, 0); }
        if(!ret.containsKey(DinoparcConstants.IMMUNITÉ)) { ret.put(DinoparcConstants.IMMUNITÉ, 0); }
        if(!ret.containsKey(DinoparcConstants.ANTIDOTE)) { ret.put(DinoparcConstants.ANTIDOTE, 0); }
        if(!ret.containsKey(DinoparcConstants.OEIL_DU_TIGRE)) { ret.put(DinoparcConstants.OEIL_DU_TIGRE, 0); }
        if(!ret.containsKey(DinoparcConstants.GRIFFES_EMPOISONNÉES)) { ret.put(DinoparcConstants.GRIFFES_EMPOISONNÉES, 0); }
        if(!ret.containsKey(DinoparcConstants.BUZE)) { ret.put(DinoparcConstants.BUZE, 0); }
        if(!ret.containsKey(DinoparcConstants.PLUME)) { ret.put(DinoparcConstants.PLUME, 0); }
        if(!ret.containsKey(DinoparcConstants.DENT)) { ret.put(DinoparcConstants.DENT, 0); }
        if(!ret.containsKey(DinoparcConstants.CRIN)) { ret.put(DinoparcConstants.CRIN, 0); }
        if(!ret.containsKey(DinoparcConstants.BOIS)) { ret.put(DinoparcConstants.BOIS, 0); }
        if(!ret.containsKey(DinoparcConstants.FEUILLE_PACIFIQUE)) { ret.put(DinoparcConstants.FEUILLE_PACIFIQUE, 0); }
        if(!ret.containsKey(DinoparcConstants.OREADE_BLANC)) { ret.put(DinoparcConstants.OREADE_BLANC, 0); }
        if(!ret.containsKey(DinoparcConstants.TIGE_RONCIVORE)) { ret.put(DinoparcConstants.TIGE_RONCIVORE, 0); }
        if(!ret.containsKey(DinoparcConstants.PERCHE_PERLEE)) { ret.put(DinoparcConstants.PERCHE_PERLEE, 0); }
        if(!ret.containsKey(DinoparcConstants.GREMILLE_GRELOTTANTE)) { ret.put(DinoparcConstants.GREMILLE_GRELOTTANTE, 0); }
        if(!ret.containsKey(DinoparcConstants.GIFT)) { ret.put(DinoparcConstants.GIFT, 0); }
        if(!ret.containsKey(DinoparcConstants.ACT_GIFT)) { ret.put(DinoparcConstants.ACT_GIFT, 0); }
        if(!ret.containsKey(DinoparcConstants.PRUNIAC)) { ret.put(DinoparcConstants.PRUNIAC, 0); }
        if(!ret.containsKey(DinoparcConstants.COINS)) { ret.put(DinoparcConstants.COINS, 0); }
        if(!ret.containsKey(DinoparcConstants.CHAMPIFUZ)) { ret.put(DinoparcConstants.CHAMPIFUZ, 0); }
        if(!ret.containsKey(DinoparcConstants.BONS)) { ret.put(DinoparcConstants.BONS, 0); }
        if(!ret.containsKey(DinoparcConstants.GOUTTE_GLU)) { ret.put(DinoparcConstants.GOUTTE_GLU, 0); }
    }
}
