package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.Collection;
import com.dinoparc.tables.records.CollectionRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.jooq.JSONB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.jooq.Record;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.dinoparc.tables.Collection.COLLECTION;

@Repository
public class JooqCollectionRepository implements PgCollectionRepository {

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqCollectionRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public Collection getPlayerCollection(UUID playerId) {
        Optional<CollectionRecord> collection = jooqContext.selectFrom(COLLECTION).where(COLLECTION.PLAYER_ID.eq(playerId)).fetchOptional();

        if (collection.isPresent()) {
            return this.fromRecord(collection.get());
        }

        // If player collection doesn't exist, create it
        return createPlayerCollection(playerId);
    }

    @Override
    public void updateCollection(UUID id, List<String> collection) {
        try {
            var newCollection = JSONB.valueOf(objectMapper.writeValueAsString(collection));
            jooqContext.update(COLLECTION).set(COLLECTION.COLLECTION_, newCollection).where(COLLECTION.ID.eq(id)).execute();
        } catch(Exception e) {
            // Epic collection not updated
        }
    }

    @Override
    public void updateEpicCollection(UUID id, List<String> epicCollection) {
        try {
            var newEpicCollection = JSONB.valueOf(objectMapper.writeValueAsString(epicCollection));
            jooqContext.update(COLLECTION).set(COLLECTION.EPIC_COLLECTION, newEpicCollection).where(COLLECTION.ID.eq(id)).execute();
        } catch(Exception e) {
            // Epic collection not updated
        }
    }

    private Collection createPlayerCollection(UUID playerId) {
        Collection toCreate = new Collection();
        toCreate.setId(UUID.randomUUID());
        toCreate.setPlayerId(playerId);
        toCreate.setCollection(new ArrayList<>());
        toCreate.setEpicCollection(new ArrayList<>());

        return jooqContext.insertInto(COLLECTION).set(toRecord(toCreate)).returning().fetchOne().map(this::fromRecord);
    }

    private CollectionRecord toRecord(Collection collection) {
        var ret = jooqContext.newRecord(COLLECTION);

        ret.setId(collection.getId());
        ret.setPlayerId(collection.getPlayerId());

        var collectionList = collection.getCollection() == null ? Collections.emptyList() : collection.getCollection();
        try {
            ret.setCollection(JSONB.valueOf(objectMapper.writeValueAsString(collectionList)));
        } catch(Exception e) {
            ret.setCollection(JSONB.valueOf("{}"));
        }

        var epicCollectionList = collection.getEpicCollection() == null ? Collections.emptyList() : collection.getEpicCollection();
        try {
            ret.setEpicCollection(JSONB.valueOf(objectMapper.writeValueAsString(epicCollectionList)));
        } catch(Exception e) {
            ret.setEpicCollection(JSONB.valueOf("{}"));
        }

        return ret;
    }

    private Collection fromRecord(Record record) {
        var collectionRecord = (CollectionRecord) record;
        Collection ret = new Collection();

        ret.setId(collectionRecord.getId());
        ret.setPlayerId(collectionRecord.getPlayerId());
        try {
            ret.setCollection(collectionRecord.getCollection() == null ? null : objectMapper.readValue(collectionRecord.getCollection().data(), List.class));
        } catch(Exception e) {
            ret.setCollection(null);
        }
        try {
            ret.setEpicCollection(collectionRecord.getEpicCollection() == null ? null : objectMapper.readValue(collectionRecord.getEpicCollection().data(), List.class));
        } catch(Exception e) {
            ret.setEpicCollection(null);
        }

        return ret;
    }
}
