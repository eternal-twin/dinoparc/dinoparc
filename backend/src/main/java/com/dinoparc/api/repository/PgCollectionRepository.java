package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.Collection;

import java.util.List;
import java.util.UUID;

public interface PgCollectionRepository {
    Collection getPlayerCollection(UUID playerId);
    void updateEpicCollection(UUID id, List<String> epicCollection);
    void updateCollection(UUID id, List<String> playerCollection);
}
