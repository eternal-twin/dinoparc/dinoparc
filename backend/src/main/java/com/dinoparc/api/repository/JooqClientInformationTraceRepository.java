package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.ClientInformationTrace;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.dinoparc.tables.ClientInformationTrace.CLIENT_INFORMATION_TRACE;

@Repository
public class JooqClientInformationTraceRepository implements PgClientInformationTraceRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqClientInformationTraceRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public void addClientInformationTrace(ClientInformationTrace toInsert) {
        jooqContext.insertInto(CLIENT_INFORMATION_TRACE)
                .set(jooqContext.newRecord(CLIENT_INFORMATION_TRACE, toInsert))
                .execute();
    }

    @Override
    public void cleanUp() {
        jooqContext.deleteFrom(CLIENT_INFORMATION_TRACE).execute();
    }
}
