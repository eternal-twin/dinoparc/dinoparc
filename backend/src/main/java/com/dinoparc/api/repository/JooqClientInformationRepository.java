package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.ClientInformation;
import com.dinoparc.api.domain.account.ClientInformationDetail;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;

import static com.dinoparc.tables.ClientInformation.CLIENT_INFORMATION;
import static com.dinoparc.tables.ClientInformationDetail.CLIENT_INFORMATION_DETAIL;

@Repository
public class JooqClientInformationRepository implements PgClientInformationRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqClientInformationRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public void register(ClientInformation clientInformation) {
        jooqContext.insertInto(CLIENT_INFORMATION).set(jooqContext.newRecord(CLIENT_INFORMATION, clientInformation)).execute();
    }

    @Override
    public void addDetail(ClientInformationDetail clientInformationDetail) {
        jooqContext.insertInto(CLIENT_INFORMATION_DETAIL).set(jooqContext.newRecord(CLIENT_INFORMATION_DETAIL, clientInformationDetail)).execute();
    }

    @Override
    public void cleanData() {
        jooqContext.deleteFrom(CLIENT_INFORMATION)
                .where(CLIENT_INFORMATION.RETRIEVED_DATE.le(OffsetDateTime.now().minusMonths(3)))
                .execute();
    }
}
