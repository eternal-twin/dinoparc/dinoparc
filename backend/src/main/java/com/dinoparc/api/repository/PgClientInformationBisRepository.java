package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.ClientInformationBis;

public interface PgClientInformationBisRepository {
    void incrementRank(String accountId);

    void cleanUpAccount(String accountId);

    void cleanUp();

    void addClientInformationBis(ClientInformationBis toInsert);
}
