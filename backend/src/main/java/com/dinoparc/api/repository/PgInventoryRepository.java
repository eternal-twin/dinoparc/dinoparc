package com.dinoparc.api.repository;

import java.util.Map;

public interface PgInventoryRepository {
    Map<String, Integer> getAll(String id);
    Integer getQty(String accountId, String name);
    void addInventoryItem(String accountId, String name, int qty);
    void substractInventoryItem(String id, String name, int qty);
    Long getTotalQtyOnServer(String name);
    void fixBondsIssues();
}
