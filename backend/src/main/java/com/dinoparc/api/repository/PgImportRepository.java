package com.dinoparc.api.repository;

import com.dinoparc.api.controllers.dto.ImportTraceDto;

import java.util.List;

public interface PgImportRepository {
    List<ImportTraceDto> findAll();

    void deleteAll();

    void delete(ImportTraceDto it);

    void save(ImportTraceDto it);
}
