package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.clan.*;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.Tournament;
import com.dinoparc.tables.records.ClanMessagesRecord;
import com.dinoparc.tables.records.ClanRecord;
import com.dinoparc.tables.records.PlayerClanRecord;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.tools.json.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.dinoparc.Tables.PLAYER_DINOZ;
import static com.dinoparc.tables.Clan.CLAN;
import static com.dinoparc.tables.ClanMessages.CLAN_MESSAGES;
import static com.dinoparc.tables.Dinoz.DINOZ;
import static com.dinoparc.tables.PlayerClan.PLAYER_CLAN;

@Repository
public class JooqClanRepository implements PgClanRepository {

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqClanRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public void createClan(Clan newClan) {
        //Create the clan in the actual database :
        jooqContext.insertInto(CLAN).set(toRecord(newClan)).execute();

        //Associate the player with the new clan in player_clan table :
        Optional<PlayerClan> playerClan = jooqContext.selectFrom(PLAYER_CLAN)
                .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(newClan.getCreatorId())))
                .fetchOptional().map(this::fromRecordPlayerClan);

        if (playerClan.isPresent()) {
            jooqContext.update(PLAYER_CLAN)
                    .set(PLAYER_CLAN.CLAN_ID, UUID.fromString(newClan.getId()))
                    .set(PLAYER_CLAN.CLAN_NAME, newClan.getName())
                    .set(PLAYER_CLAN.ROLE, DinoparcConstants.CLAN_CREATOR)
                    .set(PLAYER_CLAN.TITLE, "*Admin*")
                    .set(PLAYER_CLAN.ACTIVE_WARRIOR, true)
                    .set(PLAYER_CLAN.NB_POINTS_OCCUPATION, 0)
                    .set(PLAYER_CLAN.NB_POINTS_FIGHTS, 0)
                    .set(PLAYER_CLAN.NB_POINTS_TOTEM, 0)
                    .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerClan.get().getPlayerId()))).execute();
        } else {
            jooqContext.insertInto(PLAYER_CLAN)
                    .set(PLAYER_CLAN.PLAYER_ID, UUID.fromString(newClan.getCreatorId()))
                    .set(PLAYER_CLAN.CLAN_ID, UUID.fromString(newClan.getId()))
                    .set(PLAYER_CLAN.CLAN_NAME, newClan.getName())
                    .set(PLAYER_CLAN.ROLE, DinoparcConstants.CLAN_CREATOR)
                    .set(PLAYER_CLAN.TITLE, "*Admin*")
                    .set(PLAYER_CLAN.ACTIVE_WARRIOR, true)
                    .set(PLAYER_CLAN.NB_POINTS_OCCUPATION, 0)
                    .set(PLAYER_CLAN.NB_POINTS_FIGHTS, 0)
                    .set(PLAYER_CLAN.NB_POINTS_TOTEM, 0)
                    .execute();
        }

        try {
            //Create the ClanMessage table instance related to thw new clan :
            ArrayDeque<ForumMessage> messages = new ArrayDeque<>();
            jooqContext.insertInto(CLAN_MESSAGES)
                    .set(CLAN_MESSAGES.CLAN_ID, newClan.getId())
                    .set(CLAN_MESSAGES.MESSAGES, JSONB.valueOf(objectMapper.writeValueAsString(messages)))
                    .execute();
        } catch (JsonProcessingException e) {
            System.out.println("Error ClanMessage table creation " + newClan.getName());
        }

        //Post a welcome message to the clan :
        this.postMessageToClanForum(newClan.getName(), newClan.getId(), newClan.getName() + " -> " + newClan.getCreationDate());
    }

    @Override
    public void updateApplicationsToClan(String clandId, List<String> candidate) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.CANDIDATES, JSONB.valueOf(objectMapper.writeValueAsString(candidate)))
                    .where(CLAN.ID.eq(clandId)).execute();
        } catch (JsonProcessingException e) {
            System.out.println("Error while trying to apply to new clan " + clandId);
        }
    }

    @Override
    public void addPlayerToClan(Clan clan, String playerId) {
        clan.getMembers().add(playerId);
        clan.getCandidates().remove(playerId);

        //Associate the player with the new clan in player_clan table :
        Optional<PlayerClan> playerClan = jooqContext.selectFrom(PLAYER_CLAN)
                .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId)))
                .fetchOptional().map(this::fromRecordPlayerClan);

        if (playerClan.isPresent()) {
            jooqContext.update(PLAYER_CLAN)
                    .set(PLAYER_CLAN.CLAN_ID, UUID.fromString(clan.getId()))
                    .set(PLAYER_CLAN.CLAN_NAME, clan.getName())
                    .set(PLAYER_CLAN.ROLE, DinoparcConstants.CLAN_MEMBER)
                    .set(PLAYER_CLAN.TITLE, "")
                    .set(PLAYER_CLAN.ACTIVE_WARRIOR, true)
                    .set(PLAYER_CLAN.NB_POINTS_OCCUPATION, 0)
                    .set(PLAYER_CLAN.NB_POINTS_FIGHTS, 0)
                    .set(PLAYER_CLAN.NB_POINTS_TOTEM, 0)
                    .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId))).execute();
        } else {
            jooqContext.insertInto(PLAYER_CLAN)
                    .set(PLAYER_CLAN.PLAYER_ID, UUID.fromString(playerId))
                    .set(PLAYER_CLAN.CLAN_ID, UUID.fromString(clan.getId()))
                    .set(PLAYER_CLAN.CLAN_NAME, clan.getName())
                    .set(PLAYER_CLAN.ROLE, DinoparcConstants.CLAN_MEMBER)
                    .set(PLAYER_CLAN.TITLE, "")
                    .set(PLAYER_CLAN.ACTIVE_WARRIOR, true)
                    .set(PLAYER_CLAN.NB_POINTS_OCCUPATION, 0)
                    .set(PLAYER_CLAN.NB_POINTS_FIGHTS, 0)
                    .set(PLAYER_CLAN.NB_POINTS_TOTEM, 0)
                    .execute();
        }

        try {
            jooqContext.update(CLAN)
                    .set(CLAN.MEMBERS, JSONB.valueOf(objectMapper.writeValueAsString(clan.getMembers())))
                    .set(CLAN.CANDIDATES, JSONB.valueOf(objectMapper.writeValueAsString(clan.getCandidates())))
                    .where(CLAN.ID.eq(clan.getId())).execute();

        } catch (JsonProcessingException e) {
            System.out.println("Error while adding player to a clan : " + clan.getId() + " : player = " + playerId);
        }
    }

    @Override
    public void expellPlayerFromClan(Clan clan, String playerId) {
        clan.getMembers().remove(playerId);
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.MEMBERS, JSONB.valueOf(objectMapper.writeValueAsString(clan.getMembers())))
                    .where(CLAN.ID.eq(clan.getId())).execute();

            //Delete the player's entry in the table PLAYER_CLAN :
            jooqContext.deleteFrom(PLAYER_CLAN).where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId))).execute();

        } catch (JsonProcessingException e) {
            System.out.println("Error while adding player to a clan : " + clan.getId() + " : player = " + playerId);
        }
    }

    @Override
    public void refusePlayerApplication(Clan clan, String playerId) {
        clan.getCandidates().remove(playerId);
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.CANDIDATES, JSONB.valueOf(objectMapper.writeValueAsString(clan.getCandidates())))
                    .where(CLAN.ID.eq(clan.getId())).execute();

        } catch (JsonProcessingException e) {
            System.out.println("Error while adding player to a clan : " + clan.getId() + " : player = " + playerId);
        }
    }

    @Override
    public void updateClanNbOfDinoz(Clan clan, Integer nbDinozTotal) {
        clan.setNbDinozTotal(nbDinozTotal);
        jooqContext.update(CLAN)
                .set(CLAN.NB_DINOZ_TOTAL, nbDinozTotal)
                .where(CLAN.ID.eq(clan.getId()))
                .execute();
    }

    @Override
    public void updateClanNbOfWarriorDinoz(Clan clan, Integer nbDinozWarrior) {
        clan.setNbDinozWarriors(nbDinozWarrior);
        jooqContext.update(CLAN)
                .set(CLAN.NB_DINOZ_WARRIORS, nbDinozWarrior)
                .where(CLAN.ID.eq(clan.getId()))
                .execute();
    }

    @Override
    public void adminWipeClanPagesContent(String clanId) {
        Clan clan = this.getClan(clanId).get();
        for (Map.Entry<String, Page> entry : clan.getPages().entrySet()) {
            Page pageToWipe = clan.getPages().get(entry.getKey());
            pageToWipe.setRawContent("");
            pageToWipe.setPageTitle(clan.getName());
            clan.getPages().replace(entry.getKey(), pageToWipe);
        }

        try {
            jooqContext.update(CLAN)
                    .set(CLAN.PAGES, JSONB.valueOf(objectMapper.writeValueAsString(clan.getPages())))
                    .where(CLAN.ID.eq(clan.getId())).execute();
        } catch (JsonProcessingException e) {
            System.out.println("Error while trying to modify a clan page to database : " + clan.getId());
        }
    }

    @Override
    public void deleteGhostClan(String clanId) {
        jooqContext.deleteFrom(CLAN).where(CLAN.ID.eq(clanId)).execute();
    }

    @Override
    public void quitClan(String playerId) {
        Clan clan = this.getClanOfPlayer(playerId);
        clan.getMembers().remove(playerId);

        try {
            jooqContext.update(CLAN)
                    .set(CLAN.MEMBERS, JSONB.valueOf(objectMapper.writeValueAsString(clan.getMembers())))
                    .where(CLAN.ID.eq(clan.getId())).execute();

            //Delete the player's entry in the table PLAYER_CLAN :
            jooqContext.deleteFrom(PLAYER_CLAN).where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId))).execute();

        } catch (JsonProcessingException e) {
            System.out.println("Error while deleting player from clan : " + clan.getId() + " : player = " + playerId);
        }
    }

    @Override
    public void updatePlayerClan(PlayerClan playerClan) {
        jooqContext.update(PLAYER_CLAN)
                .set(PLAYER_CLAN.CLAN_ID, UUID.fromString(playerClan.getClanId()))
                .set(PLAYER_CLAN.CLAN_NAME, playerClan.getClanName())
                .set(PLAYER_CLAN.ROLE, playerClan.getRole())
                .set(PLAYER_CLAN.TITLE, playerClan.getTitle())
                .set(PLAYER_CLAN.ACTIVE_WARRIOR, playerClan.getActiveWarrior())
                .set(PLAYER_CLAN.NB_POINTS_OCCUPATION, playerClan.getNbPointsOccupation())
                .set(PLAYER_CLAN.NB_POINTS_FIGHTS, playerClan.getNbPointsFights())
                .set(PLAYER_CLAN.NB_POINTS_TOTEM, playerClan.getNbPointsTotem())
                .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerClan.getPlayerId()))).execute();
    }

    @Override
    public Clan modifyClanPage(Clan clan) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.PAGES, JSONB.valueOf(objectMapper.writeValueAsString(clan.getPages())))
                    .where(CLAN.ID.eq(clan.getId())).execute();
        } catch (JsonProcessingException e) {
            System.out.println("Error while trying to modify a clan page to database : " + clan.getId());
        }
        return clan;
    }

    @Override
    public ClanMessage postMessageToClanForum(String author, String clanId, String message) {
        ClanMessage myClanMessages = jooqContext.selectFrom(CLAN_MESSAGES)
                .where(CLAN_MESSAGES.CLAN_ID.eq(clanId))
                .fetchOptional().map(this::fromRecordClanMessage)
                .get();

        if (myClanMessages.getMessages().size() > 25) {
            myClanMessages.getMessages().removeLast();
        }

        ForumMessage newMessage = new ForumMessage();
        newMessage.setAuthor(author);
        newMessage.setMessage(message);
        newMessage.setDate(ZonedDateTime.now(ZoneId.of("Europe/Paris")).format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")));
        myClanMessages.getMessages().addFirst(newMessage);

        try {
            jooqContext.update(CLAN_MESSAGES)
                    .set(CLAN_MESSAGES.MESSAGES, JSONB.valueOf(objectMapper.writeValueAsString(myClanMessages)))
                    .where(CLAN_MESSAGES.CLAN_ID.eq(clanId)).execute();
        } catch (JsonProcessingException e) {
            System.out.println("Error while trying to post a ClanMessage to database : " + clanId);
        }

        return myClanMessages;
    }

    @Override
    public void updateTotem(String clanId, Totem totem) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.TOTEM, JSONB.valueOf(objectMapper.writeValueAsString(totem)))
                    .where(CLAN.ID.eq(clanId))
                    .execute();

        } catch (JsonProcessingException e) {
            System.out.println("Error while trying to update the clan Totem : " + clanId);
        }
    }

    @Override
    public void updateChest(String clanId, Map<String, Integer> chest) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.CHEST, JSONB.valueOf(objectMapper.writeValueAsString(chest)))
                    .where(CLAN.ID.eq(clanId))
                    .execute();

        } catch (JsonProcessingException e) {
            System.out.println("Error while trying to update the clan Chest : " + clanId);
        }
    }

    @Override
    public void resetClanPoints(String clanId) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.NB_POINTS_FIGHTS, 0)
                    .set(CLAN.NB_POINTS_OCCUPATION, 0)
                    .set(CLAN.NB_POINTS_TOTEM, 0)
                    .where(CLAN.ID.eq(clanId))
                    .execute();
        } catch (Exception e) {
            System.out.println("Error while trying to reset the clan points : " + clanId);
        }
    }

    @Override
    public void addClanPointsFights(String clanId, Integer nbPtsFights) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.NB_POINTS_FIGHTS, CLAN.NB_POINTS_FIGHTS.add(nbPtsFights))
                    .where(CLAN.ID.eq(clanId))
                    .execute();
        } catch (Exception e) {
            System.out.println("Error while trying to update the clan fight points : " + clanId);
        }
    }

    @Override
    public void addClanPointsOccupation(String clanId, Integer nbPtsOccupation) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.NB_POINTS_OCCUPATION, CLAN.NB_POINTS_OCCUPATION.add(nbPtsOccupation))
                    .where(CLAN.ID.eq(clanId))
                    .execute();
        } catch (Exception e) {
            System.out.println("Error while trying to update the clan occupation points : " + clanId);
        }
    }

    @Override
    public void addClanPointsFouilles(String clanId, Integer nbPtsFouilles) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.NB_POINTS_TOTEM, CLAN.NB_POINTS_TOTEM.add(nbPtsFouilles))
                    .where(CLAN.ID.eq(clanId))
                    .execute();
        } catch (Exception e) {
            System.out.println("Error while trying to update the clan totem points : " + clanId);
        }
    }

    @Override
    public void updatePosition(String clanId, Integer position) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.POSITION, position)
                    .where(CLAN.ID.eq(clanId))
                    .execute();

        } catch (Exception e) {
            System.out.println("Error while trying to update the clan position : " + clanId);
        }
    }

    @Override
    public void updateFaction(String clanId, String faction) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.FACTION, faction)
                    .where(CLAN.ID.eq(clanId))
                    .execute();

        } catch (Exception e) {
            System.out.println("Error while trying to update the clan faction : " + clanId);
        }
    }

    @Override
    public void updateChef(String clanId, String chefId) {
        try {
            jooqContext.update(CLAN)
                    .set(CLAN.CREATOR_ID, chefId)
                    .where(CLAN.ID.eq(clanId))
                    .execute();

        } catch (Exception e) {
            System.out.println("Error while trying to update the clan chef : " + clanId);
        }
    }

    @Override
    public List<Dinoz> getAllWarriorDinozOfClan(String clanId) {
        List<Dinoz> allWarriorDinozOfClan = new ArrayList<>();

        for (String clanMemberId : this.getClan(clanId).get().getMembers()) {
            allWarriorDinozOfClan.addAll(
                    jooqContext.select(DINOZ.fields())
                            .from(DINOZ)
                            .join(PLAYER_DINOZ).on(DINOZ.ID.eq(PLAYER_DINOZ.DINOZ_ID))
                            .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(clanMemberId)))
                            .and(DINOZ.MALUS_LIST.contains((JSONB.valueOf(JSONValue.toJSONString(DinoparcConstants.FACTION_1_GUERRIER))))
                                    .or(DINOZ.MALUS_LIST.contains((JSONB.valueOf(JSONValue.toJSONString(DinoparcConstants.FACTION_2_GUERRIER)))))
                                    .or(DINOZ.MALUS_LIST.contains((JSONB.valueOf(JSONValue.toJSONString(DinoparcConstants.FACTION_3_GUERRIER)))))
                            )
                            .orderBy(PLAYER_DINOZ.RANK)
                            .fetch()
                            .map(this::dinozFromRecord)
            );
        }
        return allWarriorDinozOfClan;
    }

    @Override
    public List<Dinoz> getAllDinozWarriorAliveInThatLocation(Integer placeNumber) {
        List<Dinoz> allDinozWarriorAliveInThatLocation = new ArrayList<>();
            allDinozWarriorAliveInThatLocation.addAll(
                    jooqContext.select(DINOZ.fields())
                            .from(DINOZ)
                            .where(DINOZ.PLACE_NUMBER.eq(placeNumber))
                            .and(DINOZ.LIFE.greaterThan(0))
                            .and(DINOZ.IS_IN_TOURNEY.eq(false))
                            .and(DINOZ.IS_IN_BAZAR.eq(false))
                            .and(DINOZ.MALUS_LIST.contains((JSONB.valueOf(JSONValue.toJSONString(DinoparcConstants.FACTION_1_GUERRIER))))
                                    .or(DINOZ.MALUS_LIST.contains((JSONB.valueOf(JSONValue.toJSONString(DinoparcConstants.FACTION_2_GUERRIER)))))
                                    .or(DINOZ.MALUS_LIST.contains((JSONB.valueOf(JSONValue.toJSONString(DinoparcConstants.FACTION_3_GUERRIER)))))
                            )
                            .fetch()
                            .map(this::dinozFromRecord)
            );
        return allDinozWarriorAliveInThatLocation;
    }

    @Override
    public ClanMessage getClanMessages(String clanId) {
        return jooqContext.selectFrom(CLAN_MESSAGES).where(CLAN_MESSAGES.CLAN_ID.eq(clanId)).fetchOptional().map(this::fromRecordClanMessage).get();
    }

    @Override
    public Optional<Clan> getClan(String clanId) {
        return jooqContext.selectFrom(CLAN).where(CLAN.ID.eq(clanId)).fetchOptional().map(this::fromRecord);
    }

    @Override
    public List<Clan> getAllClans() {
        return jooqContext.selectFrom(CLAN).fetch().map(this::fromRecord);
    }

    @Override
    public List<Clan> getLatest50ClansCreated() {
        List<Clan> latestClans = jooqContext.selectFrom(CLAN).orderBy(CLAN.CREATION_DATE.desc()).limit(50).fetch().map(this::fromRecord);
        for (Clan clan : latestClans) {
            String day = clan.getCreationDate().substring(0, 2);
            String month = clan.getCreationDate().substring(3, 5);
            String year = clan.getCreationDate().substring(6);
            clan.setCreationDate(year + "/" + month + "/" + day);
        }

        return latestClans;
    }

    @Override
    public Optional<PlayerClan> getPlayerClan(String clanId, String playerId) {
        return jooqContext.selectFrom(PLAYER_CLAN)
                .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId)))
                .and(PLAYER_CLAN.CLAN_ID.eq(UUID.fromString(clanId)))
                .fetchOptional().map(this::fromRecordPlayerClan);
    }

    @Override
    public Optional<PlayerClan> getPlayerClan(String playerId) {
        return jooqContext.selectFrom(PLAYER_CLAN)
                .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId)))
                .fetchOptional().map(this::fromRecordPlayerClan);
    }

    @Override
    public Clan getClanOfPlayer(String playerId) {
        var clanId = jooqContext.select(PLAYER_CLAN.CLAN_ID).from(PLAYER_CLAN).where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId))).fetchOptional();
        if (clanId.isEmpty()) {
            return null;
        }
        return getClan(clanId.get().value1().toString()).get();
    }

    private Record toRecord(Clan newClan) {
        ClanRecord ret = jooqContext.newRecord(CLAN);
        ret.setId(newClan.getId());
        ret.setCreatorId(newClan.getCreatorId());
        ret.setName(newClan.getName());
        ret.setNbDinozTotal(newClan.getNbDinozTotal());
        ret.setNbDinozWarriors(newClan.getNbDinozWarriors());
        ret.setNbPointsOccupation(newClan.getNbPointsOccupation());
        ret.setNbPointsFights(newClan.getNbPointsFights());
        ret.setNbPointsTotem(newClan.getNbPointsTotem());
        ret.setPosition(newClan.getPosition());
        ret.setFaction(newClan.getFaction());
        ret.setCreationDate(newClan.getCreationDate());

        try {
            ret.setPages(JSONB.valueOf(objectMapper.writeValueAsString(newClan.getPages())));
        } catch (Exception e) {
            System.out.println("Error in pages create/update : " + newClan.getName());
        }

        try {
            ret.setTotem(JSONB.valueOf(objectMapper.writeValueAsString(newClan.getTotem())));
        } catch (Exception e) {
            System.out.println("Error in totem create/update : " + newClan.getName());
        }

        try {
            ret.setMembers(JSONB.valueOf(objectMapper.writeValueAsString(newClan.getMembers())));
        } catch (Exception e) {
            System.out.println("Error in members create/update : " + newClan.getName());
        }

        try {
            ret.setCandidates(JSONB.valueOf(objectMapper.writeValueAsString(newClan.getCandidates())));
        } catch (Exception e) {
            System.out.println("Error in candidates create/update : " + newClan.getName());
        }

        return ret;
    }

    private Clan fromRecord(ClanRecord clanRecord) {
        Clan clan = new Clan();
        clan.setId(clanRecord.getId());
        clan.setCreatorId(clanRecord.getCreatorId());
        clan.setName(clanRecord.getName());
        clan.setNbDinozTotal(clanRecord.getNbDinozTotal());
        clan.setNbDinozWarriors(clanRecord.getNbDinozWarriors());
        clan.setNbPointsOccupation(clanRecord.getNbPointsOccupation());
        clan.setNbPointsFights(clanRecord.getNbPointsFights());
        clan.setNbPointsTotem(clanRecord.getNbPointsTotem());
        clan.setPosition(clanRecord.getPosition());
        clan.setFaction(clanRecord.getFaction());
        clan.setCreationDate(clanRecord.getCreationDate());

        try {
            Map<String, LinkedHashMap<String, Object>> pages = clanRecord.getPages() == null ? null : objectMapper.readValue(clanRecord.getPages().data(), Map.class);
            for (String key : pages.keySet()) {
                Page page = new Page();
                page.setPageTitle((String) pages.get(key).get("pageTitle"));
                page.setRawContent((String) pages.get(key).get("rawContent"));
                page.setPrivate((Boolean) pages.get(key).get("private"));
                page.setPageId((String) pages.get(key).get("pageId"));
                clan.getPages().putIfAbsent(key, page);
            }
        } catch(Exception e) {
            clan.setPages(new HashMap<>());
        }

        try {
            Totem totem = clanRecord.getTotem() == null ? null : objectMapper.readValue(clanRecord.getTotem().data(), Totem.class);
            clan.setTotem(totem);
        } catch(Exception e) {
            clan.setTotem(new Totem());
        }

        try {
            HashMap<String, Integer> chest = clanRecord.getChest() == null ? null : objectMapper.readValue(clanRecord.getChest().data(), HashMap.class);
            clan.setChest(chest);
        } catch(Exception e) {
            clan.setChest(new HashMap<>());
        }

        try {
            List<String> members = clanRecord.getMembers() == null ? null : objectMapper.readValue(clanRecord.getMembers().data(), List.class);
            clan.setMembers(members);
        } catch(Exception e) {
            clan.setMembers(new ArrayList());
        }

        try {
            List<String> candidates = clanRecord.getCandidates() == null ? null : objectMapper.readValue(clanRecord.getCandidates().data(), List.class);
            clan.setCandidates(candidates);
        } catch(Exception e) {
            clan.setCandidates(new ArrayList());
        }

        return clan;
    }

    private ClanMessage fromRecordClanMessage(ClanMessagesRecord clanMessagesRecord) {
        ClanMessage clanMessage = new ClanMessage();
        clanMessage.setClanId(clanMessagesRecord.getClanId());

        try {
            var test = clanMessagesRecord.getMessages() == null ? null : objectMapper.readValue(clanMessagesRecord.getMessages().data(), ClanMessage.class);
            clanMessage.setMessages(test.getMessages());
        } catch(Exception e) {
            System.out.print(e.getStackTrace());
        }

        return clanMessage;
    }

    private PlayerClan fromRecordPlayerClan(PlayerClanRecord playerClanRecord) {
        PlayerClan playerClan = new PlayerClan();
        playerClan.setPlayerId(playerClanRecord.getPlayerId().toString());
        playerClan.setClanId(playerClanRecord.getClanId().toString());
        playerClan.setClanName(playerClanRecord.getClanName());
        playerClan.setRole(playerClanRecord.getRole());
        playerClan.setTitle(playerClanRecord.getTitle());
        playerClan.setActiveWarrior(playerClanRecord.getActiveWarrior());
        playerClan.setNbPointsOccupation(playerClanRecord.getNbPointsOccupation());
        playerClan.setNbPointsFights(playerClanRecord.getNbPointsFights());
        playerClan.setNbPointsTotem(playerClanRecord.getNbPointsTotem());
        return playerClan;
    }

    private Dinoz dinozFromRecord(Record dinozRecord) {
        Dinoz ret = new Dinoz();
        ret.setId(dinozRecord.getValue(DINOZ.ID));
        ret.setMasterId(dinozRecord.getValue(DINOZ.MASTER_ID));
        ret.setMasterName(dinozRecord.getValue(DINOZ.MASTER_NAME));
        ret.setName(dinozRecord.getValue(DINOZ.NAME));
        ret.setRace(dinozRecord.getValue(DINOZ.RACE));
        ret.setBeginMessage(dinozRecord.getValue(DINOZ.BEGIN_MESSAGE));
        ret.setEndMessage(dinozRecord.getValue(DINOZ.END_MESSAGE));
        ret.setLife(dinozRecord.getValue(DINOZ.LIFE));
        ret.setLevel(dinozRecord.getValue(DINOZ.LEVEL));
        ret.setExperience(dinozRecord.getValue(DINOZ.EXPERIENCE));
        ret.setDanger(dinozRecord.getValue(DINOZ.DANGER));
        ret.setPlaceNumber(dinozRecord.getValue(DINOZ.PLACE_NUMBER));
        ret.setAppearanceCode(dinozRecord.getValue(DINOZ.APPEARANCE_CODE));
        ret.setDark(dinozRecord.getValue(DINOZ.IS_DARK));
        ret.setInTourney(dinozRecord.getValue(DINOZ.IS_IN_TOURNEY));
        ret.setKabukiProgression(dinozRecord.getValue(DINOZ.KABUKI_PROGRESSION));
        ret.setLastValidBotHash(dinozRecord.getValue(DINOZ.LAST_VALID_BOT_HASH));
        ret.setSkips(dinozRecord.getValue(DINOZ.SKIPS));
        ret.setInBazar(dinozRecord.getValue(DINOZ.IS_IN_BAZAR));
        ret.setEpochSecondsEndOfCherryEffect(dinozRecord.getValue(DINOZ.EPOCH_SECONDS_END_OF_CHERRY_EFFECT).longValue());
        ret.setCherryEffectMinutesLeft(dinozRecord.getValue(DINOZ.CHERRY_EFFECT_MINUTES_LEFT).longValue());
        ret.setLastFledEnnemy(dinozRecord.getValue(DINOZ.LAST_FLED_ENNEMY));
        ret.setBotOnly(dinozRecord.getValue(DINOZ.BOT_ONLY));

        try {
            ret.setPassiveList(objectMapper.readValue(dinozRecord.getValue(DINOZ.PASSIVE_LIST).data(), Map.class));
            ret.setMalusList(objectMapper.readValue(dinozRecord.getValue(DINOZ.MALUS_LIST).data(), List.class));
            ret.setElementsValues(objectMapper.readValue(dinozRecord.getValue(DINOZ.ELEMENTS_VALUES).data(), Map.class));
            ret.setSkillsMap(objectMapper.readValue(dinozRecord.getValue(DINOZ.SKILLS_MAP).data(), Map.class));
            ret.setActionsMap(objectMapper.readValue(dinozRecord.getValue(DINOZ.ACTIONS_MAP).data(), Map.class));
            TypeReference<HashMap<String, Tournament>> typeRef = new TypeReference<HashMap<String,Tournament>>() {};
            ret.setTournaments(objectMapper.readValue(dinozRecord.getValue(DINOZ.TOURNAMENTS).data(), typeRef));
        } catch(Exception e) {
            return null;
        }

        return ret;
    }
}
