package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.ClientInformation;
import com.dinoparc.api.domain.account.ClientInformationDetail;

public interface PgClientInformationRepository {
    void register(ClientInformation clientInformation);
    void addDetail(ClientInformationDetail clientInformationDetail);
    void cleanData();
}
