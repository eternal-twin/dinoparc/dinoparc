package com.dinoparc.api.repository;

import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.RestrictedDinozDto;
import com.dinoparc.api.domain.dinoz.Tournament;
import com.dinoparc.api.domain.misc.Pagination;

import java.util.*;

public interface PgDinozRepository {

    void create(Dinoz dinoz);

    void deleteById(String dinozId);

    int count();

    Optional<Dinoz> findById(String dinozId);

    List<Dinoz> findByPlayerId(String playerId);

    Optional<Dinoz> findByIdAndPlayerId(String dinozId, String playerId);

    List<RestrictedDinozDto> findRestrictedByRank(String playerId, int rankFrom, int rankTo, boolean isOwner);

    Optional<Dinoz> findByRankAndPlayerId(int rank, String id);

    List<Dinoz> getFighters(String masterId, int placeNumber, int minLvl, int maxLvl, int limit, String lastFledEnnemy);

    void processMove(String dinozId, int newPlaceNumber, Map<String, Boolean> actionsMap);

    void setMasterInfos(String dinozId, String playerId, String username);

    void setLastValidBotHash(String dinozId, int hashCode);
    
    void setLastValidBotHashByMasterName(String masterName, int hashCode);

    Dinoz setActionsAndMalusAndPassiveAndPlaceNumber(String id, Map<String, Boolean> actionsMap, List<String> malusList, Map<String, Integer> passiveList, int placeNumber);

    void setActionsAndMalus(String dinozId, Map<String, Boolean> actionsMap, List<String> malusList);

    void setActionsMap(String dinozId, Map<String, Boolean> actionsMap);

    void setBazar(String dinozId, boolean bazar);

    void setInTournament(String dinozId, boolean inTourney);

    void setTournaments(String dinozId, Map<String, Tournament> tournaments, boolean inInTourney);

    void setLife(String dinozId, int life);

    void setPlaceNumber(String dinozId, int placeNumber);

    void setAppearanceCode(String dinozId, String appearanceCode);

    void setExperience(String dinozId, int xp);

    void setExperienceWithActionMap(String dinozId, int xp, Map<String, Boolean> actionsMap);

    void setKabukiProgression(String dinozId, List<String> malusList, int kabukiProgression);

    void setDanger(String dinozId, int danger);

    void setMalusList(String dinozId, List<String> malusList);

    void setName(String id, String newName);

    void setLastFledEnnemy(String dinozId, String lastFledEnnemy);

    void setPassiveList(String dinozId, Map<String, Integer> passiveList);

    void setPassiveAndMalusList(String dinozId, Map<String, Integer> passiveList, List<String> malusList);

    void setBotOnly(String dinozId, Boolean botOnly);

    void processFireBath(String dinozId, int life, Map<String, Boolean> actionsMap);

    void processDailyReset(String id, int life, Map<String, Boolean> actionsMap, String lastFledEnnemy, Boolean dinozIsActive);

    void processDailyResetForInactiveDinoz(String id, int life, Map<String, Boolean> actionsMap, String lastFledEnnemy, Integer danger, Integer placeNumber);

    void processRock(String masterId, String dinozId, int danger, Map<String, Boolean> actionsMap, int dangerLossByAll);

    void processLvlUp(String dinozId, Map<String, Integer> skillsMap, Map<String, Integer> elementsValues, int xp, int level, Map<String, Boolean> actionsMap);

    void processAnge(String dinozId, int life, Map<String, Boolean> actionsMap);

    void processDarkPotion(String dinozId, boolean isDark, String appearanceCode, Map<String, Integer> elementsValues, Map<String, Integer> skillsMap);

    void processCherry(String dinozId, String appearanceCode, List<String> malusList, Map<String, Boolean> actionsMap, Long epochSecondsEndOfCherryEffect, int minutesLeft);

    void processCurrentCherry(String dinozId, List<String> malusList, int minutesLeft);

    void processFocus(String dinozId, Map<String, Integer> passiveList, Map<String, Integer> elementsValues);

    void processFusion(String dinozId, String appearanceCode, int level, Map<String, Integer> elementsValues);

    List<HOFDinozDto> getRanking(String race);

    Pagination<HOFDinozDto> getRanking(String race, Integer offset, Integer limit);

    List<Dinoz> findAll();

    List<Dinoz> getTournamentContestants(Integer placeNumber);

    void delete(String dinozId);

    List<String> deleteAllGhostDinozFromPlayer(String playerId);

    void processFight(String dinozId, Map<String, Boolean> actionsMap, int life, int xp, int danger, Map<String, Integer> passiveList, List<String> malusList);

    void fixSkills(String dinozId, Map<String, Integer> skillsMap);

    List<Dinoz> getAllStealableWistitis();
}
