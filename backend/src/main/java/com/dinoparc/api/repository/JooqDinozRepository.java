package com.dinoparc.api.repository;

import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.RestrictedDinozDto;
import com.dinoparc.api.domain.dinoz.Tournament;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.tables.records.DinozRecord;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.dinoparc.Tables.FIGHT_EXCLUSION;
import static com.dinoparc.Tables.PLAYER_DINOZ;
import static com.dinoparc.tables.Dinoz.DINOZ;

@Repository
public class JooqDinozRepository implements PgDinozRepository {
    private final static Condition DINOZ_WITHOUT_MASTER_ADMIN = DINOZ.MASTER_ID.notEqual("admin");

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqDinozRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(Dinoz dinoz) {
        jooqContext.insertInto(DINOZ).set(toRecord(dinoz)).execute();
    }

    @Override
    public void deleteById(String dinozId) {
        jooqContext.deleteFrom(DINOZ).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public int count() {
        return jooqContext.selectCount().from(DINOZ).fetchOneInto(Integer.class);
    }

    @Override
    public Optional<Dinoz> findById(String dinozId) {
        return jooqContext.selectFrom(DINOZ).where(DINOZ.ID.eq(dinozId)).fetchOptional().map(this::fromRecord);
    }

    @Override
    public Optional<Dinoz> findByIdAndPlayerId(String id, String playerId) {
        return jooqContext.select(DINOZ.fields())
                .from(DINOZ)
                .join(PLAYER_DINOZ).on(DINOZ.ID.eq(PLAYER_DINOZ.DINOZ_ID))
                .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(playerId)))
                .and(DINOZ.ID.eq(id))
                .fetchOptional()
                .map(record -> fromRecord(record));
    }

    @Override
    public List<Dinoz> findByPlayerId(String playerId) {
        return jooqContext.select(DINOZ.fields())
                .from(DINOZ)
                .join(PLAYER_DINOZ).on(DINOZ.ID.eq(PLAYER_DINOZ.DINOZ_ID))
                .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(playerId)))
                .orderBy(PLAYER_DINOZ.RANK)
                .fetch()
                .map(record -> fromRecord(record));
    }

    @Override
    public List<String> deleteAllGhostDinozFromPlayer(String playerId) {
        List<String> deletedDinoz = new ArrayList<>();

        List<Dinoz> allDinozOfPlayerWithGhosts = jooqContext.select(DINOZ.fields())
                .from(DINOZ)
                .where(DINOZ.MASTER_ID.eq(playerId))
                .fetch()
                .map(this::fromRecord);

        List<String> allDinozIdsInPlayerDinozList = findByPlayerId(playerId).stream().map(Dinoz::getId).toList();
        for (String dinozId : allDinozOfPlayerWithGhosts.stream().map(Dinoz::getId).toList()) {
            if (!allDinozIdsInPlayerDinozList.contains(dinozId)) {
                deletedDinoz.add(dinozId);
                this.delete(dinozId);
            }
        }

        return deletedDinoz;
    }

    @Override
    public List<RestrictedDinozDto> findRestrictedByRank(String playerId, int rankFrom, int rankTo, boolean isOwner) {
        if (isOwner) {
            return jooqContext.select(DINOZ.ID, DINOZ.NAME, DINOZ.LEVEL, DINOZ.LIFE, DINOZ.EXPERIENCE, DINOZ.APPEARANCE_CODE, DINOZ.PLACE_NUMBER, DINOZ.ACTIONS_MAP)
                    .from(DINOZ)
                    .join(PLAYER_DINOZ).on(DINOZ.ID.eq(PLAYER_DINOZ.DINOZ_ID))
                    .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(playerId)))
                    .and(PLAYER_DINOZ.RANK.between(rankFrom, rankTo))
                    .orderBy(PLAYER_DINOZ.RANK)
                    .fetch()
                    .map(record -> fromRestrictedRecord(record));
        }

        // Return less informations, since method is not called by account owner
        return jooqContext.select(DINOZ.ID, DINOZ.NAME, DINOZ.APPEARANCE_CODE, DINOZ.PLACE_NUMBER, DINOZ.LEVEL, DINOZ.RACE)
                .from(DINOZ)
                .join(PLAYER_DINOZ).on(DINOZ.ID.eq(PLAYER_DINOZ.DINOZ_ID))
                .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(playerId)))
                .and(PLAYER_DINOZ.RANK.between(rankFrom, rankTo))
                .orderBy(PLAYER_DINOZ.RANK)
                .fetch()
                .map(record -> fromRecordMinimal(record));
    }

    @Override
    public Optional<Dinoz> findByRankAndPlayerId(int rank, String playerId) {
        return jooqContext.select(DINOZ.fields())
                .from(DINOZ)
                .join(PLAYER_DINOZ).on(DINOZ.ID.eq(PLAYER_DINOZ.DINOZ_ID))
                .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(playerId)))
                .and(PLAYER_DINOZ.RANK.eq(rank))
                .fetchOptional()
                .map(record -> fromRecord(record));
    }

    @Override
    public List<Dinoz> getFighters(String masterId, int placeNumber, int minLvl, int maxLvl, int limit, String lastFledEnnemy) {
        var query = jooqContext.select(DINOZ.ID, DINOZ.NAME, DINOZ.MASTER_ID, DINOZ.MASTER_NAME, DINOZ.LEVEL,
                        DINOZ.DINOZISACTIVE, DINOZ.ELEMENTS_VALUES, DINOZ.MALUS_LIST, DINOZ.APPEARANCE_CODE, DINOZ.DANGER)
                .from(DINOZ)
                .leftJoin(FIGHT_EXCLUSION)
                    .on(FIGHT_EXCLUSION.TO_PLAYER_ID.eq(DSL.cast(DINOZ.MASTER_ID, UUID.class))
                            .and(FIGHT_EXCLUSION.FROM_PLAYER_ID.eq(UUID.fromString(masterId))))
                .where(FIGHT_EXCLUSION.ID.isNull())
                .and(DINOZ.PLACE_NUMBER.eq(placeNumber))
                .and(DINOZ.LEVEL.greaterOrEqual(minLvl))
                .and(DINOZ.LEVEL.lessOrEqual(maxLvl))
                .and(DINOZ.MASTER_ID.notEqual(masterId))
                .and(DINOZ.LIFE.greaterThan(0))
                .and(DINOZ.DANGER.greaterThan(0))
                .and(DINOZ.IS_IN_TOURNEY.eq(false))
                .and(DINOZ.IS_IN_BAZAR.eq(false));

        if (lastFledEnnemy != null) {
            query = query.and(DINOZ.ID.notEqual(lastFledEnnemy));
        }

        return query
                .orderBy(DINOZ.DANGER.desc())
                .limit(limit)
                .fetch()
                .map(this::fighterFromRecord);
    }

    @Override
    public void processMove(String dinozId, int newPlaceNumber, Map<String, Boolean> actionsMap) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ).set(DINOZ.PLACE_NUMBER, newPlaceNumber).set(DINOZ.ACTIONS_MAP, jsonbActionsMap).where(DINOZ.ID.eq(dinozId)).execute();
        }
    }

    @Override
    public void setMasterInfos(String dinozId, String playerId, String username) {
        jooqContext.update(DINOZ).set(DINOZ.MASTER_ID, playerId).set(DINOZ.MASTER_NAME, username).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setLastValidBotHash(String dinozId, int hashCode) {
        jooqContext.update(DINOZ).set(DINOZ.LAST_VALID_BOT_HASH, hashCode).set(DINOZ.SKIPS, DINOZ.SKIPS.add(1)).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setLastValidBotHashByMasterName(String masterName, int hashCode) {
        jooqContext.update(DINOZ)
                .set(DINOZ.LAST_VALID_BOT_HASH, hashCode)
                .where(DINOZ.MASTER_NAME.eq(masterName))
                .and(DINOZ.LAST_VALID_BOT_HASH.notEqual(0))
                .execute();
    }

    @Override
    public void setBotOnly(String dinozId, Boolean botOnly) {
        jooqContext.update(DINOZ).set(DINOZ.BOT_ONLY, botOnly).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public Dinoz setActionsAndMalusAndPassiveAndPlaceNumber(String dinozId, Map<String, Boolean> actionsMap, List<String> malusList, Map<String, Integer> passiveList, int placeNumber) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);
        JSONB jsonbPassiveList = this.getJsonbPassiveList(passiveList);

        if (jsonbActionsMap != null && jsonbMalusList != null && jsonbPassiveList != null) {
            return jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .set(DINOZ.PASSIVE_LIST, jsonbPassiveList)
                    .set(DINOZ.PLACE_NUMBER, placeNumber)
                    .where(DINOZ.ID.eq(dinozId))
                    .returning()
                    .fetch()
                    .map(this::fromRecord)
                    .get(0);
        }

        return null;
    }

    @Override
    public void setActionsAndMalus(String dinozId, Map<String, Boolean> actionsMap, List<String> malusList) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);

        if (jsonbActionsMap != null && jsonbMalusList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setActionsMap(String dinozId, Map<String, Boolean> actionsMap) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setBazar(String dinozId, boolean bazar) {
        jooqContext.update(DINOZ).set(DINOZ.IS_IN_BAZAR, bazar).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setInTournament(String dinozId, boolean inTourney) {
        jooqContext.update(DINOZ).set(DINOZ.IS_IN_TOURNEY, inTourney).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setTournaments(String dinozId, Map<String, Tournament> tournaments, boolean inInTourney) {
        JSONB jsonbTournaments = this.getJsonbTournaments(tournaments);

        if (jsonbTournaments != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.IS_IN_TOURNEY, inInTourney)
                    .set(DINOZ.TOURNAMENTS, jsonbTournaments)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setLife(String dinozId, int life) {
        jooqContext.update(DINOZ).set(DINOZ.LIFE, life).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setPlaceNumber(String dinozId, int placeNumber) {
        jooqContext.update(DINOZ).set(DINOZ.PLACE_NUMBER, placeNumber).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setAppearanceCode(String dinozId, String appearanceCode) {
        jooqContext.update(DINOZ).set(DINOZ.APPEARANCE_CODE, appearanceCode).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setExperience(String dinozId, int xp) {
        jooqContext.update(DINOZ).set(DINOZ.EXPERIENCE, xp).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setExperienceWithActionMap(String dinozId, int xp, Map<String, Boolean> actionsMap) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.EXPERIENCE, xp)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setKabukiProgression(String dinozId, List<String> malusList, int kabukiProgression) {
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);

        if (jsonbMalusList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .set(DINOZ.KABUKI_PROGRESSION, kabukiProgression)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setDanger(String dinozId, int danger) {
        jooqContext.update(DINOZ).set(DINOZ.DANGER, danger).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setMalusList(String dinozId, List<String> malusList) {
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);

        if (jsonbMalusList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setName(String dinozId, String newName) {
        jooqContext.update(DINOZ).set(DINOZ.NAME, newName).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setLastFledEnnemy(String dinozId, String lastFledEnnemy) {
        jooqContext.update(DINOZ).set(DINOZ.LAST_FLED_ENNEMY, lastFledEnnemy).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void setPassiveList(String dinozId, Map<String, Integer> passiveList) {
        JSONB jsonbPassiveList = this.getJsonbPassiveList(passiveList);

        if (jsonbPassiveList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.PASSIVE_LIST, jsonbPassiveList)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void setPassiveAndMalusList(String dinozId, Map<String, Integer> passiveList, List<String> malusList) {
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);
        JSONB jsonbPassiveList = this.getJsonbPassiveList(passiveList);

        if (jsonbMalusList != null && jsonbPassiveList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .set(DINOZ.PASSIVE_LIST, jsonbPassiveList)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processFireBath(String dinozId, int life, Map<String, Boolean> actionsMap) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);
        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.LIFE, life)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processDailyReset(String dinozId, int life, Map<String, Boolean> actionsMap, String lastFledEnnemy, Boolean dinozIsActive) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.LIFE, life)
                    .set(DINOZ.LAST_FLED_ENNEMY, lastFledEnnemy)
                    .set(DINOZ.DINOZISACTIVE, dinozIsActive)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processDailyResetForInactiveDinoz(String dinozId, int life, Map<String, Boolean> actionsMap, String lastFledEnnemy, Integer danger, Integer placeNumber) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.LIFE, life)
                    .set(DINOZ.LAST_FLED_ENNEMY, lastFledEnnemy)
                    .set(DINOZ.DINOZISACTIVE, false)
                    .set(DINOZ.DANGER, danger)
                    .set(DINOZ.PLACE_NUMBER, placeNumber)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processRock(String masterId, String dinozId, int danger, Map<String, Boolean> actionsMap, int dangerLossByAll) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            // Update rokky danger
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.DANGER, danger)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();

            // Update all dinoz danger
            jooqContext.update(DINOZ)
                    .set(DINOZ.DANGER, DINOZ.DANGER.sub(dangerLossByAll))
                    .where(DINOZ.ID.notEqual(dinozId))
                    .and(DINOZ.MASTER_ID.eq(masterId))
                    .execute();

            // Fix if dinozs danger less than -200
            jooqContext.update(DINOZ)
                    .set(DINOZ.DANGER, -200)
                    .where(DINOZ.MASTER_ID.eq(masterId))
                    .and(DINOZ.DANGER.lessThan(-200))
                    .execute();
        }
    }

    @Override
    public void processLvlUp(String dinozId, Map<String, Integer> skillsMap, Map<String, Integer> elementsValues, int xp, int level, Map<String, Boolean> actionsMap) {
        JSONB jsonbElementsValues = this.getJsonbElementsValues(elementsValues);
        JSONB jsonbSkillsMap = this.getJsonbSkillsMap(skillsMap);
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbElementsValues != null && jsonbSkillsMap != null && jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.SKILLS_MAP, jsonbSkillsMap)
                    .set(DINOZ.ELEMENTS_VALUES, jsonbElementsValues)
                    .set(DINOZ.EXPERIENCE, xp)
                    .set(DINOZ.LEVEL, level)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processAnge(String dinozId, int life, Map<String, Boolean> actionsMap) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.LIFE, life)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processDarkPotion(String dinozId, boolean isDark, String appearanceCode, Map<String, Integer> elementsValues, Map<String, Integer> skillsMap) {
        JSONB jsonbElementsValues = this.getJsonbElementsValues(elementsValues);
        JSONB jsonbSkillsMap = this.getJsonbSkillsMap(skillsMap);

        if (jsonbElementsValues != null && jsonbSkillsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.IS_DARK, isDark)
                    .set(DINOZ.APPEARANCE_CODE, appearanceCode)
                    .set(DINOZ.ELEMENTS_VALUES, jsonbElementsValues)
                    .set(DINOZ.SKILLS_MAP, jsonbSkillsMap)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void fixSkills(String dinozId, Map<String, Integer> skillsMap) {
        JSONB jsonbSkillsMap = this.getJsonbSkillsMap(skillsMap);

        jooqContext.update(DINOZ)
                .set(DINOZ.SKILLS_MAP, jsonbSkillsMap)
                .where(DINOZ.ID.eq(dinozId))
                .execute();
    }

    @Override
    public List<Dinoz> getAllStealableWistitis() {
        return jooqContext.select(DINOZ.fields())
                .from(DINOZ)
                .where(DINOZ.RACE.eq(DinoparcConstants.OUISTITI))
                .and(DINOZ.LEVEL.lessThan(20))
                .fetch()
                .map(this::fromRecord);
    }

    @Override
    public void processCherry(String dinozId, String appearanceCode, List<String> malusList, Map<String, Boolean> actionsMap, Long epochSecondsEndOfCherryEffect, int minutesLeft) {
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);

        if (jsonbActionsMap != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .set(DINOZ.APPEARANCE_CODE, appearanceCode)
                    .set(DINOZ.EPOCH_SECONDS_END_OF_CHERRY_EFFECT, BigDecimal.valueOf(epochSecondsEndOfCherryEffect))
                    .set(DINOZ.CHERRY_EFFECT_MINUTES_LEFT, BigDecimal.valueOf(minutesLeft))
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processCurrentCherry(String dinozId, List<String> malusList, int minutesLeft) {
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);

        if (jsonbMalusList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .set(DINOZ.CHERRY_EFFECT_MINUTES_LEFT, BigDecimal.valueOf(minutesLeft))
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processFocus(String dinozId, Map<String, Integer> passiveList, Map<String, Integer> elementsValues) {
        JSONB jsonbElementsValues = this.getJsonbElementsValues(elementsValues);
        JSONB jsonbPassiveList = this.getJsonbPassiveList(passiveList);

        if (jsonbElementsValues != null && jsonbPassiveList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ELEMENTS_VALUES, jsonbElementsValues)
                    .set(DINOZ.PASSIVE_LIST, jsonbPassiveList)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public void processFusion(String dinozId, String appearanceCode, int level, Map<String, Integer> elementsValues) {
        JSONB jsonbElementsValues = this.getJsonbElementsValues(elementsValues);

        if (jsonbElementsValues != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ELEMENTS_VALUES, jsonbElementsValues)
                    .set(DINOZ.APPEARANCE_CODE, appearanceCode)
                    .set(DINOZ.LEVEL, level)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    @Override
    public List<HOFDinozDto> getRanking(String race) {
        Condition whereCondition = "default".equals(race) ? DINOZ_WITHOUT_MASTER_ADMIN
                : DINOZ_WITHOUT_MASTER_ADMIN.and(DINOZ.RACE.eq(race));
        return jooqContext.selectFrom(DINOZ)
                .where(whereCondition)
                .orderBy(DINOZ.LEVEL.desc())
                .limit(10)
                .fetch()
                .map(this::hofFromRecord);
    }

    @Override
    public Pagination<HOFDinozDto> getRanking(String race, Integer offset, Integer limit) {
        Pagination<HOFDinozDto> ret = new Pagination<>();
        Condition whereCondition = "default".equals(race) ? DINOZ_WITHOUT_MASTER_ADMIN
                : DINOZ_WITHOUT_MASTER_ADMIN.and(DINOZ.RACE.eq(race));

        Integer totalCount = jooqContext.selectCount().from(DINOZ).where(whereCondition).fetchOneInto(Integer.class);
        ret.setTotalCount(totalCount);

        ret.setElements(jooqContext.selectFrom(DINOZ)
                .where(whereCondition)
                .orderBy(DINOZ.LEVEL.desc(), DINOZ.NAME.asc(), DINOZ.MASTER_NAME.asc(), DINOZ.ID.asc())
                .limit(limit)
                .offset(offset)
                .fetch()
                .map(this::hofFromRecord));

        Integer nextOffset = offset == null ? 0 : offset;
        nextOffset = (nextOffset + limit) < totalCount ? nextOffset + limit : null;
        ret.setOffset(nextOffset);

        return ret;
    }

    @Override
    public List<Dinoz> findAll() {
        return jooqContext.selectFrom(DINOZ).fetch().map(this::fromRecord);
    }

    @Override
    public List<Dinoz> getTournamentContestants(Integer placeNumber) {
        return jooqContext.selectFrom(DINOZ)
                .where(DINOZ.PLACE_NUMBER.eq(placeNumber))
                .and(DINOZ.IS_IN_TOURNEY.eq(true))
                .fetch()
                .map(this::fromRecord);
    }

    @Override
    public void delete(String dinozId) {
        jooqContext.deleteFrom(DINOZ).where(DINOZ.ID.eq(dinozId)).execute();
    }

    @Override
    public void processFight(String dinozId, Map<String, Boolean> actionsMap, int life, int xp, int danger, Map<String, Integer> passiveList, List<String> malusList) {
        JSONB jsonbActionsMap = this.getJsonbActionsMap(actionsMap);
        JSONB jsonbMalusList = this.getJsonbMalusList(malusList);
        JSONB jsonbPassiveList = this.getJsonbPassiveList(passiveList);

        if (jsonbActionsMap != null && jsonbMalusList != null && jsonbPassiveList != null) {
            jooqContext.update(DINOZ)
                    .set(DINOZ.ACTIONS_MAP, jsonbActionsMap)
                    .set(DINOZ.LIFE, life)
                    .set(DINOZ.EXPERIENCE, xp)
                    .set(DINOZ.DANGER, danger)
                    .set(DINOZ.MALUS_LIST, jsonbMalusList)
                    .set(DINOZ.PASSIVE_LIST, jsonbPassiveList)
                    .where(DINOZ.ID.eq(dinozId))
                    .execute();
        }
    }

    private Dinoz fromRecord(Record dinozRecord) {
        Dinoz ret = new Dinoz();
        ret.setId(dinozRecord.getValue(DINOZ.ID));
        ret.setMasterId(dinozRecord.getValue(DINOZ.MASTER_ID));
        ret.setMasterName(dinozRecord.getValue(DINOZ.MASTER_NAME));
        ret.setName(dinozRecord.getValue(DINOZ.NAME));
        ret.setRace(dinozRecord.getValue(DINOZ.RACE));
        ret.setBeginMessage(dinozRecord.getValue(DINOZ.BEGIN_MESSAGE));
        ret.setEndMessage(dinozRecord.getValue(DINOZ.END_MESSAGE));
        ret.setLife(dinozRecord.getValue(DINOZ.LIFE));
        ret.setLevel(dinozRecord.getValue(DINOZ.LEVEL));
        ret.setExperience(dinozRecord.getValue(DINOZ.EXPERIENCE));
        ret.setDanger(dinozRecord.getValue(DINOZ.DANGER));
        ret.setPlaceNumber(dinozRecord.getValue(DINOZ.PLACE_NUMBER));
        ret.setAppearanceCode(dinozRecord.getValue(DINOZ.APPEARANCE_CODE));
        ret.setDark(dinozRecord.getValue(DINOZ.IS_DARK));
        ret.setInTourney(dinozRecord.getValue(DINOZ.IS_IN_TOURNEY));
        ret.setKabukiProgression(dinozRecord.getValue(DINOZ.KABUKI_PROGRESSION));
        ret.setLastValidBotHash(dinozRecord.getValue(DINOZ.LAST_VALID_BOT_HASH));
        ret.setSkips(dinozRecord.getValue(DINOZ.SKIPS));
        ret.setInBazar(dinozRecord.getValue(DINOZ.IS_IN_BAZAR));
        ret.setEpochSecondsEndOfCherryEffect(dinozRecord.getValue(DINOZ.EPOCH_SECONDS_END_OF_CHERRY_EFFECT).longValue());
        ret.setCherryEffectMinutesLeft(dinozRecord.getValue(DINOZ.CHERRY_EFFECT_MINUTES_LEFT).longValue());
        ret.setLastFledEnnemy(dinozRecord.getValue(DINOZ.LAST_FLED_ENNEMY));
        ret.setBotOnly(dinozRecord.getValue(DINOZ.BOT_ONLY));
        ret.setDinozIsActive(dinozRecord.getValue(DINOZ.DINOZISACTIVE));

        try {
            ret.setPassiveList(objectMapper.readValue(dinozRecord.getValue(DINOZ.PASSIVE_LIST).data(), Map.class));
            ret.setMalusList(objectMapper.readValue(dinozRecord.getValue(DINOZ.MALUS_LIST).data(), List.class));
            ret.setElementsValues(objectMapper.readValue(dinozRecord.getValue(DINOZ.ELEMENTS_VALUES).data(), Map.class));
            ret.setSkillsMap(objectMapper.readValue(dinozRecord.getValue(DINOZ.SKILLS_MAP).data(), Map.class));
            ret.setActionsMap(objectMapper.readValue(dinozRecord.getValue(DINOZ.ACTIONS_MAP).data(), Map.class));
            TypeReference<HashMap<String,Tournament>> typeRef = new TypeReference<HashMap<String,Tournament>>() {};
            ret.setTournaments(objectMapper.readValue(dinozRecord.getValue(DINOZ.TOURNAMENTS).data(), typeRef));
        } catch(Exception e) {
            return null;
        }

        return ret;
    }

    private Dinoz fighterFromRecord(Record dinozRecord) {
        Dinoz ret = new Dinoz();
        ret.setId(dinozRecord.getValue(DINOZ.ID));
        ret.setMasterId(dinozRecord.getValue(DINOZ.MASTER_ID));
        ret.setMasterName(dinozRecord.getValue(DINOZ.MASTER_NAME));
        ret.setName(dinozRecord.getValue(DINOZ.NAME));
        ret.setLevel(dinozRecord.getValue(DINOZ.LEVEL));
        ret.setAppearanceCode(dinozRecord.getValue(DINOZ.APPEARANCE_CODE));
        ret.setDinozIsActive(dinozRecord.getValue(DINOZ.DINOZISACTIVE));
        ret.setDanger(dinozRecord.getValue(DINOZ.DANGER));

        try {
            ret.setElementsValues(objectMapper.readValue(dinozRecord.getValue(DINOZ.ELEMENTS_VALUES).data(), Map.class));
            ret.setMalusList(objectMapper.readValue(dinozRecord.getValue(DINOZ.MALUS_LIST).data(), List.class));
        } catch(Exception e) {
            return null;
        }

        return ret;
    }

    private HOFDinozDto hofFromRecord(DinozRecord dinozRecord) {
        HOFDinozDto ret = new HOFDinozDto();
        ret.setLevel(dinozRecord.getLevel());
        ret.setName(dinozRecord.getName());
        ret.setAppearanceCode(dinozRecord.getAppearanceCode());
        ret.setRace(dinozRecord.getRace());
        ret.setMasterName(dinozRecord.getMasterName());
        return ret;
    }

    private RestrictedDinozDto fromRestrictedRecord(Record8<String, String, Integer, Integer, Integer, String, Integer, JSONB> restrictedDinozRecord) {
        RestrictedDinozDto ret = new RestrictedDinozDto();
        ret.setId(restrictedDinozRecord.get(DINOZ.ID));
        ret.setName(restrictedDinozRecord.get(DINOZ.NAME));
        ret.setLevel(restrictedDinozRecord.get(DINOZ.LEVEL));
        ret.setLife(restrictedDinozRecord.get(DINOZ.LIFE));
        ret.setExperience(restrictedDinozRecord.get(DINOZ.EXPERIENCE));
        ret.setAppearanceCode(restrictedDinozRecord.get(DINOZ.APPEARANCE_CODE));
        ret.setPlaceNumber(restrictedDinozRecord.get(DINOZ.PLACE_NUMBER));
        try {
            ret.setActionsMap(objectMapper.readValue(restrictedDinozRecord.get(DINOZ.ACTIONS_MAP).data(), Map.class));
        } catch(Exception e) {
            return null;
        }
        return ret;
    }

    private DinozRecord toRecord(Dinoz dinoz) {
        DinozRecord ret = new DinozRecord();
        ret.setId(dinoz.getId());
        ret.setMasterId(dinoz.getMasterId() == null ? "EMPTY" : dinoz.getMasterId());
        ret.setMasterName(dinoz.getMasterName() == null ? "EMPTY" : dinoz.getMasterName());
        ret.setName(dinoz.getName() == null ? "EMPTY" : dinoz.getName());
        ret.setRace(dinoz.getRace());
        ret.setBeginMessage(dinoz.getBeginMessage());
        ret.setEndMessage(dinoz.getEndMessage());
        ret.setLife(dinoz.getLife());
        ret.setLevel(dinoz.getLevel());
        ret.setExperience(dinoz.getExperience());
        ret.setDanger(dinoz.getDanger());
        ret.setPlaceNumber(dinoz.getPlaceNumber());
        ret.setAppearanceCode(dinoz.getAppearanceCode());
        ret.setIsDark(dinoz.isDark());
        ret.setIsInTourney(dinoz.isInTourney());
        ret.setKabukiProgression(dinoz.getKabukiProgression());
        ret.setLastValidBotHash(dinoz.getLastValidBotHash());
        ret.setSkips(dinoz.getSkips());
        ret.setIsInBazar(dinoz.isInBazar());
        ret.setEpochSecondsEndOfCherryEffect(BigDecimal.valueOf(dinoz.getEpochSecondsEndOfCherryEffect()));
        ret.setCherryEffectMinutesLeft(BigDecimal.valueOf(dinoz.getCherryEffectMinutesLeft()));
        ret.setLastFledEnnemy(dinoz.getLastFledEnnemy());
        ret.setBotOnly(dinoz.isBotOnly());
        ret.setDinozisactive(dinoz.isDinozIsActive());

        try {
            ret.setPassiveList(this.getJsonbPassiveList(dinoz.getPassiveList()));
            ret.setMalusList(this.getJsonbMalusList(dinoz.getMalusList()));
            ret.setElementsValues(this.getJsonbElementsValues(dinoz.getElementsValues()));
            ret.setSkillsMap(this.getJsonbSkillsMap(dinoz.getSkillsMap()));
            ret.setActionsMap(this.getJsonbActionsMap(dinoz.getActionsMap()));
            ret.setTournaments(this.getJsonbTournaments(dinoz.getTournaments()));
        } catch(Exception e) {
            return null;
        }

        return ret;
    }

    private JSONB getJsonbActionsMap(Map<String, Boolean> actionsMap) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(actionsMap == null ? new HashMap<String, Boolean>() : actionsMap));
        } catch(Exception e) {
            return null;
        }
    }

    private JSONB getJsonbPassiveList(Map<String, Integer> passiveList) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(passiveList == null ? new HashMap<String, Integer>() : passiveList));
        } catch(Exception e) {
            return null;
        }
    }

    private JSONB getJsonbElementsValues(Map<String, Integer> elementsValues) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(elementsValues == null ? new HashMap<String, Integer>() : elementsValues));
        } catch(Exception e) {
            return null;
        }
    }

    private JSONB getJsonbMalusList(List<String> malusList) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(malusList == null ? new ArrayList<String>() : malusList));
        } catch(Exception e) {
            return null;
        }
    }

    private JSONB getJsonbSkillsMap(Map<String, Integer> skillsMaps) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(skillsMaps == null ? new HashMap<String, Integer>() : skillsMaps));
        } catch(Exception e) {
            return null;
        }
    }

    private JSONB getJsonbTournaments(Map<String, Tournament> tournaments) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(tournaments == null ? new HashMap<String, Tournament>() : tournaments));
        } catch(Exception e) {
            return null;
        }
    }

    private RestrictedDinozDto fromRecordMinimal(Record dinozRecord) {
        RestrictedDinozDto ret = new RestrictedDinozDto();
        ret.setId(dinozRecord.getValue(DINOZ.ID));
        ret.setName(dinozRecord.getValue(DINOZ.NAME));
        ret.setRace(dinozRecord.getValue(DINOZ.RACE));
        ret.setLevel(dinozRecord.getValue(DINOZ.LEVEL));
        ret.setPlaceNumber(dinozRecord.getValue(DINOZ.PLACE_NUMBER));
        ret.setAppearanceCode(dinozRecord.getValue(DINOZ.APPEARANCE_CODE));
        return ret;
    }
}
