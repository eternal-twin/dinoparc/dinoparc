package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.RecMission;

import java.util.List;
import java.util.UUID;

public interface PgRecMissionRepository {
    RecMission get(UUID recMissionId);

    List<RecMission> getMissions(boolean playerMission, boolean daily, boolean weekly, boolean monthly, int i, int rarity);
    List<RecMission> getAllMissions();

    void create(RecMission recMission);
}
