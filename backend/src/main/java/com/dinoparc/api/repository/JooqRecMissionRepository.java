package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.RecMission;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import static com.dinoparc.tables.RecMission.REC_MISSION;

@Repository
public class JooqRecMissionRepository implements PgRecMissionRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqRecMissionRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public RecMission get(UUID id) {
        return jooqContext.selectFrom(REC_MISSION).where(REC_MISSION.ID.eq(id)).fetchOneInto(RecMission.class);
    }

    @Override
    public List<RecMission> getMissions(boolean playerMission, boolean daily, boolean weekly, boolean monthly, int difficulty, int rarity) {
        return jooqContext.selectFrom(REC_MISSION)
                .where(REC_MISSION.DINOZ.eq(!playerMission))
                .and(REC_MISSION.DAILY.eq(daily))
                .and(REC_MISSION.WEEKLY.eq(weekly))
                .and(REC_MISSION.MONTHLY.eq(monthly))
                .and(REC_MISSION.DIFFICULTY.eq(difficulty))
                .and(REC_MISSION.RARITY.eq(rarity))
                .fetchInto(RecMission.class);
    }

    @Override
    public List<RecMission> getAllMissions() {
        return jooqContext.selectFrom(REC_MISSION).fetchInto(RecMission.class);
    }

    @Override
    public void create(RecMission recMission) {
        jooqContext.insertInto(REC_MISSION).set(jooqContext.newRecord(REC_MISSION, recMission)).execute();
    }
}
