package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.RoleActionHistory;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.dinoparc.tables.RoleActionHistory.ROLE_ACTION_HISTORY;

@Repository
public class JooqRoleActionHistoryRepository implements PgRoleActionRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqRoleActionHistoryRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public void create(RoleActionHistory newRoleActionHistory) {
        jooqContext.insertInto(ROLE_ACTION_HISTORY).set(jooqContext.newRecord(ROLE_ACTION_HISTORY, newRoleActionHistory)).execute();
    }
}
