package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.GameMasterAction;

public interface PgGameMasterActionsRepository {
    GameMasterAction createGameMasterAction(GameMasterAction gameMasterAction);
}
