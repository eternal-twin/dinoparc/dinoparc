package com.dinoparc.api.repository;

import com.dinoparc.api.domain.dinoz.ArmyDinoz;
import com.dinoparc.api.domain.dinoz.ArmyDinozDto;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PgArmyDinozRepository {
    Optional<ArmyDinoz> getNextArmyDinoz(UUID bossId);

    Optional<ArmyDinoz> findById(UUID fromString);

    void deleteArmyDinozAndUpdateRanks(UUID armyDinozId, UUID bossId, int removedRank);

    void updateMaxNbAttacks(UUID armyDinozId, int maxNbAttacks);

    void addFightAndBossLifeLost(UUID armyDinozId, Integer eventDinozLifeLoss);

    Optional<ArmyDinoz> getArmyDinozFromMaster(UUID bossId, UUID masterId);
    List<ArmyDinoz> getArmyDinozsFromMaster(UUID masterId);

    Optional<ArmyDinoz> getArmyDinoz(String dinozId);

    Optional<ArmyDinoz> getArmyDinoz(UUID bossId, String dinozId);

    int getNextRank(UUID bossId);

    ArmyDinoz createArmyDinoz(ArmyDinoz armyDinoz);

    List<ArmyDinozDto> getArmyDinozsDto(UUID bossId);

    void cleanAll();

    void cleanAllForBoss(UUID bossId);

    boolean isInArmy(String dinozId);
}
