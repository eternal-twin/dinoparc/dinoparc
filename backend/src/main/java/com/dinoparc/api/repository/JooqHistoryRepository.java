package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.tables.records.HistoryRecord;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.UUID;

import static com.dinoparc.tables.History.HISTORY;

@Repository
public class JooqHistoryRepository implements PgHistoryRepository {

    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private final static int MAX_HISTORY_SIZE = 1000;
    private final static UUID NPC_UUID = UUID.fromString("00000000-0000-0000-0000-000000000000");

    private DSLContext jooqContext;

    @Autowired
    public JooqHistoryRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    public Pagination<History> getPlayerHistories(String playerId, Integer offset, Integer limit) {
        Pagination<History> ret = new Pagination<>();

        Condition whereCondition = HISTORY.PLAYER_ID.eq(UUID.fromString(playerId));

        Integer totalCount = jooqContext.selectCount().from(HISTORY).where(whereCondition).fetchOneInto(Integer.class);
        ret.setTotalCount(totalCount < DinoparcConstants.MAX_VISIBLE_HISTORY_SIZE ? totalCount : DinoparcConstants.MAX_VISIBLE_HISTORY_SIZE);

        ret.setElements(jooqContext.selectFrom(HISTORY)
                .where(whereCondition)
                .orderBy(HISTORY.NUMERICALDATE.desc())
                .limit(limit)
                .offset(offset)
                .fetch()
                .map(this::fromRecord));

        Integer nextOffset = offset == null ? 0 : offset;
        nextOffset = (nextOffset + limit) < totalCount ? nextOffset + limit : null;
        ret.setOffset(nextOffset);

        return ret;
    }

    @Override
    public Optional<History> getFirstUnseenPlayerHistories(String playerId) {
        return jooqContext.selectFrom(HISTORY)
            .where(HISTORY.PLAYER_ID.eq(UUID.fromString(playerId)))
            .and(HISTORY.SEEN.eq(Boolean.FALSE))
            .and(HISTORY.RANK.lessThan(DinoparcConstants.MAX_VISIBLE_HISTORY_SIZE))
            .orderBy(HISTORY.NUMERICALDATE.desc())
            .limit(1)
            .fetchOptional()
            .map(this::fromRecord);
    }

    @Override
    public void setPlayerSeen(String playerId, Integer offset, Integer limit) {
        jooqContext.update(HISTORY)
            .set(HISTORY.SEEN, true)
            .where(HISTORY.PLAYER_ID.eq(UUID.fromString(playerId)))
            .and(HISTORY.RANK.between(offset, offset + limit - 1))
            .execute();
    }

    @Override
    public void save(History history) {
        // Update ranks
        jooqContext.update(HISTORY).set(HISTORY.RANK, HISTORY.RANK.add(1)).where(HISTORY.PLAYER_ID.eq(UUID.fromString(history.getPlayerId()))).execute();

        // Create new history line
        jooqContext.insertInto(HISTORY).set(toRecord(history)).execute();
    }

    @Override
    public void cleanHistory() {
        jooqContext.deleteFrom(HISTORY).where(HISTORY.RANK.greaterOrEqual(MAX_HISTORY_SIZE)).execute();
    }

    @Override
    public Integer countHistories(String playerId, Boolean seen) {
        Condition whereCondition = seen == null ? HISTORY.PLAYER_ID.eq(UUID.fromString(playerId))
                : HISTORY.PLAYER_ID.eq(UUID.fromString(playerId)).and(HISTORY.SEEN.eq(seen));
        return jooqContext.selectCount().from(HISTORY).where(whereCondition).fetchOneInto(Integer.class);
    }

    @Override
    public void cleanPlayerHistory(String playerId) {
        jooqContext.deleteFrom(HISTORY).where(HISTORY.PLAYER_ID.eq(UUID.fromString(playerId))).execute();
    }

    private HistoryRecord toRecord(History history) {
        var ret = jooqContext.newRecord(HISTORY, history);
        ret.set(HISTORY.ID, UUID.randomUUID());
        ZonedDateTime neoparcTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        ret.set(HISTORY.NUMERICALDATE, neoparcTime.toOffsetDateTime());

        if (ret.getPlayerId() == null) {
            // Case of NPC actions for random bazar offers
            ret.setPlayerId(NPC_UUID);
        }
        return ret;
    }

    private History fromRecord(HistoryRecord record) {
        History ret = new History();

        ret.setId(record.getId());
        ret.setPlayerId(record.getPlayerId().toString());
        ret.setSeen(record.getSeen());
        ret.setIcon(record.getIcon());
        ret.setType(record.getType());
        ret.setNumericalDate(record.getNumericaldate().plusHours(2));
        ret.setHasWon(record.getHasWon());
        ret.setFromUserName(record.getFromUserName());
        ret.setFromDinozName(record.getFromDinozName());
        ret.setToUserName(record.getToUserName());
        ret.setToDinozName(record.getToDinozName());
        ret.setExpGained(getBigDecimalIntValue(record.getExpGained()));
        ret.setLifeLost(getBigDecimalIntValue(record.getLifeLost()));
        ret.setBuyAmount(getBigDecimalIntValue(record.getBuyAmount()));
        ret.setObject(record.getObject());
        ret.setRank(record.getRank());

        return ret;
    }

    private UUID idFromPartialRecord(Record2<UUID,OffsetDateTime> record) {
        return record.get(HISTORY.ID);
    }

    private Integer getBigDecimalIntValue(BigDecimal value) {
        return value == null ? null : value.intValue();
    }
}
