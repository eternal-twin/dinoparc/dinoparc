package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.News;
import java.util.List;

public interface PgNewsRepository {
    News create(News news);
    List<News> findAll();
    void updateNewsLabels(String newsId, News news);
}
