package com.dinoparc.api.repository;

import com.dinoparc.api.controllers.dto.EventPlayerDto;
import com.dinoparc.api.domain.mission.PlayerStat;
import com.dinoparc.tables.records.PlayerStatRecord;
import kotlin.Pair;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.jooq.UpdateSetMoreStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.UUID;
import java.util.List;

import static com.dinoparc.Tables.PLAYER;
import static com.dinoparc.tables.PlayerStat.PLAYER_STAT;

@Repository
public class JooqPlayerStatRepository implements PgPlayerStatRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqPlayerStatRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public void create(UUID playerId, String dinozId) {
        jooqContext.insertInto(PLAYER_STAT).set(jooqContext.newRecord(PLAYER_STAT, new PlayerStat(playerId, dinozId))).execute();
    }
    @Override
    public void addStats(UUID playerId, String dinozId, List<Pair<TableField<PlayerStatRecord, Long>, Integer>> fields) {
        try {
            var queryUpdatePlayerResult = this.getUpdateQueryStart(fields)
                    .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                    .and(PLAYER_STAT.DINOZ_ID.isNull())
                    .returning()
                    .execute();

            if (queryUpdatePlayerResult == 0) {
                var toCreate = new PlayerStat(playerId, null);
                jooqContext.insertInto(PLAYER_STAT).set(jooqContext.newRecord(PLAYER_STAT, toCreate)).execute();
                this.getUpdateQueryStart(fields)
                    .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                    .and(PLAYER_STAT.DINOZ_ID.isNull())
                    .returning()
                    .execute();
            }

            if (dinozId != null) {
                int queryUpdatePlayerDinozResult = this.getUpdateQueryStart(fields)
                    .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                    .and(PLAYER_STAT.DINOZ_ID.eq(dinozId))
                    .returning()
                    .execute();

                if (queryUpdatePlayerDinozResult == 0) {
                    var toCreate = new PlayerStat(playerId, dinozId);
                    jooqContext.insertInto(PLAYER_STAT).set(jooqContext.newRecord(PLAYER_STAT, toCreate)).execute();
                    this.getUpdateQueryStart(fields)
                        .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                        .and(PLAYER_STAT.DINOZ_ID.eq(dinozId))
                        .returning()
                        .execute();
                }
            }
        } catch (Exception e) {
            // Error on player stat incrementation should not crash any endpoint
            e.printStackTrace();
        }
    }

    @Override
    public List<EventPlayerDto> getAllActiveHalloweenEventCounters() {
        return jooqContext.select(PLAYER.PLAYER_ID, PLAYER.NAME, PLAYER_STAT.EVENT_COUNTER)
                .from(PLAYER)
                .join(PLAYER_STAT).on(PLAYER.PLAYER_ID.eq(PLAYER_STAT.PLAYER_ID))
                .where(PLAYER_STAT.DINOZ_ID.isNull())
                .and(PLAYER_STAT.EVENT_COUNTER.greaterThan(0))
                .orderBy(PLAYER_STAT.EVENT_COUNTER.desc())
                .fetchInto(EventPlayerDto.class);
    }

    @Override
    public Long getCounter(UUID playerId, String counterName) {
        List<Long> counters = jooqContext.select(PLAYER_STAT.field(counterName, Long.class))
                .from(PLAYER_STAT)
                .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                .and(PLAYER_STAT.DINOZ_ID.isNull())
                .fetchInto(Long.class);

        return !counters.isEmpty() ? counters.get(0) : 0;
    }

    @Override
    public Long getCounter(UUID playerId, String dinozId, String counterName) {
        return jooqContext.select(PLAYER_STAT.field(counterName, Long.class))
                .from(PLAYER_STAT)
                .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                .and(PLAYER_STAT.DINOZ_ID.eq(dinozId))
                .fetchOneInto(Long.class);
    }

    @Override
    public void deletePlayerStats(UUID playerId) {
        jooqContext.deleteFrom(PLAYER_STAT).where(PLAYER_STAT.PLAYER_ID.eq(playerId)).execute();
    }

    @Override
    public void deleteDinozStats(String dinozId) {
        jooqContext.deleteFrom(PLAYER_STAT).where(PLAYER_STAT.DINOZ_ID.eq(dinozId)).execute();
    }

    @Override
    public void deleteDinozStatsByPlayerId(String playerId) {
        jooqContext.deleteFrom(PLAYER_STAT)
                .where(PLAYER_STAT.DINOZ_ID.isNotNull())
                .and(PLAYER_STAT.PLAYER_ID.eq(UUID.fromString(playerId)))
                .execute();
    }

    @Override
    public void resetAllPlayerStat(UUID playerId, TableField<PlayerStatRecord, Long> field) {
        jooqContext.update(PLAYER_STAT)
                .set(field, 0L)
                .where(PLAYER_STAT.PLAYER_ID.eq(playerId))
                .returning()
                .execute();
    }

    @Override
    public void resetAllPlayerStatForEventCounter() {
        jooqContext.update(PLAYER_STAT)
                .set(EVENT_COUNTER, 0)
                .where(PLAYER_STAT.EVENT_COUNTER.greaterThan(0))
                .execute();
    }

    private UpdateSetMoreStep<PlayerStatRecord> getUpdateQueryStart(List<Pair<TableField<PlayerStatRecord,Long>,Integer>> fields) {
        var query = jooqContext.update(PLAYER_STAT);
        var firstField = fields.get(0);
        var updateQueryStart = query.set(firstField.getFirst(), firstField.getFirst().add(firstField.getSecond()));

        for (int i = 1;i<fields.size();i++) {
            var nextField = fields.get(i);
            updateQueryStart = updateQueryStart.set(nextField.getFirst(), nextField.getFirst().add(nextField.getSecond()));
        }
        return updateQueryStart;
    }
}
