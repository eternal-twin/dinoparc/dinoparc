package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.misc.Pagination;

import java.util.List;
import java.util.Optional;

public interface PgHistoryRepository {
    Pagination<History> getPlayerHistories(String playerId, Integer offset, Integer limit);
    Optional<History> getFirstUnseenPlayerHistories(String playerId);

    void setPlayerSeen(String playerId, Integer offset, Integer limit);

    void save(History buyHistory);

    void cleanHistory();

    Integer countHistories(String id, Boolean seen);

    void cleanPlayerHistory(String playerId);
}
