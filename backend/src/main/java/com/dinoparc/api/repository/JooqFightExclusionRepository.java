package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.FightExclusion;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.tables.records.FightExclusionRecord;
import com.dinoparc.tables.records.HistoryRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

import static com.dinoparc.tables.FightExclusion.FIGHT_EXCLUSION;

@Repository
public class JooqFightExclusionRepository implements PgFightExclusionRepository {
    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqFightExclusionRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<FightExclusion> getFightExclusion(UUID fromPlayerId, UUID toPlayerId) {
        return jooqContext.selectFrom(FIGHT_EXCLUSION)
                .where(FIGHT_EXCLUSION.FROM_PLAYER_ID.eq(fromPlayerId))
                .and(FIGHT_EXCLUSION.TO_PLAYER_ID.eq(toPlayerId))
                .fetchOptional()
                .map(this::fromRecord);
    }

    @Override
    public void createFightExclusion(FightExclusion fightExclusion) {
        jooqContext.insertInto(FIGHT_EXCLUSION).set(toRecord(fightExclusion)).execute();
    }

    @Override
    public void deleteFightExclusion(UUID fightExclusionId) {
        jooqContext.deleteFrom(FIGHT_EXCLUSION).where(FIGHT_EXCLUSION.ID.eq(fightExclusionId)).execute();
    }
    private FightExclusionRecord toRecord(FightExclusion fightExclusion) {
        FightExclusionRecord record = jooqContext.newRecord(FIGHT_EXCLUSION, fightExclusion);
        record.setId(UUID.randomUUID());
        record.setNumericaldate(OffsetDateTime.now());
        return record;
    }

    private FightExclusion fromRecord(FightExclusionRecord record) {
        FightExclusion ret = new FightExclusion();

        ret.setId(record.getId());
        ret.setFromPlayerId(record.getFromPlayerId());
        ret.setToPlayerId(record.getToPlayerId());
        ret.setReason(record.getReason());

        return ret;
    }
}
