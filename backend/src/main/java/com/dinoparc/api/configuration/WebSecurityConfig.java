package com.dinoparc.api.configuration;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.exporter.otlp.http.logs.OtlpHttpLogRecordExporter;
import io.opentelemetry.exporter.otlp.http.trace.OtlpHttpSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.logs.SdkLoggerProvider;
import io.opentelemetry.sdk.logs.export.BatchLogRecordProcessor;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor;
import io.opentelemetry.semconv.ResourceAttributes;
import io.opentelemetry.semconv.ServiceAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import com.dinoparc.api.domain.account.DinoparcConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;

import net.eternaltwin.client.HttpEtwinClient;
import net.eternaltwin.oauth.client.RfcOauthClient;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
public class WebSecurityConfig {

  @Configuration
  public static class ApiConfiguration {
    @Value("${server.secret}")
    public String secret;

    @Value("${etwin.uri}")
    public String etwinUri;

    @Value("${server.backend}")
    public String backendUri;

    @Value("${etwin.client-id}")
    public String clientId;

    @Value("${etwin.client-secret}")
    public String clientSecret;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, AuthenticationManager authenticationManager) throws Exception {
      http
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(authz -> authz
          .requestMatchers(DinoparcConstants.UnrestrictedPathsArray).permitAll()
          .anyRequest().authenticated()
        )
        .addFilter(new JWTAuthorizationFilter(authenticationManager, secret))
        .sessionManagement(sm -> sm.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
      return http.build();
    }

    @Bean
    public RfcOauthClient getOauthClient() throws URISyntaxException {
      URI authUri = new URI(joinUri(etwinUri, "oauth/authorize"));
      URI tokenUri = new URI(joinUri(etwinUri, "oauth/token"));
      URI callBackUri = new URI(joinUri(backendUri, "api/account/callback"));

      return new RfcOauthClient(authUri, tokenUri, callBackUri, clientId, clientSecret);
    }

    @Bean
    public HttpEtwinClient getEtwinClient() throws URISyntaxException {

      return new HttpEtwinClient(new URI(etwinUri));
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
      throws Exception {
      return authenticationConfiguration.getAuthenticationManager();
    }

    public static String joinUri(String left, String right) {
      if (left.endsWith("/")) {
        return left + right;
      } else {
        return left + "/" + right;
      }
    }

    @Bean
    public OpenTelemetry openTelemetry(HttpEtwinClient etwinClient) {
      final String id = this.clientId;
      final String secret = this.clientSecret;
      final String credentials = id + ":" + secret;

      int envStart = id.indexOf('_') + 1;
      int envEnd = id.indexOf('@');
      String env = "dev";
      if (0 < envStart && envStart < envEnd) {
        env = id.substring(envStart, envEnd);
      }
      final String encoded = Base64.getEncoder().encodeToString(StandardCharsets.UTF_8.encode(credentials).array());

      final Resource resource =
        Resource.getDefault().toBuilder()
          .put(ServiceAttributes.SERVICE_NAME, "neoparc")
          .put(ResourceAttributes.DEPLOYMENT_ENVIRONMENT, env)
          .build();

      final SdkLoggerProvider sdkLoggerProvider =
        SdkLoggerProvider.builder()
          .addLogRecordProcessor(
            BatchLogRecordProcessor.builder(OtlpHttpLogRecordExporter.builder()
                .setEndpoint(joinUri(etwinUri, "v1/logs"))
                .addHeader("authorization", "Basic " + encoded)
                .build())
              .build())
          .setResource(resource)
          .build();

      final SdkTracerProvider sdkTracerProvider =
        SdkTracerProvider.builder()
          .addSpanProcessor(
            BatchSpanProcessor.builder(OtlpHttpSpanExporter.builder()
                .setEndpoint(joinUri(etwinUri, "v1/traces"))
                .addHeader("authorization", "Basic " + encoded)
                .build())
              .build())
          .setResource(resource)
          .build();

      final OpenTelemetry openTelemetry =
        OpenTelemetrySdk.builder()
          .setLoggerProvider(sdkLoggerProvider)
          .setTracerProvider(sdkTracerProvider)
          .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
          .buildAndRegisterGlobal();

      return openTelemetry;
    }
  }
}
