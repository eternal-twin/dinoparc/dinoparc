package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.*;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.ClanMessage;
import com.dinoparc.api.domain.clan.OccupationSummary;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.ClanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.dinoparc.api.controllers.AccountController.isValidAndNotBlockedAccount;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/clans")
public class ClanController {

    private final TokenAccountValidation tokenAccountValidation;
    private final AccountService accountService;
    private final ClanService clanService;
    private final PgPlayerRepository playerRepository;

    @Autowired
    public ClanController(TokenAccountValidation tokenAccountValidation,
                          AccountService accountService,
                          ClanService clanService,
                          PgPlayerRepository playerRepository) {
        this.tokenAccountValidation = tokenAccountValidation;
        this.accountService = accountService;
        this.clanService = clanService;
        this.playerRepository = playerRepository;
    }

    @GetMapping("/isInAClan/{accountId}")
    public PlayerClanInfoDto checkIfPlayerIsInAClan(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return playerRepository.checkIfPlayerIsInAClan(accountId);
        }
        return null;
    }

    @PostMapping("/createClan")
    public Boolean createACLan(@RequestHeader("Authorization") String token, @RequestBody CreateClanRequest createClanRequest) {
        Optional<Player> player = accountService.getAccountById(createClanRequest.getAccountId());
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            Player clanCreator = player.get();
            if (clanCreator.getCash() >= 100000 && !(playerRepository.checkIfPlayerIsInAClan(clanCreator.getId()).getInAClan())) {
                clanService.removeApplicationFromClan(player.get());
                return clanService.createClanForUser(createClanRequest, clanCreator);
            }
        }
        return false;
    }

    @GetMapping("/{accountId}")
    public Clan getClan(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.getClan(player.get().getId());
        }
        return null;
    }

    @PostMapping("/postMessage")
    public ClanMessage postMessageToClan(@RequestHeader("Authorization") String token,  @RequestBody ClanMessageRequest clanMessageRequest) {
        Optional<Player> player = accountService.getAccountById(clanMessageRequest.getAuthorId());
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(clanMessageRequest.getAuthorId()).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.postMessageToClanForum(player.get(), clanMessageRequest.getMessage());
        }
        return null;
    }

    @GetMapping("/messages/{accountId}")
    public ClanMessage getClanMessages(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.getClanMessages(accountId);
        }
        return null;
    }

    @GetMapping("/rights/{accountId}")
    public Boolean checkRightsOnClanForUser(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            return clanService.checkRightsOnClanForUser(accountId);
        }
        return null;
    }

    @PutMapping("/modifyClanPage")
    public Boolean modifyClanPage(@RequestHeader("Authorization") String token, @RequestBody CreateClanRequest createClanRequest) {
        Optional<Player> player = accountService.getAccountById(createClanRequest.getAccountId());
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(createClanRequest.getAccountId()).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.modifyOrCreateClanPage(createClanRequest);
        }
        return false;
    }

    @PutMapping("/deleteClanPage")
    public Boolean deleteClanPage(@RequestHeader("Authorization") String token, @RequestBody CreateClanRequest createClanRequest) {
        Optional<Player> player = accountService.getAccountById(createClanRequest.getAccountId());
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(createClanRequest.getAccountId()).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.deleteClanPage(createClanRequest);
        }
        return false;
    }

    @GetMapping("/displayLatests/{accountId}")
    public List<RankingClanDto> getLatestsPublicClansDisplayData(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.getLatestPublicClansDisplayDataForRankings();
        }
        return new ArrayList<>();
    }

    @GetMapping("/public/{clanId}/{accountId}")
    public PublicClanDto getPublicClanData(@RequestHeader("Authorization") String token, @PathVariable String clanId, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.getPublicClanData(clanId);
        }
        return null;
    }

    @GetMapping("/members/{clanId}/{accountId}")
    public List<PlayerClan> getAllClanMembers(@RequestHeader("Authorization") String token, @PathVariable String clanId, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.getClansMembers(clanId);
        }
        return new ArrayList<>();
    }

    @PutMapping("/apply/{clanId}/{accountId}")
    public Boolean applyToClan(@RequestHeader("Authorization") String token, @PathVariable String clanId, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && !playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.applyToClan(clanId, player.get());
        }
        return false;
    }

    @PutMapping("/remove-apply/{accountId}")
    public Boolean removeApplicationFromClan(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && !playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.removeApplicationFromClan(player.get());
        }
        return false;
    }

    @PutMapping("/process-application/{clanId}/{candidatId}/{accepted}/{accountId}")
    public Boolean processApplicationRequest(
            @RequestHeader("Authorization") String token,
            @PathVariable String clanId,
            @PathVariable String candidatId,
            @PathVariable Boolean accepted,
            @PathVariable String accountId) {

        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            if (accepted) {
                return clanService.acceptPlayerIntoClan(clanId, candidatId);
            }
            return clanService.removePlayerFromClan(clanId, candidatId);
        }

        return false;
    }

    @GetMapping("/displayAllRankings/{accountId}")
    public List<RankingClanDto> getAllPublicClansDisplayDataForRankings(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.getAllPublicClansDisplayDataForRankings();
        }
        return new ArrayList<>();
    }

    @PutMapping("/quit/{accountId}")
    public Boolean quitClan(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.quitClan(accountId);
        }
        return false;
    }

    @PutMapping("/change-admin-flag/{clanId}/{requesterId}/{userToEdit}")
    public Boolean changeAdminFlagForUser(
            @RequestHeader("Authorization") String token,
            @PathVariable String clanId,
            @PathVariable String requesterId,
            @PathVariable String userToEdit) {

        Optional<Player> player = accountService.getAccountById(requesterId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(requesterId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.changeAdminFlagForUser(clanId, requesterId, userToEdit);
        }
        return false;
    }

    @PutMapping("/change-title/{clanId}/{requesterId}/{userToEdit}/{title}")
    public Boolean changeTitleForUser(
            @RequestHeader("Authorization") String token,
            @PathVariable String clanId,
            @PathVariable String requesterId,
            @PathVariable String userToEdit,
            @PathVariable String title) {

        Optional<Player> player = accountService.getAccountById(requesterId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(requesterId).getInAClan() && title != null && !title.isEmpty()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.changeTitleForUser(clanId, requesterId, userToEdit, title);
        }
        return false;
    }

    @PostMapping("/{accountId}/totem")
    public GiveObjectDto giveAllItemsToClan(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        GiveObjectDto response = new GiveObjectDto();
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            Integer quantityGiven = 0;
            quantityGiven += clanService.giveBuzeToTotem(desiredAccount.get().getId(), Integer.MAX_VALUE);
            quantityGiven += clanService.givePlumesToTotem(desiredAccount.get().getId(), Integer.MAX_VALUE);
            quantityGiven += clanService.giveDentsToTotem(desiredAccount.get().getId(), Integer.MAX_VALUE);
            quantityGiven += clanService.giveCrinsToTotem(desiredAccount.get().getId(), Integer.MAX_VALUE);
            quantityGiven += clanService.giveBoisToTotem(desiredAccount.get().getId(), Integer.MAX_VALUE);

            if (quantityGiven > 0) {
                response.setSuccess(true);
                response.setSuccessMessageFr("Vous avez donné " + quantityGiven + " objet(s) au Totem de votre clan.");
                response.setSuccessMessageEs("Le has dado " + quantityGiven + " objeto(s) a tu tótem de clan.");
                response.setSuccessMessageEn("You gave " + quantityGiven + " object(s) to your clan's Totem.");
            } else {
                response.setInutileError(true);
            }
        } else {
            response.setInutileError(true);
        }
        return response;
    }

    @PostMapping("/{accountId}/totemBuze/{buzeQuantity}")
    public GiveObjectDto giveBuzeToClan(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String buzeQuantity) {
        GiveObjectDto response = new GiveObjectDto();
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            Integer quantityGiven = clanService.giveBuzeToTotem(desiredAccount.get().getId(), Integer.parseInt(buzeQuantity));
            if (quantityGiven > 0) {
                response.setSuccess(true);
                response.setSuccessMessageFr("Vous avez donné " + quantityGiven + " Buze(s) au Totem de votre clan.");
                response.setSuccessMessageEs("Le has dado " + quantityGiven + " Buze(s) a tu tótem de clan.");
                response.setSuccessMessageEn("You gave " + quantityGiven + " Buze(s) to your clan's Totem.");
            } else {
                response.setInutileError(true);
            }
        } else {
            response.setInutileError(true);
        }
        return response;
    }

    @PostMapping("/{accountId}/totemPlume/{plumeQuantity}")
    public GiveObjectDto givePlumesToClan(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String plumeQuantity) {
        GiveObjectDto response = new GiveObjectDto();
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            Integer quantityGiven = clanService.givePlumesToTotem(desiredAccount.get().getId(), Integer.parseInt(plumeQuantity));
            if (quantityGiven > 0) {
                response.setSuccess(true);
                response.setSuccessMessageFr("Vous avez donné " + quantityGiven + " Plume(s) de Ketchas au Totem de votre clan.");
                response.setSuccessMessageEs("Le has dado " + quantityGiven + " Pluma(s) a tu tótem de clan.");
                response.setSuccessMessageEn("You gave " + quantityGiven + " Feather(s) to your clan's Totem.");
            } else {
                response.setInutileError(true);
            }
        } else {
            response.setInutileError(true);
        }
        return response;
    }

    @PostMapping("/{accountId}/totemDent/{dentQuantity}")
    public GiveObjectDto giveDentsToClan(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dentQuantity) {
        GiveObjectDto response = new GiveObjectDto();
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            Integer quantityGiven = clanService.giveDentsToTotem(desiredAccount.get().getId(), Integer.parseInt(dentQuantity));
            if (quantityGiven > 0) {
                response.setSuccess(true);
                response.setSuccessMessageFr("Vous avez donné " + quantityGiven + " Dent(s) des Ancêtres au Totem de votre clan.");
                response.setSuccessMessageEs("Le has dado " + quantityGiven + " Diente(s) a tu tótem de clan.");
                response.setSuccessMessageEn("You gave " + quantityGiven + " Teeth to your clan's Totem.");
            } else {
                response.setInutileError(true);
            }
        } else {
            response.setInutileError(true);
        }
        return response;
    }

    @PostMapping("/{accountId}/totemCrin/{crinQuantity}")
    public GiveObjectDto giveCrinsToClan(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String crinQuantity) {
        GiveObjectDto response = new GiveObjectDto();
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            Integer quantityGiven = clanService.giveCrinsToTotem(desiredAccount.get().getId(), Integer.parseInt(crinQuantity));
            if (quantityGiven > 0) {
                response.setSuccess(true);
                response.setSuccessMessageFr("Vous avez donné " + quantityGiven + " Crin(s) de Licorne au Totem de votre clan.");
                response.setSuccessMessageEs("Le has dado " + quantityGiven + " Melena(s) a tu tótem de clan.");
                response.setSuccessMessageEn("You gave " + quantityGiven + " Unicorn Hairs to your clan's Totem.");
            } else {
                response.setInutileError(true);
            }
        } else {
            response.setInutileError(true);
        }
        return response;
    }

    @PostMapping("/{accountId}/totemBois/{boisQuantity}")
    public GiveObjectDto giveBoisToClan(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String boisQuantity) {
        GiveObjectDto response = new GiveObjectDto();
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            Integer quantityGiven = clanService.giveBoisToTotem(desiredAccount.get().getId(), Integer.parseInt(boisQuantity));
            if (quantityGiven > 0) {
                response.setSuccess(true);
                response.setSuccessMessageFr("Vous avez donné " + quantityGiven + " Bois Magique(s) au Totem de votre clan.");
                response.setSuccessMessageEs("Le has dado " + quantityGiven + " Madera(s) Magica a tu tótem de clan.");
                response.setSuccessMessageEn("You gave " + quantityGiven + " Magical Wood to your clan's Totem.");
            } else {
                response.setInutileError(true);
            }
        } else {
            response.setInutileError(true);
        }
        return response;
    }

    @PutMapping("/redeemTotemRewards/{accountId}")
    public void redeemTotemRewards(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (isValidAndNotBlockedAccount(desiredAccount) && DinoparcConstants.WAR_MODE) {
            clanService.redeemTotemRewards(desiredAccount.get());
        }
    }

    @PostMapping("/{accountId}/redeem-chest/irmas/{quantity}")
    public Boolean redeemIrmasFromClanChest(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable Integer quantity) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.tryToRedeemIrmasFromClanChest(player.get(), quantity);
        }
        return null;
    }

    @PostMapping("/{accountId}/redeem-chest/prismatiks/{quantity}")
    public Boolean redeemPrismatiksFromClanChest(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable Integer quantity) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            return clanService.tryToRedeemPrismatiksFromClanChest(player.get(), quantity);
        }
        return null;
    }

    @PutMapping("/{accountId}/{dinozId}/warrior-potion")
    public Boolean giveWarriorPotionToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(player.get(), dinozId);
            if (dinoz.isPresent()) {
                return clanService.giveWarriorPotionToDinoz(player.get().getId(), dinoz.get());
            }
        }
        return null;
    }

    @PutMapping("/{accountId}/{dinozId}/warrior-potion-remove")
    public Boolean removeWarriorPotionToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent() && playerRepository.checkIfPlayerIsInAClan(accountId).getInAClan()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(player.get(), dinozId);
            if (dinoz.isPresent() && clanService.checkIfDinozIsWarrior(dinoz.get().getMalusList())) {
                return clanService.removeWarriorPotionFromDinoz(player.get().getId(), dinoz.get());
            }
        }
        return null;
    }

    @GetMapping("/occupation-summary")
    public OccupationSummary getOccupationSummary() {
        return clanService.occupationSummary;
    }
}
