package com.dinoparc.api.controllers.dto;

public class CreateDinozAdminDto {

    private String accountId;

    private String appearanceCode;
    private String race;
    private Integer level;

    private Integer feu;
    private Integer terre;
    private Integer eau;
    private Integer foudre;
    private Integer air;

    public CreateDinozAdminDto() {

    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAppearanceCode() {
        return appearanceCode;
    }

    public void setAppearanceCode(String appearanceCode) {
        this.appearanceCode = appearanceCode;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getFeu() {
        return feu;
    }

    public void setFeu(Integer feu) {
        this.feu = feu;
    }

    public Integer getTerre() {
        return terre;
    }

    public void setTerre(Integer terre) {
        this.terre = terre;
    }

    public Integer getEau() {
        return eau;
    }

    public void setEau(Integer eau) {
        this.eau = eau;
    }

    public Integer getFoudre() {
        return foudre;
    }

    public void setFoudre(Integer foudre) {
        this.foudre = foudre;
    }

    public Integer getAir() {
        return air;
    }

    public void setAir(Integer air) {
        this.air = air;
    }
}
