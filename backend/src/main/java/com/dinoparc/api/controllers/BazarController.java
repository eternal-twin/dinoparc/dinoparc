package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.BazarService;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/bazar")
public class BazarController {
    private final AccountService accountService;
    private final BazarService bazarService;
    private final TokenAccountValidation tokenAccountValidation;

    public BazarController(AccountService accountService, BazarService bazarService, TokenAccountValidation tokenAccountValidation) {
        this.accountService = accountService;
        this.bazarService = bazarService;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @GetMapping
    public Dinoz getBazarDinozById(@RequestParam(required = true) String dinozId) {
        return bazarService.getBazarDinoz(dinozId);
    }

    @GetMapping("/{accountId}")
    public List<BazarListing> getAllBazarListings(@RequestHeader("Authorization") String token,
                                                  @PathVariable String accountId,
                                                  @RequestParam(required = true) String category,
                                                  @RequestParam(required = true) boolean active,
                                                  @RequestParam(required = false) Integer limit,
                                                  @RequestParam(required = false) Integer offset,
                                                  HttpServletResponse response) {
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
            limit = limit == null || limit < 0 || limit > 100 ? 100 : limit;
            Pagination<BazarListing> pagination = bazarService.listBazarListings(desiredAccount.get(), limit, offset, category, active);
            response.setHeader(Pagination.TOTAL_COUNT_HEADER, pagination.getTotalCount().toString());
            if (pagination.getOffset() != null) {
                response.setHeader(Pagination.OFFSET_HEADER, pagination.getOffset().toString());
                response.setHeader("Access-Control-Expose-Headers", Pagination.TOTAL_COUNT_HEADER + ", " + Pagination.OFFSET_HEADER);
            } else {
                response.setHeader("Access-Control-Expose-Headers", Pagination.TOTAL_COUNT_HEADER);
            }
            return pagination.getElements();
        }

        return null;
    }
}
