package com.dinoparc.api.controllers.dto;

public class CredentialsDto {

  private String username;
  private String pwdHash;

  public CredentialsDto() {

  }

  public CredentialsDto(String username, String pwdHash) {
    super();
    this.username = username;
    this.pwdHash = pwdHash;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPwdHash() {
    return pwdHash;
  }

  public void setPwdHash(String pwdHash) {
    this.pwdHash = pwdHash;
  }

}
