package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.PlayerService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/player")
public class PlayerController {
    private final AccountService accountService;
    private final PlayerService playerService;
    private TokenAccountValidation tokenAccountValidation;

    public PlayerController(
        AccountService accountService,
        PlayerService playerService,
        TokenAccountValidation tokenAccountValidation
    ) {
        this.accountService = accountService;
        this.playerService = playerService;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @PatchMapping("/{accountId}/imgMode/{imgMode}")
    public Player updatePlayerImgMode(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String imgMode) {
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
            return playerService.updatePlayerImgMode(desiredAccount.get(), imgMode);
        }

        return desiredAccount.get();
    }

    @PatchMapping("/{accountId}/actionMode/{actionMode}")
    public Player updateActionMode(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String actionMode) {
        Optional<Player> desiredAccount = accountService.getAccountById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
            return playerService.updatePlayerActionMode(desiredAccount.get(), actionMode);
        }

        return desiredAccount.get();
    }
}
