package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.api.services.DinozService;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/dinozs")
public class DinozController {

    private final DinozService dinozService;

    public DinozController(DinozService dinozService) {
        this.dinozService = dinozService;
    }

    @GetMapping("/chapter-rankings/{chapter}")
    public List<HOFDinozDto> getChapterRanking(@PathVariable String chapter,
                                               @RequestParam(required = false) Integer offset,
                                               @RequestParam(required = false) Integer limit,
                                               HttpServletResponse response) {
        if (chapter != null && !chapter.isEmpty()) {
            Pagination<HOFDinozDto> pagination = dinozService.getRaceRankings(chapter, offset, limit);
            response.setHeader(Pagination.TOTAL_COUNT_HEADER, pagination.getTotalCount().toString());
            if (pagination.getOffset() != null) {
                response.setHeader(Pagination.OFFSET_HEADER, pagination.getOffset().toString());
                response.setHeader("Access-Control-Expose-Headers", Pagination.TOTAL_COUNT_HEADER + ", " + Pagination.OFFSET_HEADER);
            } else {
                response.setHeader("Access-Control-Expose-Headers", Pagination.TOTAL_COUNT_HEADER);
            }
            return pagination.getElements();
        }
        return new ArrayList<>();
    }
}
