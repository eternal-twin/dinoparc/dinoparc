package com.dinoparc.api.controllers.dto;

public class FightExclusionRequestDto {
    private String fromPlayerName;
    private String toPlayerName;
    private String reason;

    public String getFromPlayerName() {
        return fromPlayerName;
    }

    public void setFromPlayerName(String fromPlayerName) {
        this.fromPlayerName = fromPlayerName;
    }

    public String getToPlayerName() {
        return toPlayerName;
    }

    public void setToPlayerName(String toPlayerName) {
        this.toPlayerName = toPlayerName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
