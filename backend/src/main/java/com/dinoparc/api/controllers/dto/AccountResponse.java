package com.dinoparc.api.controllers.dto;

import org.springframework.http.HttpStatus;
import com.dinoparc.api.domain.account.Player;

public class AccountResponse {

  private HttpStatus status;
  private Player account;
  private Integer numberOfDinoz;

  public AccountResponse() {}

  public AccountResponse(HttpStatus status, Player account, Integer dinozCount) {
    super();
    this.status = status;
    this.account = account;
    this.numberOfDinoz = dinozCount;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
  }

  public Player getAccount() {
    return account;
  }

  public void setAccount(Player account) {
    this.account = account;
  }

  public Integer getNumberOfDinoz() {
    return numberOfDinoz;
  }

  public void setNumberOfDinoz(Integer numberOfDinoz) {
    this.numberOfDinoz = numberOfDinoz;
  }
}
