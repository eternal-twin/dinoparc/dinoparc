package com.dinoparc.api.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.dinoparc.api.domain.misc.TokenAccountValidation;
import kotlin.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.dinoparc.api.controllers.dto.MessageDto;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.Message;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.MessageService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/messages")
public class MessageController {

  private final MessageService messageService;
  private final AccountService accountService;
  private final TokenAccountValidation tokenAccountValidation;

  @Autowired
  public MessageController(MessageService messageService, AccountService accountService, TokenAccountValidation tokenAccountValidation) {
    this.messageService = messageService;
    this.accountService = accountService;
    this.tokenAccountValidation = tokenAccountValidation;
  }

  @PostMapping("/{accountId}")
  public Message sendMessage(@RequestHeader("Authorization") String token,
                             @PathVariable String accountId,
                             @RequestBody MessageDto messageRequest) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      return messageService.sendMessage(desiredAccount.get(), messageRequest);
    }

    return null;
  }

  @GetMapping("/{accountId}")
  public Map<String, Pair<String, List<Message>>> getMessages(@RequestHeader("Authorization") String token,
                                                              @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      return messageService.getMessages(accountId);
    }

    return null;
  }

  @DeleteMapping("/{accountId}")
  public boolean deleteMessages(@RequestHeader("Authorization") String token,
                                @PathVariable String accountId,
                                @RequestBody ArrayList<String> accountsIdToDelete) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      return messageService.deleteMessages(desiredAccount.get(), accountsIdToDelete);
    }

    return false;
  }

  @PutMapping("/{accountId}/read")
  public boolean markMessagesAsRead(@RequestHeader("Authorization") String token,
                                    @PathVariable String accountId,
                                    @RequestBody ArrayList<String> accountsIdToRead) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      return messageService.readMessages(accountId, accountsIdToRead);
    }

    return false;
  }

  @GetMapping("/{accountId}/download/{partnerId}")
  public ResponseEntity<Resource> getThreadAsCSV(@RequestHeader("Authorization") String token,
                                                 @PathVariable String accountId,
                                                 @PathVariable String partnerId) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      String filename = "Export.csv";
      InputStreamResource file = new InputStreamResource(messageService.downloadThread(accountId, partnerId));

      return ResponseEntity.ok()
              .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
              .contentType(MediaType.parseMediaType("application/csv"))
              .body(file);
    }
    return null;
  }
}
