package com.dinoparc.api.controllers.dto.mission;

import java.util.UUID;

public class PlayerMissionRequest {
    UUID playerId;
    String dinozId;
    int missionType; // 1 = daily, 2 = weekly, 3 = monthly

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public int getMissionType() {
        return missionType;
    }

    public void setMissionType(int missionType) {
        this.missionType = missionType;
    }
}
