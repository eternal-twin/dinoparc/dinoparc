package com.dinoparc.api.controllers.dto;

import com.dinoparc.api.domain.dinoz.Dinoz;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class HOFDinozDto implements Comparable<HOFDinozDto> {

    private String name;
    private String masterName;
    private String appearanceCode;
    private Integer level;
    private String race;

    public HOFDinozDto() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getAppearanceCode() {
        return appearanceCode;
    }

    public void setAppearanceCode(String appearanceCode) {
        this.appearanceCode = appearanceCode;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    @Override
    public int compareTo(HOFDinozDto other) {
        if (this.getLevel() < other.getLevel()) {
            return 1;
        } else if (Objects.equals(this.getLevel(), other.getLevel())) {
            return 0;
        } else {
            return -1;
        }
    }
}
