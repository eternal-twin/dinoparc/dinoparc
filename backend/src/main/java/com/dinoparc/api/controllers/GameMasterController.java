package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.FightExclusionRequestDto;
import com.dinoparc.api.controllers.dto.UpdateDangerRequestDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.GameMasterAction;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import jakarta.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/game-master")
public class GameMasterController {

    private AccountService accountService;
    private BazarService bazarService;
    private DinozService dinozService;
    private FightExclusionService fightExclusionService;
    private ClientInformationService clientInformationService;
    private UtilsService utilsService;
    private PgClanRepository clanRepository;
    private PgDinozRepository dinozRepository;
    private PgGameMasterActionsRepository gameMasterActionsRepository;
    private PgInventoryRepository inventoryRepository;
    private PgPlayerRepository playerRepository;
    private PgWildWistitiRepository wildWistitiRepository;
    private TokenAccountValidation tokenAccountValidation;

    @Autowired
    public GameMasterController(AccountService accountService, BazarService bazarService,
                                DinozService dinozService, FightExclusionService fightExclusionService,
                                ClientInformationService clientInformationService,
                                UtilsService utilsService, PgClanRepository clanRepository,
                                PgDinozRepository dinozRepository, PgGameMasterActionsRepository gameMasterActionsRepository,
                                PgInventoryRepository inventoryRepository, PgPlayerRepository playerRepository,
                                PgWildWistitiRepository wildWistitiRepository, TokenAccountValidation tokenAccountValidation) {
        this.accountService = accountService;
        this.bazarService = bazarService;
        this.dinozService = dinozService;
        this.fightExclusionService = fightExclusionService;
        this.clientInformationService = clientInformationService;
        this.utilsService = utilsService;
        this.clanRepository = clanRepository;
        this.dinozRepository = dinozRepository;
        this.gameMasterActionsRepository = gameMasterActionsRepository;
        this.inventoryRepository = inventoryRepository;
        this.playerRepository = playerRepository;
        this.wildWistitiRepository = wildWistitiRepository;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @PutMapping("/{gmAccountId}/historySkip/{accountId}/{historySkip}")
    public GameMasterAction historySkip(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String accountId,
            @PathVariable Boolean historySkip) {

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setTargetAccountId(accountId);
        gameMasterAction.setActionType("historySkip");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            // If game master account exists, retrieve player to block/ban
            Optional<Player> desiredAccount = accountService.getAccountById(accountId);

            if (desiredAccount.isPresent()) {
                // If player to block/ban exists, execute requested block/ban
                playerRepository.updateHistorySkip(accountId, historySkip);

                if (!historySkip) {
                    clientInformationService.cleanSkipHistory(accountId);
                }

                gameMasterAction.setResult("Done for user " + desiredAccount.get().getName());
                gameMasterAction.setSuccess(true);
                gameMasterAction.setForbidden(false);
            } else {
                // Player to block/ban does not exist
                gameMasterAction.setResult("No user found for accountId " + accountId);
                gameMasterAction.setSuccess(false);
                gameMasterAction.setForbidden(false);
            }
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
        }

        return gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @PutMapping("/{gmAccountId}/banPlayer/{accountId}/{banned}/{blocked}/{bannedPvp}/{bannedRaid}/{bannedShiny}/{bannedWistiti}/{bannedBazar}")
    public GameMasterAction banPlayer(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String accountId,
            @PathVariable Boolean banned,
            @PathVariable Boolean blocked,
            @PathVariable Boolean bannedPvp,
            @PathVariable Boolean bannedRaid,
            @PathVariable Boolean bannedShiny,
            @PathVariable Boolean bannedWistiti,
            @PathVariable Boolean bannedBazar) {

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setTargetAccountId(accountId);
        gameMasterAction.setActionType("banPlayer");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            // If game master account exists, retrieve player to block/ban
            Optional<Player> desiredAccount = accountService.getAccountById(accountId);

            if (desiredAccount.isPresent()) {
                // If player to block/ban exists, execute requested block/ban
                playerRepository.updateBanInfos(accountId, banned, blocked, bannedPvp, bannedRaid, bannedShiny, bannedWistiti, bannedBazar);

                gameMasterAction.setResult("Done for user " + desiredAccount.get().getName());
                gameMasterAction.setSuccess(true);
                gameMasterAction.setForbidden(false);
            } else {
                // Player to block/ban does not exist
                gameMasterAction.setResult("No user found for accountId " + accountId);
                gameMasterAction.setSuccess(false);
                gameMasterAction.setForbidden(false);
            }
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
        }

        return gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @GetMapping("/{gmAccountId}/fixClansPoints/{doIt}")
    public List<String> fixClansPoints(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String doIt) {

        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixClansPoints");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            // If game master account exists, try to fix clans points
            List<Clan> allClans = clanRepository.getAllClans();

            for (Clan clan : allClans) {
                if ("Eternal DinoRPG".equals(clan.getName())) {
                    ret.add("- Ignoring Eternal DinoRPG clan due to DB consistency issue");
                } else {
                    List<String> clanPlayerIds = clan.getMembers();
                    int sumFights = 0;
                    int sumTotem = 0;

                    for (String clanPlayerId : clanPlayerIds) {
                        PlayerClan playerClan = clanRepository.getPlayerClan(clanPlayerId).get();
                        sumFights += playerClan.getNbPointsFights();
                        sumTotem += playerClan.getNbPointsTotem();
                    }

                    if (sumFights > clan.getNbPointsFights()) {
                        int missingFightPoints = sumFights - clan.getNbPointsFights();

                        if (Boolean.valueOf(doIt)) {
                            ret.add("- Going to fix fights points of '" + clan.getName() + "', adding : " + missingFightPoints);
                            clanRepository.addClanPointsFights(clan.getId(), missingFightPoints);
                        } else {
                            ret.add("- Need to fix fights points of '" + clan.getName() + "', need to add : " + missingFightPoints);
                        }
                    } else if (sumFights < clan.getNbPointsFights()) {
                        int fightPointsToRemove = sumFights - clan.getNbPointsFights();

                        if (Boolean.valueOf(doIt)) {
                            ret.add("- Going to fix fights points of '" + clan.getName() + "', removing : " + fightPointsToRemove);
                            clanRepository.addClanPointsFights(clan.getId(), fightPointsToRemove);
                        } else {
                            ret.add("- Need to fix fights points of '" + clan.getName() + "', need to remove : " + fightPointsToRemove);
                        }
                    }

                    if (sumTotem > clan.getNbPointsTotem()) {
                        int missingTotemPoints = sumTotem - clan.getNbPointsTotem();

                        if (Boolean.valueOf(doIt)) {
                            ret.add("- Going to fix totem points of '" + clan.getName() + "', adding : " + missingTotemPoints);
                            clanRepository.addClanPointsFouilles(clan.getId(), missingTotemPoints);
                        } else {
                            ret.add("- Need to fix totem points of '" + clan.getName() + "', need to add : " + missingTotemPoints);
                        }
                    } else if (sumTotem < clan.getNbPointsTotem()) {
                        int totemPointsToRemove = sumTotem - clan.getNbPointsTotem();

                        if (Boolean.valueOf(doIt)) {
                            ret.add("- Going to fix totem points of '" + clan.getName() + "', removing : " + totemPointsToRemove);
                            clanRepository.addClanPointsFouilles(clan.getId(), totemPointsToRemove);
                        } else {
                            ret.add("- Need to fix totem points of '" + clan.getName() + "', need to remove : " + totemPointsToRemove);
                        }
                    }

                    gameMasterAction.setResult(ret.stream().reduce("", String::concat));
                }
            }
            gameMasterAction.setSuccess(true);
            gameMasterAction.setForbidden(false);
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }

        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);

        return ret;
    }

    @PutMapping("/{gmAccountId}/fixInventoryIssues")
    public void fixInventoryIssues(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId) {
        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixInventoryIssues");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            inventoryRepository.fixBondsIssues();
            gameMasterAction.setResult("Fixed inventory issues");
            gameMasterAction.setSuccess(true);
            gameMasterAction.setForbidden(false);
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }

        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @PutMapping("/{gmAccountId}/deleteSpecificBazarListing/{listingId}")
    public void deleteSpecificBazarListing(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String listingId) {
        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixInventoryIssues");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            bazarService.deleteBazarListing(listingId);
            gameMasterAction.setResult("Bazar listing deleted");
            gameMasterAction.setSuccess(true);
            gameMasterAction.setForbidden(false);
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }

        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @GetMapping("/{gmAccountId}/consult-daily")
    public String consultDaily(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId) {

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            EventDinozDto wildWistiti = wildWistitiRepository.getWildWistiti();
            if (wildWistiti.getLife() > 0) {
                String lifeLeft = String.valueOf(wildWistiti.getLife());
                String fCount = String.valueOf(wildWistiti.getFightCount());
                return "He's at " + lifeLeft + " HP left. "
                        + "\nHe was fought " + fCount + " times in total."
                        + "\nFire charms left : " + wildWistiti.getBonusFire()
                        + "\nWood charms left : " + wildWistiti.getBonusWood()
                        + "\nWater charms left : " + wildWistiti.getBonusWater()
                        + "\nThunder charms left : " + wildWistiti.getBonusThunder()
                        + "\nWind charms left : " + wildWistiti.getBonusAir()
                        + "\n\nCOMBAT LOGS :\n" + Arrays.toString(EventFightService.wistitiLogs.toArray());

            }
            return "The Wistiti was caught! Come back tomorrow! " + "\n\nCOMBAT LOGS :\n" + Arrays.toString(EventFightService.wistitiLogs.toArray());
        }
        // Requester is not a game master ...
        return "No right to do this action";
    }

    @PutMapping("/{gmAccountId}/triggerMaj")
    public void triggerMaj(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId) {
        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixInventoryIssues");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {

            try {
                utilsService.maintenanceGenerale();
                gameMasterAction.setSuccess(true);
                gameMasterAction.setForbidden(false);
            } catch (Exception e) {
                gameMasterAction.setSuccess(false);
                gameMasterAction.setForbidden(false);
            }
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }

        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @PutMapping("/{gmAccountId}/modify-clan-faction/{clanId}/{faction}")
    public String modifyClanFaction (final HttpServletRequest request,
                                     @RequestHeader("Authorization") String token,
                                     @PathVariable String gmAccountId,
                                     @PathVariable String clanId,
                                     @PathVariable String faction) {

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("modifyClanFaction");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {

            switch (faction) {
                case (DinoparcConstants.FACTION_1):
                    clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_1);
                    break;
                case (DinoparcConstants.FACTION_2):
                    clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_2);
                    break;
                case (DinoparcConstants.FACTION_3):
                    clanRepository.updateFaction(clanId, DinoparcConstants.FACTION_3);
                    break;
            }
            gameMasterAction.setSuccess(true);
            gameMasterAction.setForbidden(false);
            gameMasterAction.setResult("Action done for clan " + clanId);
            gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
            return "Action done";
        }

        // Requester is not a game master ...
        gameMasterAction.setResult("No right to do this action");
        gameMasterAction.setSuccess(false);
        gameMasterAction.setForbidden(true);
        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
        return "No right to do this action";
    }

    @PutMapping("/{gmAccountId}/fixWistiti/{dinozId}")
    @Transactional
    public void fixWistiti(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String dinozId) {
        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixWistiti");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            Dinoz dinoz = dinozRepository.findById(dinozId).get();
            if (DinoparcConstants.OUISTITI.equals(dinoz.getRace())
                    && dinoz.getLevel() == 145
                    && dinoz.getElementsValues().get(DinoparcConstants.FEU) == 30
                    && dinoz.getElementsValues().get(DinoparcConstants.TERRE) == 31
                    && dinoz.getElementsValues().get(DinoparcConstants.EAU) == 31
                    && dinoz.getElementsValues().get(DinoparcConstants.FOUDRE) == 31
                    && dinoz.getElementsValues().get(DinoparcConstants.AIR) == 30) {
                dinoz.setLevel(151);
                dinozRepository.processLvlUp(dinoz.getId(), dinoz.getSkillsMap(), dinoz.getElementsValues(), 0, dinoz.getLevel(), dinoz.getActionsMap());
                gameMasterAction.setResult("Fixed wistiti");
                gameMasterAction.setSuccess(true);
                gameMasterAction.setForbidden(false);
            } else {
                gameMasterAction.setResult("Failed to fix wistiti");
                gameMasterAction.setSuccess(false);
                gameMasterAction.setForbidden(false);
            }
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }
        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @PutMapping("/{gmAccountId}/fixBlockedPlayerDanger/{blockedPlayerId}")
    @Transactional
    public void fixBlockedPlayerDanger(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @PathVariable String blockedPlayerId) {
        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("fixBlockedPlayerDanger");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            Optional<Player> blockedPlayerOpt = accountService.getAccountById(blockedPlayerId);

            if (blockedPlayerOpt.isPresent()) {
                List<Dinoz> blockedPlayerDinozs = dinozRepository.findByPlayerId(blockedPlayerId);
                for (Dinoz blockedPlayerDinoz : blockedPlayerDinozs) {
                    dinozRepository.setDanger(blockedPlayerDinoz.getId(), -200);
                }
                gameMasterAction.setResult("Fixed blocked player danger");
                gameMasterAction.setSuccess(true);
                gameMasterAction.setForbidden(false);
            } else {
                gameMasterAction.setResult("Failed to fix blocked player danger");
                gameMasterAction.setSuccess(false);
                gameMasterAction.setForbidden(false);
            }
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }
        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @DeleteMapping("/{gmAccountId}/cleanClientInformationTrace")
    @Transactional
    public void cleanClientInformationTrace(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId) {
        List<String> ret = new ArrayList<>();

        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        GameMasterAction gameMasterAction = new GameMasterAction();
        gameMasterAction.setSourceAccountId(gmAccountId);
        gameMasterAction.setActionType("cleanClientInformationTrace");
        gameMasterAction.setActionDetail(path);

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            clientInformationService.cleanClientInformationTrace();
            gameMasterAction.setResult("Client information trace table : clean OK");
            gameMasterAction.setSuccess(true);
            gameMasterAction.setForbidden(false);
        } else {
            // Requester is not a game master ...
            gameMasterAction.setResult("No right to do this action");
            gameMasterAction.setSuccess(false);
            gameMasterAction.setForbidden(true);
            ret.add("No right to do this action");
        }
        gameMasterActionsRepository.createGameMasterAction(gameMasterAction);
    }

    @PostMapping("/{gmAccountId}/addFightExclusion")
    public String addFightExclusion(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @RequestBody FightExclusionRequestDto fightExclusionRequestDto) {

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {

            return fightExclusionService.addFightExclusion(fightExclusionRequestDto);
        }
        // Requester is not a game master ...
        return "No right to do this action";
    }

    @DeleteMapping("/{gmAccountId}/deleteFightExclusion")
    public String deleteFightExclusion(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @RequestBody FightExclusionRequestDto fightExclusionRequestDto) {

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            return fightExclusionService.deleteFightExclusion(fightExclusionRequestDto);
        }
        // Requester is not a game master ...
        return "No right to do this action";
    }

    @PostMapping("/{gmAccountId}/updateDanger")
    public String updateDanger(
            final HttpServletRequest request,
            @RequestHeader("Authorization") String token,
            @PathVariable String gmAccountId,
            @RequestBody UpdateDangerRequestDto updateDangerRequest) {

        // Retrieve game master account
        Optional<Player> gameMasterAccount = accountService.getAccountById(gmAccountId);
        this.tokenAccountValidation.validate(gameMasterAccount.get(), token, null);

        if (gameMasterAccount.isPresent()
                && DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER.contains(gameMasterAccount.get().getRole())) {
            for (String dinozId : updateDangerRequest.getDinozIds()) {
                dinozService.updateInactivePlayerDinozDanger(dinozId, updateDangerRequest.getDanger());
            }
            return "Danger update done";
        }
        // Requester is not a game master ...
        return "No right to do this action";
    }
}
