package com.dinoparc.api.controllers.dto;

import java.util.ArrayList;
import java.util.List;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.dinoz.Dinoz;

public class Backup {

  private List<Player> accountsList;
  private List<Dinoz> dinozList;
  
  public Backup() {
    accountsList = new ArrayList<Player>();
    dinozList = new ArrayList<Dinoz>();
  }

  public List<Player> getAccountsList() {
    return accountsList;
  }

  public void setAccountsList(List<Player> accountsList) {
    this.accountsList = accountsList;
  }

  public List<Dinoz> getDinozList() {
    return dinozList;
  }

  public void setDinozList(List<Dinoz> dinozList) {
    this.dinozList = dinozList;
  }
  
}
