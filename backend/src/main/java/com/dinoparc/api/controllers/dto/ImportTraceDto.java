package com.dinoparc.api.controllers.dto;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ImportTraceDto {
  public String id;
  public String importedAccount;
  public String originServer;
  public List<String> importedDinoz;

  public String newHostAccountId;
  public String importationDate;

  public ImportTraceDto() {
    importedDinoz = new ArrayList<String>();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getImportedAccount() {
    return importedAccount;
  }

  public void setImportedAccount(String importedAccount) {
    this.importedAccount = importedAccount;
  }

  public List<String> getImportedDinoz() {
    return importedDinoz;
  }

  public void setImportedDinoz(List<String> importedDinoz) {
    this.importedDinoz = importedDinoz;
  }

  public String getNewHostAccountId() {
    return newHostAccountId;
  }

  public void setNewHostAccountId(String newHostAccountId) {
    this.newHostAccountId = newHostAccountId;
  }

  public String getImportationDate() {
    return importationDate;
  }

  public void setImportationDate(OffsetDateTime importationDate) {
    StringBuilder sb = new StringBuilder(importationDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
    sb.append(" ");
    sb.append(importationDate.format(DateTimeFormatter.ISO_LOCAL_TIME).substring(0, 8));
    this.importationDate = sb.toString();
  }

  public String getOriginServer() {
    return originServer;
  }

  public void setOriginServer(String originServer) {
    this.originServer = originServer;
  }

}
