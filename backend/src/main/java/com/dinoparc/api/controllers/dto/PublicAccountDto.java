package com.dinoparc.api.controllers.dto;

import java.util.ArrayList;
import java.util.List;
import com.dinoparc.api.domain.dinoz.Dinoz;

public class PublicAccountDto {

  private String id;
  private String name;
  private List<Dinoz> dinozList;
  private Integer nbPoints;
  private Double averageLevelPoints;
  private Integer nbDinoz;
  private Integer position;
  private Integer actualHermitStage;
  private Integer cap;
  private Integer nbCaptures;
  private String clanName;
  private Boolean isActive;

  public PublicAccountDto() {
    this.dinozList = new ArrayList<Dinoz>();
  }

  public PublicAccountDto(String id, String name, List<Dinoz> dinozList, Integer nbPoints, Double averageLevelPoints, Integer nbDinoz) {
    this.id = id;
    this.name = name;
    this.dinozList = dinozList;
    this.nbPoints = nbPoints;
    this.averageLevelPoints = averageLevelPoints;
    this.nbDinoz = nbDinoz;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Dinoz> getDinozList() {
    return dinozList;
  }

  public void setDinozList(List<Dinoz> dinozList) {
    this.dinozList = dinozList;
  }

  public Integer getNbPoints() {
    return nbPoints;
  }

  public void setNbPoints(Integer nbPoints) {
    this.nbPoints = nbPoints;
  }

  public Double getAverageLevelPoints() {
    return averageLevelPoints;
  }

  public void setAverageLevelPoints(Double averageLevelPoints) {
    this.averageLevelPoints = averageLevelPoints;
  }

  public Integer getNbDinoz() {
    return nbDinoz;
  }

  public void setNbDinoz(Integer nbDinoz) {
    this.nbDinoz = nbDinoz;
  }

  public Integer getPosition() {
    return position;
  }

  public void setPosition(Integer position) {
    this.position = position;
  }

  public Integer getActualHermitStage() {
    return actualHermitStage;
  }

  public void setActualHermitStage(Integer actualHermitStage) {
    this.actualHermitStage = actualHermitStage;
  }

  public Integer getCap() {
    return cap;
  }

  public void setCap(Integer cap) {
    this.cap = cap;
  }

  public Integer getNbCaptures() {
    return nbCaptures;
  }

  public void setNbCaptures(Integer nbCaptures) {
    this.nbCaptures = nbCaptures;
  }

  public String getClanName() {
    return clanName;
  }

  public void setClanName(String clanName) {
    this.clanName = clanName;
  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }
}
