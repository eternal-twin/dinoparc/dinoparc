package com.dinoparc.api.controllers.dto;

import java.util.List;

public class UpdateDangerRequestDto {
    private List<String> dinozIds;
    private int danger;

    public List<String> getDinozIds() {
        return dinozIds;
    }

    public void setDinozIds(List<String> dinozIds) {
        this.dinozIds = dinozIds;
    }

    public int getDanger() {
        return danger;
    }

    public void setDanger(int danger) {
        this.danger = danger;
    }
}
