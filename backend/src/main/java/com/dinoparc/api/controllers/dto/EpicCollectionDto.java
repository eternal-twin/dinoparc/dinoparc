package com.dinoparc.api.controllers.dto;

import java.util.List;

public class EpicCollectionDto {

  private List<String> collectionEpic;
  private List<String> numberOfLines;

  public EpicCollectionDto() {

  }

  public List<String> getCollectionEpic() {
    return collectionEpic;
  }

  public void setCollectionEpic(List<String> collectionEpic) {
    this.collectionEpic = collectionEpic;
  }

  public List<String> getNumberOfLines() {
    return numberOfLines;
  }

  public void setNumberOfLines(List<String> numberOfLines) {
    this.numberOfLines = numberOfLines;
  }

}
