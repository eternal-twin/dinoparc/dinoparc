package com.dinoparc.api.controllers.dto;

import org.springframework.http.HttpStatus;

public class AuthenticationResponse {

  private HttpStatus status;
  private String username;
  private String responseMessage;
  private String accountId;

  public AuthenticationResponse() {

  }

  public AuthenticationResponse(HttpStatus status, String username, String responseMessage,
      String accountId) {
    super();
    this.status = status;
    this.username = username;
    this.responseMessage = responseMessage;
    this.accountId = accountId;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getResponseMessage() {
    return responseMessage;
  }

  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

}
