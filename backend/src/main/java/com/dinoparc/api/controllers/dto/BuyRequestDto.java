package com.dinoparc.api.controllers.dto;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BuyRequestDto {

  private String totalPrice;

  public String irmaBuy;
  public String angeBuy;
  public String burgerBuy;
  public String painBuy;
  public String tarteBuy;
  public String medailleBuy;
  public String feuBuy;
  public String terreBuy;
  public String eauBuy;
  public String foudreBuy;
  public String airBuy;
  public String griffesBuy;
  public String loupiBuy;
  public String ramensBuy;
  public String tigerBuy;
  public String antidoteBuy;
  public String pruniacBuy;
  public String focusAggroBuy;
  public String focusNatureBuy;
  public String focusWaterBuy;
  public String prismatikBuy;
  public String bonsBuy;
  public String bonsSell;
  public String eternityPillBuy;
  public String egg11Buy;
  public String egg18Buy;
  public String pirateItemBuy;

  public BuyRequestDto() {

  }

  public boolean isValid() throws IllegalAccessException {
    List<Field> fieldList = Arrays.stream(BuyRequestDto.class.getFields()).collect(Collectors.toList());
    for (Field field : fieldList) {
      if (field.get(this) != null && Integer.parseInt((String) field.get(this)) < 0) {
          return false;
      }
    }
    return true;
  }

  public String getPirateItemBuy() {
    return pirateItemBuy;
  }

  public void setPirateItemBuy(String pirateItemBuy) {
    this.pirateItemBuy = pirateItemBuy;
  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getIrmaBuy() {
    return irmaBuy;
  }

  public void setIrmaBuy(String irmaBuy) {
    this.irmaBuy = irmaBuy;
  }

  public String getAngeBuy() {
    return angeBuy;
  }

  public void setAngeBuy(String angeBuy) {
    this.angeBuy = angeBuy;
  }

  public String getBurgerBuy() {
    return burgerBuy;
  }

  public void setBurgerBuy(String burgerBuy) {
    this.burgerBuy = burgerBuy;
  }

  public String getPainBuy() {
    return painBuy;
  }

  public void setPainBuy(String painBuy) {
    this.painBuy = painBuy;
  }

  public String getTarteBuy() {
    return tarteBuy;
  }

  public void setTarteBuy(String tarteBuy) {
    this.tarteBuy = tarteBuy;
  }

  public String getMedailleBuy() {
    return medailleBuy;
  }

  public void setMedailleBuy(String medailleBuy) {
    this.medailleBuy = medailleBuy;
  }

  public String getFeuBuy() {
    return feuBuy;
  }

  public void setFeuBuy(String feuBuy) {
    this.feuBuy = feuBuy;
  }

  public String getTerreBuy() {
    return terreBuy;
  }

  public void setTerreBuy(String terreBuy) {
    this.terreBuy = terreBuy;
  }

  public String getEauBuy() {
    return eauBuy;
  }

  public void setEauBuy(String eauBuy) {
    this.eauBuy = eauBuy;
  }

  public String getFoudreBuy() {
    return foudreBuy;
  }

  public void setFoudreBuy(String foudreBuy) {
    this.foudreBuy = foudreBuy;
  }

  public String getAirBuy() {
    return airBuy;
  }

  public void setAirBuy(String airBuy) {
    this.airBuy = airBuy;
  }

  public String getGriffesBuy() {
    return griffesBuy;
  }

  public void setGriffesBuy(String griffesBuy) {
    this.griffesBuy = griffesBuy;
  }

  public String getLoupiBuy() {
    return loupiBuy;
  }

  public void setLoupiBuy(String loupiBuy) {
    this.loupiBuy = loupiBuy;
  }

  public String getRamensBuy() {
    return ramensBuy;
  }

  public void setRamensBuy(String ramensBuy) {
    this.ramensBuy = ramensBuy;
  }

  public String getTigerBuy() {
    return tigerBuy;
  }

  public void setTigerBuy(String tigerBuy) {
    this.tigerBuy = tigerBuy;
  }

  public String getAntidoteBuy() {
    return antidoteBuy;
  }

  public void setAntidoteBuy(String antidoteBuy) {
    this.antidoteBuy = antidoteBuy;
  }

  public String getPruniacBuy() {
    return pruniacBuy;
  }

  public void setPruniacBuy(String pruniacBuy) {
    this.pruniacBuy = pruniacBuy;
  }

  public String getFocusAggroBuy() {
    return focusAggroBuy;
  }

  public void setFocusAggroBuy(String focusAggroBuy) {
    this.focusAggroBuy = focusAggroBuy;
  }

  public String getFocusNatureBuy() {
    return focusNatureBuy;
  }

  public void setFocusNatureBuy(String focusNatureBuy) {
    this.focusNatureBuy = focusNatureBuy;
  }

  public String getFocusWaterBuy() {
    return focusWaterBuy;
  }

  public void setFocusWaterBuy(String focusWaterBuy) {
    this.focusWaterBuy = focusWaterBuy;
  }

  public String getPrismatikBuy() {return prismatikBuy;}

  public void setPrismatikBuy(String prismatikBuy) {this.prismatikBuy = prismatikBuy;}

  public String getBonsBuy() {
    return bonsBuy;
  }

  public void setBonsBuy(String bonsBuy) {
    this.bonsBuy = bonsBuy;
  }

  public String getBonsSell() {
    return bonsSell;
  }

  public void setBonsSell(String bonsSell) {
    this.bonsSell = bonsSell;
  }

  public String getEternityPillBuy() {
    return eternityPillBuy;
  }

  public void setEternityPillBuy(String eternityPillBuy) {
    this.eternityPillBuy = eternityPillBuy;
  }

  public String getEgg11Buy() {
    return egg11Buy;
  }

  public void setEgg11Buy(String egg11Buy) {
    this.egg11Buy = egg11Buy;
  }

  public String getEgg18Buy() {
    return egg18Buy;
  }

  public void setEgg18Buy(String egg18Buy) {
    this.egg18Buy = egg18Buy;
  }
}
