package com.dinoparc.api.controllers.dto;

public class ClientInformationDetailRequestDto {
    private String accountId;
    private String dinozId;
    private String first;
    private String second;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }
}
