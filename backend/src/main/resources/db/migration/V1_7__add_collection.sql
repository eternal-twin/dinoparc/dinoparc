create table collection
(
  id                         uuid             primary key not null,
  player_id                  uuid             unique not null,
  collection                 jsonb,
  epic_collection            jsonb
);

create index collection_player_id on collection(player_id);