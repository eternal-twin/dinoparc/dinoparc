create table history
(
  id               uuid             primary key not null,
  player_id        uuid             not null,
  type             text             default '' not null,
  icon             text             default '' not null,
  seen             boolean          default false not null,
  has_won          boolean          default false,
  from_user_name   text             default '',
  from_dinoz_name  text             default '',
  to_user_name     text             default '',
  to_dinoz_name    text             default '',
  exp_gained       decimal          default 0,
  life_lost        decimal          default 0,
  buy_amount       decimal          default 0,
  object           text             default '',
  numericalDate   timestamp with time zone default now() not null
);

create index history_player_id on history(player_id);
