truncate table raid_boss;

drop table raid_boss;

create table boss
(
  boss_id                      uuid             not null,
  appearance_code              text             not null,
  name                         text             not null,
  place_number                 integer          not null default 1,
  hour_of_day                  integer          not null default 17,
  day_of_week                  integer          not null default 7,
  minutes_duration             integer          not null default 30,
  remaining_army_attacks       integer          not null default 7500,
  army_attacking               boolean          not null default false,
  life                         integer          not null default 0,
  first_element                text             not null default 'Feu',
  second_element               text             not null default 'Eau',
  third_element                text             not null default 'Air',
  fire                         integer          not null default 0,
  wood                         integer          not null default 0,
  water                        integer          not null default 0,
  thunder                      integer          not null default 0,
  air                          integer          not null default 0,
  bonus_fire                   integer          not null default 0,
  bonus_wood                   integer          not null default 0,
  bonus_water                  integer          not null default 0,
  bonus_thunder                integer          not null default 0,
  bonus_air                    integer          not null default 0,
  bonus_claw                   integer          not null default 0,
  fight_count                  integer          not null default 0
);

INSERT INTO public.boss
(boss_id, appearance_code, "name", place_number, hour_of_day, day_of_week, minutes_duration, remaining_army_attacks, army_attacking, life, first_element, second_element, third_element, fire, wood, water, thunder, air, bonus_fire, bonus_wood, bonus_water, bonus_thunder, bonus_air, bonus_claw, fight_count)
VALUES('00000000-0000-0000-0000-000000000000', 'BY1mbgH8QPXkmR1yfZoydv', '???', 1, 17, 7, 30, 7500, false, 5000000, 'Feu'::text, 'Eau'::text, 'Air'::text, 125, 0, 120, 0, 115, 100, 100, 100, 100, 100, 100, 0);

create table army_dinoz
(
  army_dinoz_id                uuid             not null,
  dinoz_id                     text             not null,
  master_id                    uuid             not null,
  boss_id                      uuid             not null,
  rank                         integer          not null,
  nb_attacks                   integer          not null,
  max_nb_attacks               integer          not null,
  boss_life_lost               integer          not null
);

alter table history add column rank integer not null default 0;