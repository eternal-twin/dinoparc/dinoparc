create table raid_boss
(
  appearance_code              text             not null,
  name                         text             not null,
  place_number                 integer          not null default 1,
  hour_of_day                  integer          not null default 17,
  day_of_week                  integer          not null default 7,
  life                         integer          not null default 0,
  fire                         integer          not null default 0,
  wood                         integer          not null default 0,
  water                        integer          not null default 0,
  thunder                      integer          not null default 0,
  air                          integer          not null default 0,
  bonus_fire                   integer          not null default 0,
  bonus_wood                   integer          not null default 0,
  bonus_water                  integer          not null default 0,
  bonus_thunder                integer          not null default 0,
  bonus_air                    integer          not null default 0,
  bonus_claw                   integer          not null default 0,
  fight_count                  integer          not null default 0
);

INSERT INTO public.raid_boss
(appearance_code, "name", place_number, hour_of_day, day_of_week, life, fire, wood, water, thunder, air, bonus_fire, bonus_wood, bonus_water, bonus_thunder, bonus_air, bonus_claw, fight_count)
VALUES('BY1mbgH8QPXkmR1yfZoydv#', '???', 1, 17, 7, 0, 100, 100, 100, 100, 100, 0, 0, 0, 0, 0, 0, 100);
