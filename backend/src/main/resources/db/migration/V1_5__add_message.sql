create table message
(
  id                         uuid             primary key not null,
  seen                       boolean          default false,
  account_destination        uuid             not null,
  account_sender             uuid             not null,
  body                       text             not null,
  numerical_date             timestamp with time zone default now() not null
);