create table player
(
  player_id                    uuid             primary key,
  name                         text             not null,
  cash                         integer          not null default 0,
  unlocked_dinoz               jsonb            not null,
  shop_dinoz_list              jsonb,
  nb_points                    integer          not null default 0,
  average_level_points         integer          not null default 0,
  messages                     boolean          not null,
  black_list                   jsonb            not null,
  last_login                   bigint,
  sacrifice_points             integer,
  daily_shinies_fought         integer,
  role                         text,
  hermit_stage                 integer,
  hermit_stage_current_wins    integer,
  hermit_stage_fighting_dinoz  text,
  account_dinoz_limit          integer          not null default 300,
  nb_wistiti_captured          integer,
  hunter_group                 integer
);

create table player_dinoz
(
  player_id                  uuid             not null,
  dinoz_id                   text             not null,
  rank                       integer          not null,
  PRIMARY KEY (player_id, dinoz_id)
);

create index player_dinoz_player_id on player_dinoz(player_id);