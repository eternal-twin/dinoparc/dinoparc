create table player_stat
(
  id                                   uuid             primary key not null,
  player_id                            uuid             not null,
  dinoz_id                             text,
  nb_skip                              bigint           not null default 0,
  nb_fight                             bigint           not null default 0,
  nb_won_fight                         bigint           not null default 0,
  nb_lost_fight                        bigint           not null default 0,
  nb_shiny                             bigint           not null default 0,
  nb_move                              bigint           not null default 0,
  nb_dig                               bigint           not null default 0,
  nb_raid_fight                        bigint           not null default 0,
  nb_wistiti_fight                     bigint           not null default 0,
  nb_daily_rec_mission                 bigint           not null default 0,
  nb_weekly_rec_mission                bigint           not null default 0,
  nb_monthly_rec_mission               bigint           not null default 0,
  nb_fusion                            bigint           not null default 0,
  sum_fusion                           bigint           not null default 0,
  nb_raid_fight_won                    bigint           not null default 0,
  sum_gold_won                         bigint           not null default 0,
  nb_dinoz_buy                         bigint           not null default 0,
  nb_bazar_sell                        bigint           not null default 0,
  nb_bazar_buy                         bigint           not null default 0,
  nb_bons_buy                          bigint           not null default 0,
  nb_bons_sell                         bigint           not null default 0,
  nb_egg_hatch                         bigint           not null default 0
);

create index stat_player_id on player_stat(player_id);
create index stat_dinoz_id on player_stat(dinoz_id);

create table reward
(
  id                                   uuid             primary key not null,
  description                          text,
  collection_content                   jsonb,
  gold_amount                          integer,
  inventory_content                    jsonb
);

create table rec_mission
(
  id                                   uuid             primary key not null,
  daily                                boolean          not null default false,
  weekly                               boolean          not null default false,
  monthly                              boolean          not null default false,
  difficulty                           integer          not null default 1,
  counter_name                         text             not null,
  counter_value                        integer          not null default 0,
  dinoz                                boolean          not null default false,
  reward_id                            uuid             not null,
  rarity                               integer          not null default 1
);

create table player_mission
(
  id                                   uuid             primary key not null,
  player_id                            uuid             not null,
  dinoz_id                             text,
  rec_mission_id                       uuid             not null,
  daily                                boolean          not null default false,
  weekly                               boolean          not null default false,
  monthly                              boolean          not null default false,
  reward_id                            uuid             not null,
  difficulty                           integer          not null default 1,
  rarity                               integer          not null default 1,
  counter_name                         text             not null,
  counter_initial_value                bigint           not null,
  counter_current_value                bigint           not null,
  counter_target_value                 bigint           not null,
  begin_date                           timestamp with time zone not null,
  expiration_date                      timestamp with time zone not null,
  reward_claimed                       boolean          not null default false
);

create index player_mission_player_id on player_mission(player_id);

create table archive_player_mission
(
  id                                   uuid             primary key not null,
  player_id                            uuid             not null,
  dinoz_id                             text,
  rec_mission_id                       uuid             not null,
  daily                                boolean          not null default false,
  weekly                               boolean          not null default false,
  monthly                              boolean          not null default false,
  reward_id                            uuid             not null,
  counter_initial_value                bigint           not null,
  counter_current_value                bigint           not null,
  counter_target_value                 bigint           not null,
  begin_date                           timestamp with time zone not null,
  expiration_date                      timestamp with time zone not null,
  reward_claimed                       boolean          not null default false
);

create index archive_player_mission_player_id on archive_player_mission(player_id);