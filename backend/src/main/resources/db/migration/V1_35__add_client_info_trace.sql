create table client_information_trace
(
  player_id                                  uuid             not null,
  dinoz_id                                   text,
  status                                     integer,
  details                                    text,
  retrieved_date                             timestamp with time zone not null
);