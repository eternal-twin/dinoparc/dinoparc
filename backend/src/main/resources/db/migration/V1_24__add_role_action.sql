create table role_action_history
(
  id                 uuid     primary key not null,
  author             text     not null default '',
  endpoint           text     not null default '',
  content            text     not null default '',
  success            boolean  not null default false,
  authorized         boolean  not null default false,
  numerical_date     timestamp with time zone default now()
);

update player set role = 'PLAYER' where role is null;