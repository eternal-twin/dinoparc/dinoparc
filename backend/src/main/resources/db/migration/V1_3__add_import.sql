create table import
(
  id                    uuid             primary key not null,
  imported_account      text             default '' not null,
  origin_server         text             default '' not null,
  new_host_account_id   uuid             not null,
  imported_dinozs       text[]           not null,
  importation_date      timestamp with time zone default now() not null
);