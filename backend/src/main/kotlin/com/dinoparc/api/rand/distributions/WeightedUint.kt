package com.dinoparc.api.rand.distributions

import com.dinoparc.api.rand.Distribution
import com.dinoparc.api.rand.RngCore

/**
 * Pick an index from the provided array, using the array values as weights.
 */
class WeightedUint(weights: List<UInt>) : Distribution<UInt> {
    // Partial sums of weights: the i-th entry is the sum of the weights from 0
    // to i (exclusive)
    val partialSums: ArrayList<UInt>
    // Inner uniform distribution
    val inner: UniformUint

    init {
        assert(!weights.isEmpty())
        this.partialSums = ArrayList(weights.size);
        var total: UInt = 0U;
        for (w in weights) {
            assert(w > 0U)
            total += w
            partialSums.add(total)
        }
        this.inner = UniformUint(0U, total - 1U)
    }

    override fun sample(rng: RngCore): UInt {
        val x = this.inner.sample(rng);
        for ((i, s) in this.partialSums.withIndex()) {
            if (s > x) {
                return i.toUInt();
            }
        }
        throw AssertionError("FailedToSampleIndex")
    }
}
