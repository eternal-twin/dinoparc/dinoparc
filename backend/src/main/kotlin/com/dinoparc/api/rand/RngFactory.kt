package com.dinoparc.api.rand

import net.eternaltwin.user.UserId
import java.security.MessageDigest
import java.time.Instant
import java.time.LocalDate

data class RngFactory(
    val md: MessageDigest
) {
    private fun rawByte(input: Byte): RngFactory {
        val fresh: MessageDigest  = this.md.clone() as MessageDigest;
        fresh.update(input)
        return RngFactory(fresh)
    }

    private fun rawLong(input: Long): RngFactory {
        return this.rawULong(input.toULong())
    }

    private fun rawInt(input: Int): RngFactory {
        return this.rawUInt(input.toUInt())
    }

    private fun rawULong(input: ULong): RngFactory {
        val bytesLe: ByteArray = ByteArray(8);
        for (i in 0..8) {
            bytesLe[i] = input.shr(i).toByte();
        }
        return this.rawBytes(bytesLe)
    }

    private fun rawUInt(input: UInt): RngFactory {
        val bytesLe: ByteArray = ByteArray(4);
        for (i in 0..4) {
            bytesLe[i] = input.shr(i).toByte();
        }
        return this.rawBytes(bytesLe)
    }

    private fun rawBytes(input: ByteArray): RngFactory {
        val fresh: MessageDigest  = this.md.clone() as MessageDigest;
        fresh.update(input)
        return RngFactory(fresh)
    }

    fun string(input: String): RngFactory {
        return this.rawBytes(input.toByteArray(Charsets.UTF_8));
    }

    fun user(input: UserId): RngFactory {
        return this.string(input.toUuidString());
    }

    fun instant(input: Instant): RngFactory {
        return this.rawLong(input.epochSecond).rawInt(input.nano);
    }

    fun date(input: LocalDate): RngFactory {
        return this.rawInt(input.year).rawInt(input.monthValue).rawInt(input.dayOfMonth);
    }

    fun toSeed(): ByteArray {
        return this.md.digest()
    }

    fun toRng(): Rng {
        return Rng(Lcg64Xh32.fromSeed(this.toSeed()))
    }

    override fun toString(): String {
        val seed = (this.md.clone() as MessageDigest).digest();
        val seedHex = seed.joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }
        return "RngFactory(${seedHex})"
    }

    companion object {
        @JvmStatic
        fun fromSeed(primarySeed: ByteArray): RngFactory {
            val md = MessageDigest.getInstance("SHA-256");
            md.update(primarySeed);
            return RngFactory(md)
        }
    }
}
