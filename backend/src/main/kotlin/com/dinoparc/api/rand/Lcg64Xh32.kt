package com.dinoparc.api.rand

import java.security.MessageDigest

// https://rust-random.github.io/rand/src/rand_pcg/pcg64.rs.html
class Lcg64Xh32(private var state: ULong, private val increment: ULong): RngCore {
    init {
        assert(increment.mod(2U) != 0U)
    }

    private val MULTIPLIER: ULong = 6364136223846793005U;
    private val ROTATE: Int = 59;
    private val XSHIFT: Int = 18;
    private val SPARE: Int = 27;

    override fun toString(): String {
        return "Lcg64Xh32(${this.state}, ${this.increment})"
    }

    override fun randUint(): UInt {
        val state = this.state;
        this.step();
        val rot = state.shr(ROTATE).toInt();
        val xsh = state.shr(XSHIFT).xor(state).shr(SPARE).toUInt();
        return xsh.shr(rot).or(xsh.shl(32 - rot))
    }

    private fun step() {
        this.state = (this.state * MULTIPLIER) + this.increment
    }

    companion object {
        @JvmStatic
        fun fromSeed(seed: ByteArray): Lcg64Xh32 {
            assert(seed.size >= 16);
            var a: ULong = 0U;
            var b: ULong = 0U;
            for (i in 0..8) {
                a = a.or(seed[i].toUByte().toULong().shl(8 * i))
                b = b.or(seed[8 + i].toUByte().toULong().shl(8 * i))
            }
            return fromSeed(a, b);
        }

        @JvmStatic
        fun fromSeed(a: ULong, b: ULong): Lcg64Xh32 {
            // Force `increment` to be odd.
            return Lcg64Xh32(a, b.or(1U))
        }
    }
}
